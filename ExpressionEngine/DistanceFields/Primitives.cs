﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.DistanceFields
{
	/// <summary>Widely used shapes, implemented as distance field expressions.
	/// </summary>
	public static class Primitives
	{
		/// <summary>A 3D sphere.</summary>
        /// <param name="origin">The location of the center of the sphere.
        /// </param>
        /// <param name="radius">The radius of the sphere.</param>
		public static Expression<DistanceDelegate> Sphere(Expression<Func<float>> radius, Expression<Func<Vector3>> origin)
        {
            return
                Injector.Inject(radius, radiusMock =>
                Injector.Inject<DistanceDelegate, Func<Vector3>>(origin, originMock =>
                    at => Vector3.Distance(originMock(), at) - radiusMock()));
        }
	}
}
