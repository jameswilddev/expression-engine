﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.DistanceFields
{
    /// <summary>A sample of a distance field, containing both the distance
    /// to the nearest surface and a payload field for storing material 
    /// data/etc.</summary>
    /// <typeparam name="T">The type of the payload field.</typeparam>
    public struct Sample<T> where T : struct
    {
        /// <summary>The distance to the nearest surface.</summary>
        public float Distance;

        /// <summary>Data stored alongside the distance to the nearest surface.
        /// </summary>
        public T Payload;

		/// <inheritdoc />
        public override string ToString()
        {
            return string.Format("Sample<{0}>{{ Distance: {1}, Payload: {2} }}", typeof(T).Name, Distance, Payload);
        }
    }

    /// <summary>Provides helper methods for working with 
    /// <see cref="Sample{T}"/>.</summary>
    public static class Sample
    {
        /// <summary>Wraps a given expression to determine the distance to the
        /// nearest surface with a <see cref="Sample{T}"/> constructor, where
        /// the payload is also evaluated from a given expression.</summary>
        /// <typeparam name="T">The type of payload.</typeparam>
        /// <param name="distance">The distance field expression.</param>
        /// <param name="payload">The payload to bundle.</param>
        /// <returns>An expression wrapping the distance returned by 
        /// <paramref name="distance"/> in a <see cref="Sample{T}"/>, where 
        /// <see cref="Sample{T}.Payload"/> is <paramref name="payload"/>.
		/// </returns>
        public static Expression<DistanceSampleDelegate<T>> Wrap<T>(this Expression<DistanceDelegate> distance, Expression<Func<T>> payload)
            where T : struct
        {
            if (distance == null) throw new ArgumentNullException("distance");
            if (payload == null) throw new ArgumentNullException("payload");
            return
                Injector.Inject(distance, distanceMock =>
                Injector.Inject<DistanceSampleDelegate<T>, Func<T>>(payload, payloadMock => at => new Sample<T>
                {
                    Distance = distanceMock(at),
                    Payload = payloadMock()
                }));
        }
    }
}
