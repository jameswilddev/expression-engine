﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.DistanceFields
{
	/// <summary>Calculates the surface normal at a given location in the
	/// scene.</summary>
	/// <param name="at">The location to find the surface normal at.</param>
	/// <returns>The surface normal at the location specified by
	/// <paramref name="at"/>.</returns>
	public delegate Vector3 SurfaceNormalDelegate(Vector3 at);

	/// <summary>Tools for analyzing distance fields.</summary>
	public static class Analysis
	{
		/// <summary>Determines the surface normal at specified locations in a 
		/// given <see cref="DistanceDelegate"/>.</summary>
		/// <param name="distanceField">The distance field to analyze.</param>
		/// <returns>An expression for determining the surface normal of
		/// specified locations in <paramref name="distanceField"/>.</returns>
		public static Expression<SurfaceNormalDelegate> SurfaceNormal(this Expression<DistanceDelegate> distanceField)
		{
			if (distanceField == null) throw new ArgumentNullException("distanceField");

			return
				Injector.Inject(distanceField, distanceFieldMock =>
				Injector.Inject<SurfaceNormalDelegate, Func<Vector3, Vector3, Vector3>>((at, offset) => distanceFieldMock(at + offset) * offset, sample =>
					at =>
						Vector3.Normalize(
							sample(at, new Vector3(0.001f, 0.0f, 0.0f))
							+ sample(at, new Vector3(-0.001f, 0.0f, 0.0f))
							+ sample(at, new Vector3(0.0f, 0.001f, 0.0f))
							+ sample(at, new Vector3(0.0f, -0.001f, 0.0f))
							+ sample(at, new Vector3(0.0f, 0.0f, 0.001f))
							+ sample(at, new Vector3(0.0f, 0.0f, -0.001f))
						)));
		}
	}
}
