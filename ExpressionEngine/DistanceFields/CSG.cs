﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.DistanceFields
{
    /// <summary>Expressions which combine distance field expressions to make 
    /// more complex geometry.</summary>
    public static class CSG
    {
        /// <summary>Combines two distance field expressions by merging them;
        /// any space occupied by either distance field expression is solid.
        /// </summary>
        /// <param name="left">The first distance field expression to union.
        /// </param>
        /// <param name="right">The second distance field expression to union.
        /// </param>
        /// <returns>An expression representing the union of 
        /// <paramref name="left"/> and <paramref name="right"/>.</returns>
        public static Expression<DistanceDelegate> Union(
            this Expression<DistanceDelegate> left,
            Expression<DistanceDelegate> right
        )
        {
            return
                Injector.Inject(left, leftMock =>
                Injector.Inject(right, rightMock => (Expression<DistanceDelegate>)
                    (at => Math.Min(leftMock(at), rightMock(at)))
                ));
        }

        /// <summary>Combines two distance field expressions by cutting one
        /// out of the other.
        /// </summary>
        /// <param name="left">The distance field expression to cut from.
        /// </param>
        /// <param name="right">The distance field expression to cut by.
        /// </param>
        /// <returns>An expression representing <paramref name="left"/> with 
        /// <paramref name="right"/> cut out of it.</returns>
        public static Expression<DistanceDelegate> Difference(
            this Expression<DistanceDelegate> left,
            Expression<DistanceDelegate> right
        )
        {
            return
                Injector.Inject(left, leftMock =>
                Injector.Inject(right, rightMock => (Expression<DistanceDelegate>)
                    (at => Math.Max(leftMock(at), -rightMock(at)))
                ));
        }
    }
}
