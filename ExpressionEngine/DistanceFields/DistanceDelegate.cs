﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.DistanceFields
{
    /// <summary>Gets the distance to the nearest object at a given location.
    /// </summary>
    /// <param name="at">The location to find the distance to the nearest
    /// object to.</param>
    /// <returns>The distance between <paramref name="at"/> and the nearest
    /// object.</returns>
    public delegate float DistanceDelegate(Vector3 at);

    /// <summary>Gets the distance to the nearest object at a given location.
    /// </summary>
    /// <param name="at">The location to find the distance to the nearest
    /// object to.</param>
    /// <returns>A sample of the distance between <paramref name="at"/> and the
    /// nearest object.</returns>
    public delegate Sample<T> DistanceSampleDelegate<T>(Vector3 at) where T : struct;
}
