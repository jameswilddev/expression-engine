﻿using ExpressionEngine.Falloff;
using ExpressionEngine.Materials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Lighting
{
	/// <summary>A sample of a light at a specific location.</summary>
	public struct LightSample
	{
		/// <summary>The normal of the light towards the sample.</summary>
		public Vector3 Normal;

		/// <summary>The color of the light at the sample.  This should
		/// include any intensity rolloff.</summary>
		public Vector3 Color;
	}

	/// <summary>Calculates a <see cref="LightSample"/> at a specified
	/// location.</summary>
	/// <param name="at">The location to sample from.</param>
	/// <returns>The <see cref="LightSample"/> calculated from the location 
	/// specified by <paramref name="at"/>.</returns>
	public delegate LightSample LightDelegate(Vector3 at);

	/// <summary>An expression to be evaluated for every light in a scene
	/// at a specific location, to calculate the intensity of that light.
	/// </summary>
	/// <param name="at">The location being sampled from.</param>
	/// <param name="lightNormal">The inbound normal of the light.</param>
	/// <returns>The intensity of the light, where the inbound light normal is
	/// <paramref name="lightNormal"/>, at the location specified by 
	/// <paramref name="at"/>.</returns>
	public delegate float LightingDelegate(Vector3 at, Vector3 lightNormal);

	/// <summary>Provides expressions for calculating lighting on a surface.
	/// </summary>
	public static class Sampling
	{
		/// <summary>Converts a <see cref="FalloffDelegate"/> to a
		/// <see cref="LightDelegate"/> by modulating its intensity with a 
		/// color.</summary>
		/// <param name="falloff">The <see cref="FalloffDelegate"/> to convert
		/// into a <see cref="LightDelegate"/>.</param>
		/// <param name="color">The color to modulate 
		/// <paramref name="falloff"/> with.</param>
		/// <returns>A new <see cref="LightDelegate"/> which modulates
		/// <paramref name="falloff"/> with <paramref name="color"/>.</returns>
		/// <exception cref="ArgumentNullException">Thrown when 
		/// <paramref name="falloff"/> is <see langword="null"/>.</exception>
		/// <exception cref="ArgumentNullException">Thrown when 
		/// <paramref name="color"/> is <see langword="null"/>.</exception>
		public static Expression<LightDelegate> ToLight(this Expression<FalloffDelegate> falloff, Expression<Func<Vector3>> color)
		{
			if (falloff == null) throw new ArgumentNullException("falloff");
			if (color == null) throw new ArgumentNullException("color");
			return
				Injector.Inject(falloff, falloffMock =>
				Injector.Inject<LightDelegate, Func<Vector3>>(color, colorMock =>
					at => new LightSample { Color = colorMock() * falloffMock(at).Intensity, Normal = falloffMock(at).Normal }
				));
		}

		/// <summary>Creates an expression to calculate the color of a surface.
		/// </summary>
		/// <param name="lights">A list of lights to apply to the surface.
		/// </param>
		/// <param name="emissive">The starting color of the surface.  When
		/// nonzero, this appears to make the surface glow in the dark.  May
		/// be <see langword="null"/> to signify zero.</param>
		/// <param name="calculateIntensity">A <see cref="LightingDelegate"/> 
		/// to be evaluated for every item in <paramref name="lights"/>'s 
		/// result.</param>
		/// <returns>An expression calculating the sum of 
		/// <paramref name="emissive"/> and the result of evaluating
		/// <paramref name="calculateIntensity"/> for every light in 
		/// <paramref name="lights"/>.</returns>
		/// <exception cref="ArgumentNullException">Thrown when 
		/// <paramref name="lights"/> is <see langword="null"/>.</exception>
		/// <exception cref="ArgumentNullException">Thrown when 
		/// <paramref name="lights"/> contains <see langword="null"/>.</exception>
		/// <exception cref="ArgumentNullException">Thrown when 
		/// <paramref name="calculateIntensity"/> is <see langword="null"/>.</exception>
		public static Expression<MaterialDelegate> Build(IEnumerable<Expression<LightDelegate>> lights, Expression<LightingDelegate> calculateIntensity, Expression<MaterialDelegate> emissive = null)
		{
			if (lights == null) throw new ArgumentNullException("lights");
			if (calculateIntensity == null) throw new ArgumentNullException("calculateIntensity");

			foreach (var light in lights)
			{
				if (light == null) throw new ArgumentNullException("lights", "Cannot contain null");

				if (emissive == null)
					emissive = at => light.Invoke(l => l(at)).Let(l => calculateIntensity.Invoke(ci => ci(at, l.Normal)) * l.Color);
				else
				{
					// Take a copy of the reference, else evaluating the expression forms an infinite loop!
					var copy = emissive;
					emissive =
						at => copy.Invoke(e => e(at))
							+ light.Invoke(l => l(at)).Let(l => calculateIntensity.Invoke(ci => ci(at, l.Normal)) * l.Color);
				}
			}

			return emissive ?? (at => default(Vector3));
		}
	}
}
