﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine
{
	/// <summary>Extends <see cref="float"/> to support many functions which
	/// are understood by GLSL/etc. natively.</summary>
	public static class FloatExtensions
	{
		/// <summary>Returns the greatest integer less than or equal to 
		/// <paramref name="input"/>.</summary>
		/// <param name="input">The value to determine the greatest integer 
		/// less than or equal to.</param>
		/// <returns>The greatest integer less than or equal to 
		/// <paramref name="input"/>.</returns>
		public static float Floor(this float input)
		{
			return (float)Math.Floor(input);
		}

		/// <summary>Returns the lowest integer greater than or equal to 
		/// <paramref name="input"/>.</summary>
		/// <param name="input">The value to determine the lowest integer 
		/// greater than or equal to.</param>
		/// <returns>The lowest integer greater than or equal to 
		/// <paramref name="input"/>.</returns>
		public static float Ceil(this float input)
		{
			return (float)Math.Ceiling(input);
		}

		/// <summary>Returns the fractional part of <paramref name="input"/>;
		/// the difference between <paramref name="input"/> and its 
		/// <see cref="Floor"/>.</summary>
		/// <param name="input">The value to determine the fractional part of
		/// .</param>
		/// <returns>The the fractional part of <paramref name="input"/>; the 
		/// difference between <paramref name="input"/> and its 
		/// <see cref="Floor"/>.</returns>
		public static float Fract(this float input)
		{
			return input - (float)Math.Floor(input);
		}

		/// <summary>Constrains <paramref name="input"/> to be between 
		/// <paramref name="min"/> and <paramref name="max"/>.</summary>
		/// <param name="input">The value to clamp.</param>
		/// <param name="min">The minimum value to clamp 
		/// <paramref name="input"/> by.</param>
		/// <param name="max">The maximum value to clamp
		/// <paramref name="input"/> by.</param>
		/// <returns><paramref name="input"/>, clamped between 
		/// <paramref name="min"/> and <paramref name="max"/>.</returns>
		public static float Clamp(this float input, float min, float max)
		{
			return Math.Max(Math.Min(input, max), min);
		}

		/// <summary>Blends between <paramref name="from"/> and 
		/// <paramref name="to"/> by <paramref name="alpha"/>.</summary>
		/// <param name="from">The value to return when 
		/// <paramref name="alpha"/> is zero.</param>
		/// <param name="to">The value to return when
		/// <paramref name="alpha"/> is one.</param>
		/// <param name="alpha">The value to blend by; zero is
		/// <paramref name="from"/> and one is <paramref name="to"/>.</param>
		/// <returns>A blend between <paramref name="from"/> and 
		/// <paramref name="to"/>, by <paramref name="alpha"/>.</returns>
		public static float Mix(this float from, float to, float alpha)
		{
			return from + (to - from) * alpha;
		}

		/// <summary>Finds the remainder when dividing one <see cref="float"/>
		/// by another.</summary>
		/// <param name="mod">The <see cref="float"/> to divide.</param>
		/// <param name="by">The <see cref="float"/> to divide by.</param>
		/// <returns>The remainder of dividing <paramref name="mod"/> by 
		/// <paramref name="by"/>.</returns>
		public static float Mod(this float mod, float by)
		{
			return mod - by * (mod / by).Floor();
		}

		/// <summary>Calculates the square root of <paramref name="sqrt"/>.
		/// </summary>
		/// <param name="sqrt">The <see cref="float"/> to calculate the
		/// square root of.</param>
		/// <returns>The square root of <paramref name="sqrt"/>.</returns>
		public static float Sqrt(this float sqrt)
		{
			return (float)Math.Sqrt(sqrt);
		}

		/// <summary>Calculates the reciprocal of the square root of 
		/// <paramref name="inverseSqrt"/>.</summary>
		/// <param name="inverseSqrt">The <see cref="float"/> to calculate the
		/// reciprocal of the square root of.</param>
		/// <returns>The reciprocal of the square root of 
		/// <paramref name="inverseSqrt"/>.</returns>
		public static float InverseSqrt(this float inverseSqrt)
		{
			return 1.0f / inverseSqrt.Sqrt();
		}

		/// <summary>Raises <paramref name="value"/> to the power of
		/// <paramref name="exp"/>.</summary>
		/// <param name="value">The <see cref="float"/> to raise.</param>
		/// <param name="exp">The power to raise <paramref name="value"/> to.
		/// </param>
		/// <returns><paramref name="value"/>, raised to the power of 
		/// <paramref name="exp"/>.</returns>
		public static float Pow(this float value, float exp)
		{
			return (float)Math.Pow(value, exp);
		}

		/// <summary>Calculates the sine of <paramref name="angle"/>.</summary>
		/// <param name="angle">The angle, in radians, to calculate the sine
		/// of.</param>
		/// <returns>The sine of <paramref name="angle"/>.</returns>
		public static float Sin(this float angle)
		{
			return (float)Math.Sin(angle);
		}

		/// <summary>Calculates the cosine of <paramref name="angle"/>.
		/// </summary>
		/// <param name="angle">The angle, in radians, to calculate the cosine
		/// of.</param>
		/// <returns>The cosine of <paramref name="angle"/>.</returns>
		public static float Cos(this float angle)
		{
			return (float)Math.Cos(angle);
		}
	}
}
