﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine
{
    /// <summary>Provides methods for repeatedly iterating over data.</summary>
    public static class Iterator
    {
        /// <summary>Iterates over a piece of data by passing it through an 
        /// expression a fixed number of times.</summary>
        /// <remarks>Depending upon the target platform, this may be unrolled,
        /// causing the generation of large amounts of code.</remarks>
        /// <typeparam name="T">The type data to iterate on.</typeparam>
        /// <param name="initial">The value to start from.</param>
        /// <param name="transform">An expression to evaluate the number of
        /// times specified by <paramref name="iterations"/>, mutating the
        /// value returned by <paramref name="initial"/> each time.</param>
        /// <param name="iterations">The number of times to evaluate 
        /// <paramref name="transform"/> to mutate the value returned by
        /// <paramref name="initial"/>.</param>
        /// <returns><paramref name="initial"/>, mutated by 
		/// <paramref name="transform"/> the number of times specified by
		/// <paramref name="iterations"/>.</returns>
        public static T Iterate<T>(
            T initial, 
            Expression<Func<T, T>> transform, 
            int iterations
        )
        {
            if (transform == null)
                throw new ArgumentNullException("transform");

            if(iterations < 1)
                throw new ArgumentOutOfRangeException("iterations", "Cannot be less than one");

            while (iterations-- > 0)
                initial = transform.Compile()(initial);

            return initial;
        }
    }
}
