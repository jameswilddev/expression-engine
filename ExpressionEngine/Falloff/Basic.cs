﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Falloff
{
	/// <summary>A sample of a <see cref="FalloffDelegate"/>.</summary>
	public struct FalloffSample
	{
		/// <summary>The direction of the effect of the 
		/// <see cref="FalloffDelegate"/></summary>
		public Vector3 Normal;

		/// <summary>The strength of the effect of the 
		/// <see cref="FalloffDelegate"/>.</summary>
		public float Intensity;
	}

	/// <summary>Describes the intensity and direction of an effect which
	/// varies depending upon where it is sampled.</summary>
	/// <param name="at">The location from which a sample is being taken.
	/// </param>
	/// <returns>A <see cref="FalloffSample"/> describing the intensity and
	/// direction of this <see cref="FalloffDelegate"/> at the location
	/// specified by <paramref name="at"/>.</returns>
	public delegate FalloffSample FalloffDelegate(Vector3 at);

	/// <summary>Commonly used <see cref="FalloffDelegate"/>s.</summary>
	public static class Basic
	{
		/// <summary>An ambient <see cref="FalloffDelegate"/> which returns the
		/// same <see cref="FalloffSample.Normal"/> and
		/// <see cref="FalloffSample.Intensity"/> regardless of where it is 
		/// sampled.</summary>
		/// <param name="normal">The <see cref="FalloffSample.Normal"/> to
		/// always return.</param>
		/// <returns>A <see cref="FalloffDelegate"/> returning 
		/// <paramref name="normal"/> and a constant full intensity.</returns>
		/// <exception cref="ArgumentNullException">Thrown when 
		/// <paramref name="normal"/> is <see langword="null"/>.</exception>
		public static Expression<FalloffDelegate> Directional(Expression<Func<Vector3>> normal)
		{
			if (normal == null) throw new ArgumentNullException("normal");
			return at => new FalloffSample { Normal = normal.Invoke(n => n()), Intensity = 1.0f };
		}

		/// <summary>A <see cref="FalloffDelegate"/> which rolls off to zero
		/// over a given distance from a given location.</summary>
		/// <param name="origin">The point intensity rolls off from.</param>
		/// <param name="radius">The <see cref="FalloffSample.Normal"/> to
		/// always return.</param>
		/// <returns>A <see cref="FalloffDelegate"/> describing a linear
		/// rolloff of intensity from the point specified by 
		/// <paramref name="origin"/>, where <see cref="FalloffSample.Normal"/>
		/// points away from <paramref name="origin"/>.</returns>
		/// <exception cref="ArgumentNullException">Thrown when 
		/// <paramref name="origin"/> is <see langword="null"/>.</exception>
		/// <exception cref="ArgumentNullException">Thrown when 
		/// <paramref name="radius"/> is <see langword="null"/>.</exception>
		public static Expression<FalloffDelegate> Radial(Expression<Func<Vector3>> origin, Expression<Func<float>> radius)
		{
			if (origin == null) throw new ArgumentNullException("origin");
			if (radius == null) throw new ArgumentNullException("radius");
			return
				at => new FalloffSample { Intensity = 1.0f - Math.Min(1.0f, Vector3.Distance(at, origin.Invoke(o => o())) / radius.Invoke(r => r())), Normal = Vector3.Normalize(at - origin.Invoke(o => o())) };
		}
	}
}
