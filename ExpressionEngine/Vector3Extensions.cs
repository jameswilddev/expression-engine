﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine
{
	/// <summary>Extends <see cref="Vector3"/> to support many functions which
	/// are understood by GLSL/etc. natively.</summary>
	public static class Vector3Extensions
	{
		/// <summary>Returns the greatest integer less than or equal to 
		/// each component of <paramref name="input"/>.</summary>
		/// <param name="input">The value to determine the greatest integer 
		/// less than or equal to.</param>
		/// <returns>A new <see cref="Vector3"/> containing the greatest integer 
		/// less than or equal to each component in <paramref name="input"/>.
		/// </returns>
		public static Vector3 Floor(this Vector3 input)
		{
			return new Vector3(
				input.X.Floor(),
				input.Y.Floor(),
				input.Z.Floor()
			);
		}

		/// <summary>Returns the lowest integer greater than or equal to 
		/// each component of <paramref name="input"/>.</summary>
		/// <param name="input">The value to determine the lowest integer 
		/// greater than or equal to.</param>
		/// <returns>A new <see cref="Vector3"/> containing the lowest integer 
		/// greater than or equal to each component in <paramref name="input"/>.
		/// </returns>
		public static Vector3 Ceil(this Vector3 input)
		{
			return new Vector3(
				input.X.Ceil(),
				input.Y.Ceil(),
				input.Z.Ceil()
			);
		}

		/// <summary>Returns the fractional part of each component of 
		/// <paramref name="input"/>; the difference between each component and 
		/// its <see cref="Floor"/>.</summary>
		/// <param name="input">The value to determine the fractional part of
		/// .</param>
		/// <returns>The the fractional part of each component of 
		/// <paramref name="input"/>; the difference between each component and 
		/// its <see cref="Floor"/>.</returns>
		public static Vector3 Fract(this Vector3 input)
		{
			return new Vector3(
				input.X.Fract(),
				input.Y.Fract(),
				input.Z.Fract()
			);
		}

		/// <summary>Constrains each component in <paramref name="input"/> to 
		/// be between that in <paramref name="min"/> and 
		/// <paramref name="max"/>.</summary>
		/// <param name="input">The value to clamp.</param>
		/// <param name="min">The minimum value to clamp 
		/// <paramref name="input"/> by.</param>
		/// <param name="max">The maximum value to clamp
		/// <paramref name="input"/> by.</param>
		/// <returns><paramref name="input"/>, clamped between 
		/// <paramref name="min"/> and <paramref name="max"/>.</returns>
		public static Vector3 Clamp(this Vector3 input, Vector3 min, Vector3 max)
		{
			return new Vector3(
				input.X.Clamp(min.X, max.X),
				input.Y.Clamp(min.Y, max.Y),
				input.Z.Clamp(min.Z, max.Z)
			);
		}

		/// <summary>Constrains each component in <paramref name="input"/> to 
		/// be between <paramref name="min"/> and <paramref name="max"/>.
		/// </summary>
		/// <param name="input">The value to clamp.</param>
		/// <param name="min">The minimum value to clamp 
		/// <paramref name="input"/> by.</param>
		/// <param name="max">The maximum value to clamp
		/// <paramref name="input"/> by.</param>
		/// <returns><paramref name="input"/>, clamped between 
		/// <paramref name="min"/> and <paramref name="max"/>.</returns>
		public static Vector3 Clamp(this Vector3 input, float min, float max)
		{
			return new Vector3(
				input.X.Clamp(min, max),
				input.Y.Clamp(min, max),
				input.Z.Clamp(min, max)
			);
		}

		/// <summary>Finds the remainder when dividing each component of a
		/// <see cref="Vector3"/> by a <see cref="float"/>.</summary>
		/// <param name="mod">The <see cref="Vector3"/> to divide.</param>
		/// <param name="by">The <see cref="float"/> to divide by.</param>
		/// <returns>A new <see cref="Vector3"/> containing the remainder of 
		/// dividing each component of <paramref name="mod"/> by 
		/// <paramref name="by"/>.</returns>
		public static Vector3 Mod(this Vector3 mod, Vector3 by)
		{
			return mod - by * (mod / by).Floor();
		}

		/// <summary>Finds the remainder when dividing each component of a
		/// <see cref="Vector3"/> by a <see cref="float"/>.</summary>
		/// <param name="mod">The <see cref="Vector3"/> to divide.</param>
		/// <param name="by">The <see cref="float"/> to divide by.</param>
		/// <returns>A new <see cref="Vector3"/> containing the remainder of 
		/// dividing each component of <paramref name="mod"/> by 
		/// <paramref name="by"/>.</returns>
		public static Vector3 Mod(this Vector3 mod, float by)
		{
			return mod - by * (mod / by).Floor();
		}

		/// <summary>Calculates the reciprocal of the square root of each
		/// component of <paramref name="inverseSqrt"/>.</summary>
		/// <param name="inverseSqrt">The <see cref="Vector3"/> to calculate the
		/// reciprocal of the square root of each component of.</param>
		/// <returns>A new <see cref="Vector3"/> containing the reciprocal of 
		/// the square root of each component of <paramref name="inverseSqrt"/>
		/// .</returns>
		public static Vector3 InverseSqrt(this Vector3 inverseSqrt)
		{
			return new Vector3(1.0f) / Vector3.SquareRoot(inverseSqrt);
		}

		/// <summary>Raises <paramref name="value"/> to the power of
		/// <paramref name="exp"/>.</summary>
		/// <param name="value">The <see cref="Vector3"/> to raise.</param>
		/// <param name="exp">The power to raise <paramref name="value"/> to.
		/// </param>
		/// <returns><paramref name="value"/>, raised to the power of 
		/// <paramref name="exp"/>.</returns>
		public static Vector3 Pow(this Vector3 value, Vector3 exp)
		{
			return new Vector3(
				value.X.Pow(exp.X),
				value.Y.Pow(exp.Y),
				value.Z.Pow(exp.Z)
			);
		}

		/// <summary>Calculates the sine of each component of
		/// <paramref name="angle"/>.</summary>
		/// <param name="angle">The angle, in radians, to calculate the sine
		/// of.</param>
		/// <returns>The sine of <paramref name="angle"/>.</returns>
		public static Vector3 Sin(this Vector3 angle)
		{
			return new Vector3(
				angle.X.Sin(),
				angle.Y.Sin(),
				angle.Z.Sin()
			);
		}

		/// <summary>Calculates the cosine of <paramref name="angle"/>.
		/// </summary>
		/// <param name="angle">The angle, in radians, to calculate the cosine
		/// of.</param>
		/// <returns>The cosine of <paramref name="angle"/>.</returns>
		public static Vector3 Cos(this Vector3 angle)
		{
			return new Vector3(
				angle.X.Cos(),
				angle.Y.Cos(),
				angle.Z.Cos()
			);
		}

		/// <summary>Blends between <paramref name="from"/> and 
		/// <paramref name="to"/> by <paramref name="alpha"/>.</summary>
		/// <param name="from">The value to return when 
		/// <paramref name="alpha"/> is zero.</param>
		/// <param name="to">The value to return when
		/// <paramref name="alpha"/> is one.</param>
		/// <param name="alpha">The value to blend by; zero is
		/// <paramref name="from"/> and one is <paramref name="to"/>.</param>
		/// <returns>A blend between <paramref name="from"/> and 
		/// <paramref name="to"/>, by <paramref name="alpha"/>.</returns>
		public static Vector3 Mix(this Vector3 from, Vector3 to, Vector3 alpha)
		{
			return from + (to - from) * alpha;
		}

        /// <summary>Creates a new <see cref="Vector2"/> by re-arranging the fields of a <see cref="Vector3"/>.</summary>
        /// <param name="vector">The <see cref="Vector3"/> to swizzle.</param>
        /// <param name="x">An expression selecting the field of <paramref name="vector"/> to use as <see cref="Vector2.X"/></param>
        /// <param name="y">An expression selecting the field of <paramref name="vector"/> to use as <see cref="Vector2.Y"/></param>
        /// <returns>A new <see cref="Vector2"/> created by swizzling <paramref name="vector"/>.</returns>
        public static Vector2 Swizzle(
            this Vector3 vector,
            Expression<Func<Vector3, float>> x,
            Expression<Func<Vector3, float>> y
        )
        {
            Vector2Extensions.ValidateFieldSelector(vector, x, "x");
            Vector2Extensions.ValidateFieldSelector(vector, y, "y");
            return new Vector2(x.Compile()(vector), y.Compile()(vector));
        }

        /// <summary>Creates a new <see cref="Vector3"/> by re-arranging the fields of a <see cref="Vector3"/>.</summary>
        /// <param name="vector">The <see cref="Vector3"/> to swizzle.</param>
        /// <param name="x">An expression selecting the field of <paramref name="vector"/> to use as <see cref="Vector3.X"/></param>
        /// <param name="y">An expression selecting the field of <paramref name="vector"/> to use as <see cref="Vector3.Y"/></param>
        /// <param name="z">An expression selecting the field of <paramref name="vector"/> to use as <see cref="Vector3.Z"/></param>
        /// <returns>A new <see cref="Vector3"/> created by swizzling <paramref name="vector"/>.</returns>
        public static Vector3 Swizzle(
            this Vector3 vector,
            Expression<Func<Vector3, float>> x,
            Expression<Func<Vector3, float>> y,
            Expression<Func<Vector3, float>> z
        )
        {
            Vector2Extensions.ValidateFieldSelector(vector, x, "x");
            Vector2Extensions.ValidateFieldSelector(vector, y, "y");
            Vector2Extensions.ValidateFieldSelector(vector, z, "z");
            return new Vector3(x.Compile()(vector), y.Compile()(vector), z.Compile()(vector));
        }

        /// <summary>Creates a new <see cref="Vector4"/> by re-arranging the fields of a <see cref="Vector3"/>.</summary>
        /// <param name="vector">The <see cref="Vector3"/> to swizzle.</param>
        /// <param name="x">An expression selecting the field of <paramref name="vector"/> to use as <see cref="Vector4.X"/></param>
        /// <param name="y">An expression selecting the field of <paramref name="vector"/> to use as <see cref="Vector4.Y"/></param>
        /// <param name="z">An expression selecting the field of <paramref name="vector"/> to use as <see cref="Vector4.Z"/></param>
        /// <param name="w">An expression selecting the field of <paramref name="vector"/> to use as <see cref="Vector4.W"/></param>
        /// <returns>A new <see cref="Vector4"/> created by swizzling <paramref name="vector"/>.</returns>
        public static Vector4 Swizzle(
            this Vector3 vector,
            Expression<Func<Vector3, float>> x,
            Expression<Func<Vector3, float>> y,
            Expression<Func<Vector3, float>> z,
            Expression<Func<Vector3, float>> w
        )
        {
            Vector2Extensions.ValidateFieldSelector(vector, x, "x");
            Vector2Extensions.ValidateFieldSelector(vector, y, "y");
            Vector2Extensions.ValidateFieldSelector(vector, z, "z");
            Vector2Extensions.ValidateFieldSelector(vector, w, "w");
            return new Vector4(x.Compile()(vector), y.Compile()(vector), z.Compile()(vector), w.Compile()(vector));
        }

        /// <summary>Extends a <see cref="Vector3"/> by one component, creating
        /// a new <see cref="Vector4"/>.</summary>
        /// <param name="vector">The <see cref="Vector3"/> to extend.</param>
        /// <param name="w">The component to extend <paramref name="vector"/>
        /// by.</param>
        /// <returns>A new <see cref="Vector4"/>, where 
        /// <see cref="Vector4.X"/>, <see cref="Vector4.Y"/> and 
        /// <see cref="Vector4.Z"/> are from <paramref name="vector"/>, and
        /// <see cref="Vector4.W"/> is <paramref name="w"/>.</returns>
        public static Vector4 Extend(this Vector3 vector, float w)
        {
            return new Vector4(vector.X, vector.Y, vector.Z, w);
        }
	}
}
