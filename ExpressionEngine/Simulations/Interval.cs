﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Simulations
{
    /// <summary>Called by <see cref="IInterval"/> to draw the scene.</summary>
    /// <param name="frameProgress">The progress through the current frame,
    /// where zero is the start of the frame and one is the end of the frame.
    /// </param>
    public delegate void RenderDelegate(float frameProgress);

    /// <summary>Handles simulation timing logic, dispatching events to advance
    /// the simulation state.</summary>
    public interface IInterval
    {
        /// <summary>To be called once per frame by <see cref="IContext"/>.
        /// </summary>
        void Update();

        /// <summary>Raised at the end of each call to <see cref="Update"/> to
        /// draw the scene.</summary>
        event RenderDelegate Render;

        /// <summary>Raised on the interval specified in 
        /// <see cref="IConfiguration.TickInterval"/> to advance the 
        /// simulation's state.</summary>
        event Action Tick;
    }

    /// <inheritdoc />
    [Export(typeof(IInterval))]
    public sealed class Interval : IInterval, IPartImportsSatisfiedNotification
    {
        /// <inheritdoc />
        [Import]
        public IConfiguration Configuration;

        /// <inheritdoc />
        [Import]
        public IRuntime Runtime;

        private DateTime PreviousCheck;
        private TimeSpan FrameProgress;

        /// <inheritdoc />
        public void OnImportsSatisfied()
        {
            PreviousCheck = Runtime.Now;
        }

        /// <inheritdoc />
        public void Update()
        {
            var now = Runtime.Now;
            FrameProgress += now - PreviousCheck;
            PreviousCheck = now;
            while (FrameProgress >= Configuration.TickInterval)
            {
                Tick();
                FrameProgress -= Configuration.TickInterval;
            }
            Render((float)(FrameProgress.TotalSeconds / Configuration.TickInterval.TotalSeconds));
        }

        /// <inheritdoc />
        public event RenderDelegate Render = delegate { };

        /// <inheritdoc />
        public event Action Tick = delegate { };
    }
}
