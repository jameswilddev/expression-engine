﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Simulations
{
    /// <summary>Evaluated once per frame to determine whether to continue
    /// the event loop.</summary>
    /// <param name="closeClicked"><see langword="true"/> when the window's
    /// close button was clicked this frame.</param>
    /// <returns>When <see langword="true"/>, the event loop continues.
    /// When <see langword="false"/>, the event loop ends.</returns>
    public delegate bool ContinueRunDelegate(bool closeClicked);

    /// <summary>Handles the creation and management of a native context
    /// through which to render.</summary>
    /// <remarks>Opens a context on instantation.  Expects that all calls are
    /// performed from the same thread.  The context may be used from other
    /// types but may not be available from other threads.</remarks>
    public interface IContext
    {
        /// <summary>Enters the event loop, calling
        /// <see cref="IInterval.Update"/> in a loop until the simulation ends.
        /// </summary>
        /// <param name="continueRun">Evaluated once per frame to determine 
        /// whether to continue the event loop.</param>
        /// <exception cref="ArgumentNullException">Thrown when 
        /// <paramref name="continueRun"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="InvalidOperationException">Thrown when called
        /// recursively.</exception>
        void Run(ContinueRunDelegate continueRun);

        /// <summary>The width of the render context, in pixels.</summary>
        /// <remarks>This may vary frame-to-frame.</remarks>
        int Width { get; }

        /// <summary>The height of the render context, in pixels.</summary>
        /// <remarks>This may vary frame-to-frame.</remarks>
        int Height { get; }
    }
}
