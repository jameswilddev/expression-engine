﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Simulations
{
    /// <summary>An interface to implement and export in your project to supply
    /// application-level settings.</summary>
    public interface IConfiguration
    {
        /// <summary>How frequently to raise <see cref="Interval.Tick"/>.
        /// </summary>
        TimeSpan TickInterval { get; }

        /// <summary>The name of the application.  This may be displayed in
        /// the window manager to identify it.</summary>
        string ApplicationName { get; }

        /// <summary>The default width of the window's client area, in pixels.
        /// </summary>
        int WindowWidth { get; }

        /// <summary>The default height of the window's client area, in pixels.
        /// </summary>
        int WindowHeight { get; }
    }
}
