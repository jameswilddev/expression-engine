﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Simulations
{
    /// <summary>Creates <see cref="IRenderPass"/> instances from 
    /// implementation-specific source code.</summary>
    public interface IRenderPassFactory
    {
        /// <summary>Creates a new <see cref="IRenderPass"/> instance from
        /// given implementation-specific source code.</summary>
        /// <param name="source">Source code to create a new 
        /// <see cref="IRenderPass"/> instance from.</param>
        /// <returns>A new <see cref="IRenderPass"/> to draw the source code
        /// given as <paramref name="source"/>.</returns>
        IRenderPass CreateInstance(string source);
    }

    /// <summary>Represents a full-sceen render pass typically implemented as a
    /// pixel shader on the graphics card.</summary>
    public interface IRenderPass : IDisposable
    {
        /// <summary>Renders this pass to the buffer.</summary>
        void Draw();

        /// <summary>Sets a named parameter to a given value.</summary>
        /// <param name="parameter">The name of the parameter to set.</param>
        /// <param name="value">The value to set the parameter to.</param>
        void Set(string parameter, float value);

        /// <summary>Sets a named parameter to a given value.</summary>
        /// <param name="parameter">The name of the parameter to set.</param>
        /// <param name="value">The value to set the parameter to.</param>
        void Set(string parameter, Vector2 value);

        /// <summary>Sets a named parameter to a given value.</summary>
        /// <param name="parameter">The name of the parameter to set.</param>
        /// <param name="value">The value to set the parameter to.</param>
        void Set(string parameter, Vector3 value);

        /// <summary>Sets a named parameter to a given value.</summary>
        /// <param name="parameter">The name of the parameter to set.</param>
        /// <param name="value">The value to set the parameter to.</param>
        void Set(string parameter, Vector4 value);

        /// <summary>Sets a named parameter to a given value.</summary>
        /// <param name="parameter">The name of the parameter to set.</param>
        /// <param name="value">The value to set the parameter to.</param>
        void Set(string parameter, Matrix4x4 value);
    }
}
