﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Simulations
{
    /// <summary>Used to retrieve data at runtime, such as random or time-based
    /// variables.  Allows this data to be injected during tests.</summary>
    public interface IRuntime
    {
        /// <summary>The current date and time.</summary>
        DateTime Now { get; }
    }

    /// <inheritdoc />
    [Export(typeof(IRuntime))]
    public sealed class Runtime : IRuntime
    {
        /// <inheritdoc />
        public DateTime Now { get { return DateTime.Now; } }
    }
}
