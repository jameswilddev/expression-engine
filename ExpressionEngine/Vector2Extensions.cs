﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Numerics;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine
{
    /// <summary>Extends <see cref="Vector2"/> to support many functions which
    /// are understood by GLSL/etc. natively.</summary>
    public static class Vector2Extensions
    {
        /// <summary>Returns the greatest integer less than or equal to 
        /// each component of <paramref name="input"/>.</summary>
        /// <param name="input">The value to determine the greatest integer 
        /// less than or equal to.</param>
        /// <returns>A new <see cref="Vector2"/> containing the greatest integer 
        /// less than or equal to each component in <paramref name="input"/>.
        /// </returns>
        public static Vector2 Floor(this Vector2 input)
        {
            return new Vector2(
                input.X.Floor(),
                input.Y.Floor()
            );
        }

        /// <summary>Returns the lowest integer greater than or equal to 
        /// each component of <paramref name="input"/>.</summary>
        /// <param name="input">The value to determine the lowest integer 
        /// greater than or equal to.</param>
        /// <returns>A new <see cref="Vector2"/> containing the lowest integer 
        /// greater than or equal to each component in <paramref name="input"/>.
        /// </returns>
        public static Vector2 Ceil(this Vector2 input)
        {
            return new Vector2(
                input.X.Ceil(),
                input.Y.Ceil()
            );
        }

        /// <summary>Returns the fractional part of each component of 
        /// <paramref name="input"/>; the difference between each component and 
        /// its <see cref="Floor"/>.</summary>
        /// <param name="input">The value to determine the fractional part of
        /// .</param>
        /// <returns>The the fractional part of each component of 
        /// <paramref name="input"/>; the difference between each component and 
        /// its <see cref="Floor"/>.</returns>
        public static Vector2 Fract(this Vector2 input)
        {
            return new Vector2(
                input.X.Fract(),
                input.Y.Fract()
            );
        }

        /// <summary>Constrains each component in <paramref name="input"/> to 
        /// be between that in <paramref name="min"/> and 
        /// <paramref name="max"/>.</summary>
        /// <param name="input">The value to clamp.</param>
        /// <param name="min">The minimum value to clamp 
        /// <paramref name="input"/> by.</param>
        /// <param name="max">The maximum value to clamp
        /// <paramref name="input"/> by.</param>
        /// <returns><paramref name="input"/>, clamped between 
        /// <paramref name="min"/> and <paramref name="max"/>.</returns>
        public static Vector2 Clamp(this Vector2 input, Vector2 min, Vector2 max)
        {
            return new Vector2(
                input.X.Clamp(min.X, max.X),
                input.Y.Clamp(min.Y, max.Y)
            );
        }

        /// <summary>Constrains each component in <paramref name="input"/> to 
        /// be between <paramref name="min"/> and <paramref name="max"/>.
        /// </summary>
        /// <param name="input">The value to clamp.</param>
        /// <param name="min">The minimum value to clamp 
        /// <paramref name="input"/> by.</param>
        /// <param name="max">The maximum value to clamp
        /// <paramref name="input"/> by.</param>
        /// <returns><paramref name="input"/>, clamped between 
        /// <paramref name="min"/> and <paramref name="max"/>.</returns>
        public static Vector2 Clamp(this Vector2 input, float min, float max)
        {
            return new Vector2(
                input.X.Clamp(min, max),
                input.Y.Clamp(min, max)
            );
        }

        /// <summary>Finds the remainder when dividing each component of a
        /// <see cref="Vector2"/> by a <see cref="float"/>.</summary>
        /// <param name="mod">The <see cref="Vector2"/> to divide.</param>
        /// <param name="by">The <see cref="float"/> to divide by.</param>
        /// <returns>A new <see cref="Vector2"/> containing the remainder of 
        /// dividing each component of <paramref name="mod"/> by 
        /// <paramref name="by"/>.</returns>
        public static Vector2 Mod(this Vector2 mod, Vector2 by)
        {
            return mod - by * (mod / by).Floor();
        }

        /// <summary>Finds the remainder when dividing each component of a
        /// <see cref="Vector2"/> by a <see cref="float"/>.</summary>
        /// <param name="mod">The <see cref="Vector2"/> to divide.</param>
        /// <param name="by">The <see cref="float"/> to divide by.</param>
        /// <returns>A new <see cref="Vector2"/> containing the remainder of 
        /// dividing each component of <paramref name="mod"/> by 
        /// <paramref name="by"/>.</returns>
        public static Vector2 Mod(this Vector2 mod, float by)
        {
            return mod - by * (mod / by).Floor();
        }

        /// <summary>Calculates the reciprocal of the square root of each
        /// component of <paramref name="inverseSqrt"/>.</summary>
        /// <param name="inverseSqrt">The <see cref="Vector2"/> to calculate the
        /// reciprocal of the square root of each component of.</param>
        /// <returns>A new <see cref="Vector2"/> containing the reciprocal of 
        /// the square root of each component of <paramref name="inverseSqrt"/>
        /// .</returns>
        public static Vector2 InverseSqrt(this Vector2 inverseSqrt)
        {
            return new Vector2(1.0f) / Vector2.SquareRoot(inverseSqrt);
        }

        /// <summary>Raises <paramref name="value"/> to the power of
        /// <paramref name="exp"/>.</summary>
        /// <param name="value">The <see cref="Vector2"/> to raise.</param>
        /// <param name="exp">The power to raise <paramref name="value"/> to.
        /// </param>
        /// <returns><paramref name="value"/>, raised to the power of 
        /// <paramref name="exp"/>.</returns>
        public static Vector2 Pow(this Vector2 value, Vector2 exp)
        {
            return new Vector2(
                value.X.Pow(exp.X),
                value.Y.Pow(exp.Y)
            );
        }

        /// <summary>Calculates the sine of each component of
        /// <paramref name="angle"/>.</summary>
        /// <param name="angle">The angle, in radians, to calculate the sine
        /// of.</param>
        /// <returns>The sine of <paramref name="angle"/>.</returns>
        public static Vector2 Sin(this Vector2 angle)
        {
            return new Vector2(
                angle.X.Sin(),
                angle.Y.Sin()
            );
        }

        /// <summary>Calculates the cosine of <paramref name="angle"/>.
        /// </summary>
        /// <param name="angle">The angle, in radians, to calculate the cosine
        /// of.</param>
        /// <returns>The cosine of <paramref name="angle"/>.</returns>
        public static Vector2 Cos(this Vector2 angle)
        {
            return new Vector2(
                angle.X.Cos(),
                angle.Y.Cos()
            );
        }

        /// <summary>Blends between <paramref name="from"/> and 
        /// <paramref name="to"/> by <paramref name="alpha"/>.</summary>
        /// <param name="from">The value to return when 
        /// <paramref name="alpha"/> is zero.</param>
        /// <param name="to">The value to return when
        /// <paramref name="alpha"/> is one.</param>
        /// <param name="alpha">The value to blend by; zero is
        /// <paramref name="from"/> and one is <paramref name="to"/>.</param>
        /// <returns>A blend between <paramref name="from"/> and 
        /// <paramref name="to"/>, by <paramref name="alpha"/>.</returns>
        public static Vector2 Mix(this Vector2 from, Vector2 to, Vector2 alpha)
        {
            return from + (to - from) * alpha;
        }

        internal static void ValidateFieldSelector<T>(T vector, Expression<Func<T, float>> expression, string paramName)
        {
            if (expression == null) throw new ArgumentNullException(paramName);
            var member = expression.Body as MemberExpression;
            if (member == null || member.Expression != expression.Parameters[0] || member.Member.MemberType != MemberTypes.Field) throw new ArgumentOutOfRangeException(paramName, "Swizzles should only select a member from the input vector");
        }

        /// <summary>Creates a new <see cref="Vector2"/> by re-arranging the fields of a <see cref="Vector2"/>.</summary>
        /// <param name="vector">The <see cref="Vector2"/> to swizzle.</param>
        /// <param name="x">An expression selecting the field of <paramref name="vector"/> to use as <see cref="Vector2.X"/></param>
        /// <param name="y">An expression selecting the field of <paramref name="vector"/> to use as <see cref="Vector2.Y"/></param>
        /// <returns>A new <see cref="Vector2"/> created by swizzling <paramref name="vector"/>.</returns>
        public static Vector2 Swizzle(
            this Vector2 vector,
            Expression<Func<Vector2, float>> x,
            Expression<Func<Vector2, float>> y
        )
        {
            ValidateFieldSelector(vector, x, "x");
            ValidateFieldSelector(vector, y, "y");
            return new Vector2(x.Compile()(vector), y.Compile()(vector));
        }

        /// <summary>Creates a new <see cref="Vector3"/> by re-arranging the fields of a <see cref="Vector2"/>.</summary>
        /// <param name="vector">The <see cref="Vector2"/> to swizzle.</param>
        /// <param name="x">An expression selecting the field of <paramref name="vector"/> to use as <see cref="Vector3.X"/></param>
        /// <param name="y">An expression selecting the field of <paramref name="vector"/> to use as <see cref="Vector3.Y"/></param>
        /// <param name="z">An expression selecting the field of <paramref name="vector"/> to use as <see cref="Vector3.Z"/></param>
        /// <returns>A new <see cref="Vector3"/> created by swizzling <paramref name="vector"/>.</returns>
        public static Vector3 Swizzle(
            this Vector2 vector,
            Expression<Func<Vector2, float>> x,
            Expression<Func<Vector2, float>> y,
            Expression<Func<Vector2, float>> z
        )
        {
            ValidateFieldSelector(vector, x, "x");
            ValidateFieldSelector(vector, y, "y");
            ValidateFieldSelector(vector, z, "z");
            return new Vector3(x.Compile()(vector), y.Compile()(vector), z.Compile()(vector));
        }

        /// <summary>Creates a new <see cref="Vector4"/> by re-arranging the fields of a <see cref="Vector2"/>.</summary>
        /// <param name="vector">The <see cref="Vector2"/> to swizzle.</param>
        /// <param name="x">An expression selecting the field of <paramref name="vector"/> to use as <see cref="Vector4.X"/></param>
        /// <param name="y">An expression selecting the field of <paramref name="vector"/> to use as <see cref="Vector4.Y"/></param>
        /// <param name="z">An expression selecting the field of <paramref name="vector"/> to use as <see cref="Vector4.Z"/></param>
        /// <param name="w">An expression selecting the field of <paramref name="vector"/> to use as <see cref="Vector4.W"/></param>
        /// <returns>A new <see cref="Vector4"/> created by swizzling <paramref name="vector"/>.</returns>
        public static Vector4 Swizzle(
            this Vector2 vector,
            Expression<Func<Vector2, float>> x,
            Expression<Func<Vector2, float>> y,
            Expression<Func<Vector2, float>> z,
            Expression<Func<Vector2, float>> w
        )
        {
            ValidateFieldSelector(vector, x, "x");
            ValidateFieldSelector(vector, y, "y");
            ValidateFieldSelector(vector, z, "z");
            ValidateFieldSelector(vector, w, "w");
            return new Vector4(x.Compile()(vector), y.Compile()(vector), z.Compile()(vector), w.Compile()(vector));
        }

        /// <summary>Extends a <see cref="Vector2"/> by one component, creating
        /// a new <see cref="Vector3"/>.</summary>
        /// <param name="vector">The <see cref="Vector2"/> to extend.</param>
        /// <param name="z">The component to extend <paramref name="vector"/>
        /// by.</param>
        /// <returns>A new <see cref="Vector3"/>, where <see cref="Vector3.X"/>
        /// and <see cref="Vector3.Y"/> are from <paramref name="vector"/>, and
        /// <see cref="Vector3.Z"/> is <paramref name="z"/>.</returns>
        public static Vector3 Extend(this Vector2 vector, float z)
        {
            return new Vector3(vector.X, vector.Y, z);
        }

        /// <summary>Extends a <see cref="Vector2"/> by two components, 
        /// creating a new <see cref="Vector4"/>.</summary>
        /// <param name="vector">The <see cref="Vector2"/> to extend.</param>
        /// <param name="z">The first component to extend 
        /// <paramref name="vector"/> by.</param>
        /// <param name="w">The second component to extend 
        /// <paramref name="vector"/> by.</param>
        /// <returns>A new <see cref="Vector4"/>, where <see cref="Vector4.X"/>
        /// and <see cref="Vector4.Y"/> are from <paramref name="vector"/>,
        /// <see cref="Vector4.Z"/> is <paramref name="z"/> and
        /// <see cref="Vector4.W"/> is <paramref name="w"/>.</returns>
        public static Vector4 Extend(this Vector2 vector, float z, float w)
        {
            return new Vector4(vector.X, vector.Y, z, w);
        }
    }
}
