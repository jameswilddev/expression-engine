﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Materials
{
	/// <summary>Calculates the color of a surface at a specified location.
	/// </summary>
	/// <param name="at">The location to sample the color of.</param>
	/// <returns>The color of the surface at the location specified by 
	/// <paramref name="at"/>.</returns>
	public delegate Vector3 MaterialDelegate(Vector3 location);

	/// <summary>Provides tools for building and combining materials.</summary>
	public static class Builder
	{
		/// <summary>Modulates a given <see cref="MaterialDelegate"/> with a
		/// given material color.</summary>
		/// <param name="material">The <see cref="MaterialDelegate"/> to color.
		/// </param>
		/// <param name="color">The color to modulate 
		/// <paramref name="material"/> with.</param>
		/// <returns>An expression modulating <paramref name="material"/> with
		/// <paramref name="color"/>.</returns>
		/// <exception cref="ArgumentNullException">Thrown when 
		/// <paramref name="material"/> is <see langword="null"/>.</exception>
		/// <exception cref="ArgumentNullException">Thrown when 
		/// <paramref name="color"/> is <see langword="null"/>.</exception>
		public static Expression<MaterialDelegate> Color(this Expression<MaterialDelegate> material, Expression<Func<Vector3>> color)
		{
			if (material == null) throw new ArgumentNullException("material");
			if (color == null) throw new ArgumentNullException("color");

			return
				at => material.Invoke(m => m(at)) * color.Invoke(c => c());
		}

		/// <summary>Blends between <paramref name="from"/> and 
		/// <paramref name="to"/> by <paramref name="alpha"/>.</summary>
		/// <param name="from">The <see cref="MaterialDelegate"/> to return 
		/// when <paramref name="alpha"/> is zero.</param>
		/// <param name="to">The <see cref="MaterialDelegate"/> to return when
		/// <paramref name="alpha"/> is one.</param>
		/// <param name="alpha">The value to blend by; zero is
		/// <paramref name="from"/> and one is <paramref name="to"/>.</param>
		/// <returns>A blend between <paramref name="from"/> and 
		/// <paramref name="to"/>, by <paramref name="alpha"/>.</returns>
		/// <exception cref="ArgumentNullException">Thrown when 
		/// <paramref name="from"/> is <see langword="null"/>.</exception>
		/// <exception cref="ArgumentNullException">Thrown when 
		/// <paramref name="to"/> is <see langword="null"/>.</exception>
		/// <exception cref="ArgumentNullException">Thrown when 
		/// <paramref name="alpha"/> is <see langword="null"/>.</exception>
		public static Expression<MaterialDelegate> Mix(this Expression<MaterialDelegate> from, Expression<MaterialDelegate> to, Expression<Func<float>> alpha)
		{
			if (from == null) throw new ArgumentNullException("from");
			if (to == null) throw new ArgumentNullException("to");
			if (alpha == null) throw new ArgumentNullException("alpha");
			return at => Vector3.Lerp(from.Invoke(f => f(at)), to.Invoke(t => t(at)), alpha.Invoke(a => a()));
		}
	}
}
