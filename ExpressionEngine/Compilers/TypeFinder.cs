﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Compilers
{
    /// <summary>Lists every type in an expression tree.  Used to build the
    /// header of shader code.</summary>
    public interface ITypeFinder
    {
        /// <summary>Lists every type in an expression tree.  Used to build the
        /// header of shader code.</summary>
        /// <typeparam name="T">The delegate type the expression implements.
        /// </typeparam>
        /// <param name="source">The expression tree to scan for types.</param>
        /// <returns>Every type in <paramref name="source"/>.  This will not
        /// contain duplicates, but may be in any order.</returns>
        IEnumerable<Type> FindTypes<T>(Expression<T> source);
    }

    /// <inheritdoc />
    [Export(typeof(ITypeFinder))]
    public sealed class TypeFinder : ITypeFinder
    {
        private sealed class Scanner : ExpressionVisitor
        {
            public List<Type> Types = new List<Type>();

            protected override Expression VisitMember(MemberExpression node)
            {
                var constant = node.Expression as ConstantExpression;
                if(constant != null)
                {
                    var field = node.Member as FieldInfo;
                    if(field != null)
                    {
                        Visit((Expression)field.GetValue(constant.Value));
                    }
                }
                return base.VisitMember(node);
            }

            public override Expression Visit(Expression node)
            {
                if(node != null)
                    Types.Add(node.Type);
                return base.Visit(node);
            }
        }

        /// <inheritdoc />
        public IEnumerable<Type> FindTypes<T>(Expression<T> source)
        {
            if (source == null)
                throw new ArgumentNullException("source");
            var scanner = new Scanner();
            scanner.Visit(source.Body);
            return scanner.Types
                .Union(source.Parameters.Select(s => s.Type))
                .Union(Enumerable.Repeat(source.ReturnType, 1));
        }
    }
}
