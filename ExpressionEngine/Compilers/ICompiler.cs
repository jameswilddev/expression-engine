﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using ExpressionEngine.Simulations;
using System.Numerics;

namespace ExpressionEngine.Compilers
{
    /// <summary>Compiles an expression tree to source code for another
    /// language.</summary>
    /// <remarks>Implementors typically only support a limited subset of the 
    /// functionality present in C#.</remarks>
    public interface ICompiler
    {
        /// <summary>Compiles the given expression tree into the language
        /// supported by the matching implementation of 
        /// <see cref="IRenderPass"/>.  The delegate type must take a 
        /// <see cref="Vector2"/> called "pixel" as its first argument; the
        /// pixel location being shaded relative to the bottom left corner of
        /// the render context.  The returned <see cref="Vector3"/> is taken
        /// as the pixel color.</summary>
        /// <remarks>Implementors typically only support a limited subset of 
        /// the functionality present in C#.  The first </remarks>
        /// <typeparam name="T">The delegate type <paramref name="source"/>
        /// implements.</typeparam>
        /// <param name="source">An expression tree to compile.</param>
        /// <returns>Source code representing the expression tree given as
        /// <paramref name="source"/> in the language supported by this
        /// <see cref="ICompiler"/> implementation.</returns>
        /// <exception cref="ArgumentNullException">Thrown when 
        /// <paramref name="source"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when 
        /// <typeparamref name="T"/> does not take at least one parameter.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when 
        /// <typeparamref name="T"/>'s first parameter is not a 
        /// <see cref="Vector2"/>.</exception>
        String Compile<T>(Expression<T> source);
    }
}
