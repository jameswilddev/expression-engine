﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine
{
    /// <summary>Provides services for injecting expression trees into other
    /// expression trees.</summary>
    public static class Injector
    {
        private sealed class Replacer : ExpressionVisitor
        {
            public Expression Replace;
            public Expression With;

            public override Expression Visit(Expression node)
            {
                if (node == Replace) return With;
                return base.Visit(node);
            }
        }

        private sealed class MethodSwapper : ExpressionVisitor
        {
            public Object Replace;
            public Expression With;
            public ReadOnlyCollection<ParameterExpression> Parameters;

            protected override Expression VisitInvocation(InvocationExpression node)
            {
                // The mock we injected becomes a field in a closure object
                // which is part of the delegate called to get the expression
                // tree to inject into.
                var level0 = node.Expression as MemberExpression;
                if (level0 == null) return base.VisitInvocation(node);
                var level1 = level0.Expression as ConstantExpression;
                if (level1 == null) return base.VisitInvocation(node);
                if (level0.Member.MemberType != MemberTypes.Field) return base.VisitInvocation(node);
                var method = (level0.Member as FieldInfo).GetValue(level1.Value);
                if (method == Replace)
                {
                    var expression = With;
                    for (int i = 0; i < Parameters.Count; i++ )
                    {
                        expression = new Replacer { Replace = Parameters[i], With = node.Arguments[i] }.VisitAndConvert(expression, "caller");
                    }
                    return expression;
                }
                return base.VisitInvocation(node);
            }
        }

        /// <summary>Injects a given expression tree into another using an
        /// instance of a delegate as a mock for the expression tree to inject.
        /// </summary>
        /// <typeparam name="TInto">The delegate type 
        /// <typeparamref name="TInject"/> is being injected into.</typeparam>
        /// <typeparam name="TInject">The delegate type to inject into 
        /// <typeparamref name="TInto"/>.</typeparam>
        /// <param name="inject">The expression to inject into 
        /// <paramref name="into"/>.</param>
        /// <param name="into">A delegate returning an expression tree to
        /// inject into, where the argument is a mock to call.  Any calls to
        /// the mock will be replaced with <paramref name="inject"/>.</param>
        /// <returns>A copy of the expression tree returned by 
        /// <paramref name="into"/>, with each call to the first argument
        /// replaced with <paramref name="inject"/>.</returns>
        /// <exception cref="ArgumentNullException">Thrown when 
        /// <paramref name="inject"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentNullException">Thrown when 
        /// <paramref name="into"/> is <see langword="null"/>.</exception>
        /// <exception cref="InvalidOperationException">Thrown when 
        /// <paramref name="into"/> returns <see langword="null"/>.</exception>
        public static Expression<TInto> Inject<TInto, TInject>(Expression<TInject> inject, Func<TInject, Expression<TInto>> into)
            where TInject : class
        {
            if (inject == null) throw new ArgumentNullException("inject");
            if (into == null) throw new ArgumentNullException("into");
            var mock = Expression.Lambda<TInject>(Expression.Throw(Expression.Constant(new InvalidOperationException("This delegate should be used as a mock, and should never be invoked outside of the expression tree being injected into")), inject.ReturnType), inject.Parameters).Compile();
            var result = into(mock);
            if (result == null) throw new InvalidOperationException("into should not return null");
            return (Expression<TInto>)new MethodSwapper{ Replace = mock, With = inject.Body, Parameters = inject.Parameters }.Visit(result);
        }

		/// <summary>Calculates a value, usually storing it in a temporary
		/// variable, and then provides that value as an argument to an 
		/// expression.</summary>
		/// <typeparam name="T">The type to store in a temporary variable.
		/// </typeparam>
		/// <typeparam name="TResult">The return type of the expression in use.
		/// </typeparam>
		/// <param name="let">The value to store as a temporary variable.
		/// </param>
		/// <param name="use">The expression to calculate using 
		/// <paramref name="let"/> as its argument.</param>
		/// <returns>The value of <paramref name="use"/> given 
		/// <paramref name="let"/>.</returns>
		/// <exception cref="ArgumentNullException">Thrown when 
		/// <paramref name="use"/> is <see langword="null"/>.</exception>
        public static TResult Let<T, TResult>(this T let, Expression<Func<T, TResult>> use)
        {
			if (use == null) throw new ArgumentNullException("use");
            return use.Compile()(let);
        }

		/// <summary>Invokes an expression tree.  Compilers should recognize
		/// and collapse this call, including the invoked expression tree in
		/// the parent expression tree.</summary>
		/// <typeparam name="T">The delegate type the expression being invoked
		/// is based upon.</typeparam>
		/// <typeparam name="TResult">The return type of the delegate.
		/// </typeparam>
		/// <param name="expression">The expression to invoke.</param>
		/// <param name="invocation">An expression which invokes 
		/// <paramref name="expression"/>.</param>
		/// <returns>The result of invoking <paramref name="expression"/> using
		/// <paramref name="invocation"/>.</returns>
		/// <exception cref="ArgumentNullException">Thrown when 
		/// <paramref name="expression"/> is <see langword="null"/>.
		/// </exception>
		/// <exception cref="ArgumentNullException">Thrown when 
		/// <paramref name="invocation"/> is <see langword="null"/>.
		/// </exception>
		/// <exception cref="ArgumentOutOfRangeException">Thrown when 
		/// <paramref name="invocation"/> does not invoke 
		/// <paramref name="expression"/>.</exception>
		public static TResult Invoke<T, TResult>(this Expression<T> expression, Expression<Func<T, TResult>> invocation)
		{
			if (expression == null) throw new ArgumentNullException("expression");
			if (invocation == null) throw new ArgumentNullException("invocation");

			if (invocation.Body.NodeType != ExpressionType.Invoke) throw new ArgumentOutOfRangeException("invocation", "Must be an invocation of the expression");
			
			return invocation.Compile()(expression.Compile());
		}
    }
}
