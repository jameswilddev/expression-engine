﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine
{
	/// <summary>Extends <see cref="int"/> to support many functions which
	/// are understood by GLSL/etc. natively.</summary>
	public static class IntExtensions
	{
		/// <summary>Constrains <paramref name="input"/> to be between 
		/// <paramref name="min"/> and <paramref name="max"/>.</summary>
		/// <param name="input">The value to clamp.</param>
		/// <param name="min">The minimum value to clamp 
		/// <paramref name="input"/> by.</param>
		/// <param name="max">The maximum value to clamp
		/// <paramref name="input"/> by.</param>
		/// <returns><paramref name="input"/>, clamped between 
		/// <paramref name="min"/> and <paramref name="max"/>.</returns>
		public static int Clamp(this int input, int min, int max)
		{
			return Math.Max(Math.Min(input, max), min);
		}
	}
}
