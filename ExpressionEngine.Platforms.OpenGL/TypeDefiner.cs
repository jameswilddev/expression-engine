﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Platforms.OpenGL
{
    /// <summary>Generates GLSL to define the given types.</summary>
    public interface ITypeDefiner
    {
        /// <summary>Generates GLSL to define given types.</summary>
        /// <remarks>This should be in the header block of your shader.
        /// </remarks>
        /// <param name="types">The types to define.  Types already defined by
        /// GLSL (<see cref="Vector3"/>, <see cref="int"/>, etc.), duplicate
        /// entries, reference types and <see langword="null"/> values will be 
        /// ignored.</param>
        /// <returns>GLSL code defining the types listed in 
        /// <paramref name="types"/>.</returns>
        /// <exception cref="ArgumentNullException">Thrown when 
        /// <paramref name="types"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when 
        /// <paramref name="types"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when 
        /// <paramref name="types"/> contains values which are impossible for
        /// GLSL to natively support such as reference types.</exception>
        /// <exception cref="NotSupportedException">Thrown when 
        /// <paramref name="types"/> contains values which are impossible for
        /// GLSL to natively support such as reference types.</exception>
        /// <exception cref="NotImplementedException">Thrown when 
        /// <paramref name="types"/> contains values which are impossible for
        /// GLSL to natively support such as reference types.</exception>
        string DefineTypes(IEnumerable<Type> types);
    }

    /// <inheritdoc />
    [Export(typeof(ITypeDefiner))]
    public sealed class TypeDefiner : ITypeDefiner
    {
        private sealed class Runtime
        {
            public readonly StringBuilder StringBuilder = new StringBuilder();

			public bool FirstDeclaration = true;

            public readonly Dictionary<Type, string> AlreadyListed = new Dictionary<Type, string>
            {
                {typeof(Matrix4x4), "mat4"},
                {typeof(Vector2), "vec2"},
                {typeof(Vector3), "vec3"},
                {typeof(Vector4), "vec4"},
                {typeof(int), "int"},
                {typeof(float), "float"},
                {typeof(bool), "bool"}
            };
        }

        private string GetTypeName(Type type, Runtime runtime)
        {
            if (runtime.AlreadyListed.ContainsKey(type)) 
                return runtime.AlreadyListed[type];

            var name = FunctionCompiler.GetTypeName(type);
            runtime.AlreadyListed[type] = name;

			if (!runtime.FirstDeclaration)
			{
				runtime.StringBuilder.AppendLine();
				runtime.StringBuilder.AppendLine();
			}

			runtime.FirstDeclaration = false;
            runtime.StringBuilder.Append(string.Format(@"struct {0} {{{1}
}};", name, string.Concat(type.GetFields()
				.Where(f => !f.IsStatic)
				// Sort by location in memory.  This is necessary because GetFields() doesn't return the fields
				// in any specific order, we need a consistent order for GLSL's constructor syntax, and
				// we can't use name as vec4 in particular uses XYZW which would sort as WXYZ.
				.OrderBy(p => Marshal.OffsetOf(type, p.Name).ToInt32())
				.Select(f => string.Format("{0}\t{1} {2};", Environment.NewLine, GetTypeName(f.FieldType, runtime), f.Name)))));

            return name;
        }

        /// <inheritdoc />
        public string DefineTypes(IEnumerable<Type> types)
        {
            if(types == null) throw new ArgumentNullException("types");

            var runtime = new Runtime();

            foreach(var type in types)
            {
                if (type == null || !type.IsValueType)
                    continue;

                GetTypeName(type, runtime);
            }

            return runtime.StringBuilder.ToString();
        }
    }
}
