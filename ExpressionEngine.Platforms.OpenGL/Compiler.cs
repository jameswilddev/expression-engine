﻿using ExpressionEngine.Compilers;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Linq.Expressions;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Platforms.OpenGL
{
	/// <inheritdoc />
    [Export(typeof(ICompiler))]
    public sealed class Compiler : ICompiler
    {
		/// <inheritdoc />
        [Import]
        public IFunctionCompiler FunctionCompiler;

		/// <inheritdoc />
        [Import]
        public ITypeDefiner TypeDefiner;

		/// <inheritdoc />
        [Import]
        public ITypeFinder TypeFinder;

		/// <inheritdoc />
        public string Compile<T>(Expression<T> source)
        {
            if (source == null) throw new ArgumentNullException("source");
            if (!source.Parameters.Any()) 
                throw new ArgumentOutOfRangeException("source", "Must have at least one parameter");
            if(source.Parameters[0].Type != typeof(Vector2))
                throw new ArgumentOutOfRangeException("source", "The first parameter must be a Vector2");
            if (source.Parameters[0].Name != "pixel")
                throw new ArgumentOutOfRangeException("source", "The first parameter's name must be \"pixel\"");
            if(source.ReturnType != typeof(Vector3))
                throw new ArgumentOutOfRangeException("source", "The return type must be Vector3");
			
			var script = "";

			var typeDefinitions = TypeDefiner.DefineTypes(TypeFinder.FindTypes(source));
			if(!string.IsNullOrEmpty(typeDefinitions))
			{
				script += typeDefinitions;
				script += Environment.NewLine;
				script += Environment.NewLine;
			}

			var parameters = string.Join(Environment.NewLine, source.Parameters.Skip(1).Select(p => string.Format("uniform {0} parameter_{1};", OpenGL.FunctionCompiler.GetTypeName(p.Type), p.Name)));
			if(!string.IsNullOrEmpty(parameters))
			{
				script += parameters;
				script += Environment.NewLine;
				script += Environment.NewLine;
			}

			script += string.Format(
 @"vec3 run(vec2 parameter_pixel) {{
{0}
}}

void main() {{
	gl_FragColor = vec4(run(gl_FragCoord.xy), 1.0);
}}",string.Join(Environment.NewLine, FunctionCompiler.Compile(source).Split(new[]{Environment.NewLine}, StringSplitOptions.None).Select(l => string.Format("\t{0}", l))));

			return script;
        }
    }
}
