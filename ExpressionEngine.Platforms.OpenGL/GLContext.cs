﻿using ExpressionEngine.Simulations;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Platforms.OpenGL
{
    /// <inheritdoc />
    [Export(typeof(IContext))]
    public sealed class GLContext : IContext, IPartImportsSatisfiedNotification, IDisposable
    {
        /// <inheritdoc />
        [Import]
        public IInterval Interval;

        /// <inheritdoc />
        [Import]
        public IGLFW GLFW;

        /// <inheritdoc />
        [Import]
        public IGL GL;

        /// <inheritdoc />
        [Import]
        public IConfiguration Configuration;

        /// <inheritdoc />
        public int Width { get; private set; }

        /// <inheritdoc />
        public int Height { get; private set; }

        private bool TerminateOnDispose = false;
        private IntPtr Window = (IntPtr)null;

        private void WindowSize(IntPtr window, int width, int height)
        {
            if (width == 0 || height == 0) return;
            Width = width;
            Height = height;
            GL.Viewport(0, 0, width, height);
            Interval.Update();
            GLFW.SwapBuffers(window);
        }

        private WindowSizeDelegate Storage; 

        /// <inheritdoc />
        public void OnImportsSatisfied()
        {
            if (!GLFW.Init())
                throw new ExternalException("GLFW failed to initialize");

            TerminateOnDispose = true;

            Width = Configuration.WindowWidth;
            Height = Configuration.WindowHeight;
            Window = GLFW.CreateWindow(Configuration.WindowWidth, Configuration.WindowHeight, Configuration.ApplicationName, (IntPtr)null, (IntPtr)null);
            if (Window == (IntPtr)null)
                throw new ExternalException("GLFW failed to create a window");

            Storage = WindowSize;
            GLFW.SetWindowSizeCallback(Window, Storage);

            GLFW.SwapInterval(1);
            GLFW.MakeContextCurrent(Window);
        }

        private bool Running = false;

        /// <inheritdoc />
        public void Run(ContinueRunDelegate continueRun)
        {
            if (continueRun == null) throw new ArgumentNullException("continueRun");
            if (Running) throw new InvalidOperationException("Run should not be called recursively");

            try
            {
                Running = true;
                GLFW.SetWindowShouldClose(Window, false);
                while (continueRun(GLFW.WindowShouldClose(Window)))
                {
                    GLFW.SetWindowShouldClose(Window, false);
                    Interval.Update();
                    GLFW.SwapBuffers(Window);
                    GLFW.PollEvents();
                }
            }
            finally
            {
                Running = false;
            }
        }

        /// <inheritdoc />
        public void Dispose()
        {
            if (Window != (IntPtr)null)
                GLFW.DestroyWindow(Window);
            Window = (IntPtr)null;

            if (TerminateOnDispose)
                GLFW.Terminate();
            TerminateOnDispose = false;
        }
    }
}
