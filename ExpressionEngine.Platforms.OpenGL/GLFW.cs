﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Platforms.OpenGL
{
    /// <summary>An interface exposing the GLFW library.</summary>
    /// <remarks>This is intended to be used internally, but is publically 
    /// accessible for testing.</remarks>
    public interface IGLFW
    {
        /// <summary>Initializes GLFW.</summary>
        /// <returns><see langword="true"/> if initialization was successful, 
        /// else <see langword="false"/>.</returns>
        bool Init();

        /// <summary>Terminates GLFW and frees all allocated resources.
        /// </summary>
        void Terminate();

        /// <summary>Creates a new GLFW window.</summary>
        /// <param name="width">The client area width, in pixels.</param>
        /// <param name="height">The client area height, in pixels.</param>
        /// <param name="title">The title of the window.</param>
        /// <param name="monitor">The monitor to display on; leave 
        /// <see langword="null"/> for default.</param>
        /// <param name="share">A window to share resources with; leave 
        /// <see langword="null"/> to not share resources.</param>
        /// <returns></returns>
        IntPtr CreateWindow(int width, int height, string title, IntPtr monitor, IntPtr share);

        /// <summary>Closes a previously created GLFW window.</summary>
        /// <param name="window">The handle of the window to close.</param>
        void DestroyWindow(IntPtr window);

        /// <summary>Invokes any queued callbacks for button presses, window
        /// closures, etc.</summary>
        void PollEvents();

        /// <summary>Sends the rendered buffer to the screen.</summary>
        /// <param name="window">The window to swap buffers for.</param>
        void SwapBuffers(IntPtr window);

        /// <summary>Sets the "swap interval" used when synchronizing the
        /// render buffer and the screen.</summary>
        /// <remarks>Many implementations ignore this.</remarks>
        /// <param name="interval">Zero disables v-sync and may cause tearing.
        /// Greater than zero enables v-sync.  Less than zero enables v-sync
        /// when framerate is at or above the refresh rate.</param>
        void SwapInterval(int interval);

        /// <summary>Returns <see langword="true"/> once the given window's
        /// close button has been pressed.</summary>
        /// <param name="window">The window to check.</param>
        /// <returns>If <paramref name="window"/>'s close button has been
        /// pressed, <see langword="true"/>, else, <see langword="false"/>.
        /// </returns>
        bool WindowShouldClose(IntPtr window);

        /// <summary>Sets the flag read by <see cref="WindowShouldClose"/>
        /// manually.</summary>
        /// <remarks>This is used to clear the flag should we choose to ignore
        /// it.</remarks>
        /// <param name="window">The window to set the flag for.</param>
        /// <param name="value">The value to set the flag to.</param>
        void SetWindowShouldClose(IntPtr window, bool value);

        /// <summary>Specifies the window to render to.</summary>
        /// <param name="window">The window to render to.</param>
        void MakeContextCurrent(IntPtr window);

        /// <summary>Specifies a callback to execute on the resize of a window.
        /// </summary>
        /// <param name="window">The window to set the resize callback for.
        /// </param>
        /// <param name="callback">A callback to execute when the window
        /// specified by <paramref name="window"/> is resized.</param>
        void SetWindowSizeCallback(IntPtr window, WindowSizeDelegate callback);
    }

    /// <summary>A delegate to be executed when a window is resized.</summary>
    /// <param name="window">The window being resized.</param>
    /// <param name="width">The width of the render context, in pixels.</param>
    /// <param name="height">The height of the render context, in pixels.</param>
    public delegate void WindowSizeDelegate(IntPtr window, int width, int height);

    /// <inheritdoc />
    [Export(typeof(IGLFW))]
    public sealed class GLFW : IGLFW
    {
        /// <inheritdoc />
        public bool Init()
        {
            return GLFWInterop.glfwInit();
        }

        /// <inheritdoc />
        public void Terminate()
        {
            GLFWInterop.glfwTerminate();
        }

        /// <inheritdoc />
        public IntPtr CreateWindow(int width, int height, string title, IntPtr monitor, IntPtr share)
        {
            return GLFWInterop.glfwCreateWindow(width, height, title, monitor, share);
        }

        /// <inheritdoc />
        public void DestroyWindow(IntPtr window)
        {
            GLFWInterop.glfwDestroyWindow(window);
        }

        /// <inheritdoc />
        public void PollEvents()
        {
            GLFWInterop.glfwPollEvents();
        }

        /// <inheritdoc />
        public void SwapBuffers(IntPtr window)
        {
            GLFWInterop.glfwSwapBuffers(window);
        }

        /// <inheritdoc />
        public void SwapInterval(int interval)
        {
            GLFWInterop.glfwSwapInterval(interval);
        }

        /// <inheritdoc />
        public bool WindowShouldClose(IntPtr window)
        {
            return GLFWInterop.glfwWindowShouldClose(window);
        }

        /// <inheritdoc />
        public void SetWindowShouldClose(IntPtr window, bool value)
        {
            GLFWInterop.glfwSetWindowShouldClose(window, value);
        }

        /// <inheritdoc />
        public void MakeContextCurrent(IntPtr window)
        {
            GLFWInterop.glfwMakeContextCurrent(window);
        }

        /// <inheritdoc />
        public void SetWindowSizeCallback(IntPtr window, WindowSizeDelegate callback)
        {
            GLFWInterop.glfwSetWindowSizeCallback(window, callback);
        }
    }

    internal static class GLFWInterop
    {
        [DllImport("glfw3.dll", CallingConvention = CallingConvention.Cdecl)]
        [return:MarshalAs(UnmanagedType.Bool)]
        public static extern bool glfwInit();

        [DllImport("glfw3.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void glfwTerminate();

        [DllImport("glfw3.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void glfwSwapInterval(int interval);

        [DllImport("glfw3.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr glfwCreateWindow(int width, int height, string title, IntPtr monitor, IntPtr share);

        [DllImport("glfw3.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void glfwDestroyWindow(IntPtr window);

        [DllImport("glfw3.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void glfwSwapBuffers(IntPtr window);

        [DllImport("glfw3.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void glfwPollEvents();

        [DllImport("glfw3.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool glfwWindowShouldClose(IntPtr window);

        [DllImport("glfw3.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern bool glfwSetWindowShouldClose(IntPtr window, bool value);

        [DllImport("glfw3.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void glfwMakeContextCurrent(IntPtr window);

        [DllImport("glfw3.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void glfwSetWindowSizeCallback(IntPtr window, WindowSizeDelegate callback);
    }
}
