﻿using ExpressionEngine.Simulations;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Platforms.OpenGL
{
    /// <inheritdoc />
    [Export(typeof(IRenderPassFactory))]
    public sealed class RenderPassFactory : IRenderPassFactory
    {
        /// <inheritdoc />
        [Import]
        public IGL GL { get; set; }

        /// <inheritdoc />
        [Import]
        public IGLExtensions GLExtensions { get; set; }

        /// <inheritdoc />
        [Import]
        public IInterval Interval { get; set; }

        /// <inheritdoc />
        public IRenderPass CreateInstance(string source)
        {
            if (source == null) throw new ArgumentNullException("source");

            var vertexShader = GLExtensions.CreateShader(GLConstants.GL_VERTEX_SHADER);

            if (vertexShader == 0)
                throw new ExternalException("Failed to allocate a vertex shader");

            try
            {
                var vertexSource = "void main() { gl_Position = ftransform(); }";

                GLExtensions.ShaderSource(vertexShader, 1, new[] { vertexSource }, new[] { vertexSource.Length });

                GLExtensions.CompileShader(vertexShader);

                Int32 status = 0;

                unsafe
                {
                    GLExtensions.GetShaderiv(vertexShader, GLConstants.GL_COMPILE_STATUS, (IntPtr)(&status));
                }

                if (status == 0)
                {
                    Int32 logLength = 0;

                    unsafe
                    {
                        GLExtensions.GetShaderiv(vertexShader, GLConstants.GL_INFO_LOG_LENGTH, (IntPtr)(&logLength));
                    }

                    var log = new Byte[logLength];

                    unsafe
                    {
                        fixed (Byte* logBytes = log)
                            GLExtensions.GetShaderInfoLog(vertexShader, logLength, (IntPtr)(&logLength), (IntPtr)(logBytes));
                    }

                    throw new ExternalException(String.Format("An error occurred while compiling the vertex shader: \"{0}\"", Encoding.ASCII.GetString(log)));
                }

                var fragmentShader = GLExtensions.CreateShader(GLConstants.GL_FRAGMENT_SHADER);

                if (fragmentShader == 0)
                    throw new ExternalException("Failed to allocate a fragment shader");

                try
                {

                    GLExtensions.ShaderSource(fragmentShader, 1, new[] { source }, new[] { source.Length });

                    GLExtensions.CompileShader(fragmentShader);

                    unsafe
                    {
                        GLExtensions.GetShaderiv(fragmentShader, GLConstants.GL_COMPILE_STATUS, (IntPtr)(&status));
                    }

                    if (status == 0)
                    {
                        Int32 logLength = 0;

                        unsafe
                        {
                            GLExtensions.GetShaderiv(fragmentShader, GLConstants.GL_INFO_LOG_LENGTH, (IntPtr)(&logLength));
                        }

                        var log = new Byte[logLength];

                        unsafe
                        {
                            fixed (Byte* logBytes = log)
                                GLExtensions.GetShaderInfoLog(fragmentShader, logLength, (IntPtr)(&logLength), (IntPtr)(logBytes));
                        }

                        throw new ExternalException(String.Format("An error occurred while compiling the fragment shader: \"{0}\"", Encoding.ASCII.GetString(log)));
                    }

                    var program = GLExtensions.CreateProgram();
                    if (program == 0)
                        throw new ExternalException("Failed to allocate a program");

                    try
                    {
                        GLExtensions.AttachShader(program, vertexShader);

                        try
                        {
                            GLExtensions.AttachShader(program, fragmentShader);
                            try
                            {
                                GLExtensions.LinkProgram(program);

                                unsafe
                                {
                                    GLExtensions.GetProgramiv(program, GLConstants.GL_LINK_STATUS, (IntPtr)(&status));
                                }

                                if (status == 0)
                                {
                                    Int32 logLength = 0;

                                    unsafe
                                    {
                                        GLExtensions.GetProgramiv(program, GLConstants.GL_INFO_LOG_LENGTH, (IntPtr)(&logLength));
                                    }

                                    var log = new Byte[logLength];

                                    unsafe
                                    {
                                        fixed (Byte* logBytes = log)
                                            GLExtensions.GetProgramInfoLog(program, logLength, (IntPtr)(&logLength), (IntPtr)(logBytes));
                                    }

                                    throw new ExternalException(String.Format("An error occurred while linking the program: \"{0}\"", Encoding.ASCII.GetString(log)));
                                }

                                return new RenderPass(program, GL, GLExtensions);
                            }
                            finally
                            {
                                GLExtensions.DetachShader(program, fragmentShader);
                            }
                        }
                        finally
                        {
                            GLExtensions.DetachShader(program, vertexShader);
                        }
                    }
                    catch
                    {
                        GLExtensions.DeleteProgram(program);
                        throw;
                    }
                }
                finally
                {
                    GLExtensions.DeleteShader(fragmentShader);
                }
            }
            finally
            {
                GLExtensions.DeleteShader(vertexShader);
            }
        }

        private sealed class RenderPass : IRenderPass
        {
            private UInt32? Program;
            private readonly IGL GL;
            private readonly IGLExtensions GLExtensions;

            public RenderPass(UInt32 program, IGL gl, IGLExtensions glExtensions)
            {
                Program = program;
                GL = gl;
                GLExtensions = glExtensions;
            }

            private static readonly Vector2[] Vertices = new Vector2[]
            {
                new Vector2(-1.0f, -1.0f),
                new Vector2(-1.0f, 1.0f),
                new Vector2(1.0f, 1.0f),
                new Vector2(1.0f, -1.0f)
            };

            private static readonly byte[] Indices = new byte[]
            {
                0, 1, 2,
                2, 3, 0
            };

            public void Draw()
            {
                if (!Program.HasValue) throw new InvalidOperationException("Cannot be drawn once disposed");
                GLExtensions.UseProgram(Program.Value);
                GL.EnableClientState(GLConstants.GL_VERTEX_ARRAY);
                unsafe
                {
                    fixed (Vector2* vertices = Vertices)
                    {
                        GL.VertexPointer(2, GLConstants.GL_FLOAT, 0, (IntPtr)vertices);
                        fixed (byte* indices = Indices)
                        {
                            GL.DrawElements(GLConstants.GL_TRIANGLES, 6, GLConstants.GL_UNSIGNED_BYTE, (IntPtr)indices);
                        }
                    }
                }
                GL.DisableClientState(GLConstants.GL_VERTEX_ARRAY);
                GLExtensions.UseProgram(0);
            }

            private Int32 ValidateParameter(string parameter)
            {
                if (!Program.HasValue) throw new InvalidOperationException("Cannot set parameters once disposed");
                if (parameter == null) throw new ArgumentNullException("parameter");
                var location = GLExtensions.GetUniformLocation(Program.Value, string.Format("parameter_{0}", parameter));
                if (location == -1) throw new ArgumentOutOfRangeException("parameter", "No matching uniform exists in the shader");
                GLExtensions.UseProgram(Program.Value);
                return location;
            }

            public void Set(string parameter, float value)
            {
                GLExtensions.Uniform1f(ValidateParameter(parameter), value);
                GLExtensions.UseProgram(0);
            }

            public void Set(string parameter, System.Numerics.Vector2 value)
            {
                GLExtensions.Uniform2f(ValidateParameter(parameter), value.X, value.Y);
                GLExtensions.UseProgram(0);
            }

            public void Set(string parameter, System.Numerics.Vector3 value)
            {
                GLExtensions.Uniform3f(ValidateParameter(parameter), value.X, value.Y, value.Z);
                GLExtensions.UseProgram(0);
            }

            public void Set(string parameter, System.Numerics.Vector4 value)
            {
                GLExtensions.Uniform4f(ValidateParameter(parameter), value.X, value.Y, value.Z, value.W);
                GLExtensions.UseProgram(0);
            }

            public void Set(string parameter, System.Numerics.Matrix4x4 value)
            {
                unsafe
                {
                    GLExtensions.UniformMatrix4fv(ValidateParameter(parameter), 1, true, (IntPtr)(&value));
                    GLExtensions.UseProgram(0);
                }
            }

            public void Dispose()
            {
                if (!Program.HasValue) return;
                GLExtensions.DeleteProgram(Program.Value);
                Program = null;
            }
        }
    }
}
