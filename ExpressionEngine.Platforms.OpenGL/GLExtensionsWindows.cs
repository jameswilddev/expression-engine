﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Platforms.OpenGL
{
    /// <inheritdoc />
    [Export(typeof(IGLExtensions))]
    public sealed class GLExtensionsWindows : IGLExtensions
    {
        [DllImport("opengl32.dll", CharSet = CharSet.Ansi)]
        private static extern IntPtr wglGetProcAddress(string proc);

        private delegate UInt32 CreateShaderDelegate(UInt32 type);

        [UnmanagedFunctionPointer(CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        private delegate void ShaderSourceDelegate(UInt32 shader, Int32 count, string[] sources, Int32[] lengths);

        private delegate void CompileShaderDelegate(UInt32 type);

        private delegate UInt32 CreateProgramDelegate();

        private delegate void AttachShaderDelegate(UInt32 program, UInt32 shader);

        private delegate void GetProgramivDelegate(UInt32 program, UInt32 pname, IntPtr param);

        [UnmanagedFunctionPointer(CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        private delegate void GetProgramInfoLogDelegate(UInt32 program, Int32 maxLength, IntPtr length, IntPtr infoLog);

        /// <inheritdoc />
        public UInt32 CreateShader(UInt32 type)
        {
            return ((CreateShaderDelegate)Marshal.GetDelegateForFunctionPointer(wglGetProcAddress("glCreateShader"), typeof(CreateShaderDelegate)))(type);
        }

        /// <inheritdoc />
        public void DeleteShader(UInt32 shader)
        {
            ((CompileShaderDelegate)Marshal.GetDelegateForFunctionPointer(wglGetProcAddress("glDeleteShader"), typeof(CompileShaderDelegate)))(shader);
        }

        /// <inheritdoc />
        public void ShaderSource(UInt32 shader, Int32 count, string[] sources, Int32[] lengths)
        {
            ((ShaderSourceDelegate)Marshal.GetDelegateForFunctionPointer(wglGetProcAddress("glShaderSource"), typeof(ShaderSourceDelegate)))(shader, count, sources, lengths);
        }

        /// <inheritdoc />
        public void CompileShader(UInt32 shader)
        {
            ((CompileShaderDelegate)Marshal.GetDelegateForFunctionPointer(wglGetProcAddress("glCompileShader"), typeof(CompileShaderDelegate)))(shader);
        }

        /// <inheritdoc />
        public void LinkProgram(UInt32 program)
        {
            ((CompileShaderDelegate)Marshal.GetDelegateForFunctionPointer(wglGetProcAddress("glLinkProgram"), typeof(CompileShaderDelegate)))(program);
        }

        /// <inheritdoc />
        public void UseProgram(UInt32 program)
        {
            ((CompileShaderDelegate)Marshal.GetDelegateForFunctionPointer(wglGetProcAddress("glUseProgram"), typeof(CompileShaderDelegate)))(program);
        }

        /// <inheritdoc />
        public UInt32 CreateProgram()
        {
            return ((CreateProgramDelegate)Marshal.GetDelegateForFunctionPointer(wglGetProcAddress("glCreateProgram"), typeof(CreateProgramDelegate)))();
        }

        /// <inheritdoc />
        public void DeleteProgram(UInt32 program)
        {
            ((CompileShaderDelegate)Marshal.GetDelegateForFunctionPointer(wglGetProcAddress("glDeleteProgram"), typeof(CompileShaderDelegate)))(program);
        }

        /// <inheritdoc />
        public void AttachShader(UInt32 program, UInt32 shader)
        {
            ((AttachShaderDelegate)Marshal.GetDelegateForFunctionPointer(wglGetProcAddress("glAttachShader"), typeof(AttachShaderDelegate)))(program, shader);
        }

        /// <inheritdoc />
        public void DetachShader(UInt32 program, UInt32 shader)
        {
            ((AttachShaderDelegate)Marshal.GetDelegateForFunctionPointer(wglGetProcAddress("glDetachShader"), typeof(AttachShaderDelegate)))(program, shader);
        }

        /// <inheritdoc />
        public void GetProgramiv(UInt32 program, UInt32 pname, IntPtr param)
        {
            ((GetProgramivDelegate)Marshal.GetDelegateForFunctionPointer(wglGetProcAddress("glGetProgramiv"), typeof(GetProgramivDelegate)))(program, pname, param);
        }

        /// <inheritdoc />
        public void GetProgramInfoLog(UInt32 program, Int32 maxLength, IntPtr length, IntPtr infoLog)
        {
            ((GetProgramInfoLogDelegate)Marshal.GetDelegateForFunctionPointer(wglGetProcAddress("glGetProgramInfoLog"), typeof(GetProgramInfoLogDelegate)))(program, maxLength, length, infoLog);
        }

        /// <inheritdoc />
        public void GetShaderiv(UInt32 program, UInt32 pname, IntPtr param)
        {
            ((GetProgramivDelegate)Marshal.GetDelegateForFunctionPointer(wglGetProcAddress("glGetShaderiv"), typeof(GetProgramivDelegate)))(program, pname, param);
        }

        /// <inheritdoc />
        public void GetShaderInfoLog(UInt32 program, Int32 maxLength, IntPtr length, IntPtr infoLog)
        {
            ((GetProgramInfoLogDelegate)Marshal.GetDelegateForFunctionPointer(wglGetProcAddress("glGetShaderInfoLog"), typeof(GetProgramInfoLogDelegate)))(program, maxLength, length, infoLog);
        }

        [UnmanagedFunctionPointer(CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        private delegate Int32 GetUniformLocationDelegate(UInt32 program, string name);

        /// <inheritdoc />
        public Int32 GetUniformLocation(UInt32 program, string name)
        {
            return ((GetUniformLocationDelegate)Marshal.GetDelegateForFunctionPointer(wglGetProcAddress("glGetUniformLocation"), typeof(GetUniformLocationDelegate)))(program, name);
        }

        private delegate void Uniform1fDelegate(Int32 location, float x);

        /// <inheritdoc />
        public void Uniform1f(Int32 location, float x)
        {
            ((Uniform1fDelegate)Marshal.GetDelegateForFunctionPointer(wglGetProcAddress("glUniform1f"), typeof(Uniform1fDelegate)))(location, x);
        }

        private delegate void Uniform2fDelegate(Int32 location, float x, float y);

        /// <inheritdoc />
        public void Uniform2f(Int32 location, float x, float y)
        {
            ((Uniform2fDelegate)Marshal.GetDelegateForFunctionPointer(wglGetProcAddress("glUniform2f"), typeof(Uniform2fDelegate)))(location, x, y);
        }

        private delegate void Uniform3fDelegate(Int32 location, float x, float y, float z);

        /// <inheritdoc />
        public void Uniform3f(Int32 location, float x, float y, float z)
        {
            ((Uniform3fDelegate)Marshal.GetDelegateForFunctionPointer(wglGetProcAddress("glUniform3f"), typeof(Uniform3fDelegate)))(location, x, y, z);
        }

        private delegate void Uniform4fDelegate(Int32 location, float x, float y, float z, float w);

        /// <inheritdoc />
        public void Uniform4f(Int32 location, float x, float y, float z, float w)
        {
            ((Uniform4fDelegate)Marshal.GetDelegateForFunctionPointer(wglGetProcAddress("glUniform4f"), typeof(Uniform4fDelegate)))(location, x, y, z, w);
        }

        private delegate void UniformMatrix4fvDelegate(Int32 location, Int32 count, bool transpose, IntPtr value);

        /// <inheritdoc />
        public unsafe void UniformMatrix4fv(Int32 location, Int32 count, bool transpose, IntPtr value)
        {
            ((UniformMatrix4fvDelegate)Marshal.GetDelegateForFunctionPointer(wglGetProcAddress("glUniformMatrix4fv"), typeof(UniformMatrix4fvDelegate)))(location, count, transpose, value);
        }
    }
}
