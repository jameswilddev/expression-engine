﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Platforms.OpenGL
{
    /// <summary>An interface exposing OpenGL 1.1 functionality.</summary>
    /// <remarks>Use the platform-specific <see cref="IGLExtensions"/> 
    /// interface to access functions from newer versions.</remarks>
    public interface IGL
    {
        /// <summary>Enables a vertex array channel.</summary>
        /// <param name="cap">The channel to enable.</param>
        void EnableClientState(UInt32 cap);

        /// <summary>Disables a vertex array channel.</summary>
        /// <param name="cap">The channel to enable.</param>
        void DisableClientState(UInt32 cap);

        /// <summary>Sets the memory location to read vertices from when 
        /// rendering vertex arrays.</summary>
        /// <param name="size">The number of dimensions.</param>
        /// <param name="type">The type of each component of each vector in 
        /// <paramref name="pointer"/>.</param>
        /// <param name="stride">The number of <see cref="Byte"/>s between each
        /// vector in <paramref name="pointer"/>.  Zero automatically uses
        /// <paramref name="size"/> * sizeof(<paramref name="type"/>).</param>
        /// <param name="pointer">The memory address to read vertex coordinates 
        /// from.</param>
        unsafe void VertexPointer(Int32 size, UInt32 type, Int32 stride, IntPtr pointer);

        /// <summary>Draws indexed geometry using vertex arrays previously set
        /// up by other calls.</summary>
        /// <param name="mode">The type of geometry to draw.</param>
        /// <param name="count">The number of indices to read.</param>
        /// <param name="type">The type indices are stored as.</param>
        /// <param name="indices">The memory address to read indices from.
        /// </param>
        unsafe void DrawElements(UInt32 mode, Int32 count, UInt32 type, IntPtr indices);

        /// <summary>Sets the area of the render context to render to, clipping
        /// rendered geometry and remapping normalized device coordinates.
        /// This does not affect clearing.</summary>
        /// <param name="x">The number of pixels between the left border of the
        /// render area and the left border of the context.</param>
        /// <param name="y">The number of pixels between the bottom border of 
        /// the render area and the bottom border of the context.</param>
        /// <param name="width">The width of the viewport in pixels.</param>
        /// <param name="height">The height of the viewport in pixels.</param>
        void Viewport(Int32 x, Int32 y, Int32 width, Int32 height);
    }

    /// <summary>Commonly used OpenGL constants.</summary>
    public static class GLConstants
    {
        /// <summary>Enables and disables reading vertex locations when drawing
        /// geometry from arrays.</summary>
        public const UInt32 GL_VERTEX_ARRAY = 32884;

        /// <summary>A datatype equivalent to <see cref="float"/>.</summary>
        public const UInt32 GL_FLOAT = 5126;

        /// <summary>A datatype equivalent to <see cref="byte"/>.</summary>
        public const UInt32 GL_UNSIGNED_BYTE = 5121;

        /// <summary>Draws triangles from each set of three indices.</summary>
        public const UInt32 GL_TRIANGLES = 4;

        /// <summary>A shader which is executed per-vertex.</summary>
        public const UInt32 GL_VERTEX_SHADER = 35633;

        /// <summary>A shader which is executed per-fragment.</summary>
        public const UInt32 GL_FRAGMENT_SHADER = 35632;

        /// <summary>Retrieves whether linking the shader succeeded.</summary>
        public const UInt32 GL_COMPILE_STATUS = 35713;

        /// <summary>Retrieves whether linking the program succeeded.</summary>
        public const UInt32 GL_LINK_STATUS = 35714;

        /// <summary>Retrieves the size of the compile/link log.</summary>
        public const UInt32 GL_INFO_LOG_LENGTH = 35716;
    }

    /// <inheritdoc />
    [Export(typeof(IGL))]
    public sealed class GL : IGL
    {
        /// <inheritdoc />
        public void EnableClientState(UInt32 cap)
        {
            GLInterop.glEnableClientState(cap);
        }

        /// <inheritdoc />
        public void DisableClientState(UInt32 cap)
        {
            GLInterop.glDisableClientState(cap);
        }

        /// <inheritdoc />
        public unsafe void VertexPointer(Int32 size, UInt32 type, Int32 stride, IntPtr pointer)
        {
            GLInterop.glVertexPointer(size, type, stride, pointer);
        }

        /// <inheritdoc />
        public unsafe void DrawElements(UInt32 mode, Int32 count, UInt32 type, IntPtr indices)
        {
            GLInterop.glDrawElements(mode, count, type, indices);
        }

        /// <inheritdoc />
        public void Viewport(Int32 x, Int32 y, Int32 width, Int32 height)
        {
            GLInterop.glViewport(x, y, width, height);
        }
    }

    internal static class GLInterop
    {
        [DllImport("opengl32.dll")]
        public static extern void glEnableClientState(UInt32 cap);

        [DllImport("opengl32.dll")]
        public static extern void glDisableClientState(UInt32 cap);

        [DllImport("opengl32.dll")]
        public static extern void glVertexPointer(Int32 size, UInt32 type, Int32 stride, IntPtr pointer);

        [DllImport("opengl32.dll")]
        public static extern void glDrawElements(UInt32 mode, Int32 count, UInt32 type, IntPtr indices);

        [DllImport("opengl32.dll")]
        public static extern void glViewport(Int32 x, Int32 y, Int32 width, Int32 height);
    }

    /// <summary>An interface exposing OpenGL functionality newer than version
    /// 1.1.</summary>
    /// <remarks>This is separate from <see cref="IGL"/> as on many platforms
    /// it is implemented in a platform-specific manner.</remarks>
    public interface IGLExtensions
    {
        /// <summary>Allocates a new shader ID.</summary>
        /// <param name="shaderType">The type of shader being created.</param>
        /// <returns>If successfully allocated, the ID of the shader, else,
        /// zero.</returns>
        UInt32 CreateShader(UInt32 shaderType);

        /// <summary>Deletes a previously allocated shader.</summary>
        /// <param name="shader">The shader to delete.</param>
        void DeleteShader(UInt32 shader);

        /// <summary>Sets the source code held by a shader.</summary>
        /// <param name="shader">The shader to set the source code of.</param>
        /// <param name="count">The number of source files.</param>
        /// <param name="sources">A list of the source files to concatenate
        /// and store against the shader.</param>
        /// <param name="lengths">A list of the lengths of the source files
        /// to concatenate and store against the shader; should match
        /// <paramref name="sources"/>.</param>
        void ShaderSource(UInt32 shader, Int32 count, string[] sources, int[] lengths);

        /// <summary>Compiles a shader.</summary>
        /// <param name="shader">The shader to compile.</param>
        void CompileShader(UInt32 shader);

        /// <summary>Allocates a new program ID.</summary>
        /// <returns>If successfully allocated, the ID of the program, else,
        /// zero.</returns>
        UInt32 CreateProgram();

        /// <summary>Deletes a previously allocated program.</summary>
        /// <param name="program">The program to delete.</param>
        void DeleteProgram(UInt32 program);

        /// <summary>Specifies that a shader is part of a program.</summary>
        /// <param name="program">The program to attach 
        /// <paramref name="shader"/> to.</param>
        /// <param name="shader">The shader to attach to 
        /// <paramref name="program"/>.</param>
        void AttachShader(UInt32 program, UInt32 shader);

        /// <summary>Removes the link between a shader and a program.</summary>
        /// <param name="program">The program to detach 
        /// <paramref name="shader"/> from.</param>
        /// <param name="shader">The shader to detach from
        /// <paramref name="program"/>.</param>
        void DetachShader(UInt32 program, UInt32 shader);

        /// <summary>Builds the specified program using the attached shaders.
        /// </summary>
        /// <param name="program">The program to build.</param>
        void LinkProgram(UInt32 program);

        /// <summary>Specifies which program to use to render geometry.
        /// </summary>
        /// <param name="program">The program to use; if zero, reverts to the
        /// fixed-function pipeline.</param>
        void UseProgram(UInt32 program);
        
        /// <summary>Retrieves a piece of information about a given program.
        /// </summary>
        /// <param name="program">The program to retrieve information about.
        /// </param>
        /// <param name="pname">The information to retrieve.</param>
        /// <param name="param">The pointer to memory to store the information 
        /// in.</param>
        unsafe void GetProgramiv(UInt32 program, UInt32 pname, IntPtr param);

        /// <summary>Retrieves the link log for a given program; useful for
        /// debugging failures to link.</summary>
        /// <param name="program">The program to retrieve the link log for.
        /// </param>
        /// <param name="maxLength">The maximum number of characters to copy
        /// to the address specified by <paramref name="infoLog"/>; this does 
        /// not include the null terminator.</param>
        /// <param name="length">A pointer to a memory location to store the
        /// number of bytes written to.</param>
        /// <param name="infoLog">A pointer to copy the log to.</param>
        unsafe void GetProgramInfoLog(UInt32 program, Int32 maxLength, IntPtr length, IntPtr infoLog);

        /// <summary>Retrieves a piece of information about a given shader.
        /// </summary>
        /// <param name="shader">The shader to retrieve information about.
        /// </param>
        /// <param name="pname">The information to retrieve.</param>
        /// <param name="param">The pointer to memory to store the information 
        /// in.</param>
        unsafe void GetShaderiv(UInt32 shader, UInt32 pname, IntPtr param);

        /// <summary>Retrieves the compile log for a given shader; useful for
        /// debugging failures to compile.</summary>
        /// <param name="shader">The shader to retrieve the compile log for.
        /// </param>
        /// <param name="maxLength">The maximum number of characters to copy
        /// to the address specified by <paramref name="infoLog"/>; this does 
        /// not include the null terminator.</param>
        /// <param name="length">A pointer to a memory location to store the
        /// number of bytes written to.</param>
        /// <param name="infoLog">A pointer to copy the log to.</param>
        unsafe void GetShaderInfoLog(UInt32 shader, Int32 maxLength, IntPtr length, IntPtr infoLog);

        /// <summary>Retrieves the location of the specified uniform in the
        /// given program.</summary>
        /// <param name="program">The program to find the location of
        /// <paramref name="name"/> in.</param>
        /// <param name="name">The uniform to find the location of in
        /// <paramref name="program"/>.</param>
        /// <returns>The location of the uniform specified by 
        /// <paramref name="name"/> in <paramref name="program"/>.  If no match
        /// is found, -1 is returned.</returns>
        Int32 GetUniformLocation(UInt32 program, [MarshalAs(UnmanagedType.AnsiBStr)] string name);

        /// <summary>Sets a one-dimensional floating point uniform in the
        /// current program to the given value.</summary>
        /// <param name="location">The location of the uniform in the current
        /// program to set to the value specified by <paramref name="x"/>.
        /// </param>
        /// <param name="x">The value to set the uniform specified by 
        /// <paramref name="location"/> to in the current program.</param>
        void Uniform1f(Int32 location, float x);

        /// <summary>Sets a two-dimensional floating point uniform in the
        /// current program to the given value.</summary>
        /// <param name="location">The location of the uniform in the current
        /// program to set to the value specified by <paramref name="x"/> and
        /// <paramref name="y"/>.
        /// </param>
        /// <param name="x">The value to set the first dimension of the uniform
        /// specified by <paramref name="location"/> to in the current program.
        /// </param>
        /// <param name="y">The value to set the second dimension of the 
        /// uniform specified by <paramref name="location"/> to in the current 
        /// program.</param>
        void Uniform2f(Int32 location, float x, float y);

        /// <summary>Sets a three-dimensional floating point uniform in the
        /// current program to the given value.</summary>
        /// <param name="location">The location of the uniform in the current
        /// program to set to the value specified by <paramref name="x"/>,
        /// <paramref name="y"/> and <paramref name="z"/>.
        /// </param>
        /// <param name="x">The value to set the first dimension of the uniform
        /// specified by <paramref name="location"/> to in the current program.
        /// </param>
        /// <param name="y">The value to set the second dimension of the 
        /// uniform specified by <paramref name="location"/> to in the current 
        /// program.</param>
        /// <param name="z">The value to set the third dimension of the 
        /// uniform specified by <paramref name="location"/> to in the current 
        /// program.</param>
        void Uniform3f(Int32 location, float x, float y, float z);

        /// <summary>Sets a four-dimensional floating point uniform in the
        /// current program to the given value.</summary>
        /// <param name="location">The location of the uniform in the current
        /// program to set to the value specified by <paramref name="x"/>,
        /// <paramref name="y"/>, <paramref name="y"/> and 
        /// <paramref name="z"/>.</param>
        /// <param name="x">The value to set the first dimension of the uniform
        /// specified by <paramref name="location"/> to in the current program.
        /// </param>
        /// <param name="y">The value to set the second dimension of the 
        /// uniform specified by <paramref name="location"/> to in the current 
        /// program.</param>
        /// <param name="z">The value to set the third dimension of the 
        /// uniform specified by <paramref name="location"/> to in the current 
        /// program.</param>
        /// <param name="w">The value to set the fourth dimension of the 
        /// uniform specified by <paramref name="location"/> to in the current 
        /// program.</param>
        void Uniform4f(Int32 location, float x, float y, float z, float w);

        /// <summary>Sets one or more 4x4 matrix uniforms in the current 
        /// program to the given value.</summary>
        /// <param name="location">The location of the uniform in the current
        /// program to set to the value specified by <paramref name="value"/>.
        /// </param>
        /// <param name="count">The number of matrices to copy from
        /// <paramref name="value"/> to the uniform being updated.</param>
        /// <param name="transpose">When <see langword="true"/>, the matrices
        /// in <paramref name="value"/> are transposed on the way to the
        /// target uniform.</param>
        /// <param name="value">The matrices to copy to the uniform specified
        /// by <paramref name="location"/> in the current program.</param>
        unsafe void UniformMatrix4fv(Int32 location, Int32 count, [MarshalAs(UnmanagedType.U1)] bool transpose, IntPtr value);
    }
}
