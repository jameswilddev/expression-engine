﻿using ExpressionEngine.Compilers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Linq.Expressions;
using System.Numerics;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Platforms.OpenGL
{
	/// <summary>Compiles expression trees to GLSL function bodies.</summary>
	public interface IFunctionCompiler
	{
		/// <summary>Compiles a given expression tree to a GLSL function body.
		/// </summary>
		/// <remarks>Only a limited subset of C# expression tree nodes are
		/// supported.</remarks>
		/// <typeparam name="T">The delegate type the expression to compile
		/// implements.</typeparam>
		/// <param name="source">The expression tree to compile to a GLSL
		/// function body.</param>
		/// <returns>A GLSL function body built from <paramref name="source"/>.
		/// </returns>
		/// <exception cref="ArgumentNullException">Thrown when 
		/// <paramref name="source"/> is <see langword="null"/>.</exception>
		/// <exception cref="ArgumentOutOfRangeException">Thrown when 
		/// <typeparamref name="T"/> does not take at least one parameter.
		/// </exception>
		/// <exception cref="ArgumentOutOfRangeException">Thrown when 
		/// <typeparamref name="T"/>'s first parameter is not a 
		/// <see cref="Vector2"/>.</exception>
		string Compile<T>(Expression<T> source);
	}

    /// <inheritdoc />
    [Export(typeof(IFunctionCompiler))]
    public sealed class FunctionCompiler : IFunctionCompiler
    {
        // Ensures that a type can be supported by GLSL; it must be a value type with at least one instance field.
        private static void CheckType(Type type)
        {
            if (!type.IsValueType) throw new NotSupportedException("Reference types are not supported");
            if (type.IsPrimitive)
			{
				if (!new[] { typeof(bool), typeof(int), typeof(float) }.Contains(type))
					throw new NotImplementedException(string.Format("The primitive type \"{0}\" is currently unimplemented", type));
			}
			else
            {
                if (!type.GetFields().Any(f => !f.IsStatic)) throw new NotSupportedException("Empty custom structs are not supported");
                foreach (var field in type.GetFields().Where(f => !f.IsStatic))
                    CheckType(field.FieldType);
                if (!type.IsLayoutSequential)
                    throw new NotSupportedException("Custom structs with layout types other than sequential are not supported");
            }
        }

        // Determines the name of the given type in GLSL.
        // For custom structs, a name will be generated from the full CLR type name.
        internal static String GetTypeName(Type type)
        {
            if (type == typeof(bool)) return "bool";
            if (type == typeof(float)) return "float";
            if (type == typeof(int)) return "int";
            if (type == typeof(Vector2)) return "vec2";
            if (type == typeof(Vector3)) return "vec3";
            if (type == typeof(Vector4)) return "vec4";
            if (type == typeof(Matrix4x4)) return "mat4";
            CheckType(type);
            if (type.IsGenericType)
            {
                var name = type.GetGenericTypeDefinition().FullName.Replace('.', '_').Replace('+', '_');
                name = name.Remove(name.IndexOf('`'));
                return String.Format("customStruct_{0}__{1}", name, String.Join("__", type.GenericTypeArguments.Select(GetTypeName)));
            }
            return String.Format("customStruct_{0}", type.FullName.Replace('.', '_').Replace('+', '_'));
        }

        // A container for the state held when building the GLSL; retains the source code so far
        // and aliases for the expressions interpreted so far.
        private sealed class Runtime
        {
            public readonly StringBuilder StringBuilder = new StringBuilder();

            public struct Mapping
            {
                public string Alias;
                public string Generated;
            }

			public Dictionary<Expression, Expression> Aliases = new Dictionary<Expression, Expression>();
            public Dictionary<Expression, Mapping> NameMap = new Dictionary<Expression, Mapping> { };
            public int TempId = 0;
            public String NewName(Type type, String content)
            {
                var match = NameMap.SingleOrDefault(nm => nm.Value.Generated == content);
                if (match.Key != null) return match.Value.Alias;
                var name = String.Format("temp_{0}", TempId++);
                StringBuilder.AppendFormat("{0} {1} = {2};", GetTypeName(type), name, content);
                StringBuilder.AppendLine();
                return name;
            }
            public String NewName(Expression expression, String content)
            {
                var match = NameMap.SingleOrDefault(nm => nm.Value.Generated == content);
                if (match.Key != null) return match.Value.Alias;
                var name = NewName(expression.Type, content);
                NameMap[expression] = new Mapping
                {
                    Alias = name,
                    Generated = content
                };
                return name;
            }
        }

        private static String CompileBinary(String symbol, Expression source, Runtime runtime)
        {
            var binary = source as BinaryExpression;
            return runtime.NewName(source, String.Format(
                "{0} {1} {2}",
                Compile(binary.Left, runtime),
                symbol,
                Compile(binary.Right, runtime)
            ));
        }

        private static String CompileUnary(String symbol, Expression source, Runtime runtime)
        {
            var unary = source as UnaryExpression;
            return runtime.NewName(source, String.Format(
                "{0}{1}",
                symbol,
                Compile(unary.Operand, runtime)
            ));
        }

        private static String CompileConstant(Object value)
        {
            if (value == null) throw new NotSupportedException("Reference types are not supported");
            if (value.GetType() == typeof(int)) return value.ToString();
            if (value.GetType() == typeof(float)) return ((float)value).ToString("#########0.0#######");
            if (value.GetType() == typeof(bool)) return value.ToString().ToLowerInvariant();
            if (value.GetType() == typeof(Vector2)) return "vec2(0.0)";
            if (value.GetType() == typeof(Vector3)) return "vec3(0.0)";
            if (value.GetType() == typeof(Vector4)) return "vec4(0.0)";
            if (value.GetType() == typeof(Vector4)) return "mat4(0.0)";
            CheckType(value.GetType());
            if (!value.Equals(Activator.CreateInstance(value.GetType()))) throw new NotImplementedException(String.Format("Constant expressions for type \"{0}\" are currently unimplemented", value.GetType()));
            return MemberInit(value.GetType(), Enumerable.Empty<MemberBinding>(), null);
        }

        // If these members are read, they are replaced with GLSL equivalents.
        private static readonly ReadOnlyDictionary<MemberInfo, String> MemberOverrides = new ReadOnlyDictionary<MemberInfo, String>
            (new Dictionary<MemberInfo, String>
        {
            {typeof(Vector2).GetField("X"), ".x"},
            {typeof(Vector2).GetField("Y"), ".y"},
            {typeof(Vector3).GetField("X"), ".x"},
            {typeof(Vector3).GetField("Y"), ".y"},
            {typeof(Vector3).GetField("Z"), ".z"},
            {typeof(Vector4).GetField("X"), ".x"},
            {typeof(Vector4).GetField("Y"), ".y"},
            {typeof(Vector4).GetField("Z"), ".z"},
            {typeof(Vector4).GetField("W"), ".w"},
            // These are intentionally the wrong way around, as GLSL uses
            // column-major matrices, and System.Numerics.Vectors uses
            // row-major matrices.
            {typeof(Matrix4x4).GetField("M11"), "[0][0]"},
            {typeof(Matrix4x4).GetField("M21"), "[0][1]"},
            {typeof(Matrix4x4).GetField("M31"), "[0][2]"},
            {typeof(Matrix4x4).GetField("M41"), "[0][3]"},
            {typeof(Matrix4x4).GetField("M12"), "[1][0]"},
            {typeof(Matrix4x4).GetField("M22"), "[1][1]"},
            {typeof(Matrix4x4).GetField("M32"), "[1][2]"},
            {typeof(Matrix4x4).GetField("M42"), "[1][3]"},
            {typeof(Matrix4x4).GetField("M13"), "[2][0]"},
            {typeof(Matrix4x4).GetField("M23"), "[2][1]"},
            {typeof(Matrix4x4).GetField("M33"), "[2][2]"},
            {typeof(Matrix4x4).GetField("M43"), "[2][3]"},
            {typeof(Matrix4x4).GetField("M14"), "[3][0]"},
            {typeof(Matrix4x4).GetField("M24"), "[3][1]"},
            {typeof(Matrix4x4).GetField("M34"), "[3][2]"},
            {typeof(Matrix4x4).GetField("M44"), "[3][3]"}
        });

        /// <summary>The method calls we directly support.  The GLSL generated includes the value as the function name, the arguments to the method, and if the method is not static, the object instance as the first argument.</summary>
        private static readonly ReadOnlyDictionary<MethodInfo, String> SupportedFunctions = new ReadOnlyDictionary<MethodInfo, String>
            (new Dictionary<MethodInfo, String>
		{
			//{typeof(Vector2).GetMethod("Length"), "length"},
			{typeof(Math).GetMethod("Max", new[]{typeof(float), typeof(float)}), "max"},
			{typeof(Math).GetMethod("Min", new[]{typeof(float), typeof(float)}), "min"},
			{typeof(Math).GetMethod("Abs", new[]{typeof(float)}), "abs"},
			{typeof(Math).GetMethod("Max", new[]{typeof(int), typeof(int)}), "max"},
			{typeof(Math).GetMethod("Min", new[]{typeof(int), typeof(int)}), "min"},
			{typeof(Math).GetMethod("Abs", new[]{typeof(int)}), "abs"},
			{typeof(FloatExtensions).GetMethod("Pow", new[]{typeof(float), typeof(float)}), "pow"},
			{typeof(FloatExtensions).GetMethod("Mod", new[]{typeof(float), typeof(float)}), "mod"},
			{typeof(FloatExtensions).GetMethod("Sqrt", new[]{typeof(float)}), "sqrt"},
			{typeof(FloatExtensions).GetMethod("InverseSqrt", new[]{typeof(float)}), "inversesqrt"},
			{typeof(FloatExtensions).GetMethod("Fract", new[]{typeof(float)}), "fract"},
			{typeof(FloatExtensions).GetMethod("Floor", new[]{typeof(float)}), "floor"},
			{typeof(FloatExtensions).GetMethod("Ceil", new[]{typeof(float)}), "ceil"},
			{typeof(FloatExtensions).GetMethod("Sin", new[]{typeof(float)}), "sin"},
			{typeof(FloatExtensions).GetMethod("Cos", new[]{typeof(float)}), "cos"},
			{typeof(FloatExtensions).GetMethod("Clamp", new[]{typeof(float), typeof(float), typeof(float)}), "clamp"},
			{typeof(FloatExtensions).GetMethod("Mix", new[]{typeof(float), typeof(float), typeof(float)}), "mix"},
			{typeof(Vector2).GetMethod("Length"), "length"},
			{typeof(Vector3).GetMethod("Length"), "length"},
			{typeof(Vector4).GetMethod("Length"), "length"},
			{typeof(Vector2).GetMethod("Distance"), "distance"},
			{typeof(Vector3).GetMethod("Distance"), "distance"},
			{typeof(Vector4).GetMethod("Distance"), "distance"},
			{typeof(Vector2).GetMethod("Abs"), "abs"},
			{typeof(Vector3).GetMethod("Abs"), "abs"},
			{typeof(Vector4).GetMethod("Abs"), "abs"},
			{typeof(Vector2).GetMethod("Min"), "min"},
			{typeof(Vector3).GetMethod("Min"), "min"},
			{typeof(Vector4).GetMethod("Min"), "min"},
			{typeof(Vector2).GetMethod("Max"), "max"},
			{typeof(Vector3).GetMethod("Max"), "max"},
			{typeof(Vector4).GetMethod("Max"), "max"},
			{typeof(Vector2Extensions).GetMethod("Mix"), "mix"},
			{typeof(Vector3Extensions).GetMethod("Mix"), "mix"},
			{typeof(Vector4Extensions).GetMethod("Mix"), "mix"},
			{typeof(Vector2).GetMethod("Lerp"), "mix"},
			{typeof(Vector3).GetMethod("Lerp"), "mix"},
			{typeof(Vector4).GetMethod("Lerp"), "mix"},
			{typeof(Vector2).GetMethod("Normalize"), "normalize"},
			{typeof(Vector3).GetMethod("Normalize"), "normalize"},
			{typeof(Vector4).GetMethod("Normalize"), "normalize"},
			{typeof(Vector2).GetMethod("Reflect"), "reflect"},
			{typeof(Vector3).GetMethod("Reflect"), "reflect"},
			{typeof(Vector4Extensions).GetMethod("Reflect"), "reflect"},
            {typeof(Vector3).GetMethod("Cross"), "cross"},
			{typeof(Vector2Extensions).GetMethod("Clamp", new[]{typeof(Vector2), typeof(Vector2), typeof(Vector2)}), "clamp"},
			{typeof(Vector3Extensions).GetMethod("Clamp", new[]{typeof(Vector3), typeof(Vector3), typeof(Vector3)}), "clamp"},
			{typeof(Vector4Extensions).GetMethod("Clamp", new[]{typeof(Vector4), typeof(Vector4), typeof(Vector4)}), "clamp"},
			{typeof(Vector2Extensions).GetMethod("Clamp", new[]{typeof(Vector2), typeof(float), typeof(float)}), "clamp"},
			{typeof(Vector3Extensions).GetMethod("Clamp", new[]{typeof(Vector3), typeof(float), typeof(float)}), "clamp"},
			{typeof(Vector4Extensions).GetMethod("Clamp", new[]{typeof(Vector4), typeof(float), typeof(float)}), "clamp"},
			{typeof(Vector2Extensions).GetMethod("Fract"), "fract"},
			{typeof(Vector3Extensions).GetMethod("Fract"), "fract"},
			{typeof(Vector4Extensions).GetMethod("Fract"), "fract"},
			{typeof(Vector2Extensions).GetMethod("Ceil"), "ceil"},
			{typeof(Vector3Extensions).GetMethod("Ceil"), "ceil"},
			{typeof(Vector4Extensions).GetMethod("Ceil"), "ceil"},
			{typeof(Vector2Extensions).GetMethod("Floor"), "floor"},
			{typeof(Vector3Extensions).GetMethod("Floor"), "floor"},
			{typeof(Vector4Extensions).GetMethod("Floor"), "floor"},
			{typeof(Vector2).GetMethod("Dot"), "dot"},
			{typeof(Vector3).GetMethod("Dot"), "dot"},
			{typeof(Vector4).GetMethod("Dot"), "dot"},
			{typeof(Vector2Extensions).GetMethod("Mod", new[]{typeof(Vector2), typeof(Vector2)}), "mod"},
			{typeof(Vector3Extensions).GetMethod("Mod", new[]{typeof(Vector3), typeof(Vector3)}), "mod"},
			{typeof(Vector4Extensions).GetMethod("Mod", new[]{typeof(Vector4), typeof(Vector4)}), "mod"},
			{typeof(Vector2Extensions).GetMethod("Mod", new[]{typeof(Vector2), typeof(float)}), "mod"},
			{typeof(Vector3Extensions).GetMethod("Mod", new[]{typeof(Vector3), typeof(float)}), "mod"},
			{typeof(Vector4Extensions).GetMethod("Mod", new[]{typeof(Vector4), typeof(float)}), "mod"},
			{typeof(Vector2Extensions).GetMethod("Sin"), "sin"},
			{typeof(Vector3Extensions).GetMethod("Sin"), "sin"},
			{typeof(Vector4Extensions).GetMethod("Sin"), "sin"},
			{typeof(Vector2Extensions).GetMethod("Cos"), "cos"},
			{typeof(Vector3Extensions).GetMethod("Cos"), "cos"},
			{typeof(Vector4Extensions).GetMethod("Cos"), "cos"},
			{typeof(Vector2Extensions).GetMethod("Pow"), "pow"},
			{typeof(Vector3Extensions).GetMethod("Pow"), "pow"},
			{typeof(Vector4Extensions).GetMethod("Pow"), "pow"},
			{typeof(Vector2Extensions).GetMethod("InverseSqrt"), "inversesqrt"},
			{typeof(Vector3Extensions).GetMethod("InverseSqrt"), "inversesqrt"},
			{typeof(Vector4Extensions).GetMethod("InverseSqrt"), "inversesqrt"},
			{typeof(Vector2).GetMethod("SquareRoot"), "sqrt"},
			{typeof(Vector3).GetMethod("SquareRoot"), "sqrt"},
			{typeof(Vector4).GetMethod("SquareRoot"), "sqrt"},
            {typeof(IntExtensions).GetMethod("Clamp"), "clamp"},
            {typeof(Matrix4x4).GetMethod("Transpose"), "transpose"},
            {typeof(Vector2Extensions).GetMethod("Extend", new[]{typeof(Vector2), typeof(float)}), "vec3"},
            {typeof(Vector2Extensions).GetMethod("Extend", new[]{typeof(Vector2), typeof(float), typeof(float)}), "vec4"},
            {typeof(Vector3Extensions).GetMethod("Extend", new[]{typeof(Vector3), typeof(float)}), "vec4"}
		});

        /// <summary>Types which may be used with the standard arithmetic operators such as +/-/*//.</summary>
        private static ReadOnlyCollection<KeyValuePair<Type, Type>> ArithmeticTypes = new ReadOnlyCollection<KeyValuePair<Type, Type>>
        (
            new List<KeyValuePair<Type, Type>>
			{
				new KeyValuePair<Type, Type>(typeof(float), typeof(float)),
				new KeyValuePair<Type, Type>(typeof(int), typeof(int)),
				new KeyValuePair<Type, Type>(typeof(Vector2), typeof(float)),
				new KeyValuePair<Type, Type>(typeof(Vector3), typeof(float)),
				new KeyValuePair<Type, Type>(typeof(Vector4), typeof(float)),
				new KeyValuePair<Type, Type>(typeof(Vector2), typeof(Vector2)),
				new KeyValuePair<Type, Type>(typeof(Vector3), typeof(Vector3)),
				new KeyValuePair<Type, Type>(typeof(Vector4), typeof(Vector4))
			}
        );

        /// <summary>Types which may be compared using &lt;/&gt;.</summary>
        private static ReadOnlyCollection<KeyValuePair<Type, Type>> ComparableTypes = new ReadOnlyCollection<KeyValuePair<Type, Type>>
        (
            new List<KeyValuePair<Type, Type>>
			{
				new KeyValuePair<Type, Type>(typeof(float), typeof(float)),
				new KeyValuePair<Type, Type>(typeof(int), typeof(int))
			}
        );

        // Types which may be cast using a struct initializer.
        // Key = from, Value = to.
        // to( <a value of the from type> )
        private static ReadOnlyCollection<KeyValuePair<Type, Type>> CastableTypes = new ReadOnlyCollection<KeyValuePair<Type, Type>>
        (
            new List<KeyValuePair<Type, Type>>
			{
				new KeyValuePair<Type, Type>(typeof(int), typeof(float)),
				new KeyValuePair<Type, Type>(typeof(float), typeof(int))
			}
        );

        // Given a type and a list of type bindings, builds a struct initializer from it.
        private static String MemberInit(Type type, IEnumerable<MemberBinding> bindings, Runtime runtime)
        {
            var builder = new StringBuilder();
            CheckType(type);
            builder.Append(GetTypeName(type));
            builder.Append('(');

            if (bindings.Any(b => b.Member.MemberType != MemberTypes.Field))
                throw new NotSupportedException("Initializing members other than fields in a member initializer is not supported");

            var fields = type
                .GetFields()
                .Where(f => !f.IsStatic)
                // Sort by location in memory.  This is necessary because GetFields() doesn't return the fields
                // in any specific order, we need a consistent order for GLSL's constructor syntax, and
                // we can't use name as vec4 in particular uses XYZW which would sort as WXYZ.
                .OrderBy(p => Marshal.OffsetOf(type, p.Name).ToInt32());

            // Special case: we need to change the order for matrices as we are mapping from row-major to
            // column-major.
            if (type == typeof(Matrix4x4))
                fields = fields.OrderBy(p => p.Name[2]).ThenBy(p => p.Name[1]);

            foreach (var field in fields)
            {
                var binding = bindings.SingleOrDefault(b => b.Member == field);
                if (binding == null)
                    builder.Append(CompileConstant(Activator.CreateInstance(field.FieldType)));
                else
                {
                    switch (binding.BindingType)
                    {
                        case MemberBindingType.Assignment:
                            builder.Append(Compile((binding as MemberAssignment).Expression, runtime));
                            break;

                        case MemberBindingType.MemberBinding:
                            var memberBinding = binding as MemberMemberBinding;
                            builder.Append(runtime.NewName(field.FieldType, MemberInit(field.FieldType, memberBinding.Bindings, runtime)));
                            break;

                        default:
                            throw new NotImplementedException(String.Format("Member initialization bindings of type \"{0}\" are currently unimplemented", binding.BindingType));
                    }
                }
                if (field != fields.Last()) builder.Append(", ");
            }
            builder.Append(')');
            return builder.ToString();
        }

        private static void CheckEquatable(Expression source)
        {
            var binaryExpression = source as BinaryExpression;
            var left = binaryExpression.Left.Type;
            var right = binaryExpression.Right.Type;
            if (left == right) return;
            throw new NotSupportedException(String.Format("Equality comparisons between types \"{0}\" and \"{1}\" are not supported", left, right));
        }

        private static void CheckComparable(Expression source)
        {
            var binaryExpression = source as BinaryExpression;
            var left = binaryExpression.Left.Type;
            var right = binaryExpression.Right.Type;
            if (ComparableTypes.Any(at => at.Key == left && at.Value == right)) return;
            if (ComparableTypes.Any(at => at.Key == right && at.Value == left)) return;
            throw new NotSupportedException(String.Format("Comparisons between types \"{0}\" and \"{1}\" are not supported", left, right));
        }

        private static void CheckArithmetic(Expression source)
        {
            var binaryExpression = source as BinaryExpression;
            var left = binaryExpression.Left.Type;
            var right = binaryExpression.Right.Type;

            // Special exception - * is allowed between Matrix4x4 instances
            // so you can collapse matrices together, but no other operation.
            if (left == typeof(Matrix4x4) && right == typeof(Matrix4x4) && source.NodeType == ExpressionType.Multiply) return;

            if (ArithmeticTypes.Any(at => at.Key == left && at.Value == right)) return;
            if (ArithmeticTypes.Any(at => at.Key == right && at.Value == left)) return;
            throw new NotSupportedException(String.Format("Arithmetic operations between types \"{0}\" and \"{1}\" are not supported", left, right));
        }

        private static String Compile(Expression source, Runtime runtime)
        {
			if(runtime.Aliases.ContainsKey(source)) return Compile(runtime.Aliases[source], runtime);
            if (runtime.NameMap.ContainsKey(source)) return runtime.NameMap[source].Alias;
            switch (source.NodeType)
            {
                case ExpressionType.MemberInit:
                    return runtime.NewName(source, MemberInit(source.Type, (source as MemberInitExpression).Bindings, runtime));

                case ExpressionType.Add:
                    CheckArithmetic(source);
                    return CompileBinary("+", source, runtime);
                case ExpressionType.Subtract:
                    CheckArithmetic(source);
                    return CompileBinary("-", source, runtime);
                case ExpressionType.Multiply:
                    CheckArithmetic(source);
                    return CompileBinary("*", source, runtime);
                case ExpressionType.Divide:
                    CheckArithmetic(source);
                    return CompileBinary("/", source, runtime);
                case ExpressionType.AndAlso:
                    return CompileBinary("&&", source, runtime);
                case ExpressionType.OrElse:
                    return CompileBinary("||", source, runtime);
                case ExpressionType.Equal:
                    CheckEquatable(source);
                    return CompileBinary("==", source, runtime);
                case ExpressionType.NotEqual:
                    CheckEquatable(source);
                    return CompileBinary("!=", source, runtime);
                case ExpressionType.LessThan:
                    CheckComparable(source);
                    return CompileBinary("<", source, runtime);
                case ExpressionType.LessThanOrEqual:
                    CheckComparable(source);
                    return CompileBinary("<=", source, runtime);
                case ExpressionType.GreaterThan:
                    CheckComparable(source);
                    return CompileBinary(">", source, runtime);
                case ExpressionType.GreaterThanOrEqual:
                    CheckComparable(source);
                    return CompileBinary(">=", source, runtime);

                case ExpressionType.Not:
                    if (source.Type != typeof(bool))
                        throw new NotImplementedException(String.Format("Not expressions for type \"{0}\" are currently unimplemented", source.Type));
                    return CompileUnary("!", source, runtime);
                case ExpressionType.Negate:
                    if (!ArithmeticTypes.Any(at => at.Key == source.Type && at.Value == source.Type))
                        throw new NotImplementedException(String.Format("Negate expressions for type \"{0}\" are currently unimplemented", source.Type));
                    return CompileUnary("-", source, runtime);
                case ExpressionType.Convert:
                    var conversion = source as UnaryExpression;
                    if (!CastableTypes.Any(c => c.Key == conversion.Operand.Type && c.Value == conversion.Type))
                        throw new NotImplementedException(String.Format("Conversions from type \"{0}\" to type \"{1}\" are currently unimplemented", conversion.Operand.Type, conversion.Type));
                    return runtime.NewName(source, String.Format("{0}({1})", GetTypeName(source.Type), Compile(conversion.Operand, runtime)));

                case ExpressionType.Constant:
                    return CompileConstant((source as ConstantExpression).Value);

                case ExpressionType.Call:
                    var invoke = source as MethodCallExpression;

					if(invoke.Method.IsGenericMethod && invoke.Method.GetGenericMethodDefinition() == typeof(Injector).GetMethod("Invoke"))
					{
						var invocationUnary = invoke.Arguments[1] as UnaryExpression;
						if (invocationUnary == null) throw new ArgumentNullException("source", "Cannot invoke using null invocations");
						var invocationLambda = invocationUnary.Operand as LambdaExpression;
						if (invocationLambda == null) throw new ArgumentNullException("source", "Cannot invoke using null invocations");
						var invocation = invocationLambda.Body as InvocationExpression;
						if (invocation == null) throw new ArgumentOutOfRangeException("source", "Invocations must invoke the expression parameter");

						var expressionMember = invoke.Arguments[0] as MemberExpression;
						if (expressionMember == null) throw new ArgumentNullException("source", "Cannot invoke null expressions");
						var expressionConstant = expressionMember.Expression as ConstantExpression;
						if (expressionConstant == null) throw new ArgumentNullException("source", "Cannot invoke null expressions");
						var expressionField = expressionMember.Member as FieldInfo;
						if (expressionField == null) throw new ArgumentNullException("source", "Cannot invoke null expressions");
						var expression = expressionField.GetValue(expressionConstant.Value) as LambdaExpression;
						if (expression == null) throw new ArgumentNullException("source", "Cannot invoke null expressions");

						for (int i = 0; i < invocation.Arguments.Count; i++)
							runtime.Aliases[expression.Parameters[i]] = invocation.Arguments[i];

						return Compile(expression.Body, runtime);
					}

                    if(invoke.Method.IsGenericMethod && invoke.Method.GetGenericMethodDefinition() == typeof(Iterator).GetMethod("Iterate"))
                    {
						// Always generate a new name as we're going to mutate this variable.
						var initial = Compile(invoke.Arguments[0], runtime);
						var monad = string.Format("temp_{0}", runtime.TempId++);
						runtime.StringBuilder.AppendLine(string.Format("{0} {1} = {2};", GetTypeName(invoke.Arguments[0].Type), monad, initial));

                        var iterationsConstant = invoke.Arguments[2] as ConstantExpression;
						if (((int)iterationsConstant.Value) < 1) throw new ArgumentOutOfRangeException("source", "Iterators' iteration counts cannot be less than one");
                        runtime.StringBuilder.AppendLine(string.Format("for(int temp_{0} = 0; temp_{0} < {1}; temp_{0}++) {{", runtime.TempId++, iterationsConstant.Value));

						// At present, we create a separate runtime instance for the body of the loop and copy the generated code/latest temp variable name back to the parent runtime.
						// This prevents any temporary variables calculated inside the loop being used outside the loop.
						// In future, it would be much better to instead isolate parts of the calculation which don't rely on the monad, and move them to before the loop.
						// This would require a big restructure however.
                        var scope = new Runtime { NameMap = new Dictionary<Expression, Runtime.Mapping>(runtime.NameMap), TempId = runtime.TempId };
                        var transformUnary = invoke.Arguments[1] as UnaryExpression;
						if (transformUnary == null) throw new ArgumentNullException("source", "Iterator transforms cannot be null");
                        var transformLambda = transformUnary.Operand as LambdaExpression;
						if (transformLambda == null) throw new ArgumentNullException("source", "Iterator transforms cannot be null");

                        scope.NameMap[transformLambda.Parameters[0]] = new Runtime.Mapping { Alias = monad };
                        var scopeOutput = Compile(transformLambda.Body, scope);
                        runtime.StringBuilder.Append(scope.StringBuilder.ToString());
						runtime.TempId = scope.TempId;
                        runtime.StringBuilder.AppendLine(string.Format("{0} = {1};", monad, scopeOutput));

                        runtime.StringBuilder.AppendLine("}");

                        return monad;
                    }

					if (invoke.Method.IsGenericMethod && invoke.Method.GetGenericMethodDefinition() == typeof(Injector).GetMethod("Let"))
					{
						var transformUnary = invoke.Arguments[1] as UnaryExpression;
						if (transformUnary == null) throw new ArgumentNullException("source", "Let usages cannot be null");
						var transformLambda = transformUnary.Operand as LambdaExpression;
						if (transformLambda == null) throw new ArgumentNullException("source", "Let usages cannot be null");

						var let = Compile(invoke.Arguments[0], runtime);
						runtime.NameMap[transformLambda.Parameters[0]] = new Runtime.Mapping { Alias = let };

						return Compile(transformLambda.Body, runtime);
					}


                    // Swizzles are compiled inline.
                    if (invoke.Method == typeof(Vector2Extensions).GetMethod("Swizzle", new[] { typeof(Vector2), typeof(Expression<Func<Vector2, float>>), typeof(Expression<Func<Vector2, float>>) })
                        || invoke.Method == typeof(Vector2Extensions).GetMethod("Swizzle", new[] { typeof(Vector2), typeof(Expression<Func<Vector2, float>>), typeof(Expression<Func<Vector2, float>>), typeof(Expression<Func<Vector2, float>>) })
                        || invoke.Method == typeof(Vector2Extensions).GetMethod("Swizzle", new[] { typeof(Vector2), typeof(Expression<Func<Vector2, float>>), typeof(Expression<Func<Vector2, float>>), typeof(Expression<Func<Vector2, float>>), typeof(Expression<Func<Vector2, float>>) })
                        || invoke.Method == typeof(Vector3Extensions).GetMethod("Swizzle", new[] { typeof(Vector3), typeof(Expression<Func<Vector3, float>>), typeof(Expression<Func<Vector3, float>>) })
                        || invoke.Method == typeof(Vector3Extensions).GetMethod("Swizzle", new[] { typeof(Vector3), typeof(Expression<Func<Vector3, float>>), typeof(Expression<Func<Vector3, float>>), typeof(Expression<Func<Vector3, float>>) })
                        || invoke.Method == typeof(Vector3Extensions).GetMethod("Swizzle", new[] { typeof(Vector3), typeof(Expression<Func<Vector3, float>>), typeof(Expression<Func<Vector3, float>>), typeof(Expression<Func<Vector3, float>>), typeof(Expression<Func<Vector3, float>>) })
                        || invoke.Method == typeof(Vector4Extensions).GetMethod("Swizzle", new[] { typeof(Vector4), typeof(Expression<Func<Vector4, float>>), typeof(Expression<Func<Vector4, float>>) })
                        || invoke.Method == typeof(Vector4Extensions).GetMethod("Swizzle", new[] { typeof(Vector4), typeof(Expression<Func<Vector4, float>>), typeof(Expression<Func<Vector4, float>>), typeof(Expression<Func<Vector4, float>>) })
                        || invoke.Method == typeof(Vector4Extensions).GetMethod("Swizzle", new[] { typeof(Vector4), typeof(Expression<Func<Vector4, float>>), typeof(Expression<Func<Vector4, float>>), typeof(Expression<Func<Vector4, float>>), typeof(Expression<Func<Vector4, float>>) }))
                    {
                        return String.Format("{0}.{1}", Compile(invoke.Arguments[0], runtime), string.Concat(invoke.Arguments.Skip(1).Select(a =>
                        {
                            if (a == null) throw new ArgumentNullException("source", "Swizzle selectors cannot be null");

                            var constant = a as ConstantExpression;
                            if (constant != null && constant.Value == null) throw new ArgumentNullException("source", "Swizzle selectors cannot be null");

                            var quote = a as UnaryExpression;
                            if (quote == null) throw new ArgumentOutOfRangeException("source", "Swizzles should only select a member from the input vector");
                            var lambda = quote.Operand as LambdaExpression;
                            if (lambda == null) throw new ArgumentOutOfRangeException("source", "Swizzles should only select a member from the input vector");
                            var member = lambda.Body as MemberExpression;
                            if (member == null) throw new ArgumentOutOfRangeException("source", "Swizzles should only select a member from the input vector");
                            if (member.Expression != lambda.Parameters[0]) throw new ArgumentOutOfRangeException("source", "Swizzles should only select a member from the input vector");

                            return member.Member.Name.ToLowerInvariant().First();
                        })));
                    }

                    // These methods have no GLSL equivalent.  They must be compiled to conditional statements.
                    if (invoke.Method == typeof(Math).GetMethod("Max", new[] { typeof(int), typeof(int) }))
                        return runtime.NewName(source, String.Format(
                            "{0} > {1} ? {0} : {1}",
                            Compile(invoke.Arguments[0], runtime),
                            Compile(invoke.Arguments[1], runtime)
                        ));


                    if (invoke.Method == typeof(Math).GetMethod("Min", new[] { typeof(int), typeof(int) }))
                        return runtime.NewName(source, String.Format(
                            "{0} < {1} ? {0} : {1}",
                            Compile(invoke.Arguments[0], runtime),
                            Compile(invoke.Arguments[1], runtime)
                        ));

                    // These methods compile to a multiply in GLSL, but we have to expand to a vec4 for vec2/vec3.
                    if (invoke.Method == typeof(Vector2).GetMethod("Transform", new[] { typeof(Vector2), typeof(Matrix4x4) }))
                        return runtime.NewName(source, String.Format(
                            "vec4({0}, 0.0, 1.0) * {1}",
                            Compile(invoke.Arguments[0], runtime),
                            Compile(invoke.Arguments[1], runtime)
                        ));

                    if (invoke.Method == typeof(Vector3).GetMethod("Transform", new[] { typeof(Vector3), typeof(Matrix4x4) }))
                        return runtime.NewName(source, String.Format(
                            "vec4({0}, 1.0) * {1}",
                            Compile(invoke.Arguments[0], runtime),
                            Compile(invoke.Arguments[1], runtime)
                        ));

                    if (invoke.Method == typeof(Vector4).GetMethod("Transform", new[] { typeof(Vector4), typeof(Matrix4x4) }))
                        return runtime.NewName(source, String.Format(
                            "{0} * {1}",
                            Compile(invoke.Arguments[0], runtime),
                            Compile(invoke.Arguments[1], runtime)
                        ));

                    if (invoke.Object != null)
                    {
                        // Support .Equals in types which implement their own with a typed argument.
                        if (invoke.Method == invoke.Object.Type.GetMethod("Equals", new[] { invoke.Object.Type }))
                            return runtime.NewName(source, String.Format(
                                "{0} == {1}",
                                Compile(invoke.Object, runtime),
                                Compile(invoke.Arguments[0], runtime)
                            ));

                        // Support for .Equals in value types which don't directly implement it.
                        // (the implementation in the object base class)
                        if (invoke.Arguments.Count == 1 && invoke.Method == typeof(object).GetMethod("Equals", new[] { typeof(object) }))
                        {
                            // The parameter is always going to be a value type, and will therefore always be cast to object.
                            var collapse = ((UnaryExpression)invoke.Arguments[0]).Operand;

                            if (invoke.Object.Type != collapse.Type)
                                throw new NotSupportedException(String.Format(
                                    "Equality comparisons between types \"{0}\" and \"{1}\" are not supported",
                                    invoke.Object.Type,
                                    collapse.Type
                                ));

                            return runtime.NewName(source, String.Format(
                                "{0} == {1}",
                                Compile(invoke.Object, runtime),
                                Compile(collapse, runtime)
                            ));
                        }
                    }

                    // If the method matches none of the above, it might be a GLSL function.
                    if (!SupportedFunctions.ContainsKey(invoke.Method))
                        throw new NotImplementedException(String.Format("Calls to \"{0}\" are currently unimplemented", invoke.Method));

                    return runtime.NewName(source, String.Format(
                        "{0}({1})",
                        SupportedFunctions[invoke.Method],
                        // If the method is an instance method, include the instance as the first argument.
                        String.Join(", ",
                            (invoke.Object == null ? Enumerable.Empty<Expression>() : Enumerable.Repeat(invoke.Object, 1))
                            .Union(invoke.Arguments)
                            .Select(a => Compile(a, runtime))
                        )));

                case ExpressionType.New:
                    var construct = source as NewExpression;
                    CheckType(construct.Type);
                    if (construct.Type == typeof(Vector2) || construct.Type == typeof(Vector2) || construct.Type == typeof(Vector3) || construct.Type == typeof(Vector4))
                    {
                        // Parameterless constructors.
                        if (construct.Arguments.Count == 0)
                            return CompileConstant(Activator.CreateInstance(construct.Type));

                        return runtime.NewName(source, String.Format(
                            "{0}({1})",
                            GetTypeName(construct.Type),
                            String.Join(", ", construct.Arguments.Select(c => Compile(c, runtime)))));
                    }

                    // Special case: we need to reorder the arguments for matrices as we are going from row-major
                    // order to column-major order.
                    if (construct.Type == typeof(Matrix4x4) && construct.Arguments.Count == 16)
                        return runtime.NewName(source, String.Format(
                            "{0}({1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16})",
                            GetTypeName(construct.Type),
                            Compile(construct.Arguments[0], runtime),
                            Compile(construct.Arguments[4], runtime),
                            Compile(construct.Arguments[8], runtime),
                            Compile(construct.Arguments[12], runtime),
                            Compile(construct.Arguments[1], runtime),
                            Compile(construct.Arguments[5], runtime),
                            Compile(construct.Arguments[9], runtime),
                            Compile(construct.Arguments[13], runtime),
                            Compile(construct.Arguments[2], runtime),
                            Compile(construct.Arguments[6], runtime),
                            Compile(construct.Arguments[10], runtime),
                            Compile(construct.Arguments[14], runtime),
                            Compile(construct.Arguments[3], runtime),
                            Compile(construct.Arguments[7], runtime),
                            Compile(construct.Arguments[11], runtime),
                            Compile(construct.Arguments[15], runtime)
                        ));

                    if (construct.Arguments.Any())
                        throw new NotImplementedException(String.Format("Constructor expressions with parameters for type \"{0}\" are currently unimplemented", construct.Type));
                    return runtime.NewName(source, MemberInit(source.Type, Enumerable.Empty<MemberBinding>(), runtime));

                case ExpressionType.Conditional:
                    var conditional = source as ConditionalExpression;
                    return runtime.NewName(source, String.Format(
                            "{0} ? {1} : {2}",
                            Compile(conditional.Test, runtime),
                            Compile(conditional.IfTrue, runtime),
                            Compile(conditional.IfFalse, runtime)
                        ));

                case ExpressionType.MemberAccess:
                    {
                        var member = source as MemberExpression;

                        if (MemberOverrides.ContainsKey(member.Member))
                            return String.Format(
                                "{0}{1}",
                                Compile(member.Expression, runtime),
                                MemberOverrides[member.Member]
                                );

                        if (member.Member == typeof(Matrix4x4).GetProperty("Identity"))
                            return "mat4(1.0)";

                        if (member.Member.MemberType != MemberTypes.Field)
                            throw new NotSupportedException("Access of members other than fields is not supported");
                        return String.Format(
                            "{0}.{1}",
                            Compile(member.Expression, runtime),
                            member.Member.Name
                            );
                    }

                default:
                    throw new NotImplementedException(String.Format("Expressions of type \"{0}\" are currently unimplemented", source.NodeType));
            }
        }

        /// <inheritdoc />
        public String Compile<T>(Expression<T> source)
        {
            if (source == null) throw new ArgumentNullException("source");
            var runtime = new Runtime();
            foreach (var parameter in source.Parameters)
            {
                CheckType(parameter.Type);
                runtime.NameMap[parameter] = new Runtime.Mapping { Alias = String.Format("parameter_{0}", parameter.Name) };
            }
            runtime.StringBuilder.AppendFormat("return {0};", Compile(source.Body, runtime));
            return runtime.StringBuilder.ToString();
        }
    }
}
