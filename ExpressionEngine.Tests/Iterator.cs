﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionEngine.Tests
{
    [TestClass]
    public sealed class IteratorTests
    {
        [TestMethod]
        public void TransformNullThrowsException()
        {
            try
            {
                Iterator.Iterate(4.0f, null, 5);
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("transform", ex.ParamName);
                Assert.AreEqual(@"Value cannot be null.
Parameter name: transform", ex.Message);
            }
        }

        [TestMethod]
        public void IterationsLessThanOneThrowsException()
        {
            try
            {
                Iterator.Iterate(4.0f, i => i, 0);
                Assert.Fail();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Assert.AreEqual("iterations", ex.ParamName);
                Assert.AreEqual(@"Cannot be less than one
Parameter name: iterations", ex.Message);
            }
        }

        [TestMethod]
        public void BasicIteration()
        {
            Assert.AreEqual(15, Iterator.Iterate(-5, x => x + 4, 5));
        }

        [TestMethod]
        public void TwoLevelIteration()
        {
            Assert.AreEqual(35, Iterator.Iterate(-5, x => Iterator.Iterate(x, y => y + 2, 4), 5));
        }
    }
}
