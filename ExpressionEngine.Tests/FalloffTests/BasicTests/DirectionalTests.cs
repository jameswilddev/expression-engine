﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;
using ExpressionEngine.Falloff;

namespace ExpressionEngine.Tests.FalloffTests.BasicTests
{
	[TestClass]
	public sealed class DirectionalTests
	{
		[TestMethod]
		public void NormalNullThrowsException()
		{
			try
			{
				Basic.Directional(null);
				Assert.Fail();
			}
			catch (ArgumentNullException ex)
			{
				Assert.AreEqual("normal", ex.ParamName);
				Assert.AreEqual(@"Value cannot be null.
Parameter name: normal", ex.Message);
			}
		}

		[TestMethod]
		public void Call()
		{
			Assert.AreEqual(new FalloffSample { Normal = new Vector3(4.0f, 8.0f, 2.0f), Intensity = 1.0f },
				Basic.Directional(() => new Vector3(4.0f, 8.0f, 2.0f)).Compile()(new Vector3(2.0f, 3.0f, 1.0f)));
		}
	}
}
