﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Falloff;
using System.Numerics;

namespace ExpressionEngine.Tests.FalloffTests.BasicTests
{
	[TestClass]
	public sealed class RadialTests
	{
		[TestMethod]
		public void OriginNullThrowsException()
		{
			try
			{
				Basic.Radial(null, () => 3.0f);
				Assert.Fail();
			}
			catch (ArgumentNullException ex)
			{
				Assert.AreEqual("origin", ex.ParamName);
				Assert.AreEqual(@"Value cannot be null.
Parameter name: origin", ex.Message);
			}
		}

		[TestMethod]
		public void RadiusNullThrowsException()
		{
			try
			{
				Basic.Radial(() => default(Vector3), null);
				Assert.Fail();
			}
			catch (ArgumentNullException ex)
			{
				Assert.AreEqual("radius", ex.ParamName);
				Assert.AreEqual(@"Value cannot be null.
Parameter name: radius", ex.Message);
			}
		}

		[TestMethod]
		public void FullIntensityAtOrigin()
		{
			var result = Basic.Radial(() => new Vector3(6.0f, 7.0f, 2.0f), () => 4.0f).Compile()(new Vector3(6.0f, 7.0f, 2.0f));
			Assert.AreEqual(1.0f, result.Intensity);
			// Normal is indeterminate.
		}

		[TestMethod]
		public void LinearRolloffBetweenOriginAndBorder()
		{
			var result = Basic.Radial(() => new Vector3(6.0f, 7.0f, 2.0f), () => 4.0f).Compile()(new Vector3(5.3414144400618579121485089562876f, 7.9220197839133989229920874611974f, 1.6048486640371147472891053737725f));
			Assert.AreEqual(0.7f, result.Intensity, 0.001f);

			// Rounding error is acceptable here.
			Assert.AreEqual(-0.54882129994845173987624253642703f, result.Normal.X, 0.001f);
			Assert.AreEqual(0.76834981992783243582673955099784f, result.Normal.Y, 0.001f);
			Assert.AreEqual(-0.32929277996907104392574552185622f, result.Normal.Z, 0.001f);
		}

		[TestMethod]
		public void ZeroIntensityAtBorder()
		{
			var result = Basic.Radial(() => new Vector3(6.0f, 7.0f, 2.0f), () => 4.0f).Compile()(new Vector3(3.8047148002061930404950298542919f, 10.073399279711329743306958203991f, 0.68282888012371582429701791257512f));
			Assert.AreEqual(0.0f, result.Intensity);

			// Rounding error is acceptable here.
			Assert.AreEqual(-0.54882129994845173987624253642703f, result.Normal.X, 0.001f);
			Assert.AreEqual(0.76834981992783243582673955099784f, result.Normal.Y, 0.001f);
			Assert.AreEqual(-0.32929277996907104392574552185622f, result.Normal.Z, 0.001f);
		}

		[TestMethod]
		public void ZeroIntensityBeyondBorder()
		{
			var result = Basic.Radial(() => new Vector3(6.0f, 7.0f, 2.0f), () => 4.0f).Compile()(new Vector3(3.2558935002577413006187873178649f, 10.841749099639162179133697754989f, 0.3535361001546447803712723907189f));
			Assert.AreEqual(0.0f, result.Intensity);

			// Rounding error is acceptable here.
			Assert.AreEqual(-0.54882129994845173987624253642703f, result.Normal.X, 0.001f);
			Assert.AreEqual(0.76834981992783243582673955099784f, result.Normal.Y, 0.001f);
			Assert.AreEqual(-0.32929277996907104392574552185622f, result.Normal.Z, 0.001f);
		}
	}
}
