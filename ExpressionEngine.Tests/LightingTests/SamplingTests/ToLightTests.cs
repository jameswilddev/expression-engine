﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;
using ExpressionEngine.Lighting;
using ExpressionEngine.Falloff;

namespace ExpressionEngine.Tests.LightingTests.SamplingTests
{
	[TestClass]
	public sealed class ToLightTests
	{
		[TestMethod]
		public void FalloffNullThrowsException()
		{
			try
			{
				Sampling.ToLight(null, () => default(Vector3));
				Assert.Fail();
			}
			catch (ArgumentNullException ex)
			{
				Assert.AreEqual("falloff", ex.ParamName);
				Assert.AreEqual(@"Value cannot be null.
Parameter name: falloff", ex.Message);
			}
		}

		[TestMethod]
		public void ColorNullThrowsException()
		{
			try
			{
				Sampling.ToLight(at => default(FalloffSample), null);
				Assert.Fail();
			}
			catch (ArgumentNullException ex)
			{
				Assert.AreEqual("color", ex.ParamName);
				Assert.AreEqual(@"Value cannot be null.
Parameter name: color", ex.Message);
			}
		}

		[TestMethod]
		public void Call()
		{
			var result = Sampling.ToLight(at => new FalloffSample { Normal = at * 2.0f, Intensity = 6.0f }, () => new Vector3(2.0f, 4.0f, 8.0f)).Compile()(new Vector3(4.0f, 5.0f, 5.5f));

			Assert.AreEqual(new Vector3(8.0f, 10.0f, 11.0f), result.Normal);
			Assert.AreEqual(new Vector3(12.0f, 24.0f, 48.0f), result.Color);
		}
	}
}
