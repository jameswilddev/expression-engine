﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Lighting;
using System.Numerics;
using System.Linq.Expressions;
using System.Linq;

namespace ExpressionEngine.Tests.LightingTests.SamplingTests
{
	[TestClass]
	public sealed class BuildTests
	{
		[TestMethod]
		public void LightsNullThrowsException()
		{
			try
			{
				Sampling.Build(null, (at, lightNormal) => 0.0f, at => default(Vector3));
				Assert.Fail();
			}
			catch (ArgumentNullException ex)
			{
				Assert.AreEqual("lights", ex.ParamName);
				Assert.AreEqual(@"Value cannot be null.
Parameter name: lights", ex.Message);
			}
		}

		[TestMethod]
		public void LightsContainsNullThrowsException()
		{
			try
			{
				Sampling.Build(new Expression<LightDelegate>[] { at => default(LightSample), null, at => default(LightSample) }, (at, lightNormal) => 0.0f, at => default(Vector3));
				Assert.Fail();
			}
			catch (ArgumentNullException ex)
			{
				Assert.AreEqual("lights", ex.ParamName);
				Assert.AreEqual(@"Cannot contain null
Parameter name: lights", ex.Message);
			}
		}

		[TestMethod]
		public void CalculateIntensityNullThrowsException()
		{
			try
			{
				Sampling.Build(new Expression<LightDelegate>[] { at => default(LightSample), at => default(LightSample) }, null, at => default(Vector3));
				Assert.Fail();
			}
			catch (ArgumentNullException ex)
			{
				Assert.AreEqual("calculateIntensity", ex.ParamName);
				Assert.AreEqual(@"Value cannot be null.
Parameter name: calculateIntensity", ex.Message);
			}
		}

		[TestMethod]
		public void NoLightsOrEmissive()
		{
			Assert.AreEqual(default(Vector3), Sampling.Build(Enumerable.Empty<Expression<LightDelegate>>(), (at, lightNormal) => 8.0f, null).Compile()(new Vector3(4.0f, 8.0f, 7.0f)));
		}

		[TestMethod]
		public void LightsOnly()
		{
			// Light A
			// color 28.0f, 56.0f, 49.0f
			// normal 44.0f, 88.0f, 77.0f
			// intensity 176 704 539 = 1419
			// contribution 39732 79464 69531
			// Light B
			// color 12.0f, 24.0f, 21.0f
			// normal 24.0f, 48.0f, 42.0f
			// intensity 96 384 294 = 774
			// contribution 9288 18576 16254
			Assert.AreEqual(new Vector3(49020.0f, 98040.0f, 85785.0f), Sampling.Build(new Expression<LightDelegate>[] { at => new LightSample { Color = at * 7.0f, Normal = at * 11.0f }, at => new LightSample { Color = at * 3.0f, Normal = at * 6.0f } }, (at, lightNormal) => Vector3.Dot(at, lightNormal), null).Compile()(new Vector3(4.0f, 8.0f, 7.0f)));
		}

		[TestMethod]
		public void EmissiveOnly()
		{
			Assert.AreEqual(new Vector3(8.0f, 16.0f, 14.0f), Sampling.Build(Enumerable.Empty<Expression<LightDelegate>>(), (at, lightNormal) => 8.0f, at => at * 2.0f).Compile()(new Vector3(4.0f, 8.0f, 7.0f)));
		}

		[TestMethod]
		public void EmissiveAndLights()
		{
			Assert.AreEqual(new Vector3(49028.0f, 98056.0f, 85799.0f), Sampling.Build(new Expression<LightDelegate>[] { at => new LightSample { Color = at * 7.0f, Normal = at * 11.0f }, at => new LightSample { Color = at * 3.0f, Normal = at * 6.0f } }, (at, lightNormal) => Vector3.Dot(at, lightNormal), at => at * 2.0f).Compile()(new Vector3(4.0f, 8.0f, 7.0f)));
		}
	}
}
