﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.Vector2Extensions.Tests
{
	[TestClass]
	public sealed class Mix
	{
		[TestMethod]
		public void Call()
		{
			Assert.AreEqual(
				new Vector2(160.0f, 34.0f),
				new Vector2(100.0f, 32.0f)
				.Mix(
					new Vector2(180.0f, 40.0f),
					new Vector2(0.75f, 0.25f)
				));
		}
	}
}
