﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.Vector2Extensions.Tests
{
	[TestClass]
	public sealed class Fract
	{
		#region X Axis
		[TestMethod]
		public void XZero()
		{
			Assert.AreEqual(0.0f, new Vector4 { X = 0.0f }.Fract().X);
		}

		[TestMethod]
		public void XLessThanOneGreaterThanZero()
		{
			Assert.AreEqual(0.6f, new Vector4 { X = 0.6f }.Fract().X);
		}

		[TestMethod]
		public void XOne()
		{
			Assert.AreEqual(0.0f, new Vector4 { X = 1.0f}.Fract().X);
		}

		[TestMethod]
		public void XLessThanTwoGreaterThanOne()
		{
			Assert.AreEqual(0.6f, new Vector4 { X = 1.6f}.Fract().X);
		}

		[TestMethod]
		public void XLessThanZeroGreaterThanNegativeOne()
		{
			Assert.AreEqual(0.6f, new Vector4 { X = -0.4f}.Fract().X);
		}

		[TestMethod]
		public void XNegativeOne()
		{
			Assert.AreEqual(0.0f, new Vector4 { X = -1.0f}.Fract().X);
		}

		[TestMethod]
		public void XLessThanNegativeOneGreaterThanNegativeTwo()
		{
			Assert.AreEqual(0.6f, new Vector4 { X = -1.4f}.Fract().X);
		}
		#endregion

		#region Y Axis
		[TestMethod]
		public void YZero()
		{
			Assert.AreEqual(0.0f, new Vector4 { Y = 0.0f }.Fract().Y);
		}

		[TestMethod]
		public void YLessThanOneGreaterThanZero()
		{
			Assert.AreEqual(0.6f, new Vector4 { Y = 0.6f }.Fract().Y);
		}

		[TestMethod]
		public void YOne()
		{
			Assert.AreEqual(0.0f, new Vector4 { Y = 1.0f }.Fract().Y);
		}

		[TestMethod]
		public void YLessThanTwoGreaterThanOne()
		{
			Assert.AreEqual(0.6f, new Vector4 { Y = 1.6f }.Fract().Y);
		}

		[TestMethod]
		public void YLessThanZeroGreaterThanNegativeOne()
		{
			Assert.AreEqual(0.6f, new Vector4 { Y = -0.4f }.Fract().Y);
		}

		[TestMethod]
		public void YNegativeOne()
		{
			Assert.AreEqual(0.0f, new Vector4 { Y = -1.0f }.Fract().Y);
		}

		[TestMethod]
		public void YLessThanNegativeOneGreaterThanNegativeTwo()
		{
			Assert.AreEqual(0.6f, new Vector4 { Y = -1.4f }.Fract().Y);
		}
		#endregion

		#region Z Axis
		[TestMethod]
		public void ZZero()
		{
			Assert.AreEqual(0.0f, new Vector4 { Z = 0.0f }.Fract().Z);
		}

		[TestMethod]
		public void ZLessThanOneGreaterThanZero()
		{
			Assert.AreEqual(0.6f, new Vector4 { Z = 0.6f }.Fract().Z);
		}

		[TestMethod]
		public void ZOne()
		{
			Assert.AreEqual(0.0f, new Vector4 { Z = 1.0f }.Fract().Z);
		}

		[TestMethod]
		public void ZLessThanTwoGreaterThanOne()
		{
			Assert.AreEqual(0.6f, new Vector4 { Z = 1.6f }.Fract().Z);
		}

		[TestMethod]
		public void ZLessThanZeroGreaterThanNegativeOne()
		{
			Assert.AreEqual(0.6f, new Vector4 { Z = -0.4f }.Fract().Z);
		}

		[TestMethod]
		public void ZNegativeOne()
		{
			Assert.AreEqual(0.0f, new Vector4 { Z = -1.0f }.Fract().Z);
		}

		[TestMethod]
		public void ZLessThanNegativeOneGreaterThanNegativeTwo()
		{
			Assert.AreEqual(0.6f, new Vector4 { Z = -1.4f }.Fract().Z);
		}
		#endregion

		#region W Axis
		[TestMethod]
		public void WZero()
		{
			Assert.AreEqual(0.0f, new Vector4 { W = 0.0f }.Fract().W);
		}

		[TestMethod]
		public void WLessThanOneGreaterThanZero()
		{
			Assert.AreEqual(0.6f, new Vector4 { W = 0.6f }.Fract().W);
		}

		[TestMethod]
		public void WOne()
		{
			Assert.AreEqual(0.0f, new Vector4 { W = 1.0f }.Fract().W);
		}

		[TestMethod]
		public void WLessThanTwoGreaterThanOne()
		{
			Assert.AreEqual(0.6f, new Vector4 { W = 1.6f }.Fract().W);
		}

		[TestMethod]
		public void WLessThanZeroGreaterThanNegativeOne()
		{
			Assert.AreEqual(0.6f, new Vector4 { W = -0.4f }.Fract().W);
		}

		[TestMethod]
		public void WNegativeOne()
		{
			Assert.AreEqual(0.0f, new Vector4 { W = -1.0f }.Fract().W);
		}

		[TestMethod]
		public void WLessThanNegativeOneGreaterThanNegativeTwo()
		{
			Assert.AreEqual(0.6f, new Vector4 { W = -1.4f }.Fract().W);
		}
		#endregion
	}
}
