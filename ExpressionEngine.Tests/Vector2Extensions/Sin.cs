﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.Vector2Extensions.Tests
{
	[TestClass]
	public sealed class Sin
	{
		[TestMethod]
		public void X()
		{
			Assert.AreEqual(0.0f,
				new Vector2 { X = 0.0f }.Sin().X);
			Assert.AreEqual(0.70710678118654752440084436210485f,
				new Vector2 { X = 0.78539816339744830961566084581988f }.Sin().X);
			Assert.AreEqual(1.0f,
				new Vector2 { X = 1.5707963267948966192313216916398f }.Sin().X);
		}

		[TestMethod]
		public void Y()
		{
			Assert.AreEqual(0.0f,
				new Vector2 { Y = 0.0f }.Sin().Y);
			Assert.AreEqual(0.70710678118654752440084436210485f,
				new Vector2 { Y = 0.78539816339744830961566084581988f }.Sin().Y);
			Assert.AreEqual(1.0f,
				new Vector2 { Y = 1.5707963267948966192313216916398f }.Sin().Y);
		}
	}
}
