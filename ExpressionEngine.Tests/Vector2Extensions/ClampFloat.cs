﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.Vector2Extensions.Tests
{
	[TestClass]
	public sealed class ClampFloat
	{
		#region X
		[TestMethod]
		public void XBetweenMinAndMax()
		{
			Assert.AreEqual(4.0f, new Vector2 { X = 4.0f }.Clamp(2.0f, 5.0f).X);
		}

		[TestMethod]
		public void XLessThanMin()
		{
			Assert.AreEqual(2.0f, new Vector2 { X = 1.0f }.Clamp(2.0f, 5.0f).X);
		}

		[TestMethod]
		public void XGreaterThanMax()
		{
			Assert.AreEqual(5.0f, new Vector2 { X = 6.0f }.Clamp(2.0f, 5.0f).X);
		}
		#endregion

		#region Y
		[TestMethod]
		public void YBetweenMinAndMax()
		{
			Assert.AreEqual(4.0f, new Vector2 { Y = 4.0f }.Clamp(2.0f, 5.0f).Y);
		}

		[TestMethod]
		public void YLessThanMin()
		{
			Assert.AreEqual(2.0f, new Vector2 { Y = 1.0f }.Clamp(2.0f, 5.0f).Y);
		}

		[TestMethod]
		public void YGreaterThanMax()
		{
			Assert.AreEqual(5.0f, new Vector2 { Y = 6.0f }.Clamp(2.0f, 5.0f).Y);
		}
		#endregion
	}
}
