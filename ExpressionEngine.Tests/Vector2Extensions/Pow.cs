﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.Vector2Extensions.Tests
{
	[TestClass]
	public sealed class Pow
	{
		[TestMethod]
		public void Call()
		{
			Assert.AreEqual(
				new Vector2(27.0f, 64.0f),
				new Vector2(3.0f, 8.0f)
				.Pow(new Vector2(3.0f, 2.0f)));
		}
	}
}
