﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.Vector2Extensions.Tests
{
	[TestClass]
	public sealed class ModFloat
	{
		[TestMethod]
		public void LeftPositiveRightPositive()
		{
			Assert.AreEqual(new Vector2(3.0f, 2.9f),
				new Vector2(7.0f, 6.9f).Mod(4.0f));
		}

		[TestMethod]
		public void LeftNegativeRightPositive()
		{
			var result = new Vector2(-7.0f, -6.9f).Mod(4.0f);
			Assert.AreEqual(result.X, 1.0f, 0.001f);
			Assert.AreEqual(result.Y, 1.1f, 0.001f);
		}

		[TestMethod]
		public void LeftPositiveRightNegative()
		{
			var result = new Vector2(7.0f, 6.9f).Mod(-4.0f);
			Assert.AreEqual(result.X, -1.0f, 0.001f);
			Assert.AreEqual(result.Y, -1.1f, 0.001f);
		}

		[TestMethod]
		public void LeftNegativeRightNegative()
		{
			Assert.AreEqual(new Vector2(-3.0f, -2.9f),
				new Vector2(-7.0f, -6.9f).Mod(-4.0f));
		}
	}
}
