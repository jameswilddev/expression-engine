﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.Vector2Extensions.Tests
{
	[TestClass]
	public sealed class Ceil
	{
		#region X
		[TestMethod]
		public void XZero()
		{
			Assert.AreEqual(0.0f, new Vector2 { X = 0.0f }.Ceil().X);
		}

		[TestMethod]
		public void XLessThanOneGreaterThanZero()
		{
			Assert.AreEqual(1.0f, new Vector2 { X = 0.6f }.Ceil().X);
		}

		[TestMethod]
		public void XOne()
		{
			Assert.AreEqual(1.0f, new Vector2 { X = 1.0f }.Ceil().X);
		}

		[TestMethod]
		public void XLessThanTwoGreaterThanOne()
		{
			Assert.AreEqual(2.0f, new Vector2 { X = 1.6f }.Ceil().X);
		}

		[TestMethod]
		public void XLessThanZeroGreaterThanNegativeOne()
		{
			Assert.AreEqual(0.0f, new Vector2 { X = -0.4f }.Ceil().X);
		}

		[TestMethod]
		public void XNegativeOne()
		{
			Assert.AreEqual(-1.0f, new Vector2 { X = -1.0f }.Ceil().X);
		}

		[TestMethod]
		public void XLessThanNegativeOneGreaterThanNegativeTwo()
		{
			Assert.AreEqual(-1.0f, new Vector2 { X = -1.4f }.Ceil().X);
		}
		#endregion

		#region Y
		[TestMethod]
		public void YZero()
		{
			Assert.AreEqual(0.0f, new Vector2 { Y = 0.0f }.Ceil().Y);
		}

		[TestMethod]
		public void YLessThanOneGreaterThanZero()
		{
			Assert.AreEqual(1.0f, new Vector2 { Y = 0.6f }.Ceil().Y);
		}

		[TestMethod]
		public void YOne()
		{
			Assert.AreEqual(1.0f, new Vector2 { Y = 1.0f }.Ceil().Y);
		}

		[TestMethod]
		public void YLessThanTwoGreaterThanOne()
		{
			Assert.AreEqual(2.0f, new Vector2 { Y = 1.6f }.Ceil().Y);
		}

		[TestMethod]
		public void YLessThanZeroGreaterThanNegativeOne()
		{
			Assert.AreEqual(0.0f, new Vector2 { Y = -0.4f }.Ceil().Y);
		}

		[TestMethod]
		public void YNegativeOne()
		{
			Assert.AreEqual(-1.0f, new Vector2 { Y = -1.0f }.Ceil().Y);
		}

		[TestMethod]
		public void YLessThanNegativeOneGreaterThanNegativeTwo()
		{
			Assert.AreEqual(-1.0f, new Vector2 { Y = -1.4f }.Ceil().Y);
		}
		#endregion
	}
}
