﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.Vector2Extensions.Tests
{
	[TestClass]
	public sealed class Floor
	{
		#region X Axis
		[TestMethod]
		public void XZero()
		{
			Assert.AreEqual(0.0f, new Vector2 { X = 0.0f }.Floor().X);
		}

		[TestMethod]
		public void XLessThanOneGreaterThanZero()
		{
			Assert.AreEqual(0.0f, new Vector2 { X = 0.6f }.Floor().X);
		}

		[TestMethod]
		public void XOne()
		{
			Assert.AreEqual(1.0f, new Vector2 { X = 1.0f }.Floor().X);
		}

		[TestMethod]
		public void XLessThanTwoGreaterThanOne()
		{
			Assert.AreEqual(1.0f, new Vector2 { X = 1.6f }.Floor().X);
		}

		[TestMethod]
		public void XLessThanZeroGreaterThanNegativeOne()
		{
			Assert.AreEqual(-1.0f, new Vector2 { X = -0.4f }.Floor().X);
		}

		[TestMethod]
		public void XNegativeOne()
		{
			Assert.AreEqual(-1.0f, new Vector2 { X = -1.0f }.Floor().X);
		}

		[TestMethod]
		public void XLessThanNegativeOneGreaterThanNegativeTwo()
		{
			Assert.AreEqual(-2.0f, new Vector2 { X = -1.4f }.Floor().X);
		}
		#endregion

		#region Y Axis
		[TestMethod]
		public void YZero()
		{
			Assert.AreEqual(0.0f, new Vector2 { Y = 0.0f }.Floor().Y);
		}

		[TestMethod]
		public void YLessThanOneGreaterThanZero()
		{
			Assert.AreEqual(0.0f, new Vector2 { Y = 0.6f }.Floor().Y);
		}

		[TestMethod]
		public void YOne()
		{
			Assert.AreEqual(1.0f, new Vector2 { Y = 1.0f }.Floor().Y);
		}

		[TestMethod]
		public void YLessThanTwoGreaterThanOne()
		{
			Assert.AreEqual(1.0f, new Vector2 { Y = 1.6f }.Floor().Y);
		}

		[TestMethod]
		public void YLessThanZeroGreaterThanNegativeOne()
		{
			Assert.AreEqual(-1.0f, new Vector2 { Y = -0.4f }.Floor().Y);
		}

		[TestMethod]
		public void YNegativeOne()
		{
			Assert.AreEqual(-1.0f, new Vector2 { Y = -1.0f }.Floor().Y);
		}

		[TestMethod]
		public void YLessThanNegativeOneGreaterThanNegativeTwo()
		{
			Assert.AreEqual(-2.0f, new Vector2 { Y = -1.4f }.Floor().Y);
		}
		#endregion
	}
}
