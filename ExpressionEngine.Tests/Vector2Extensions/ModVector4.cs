﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.Vector2Extensions.Tests
{
	[TestClass]
	public sealed class ModVector2
	{
		[TestMethod]
		public void LeftPositiveRightPositive()
		{
			var result = new Vector2(7.0f, 13.9f)
				.Mod(new Vector2(4.0f, 6.0f));
			Assert.AreEqual(result.X, 3.0f, 0.001f);
			Assert.AreEqual(result.Y, 1.9f, 0.001f);
		}

		[TestMethod]
		public void LeftNegativeRightPositive()
		{
			var result = new Vector2(-7.0f, -13.9f)
				.Mod(new Vector2(4.0f, 6.0f));
			Assert.AreEqual(result.X, 1.0f, 0.001f);
			Assert.AreEqual(result.Y, 4.1f, 0.001f);
		}

		[TestMethod]
		public void LeftPositiveRightNegative()
		{
			var result = new Vector2(7.0f, 13.9f)
				.Mod(new Vector2(-4.0f, -6.0f));
			Assert.AreEqual(result.X, -1.0f, 0.001f);
			Assert.AreEqual(result.Y, -4.1f, 0.001f);
		}

		[TestMethod]
		public void LeftNegativeRightNegative()
		{
			var result = new Vector2(-7.0f, -13.9f)
				.Mod(new Vector2(-4.0f, -6.0f));
			Assert.AreEqual(result.X, -3.0f, 0.001f);
			Assert.AreEqual(result.Y, -1.9f, 0.001f);
		}
	}
}
