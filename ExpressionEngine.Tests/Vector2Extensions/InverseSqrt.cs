﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.Vector2Extensions.Tests
{
	[TestClass]
	public sealed class InverseSqrt
	{
		[TestMethod]
		public void Call()
		{
			Assert.AreEqual(new Vector2(0.2f, 0.125f), 
				new Vector2(25.0f, 64.0f).InverseSqrt());
		}
	}
}
