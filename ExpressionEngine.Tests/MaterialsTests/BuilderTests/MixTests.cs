﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Materials;
using System.Numerics;
using System.Linq.Expressions;

namespace ExpressionEngine.Tests.MaterialsTests.BuilderTests
{
	[TestClass]
	public sealed class MixTests
	{
		[TestMethod]
		public void FromNullThrowsException()
		{
			try
			{
				Builder.Mix(null, at => default(Vector3), () => 0.5f);
				Assert.Fail();
			}
			catch (ArgumentNullException ex)
			{
				Assert.AreEqual("from", ex.ParamName);
				Assert.AreEqual(@"Value cannot be null.
Parameter name: from", ex.Message);
			}
		}

		[TestMethod]
		public void ToNullThrowsException()
		{
			try
			{
				Builder.Mix(at => default(Vector3), null, () => 0.5f);
				Assert.Fail();
			}
			catch (ArgumentNullException ex)
			{
				Assert.AreEqual("to", ex.ParamName);
				Assert.AreEqual(@"Value cannot be null.
Parameter name: to", ex.Message);
			}
		}

		[TestMethod]
		public void AlphaNullThrowsException()
		{
			try
			{
				Builder.Mix(at => default(Vector3), at => default(Vector3), null);
				Assert.Fail();
			}
			catch (ArgumentNullException ex)
			{
				Assert.AreEqual("alpha", ex.ParamName);
				Assert.AreEqual(@"Value cannot be null.
Parameter name: alpha", ex.Message);
			}
		}

		[TestMethod]
		public void Call()
		{
			Expression<MaterialDelegate> from = at => at + new Vector3(3.0f);
			Expression<MaterialDelegate> to = at => at + new Vector3(7.0f);
			Expression<Func<float>> alpha = () => 0.7f;
			Assert.AreEqual(new Vector3(9.8f, 7.8f, 13.8f), from.Mix(to, alpha).Compile()(new Vector3(4.0f, 2.0f, 8.0f)));
		}
	}
}
