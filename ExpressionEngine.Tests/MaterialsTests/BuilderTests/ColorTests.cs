﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Materials;
using System.Numerics;
using System.Linq.Expressions;

namespace ExpressionEngine.Tests.MaterialsTests.BuilderTests
{
	[TestClass]
	public sealed class ColorTests
	{
		[TestMethod]
		public void MaterialNullThrowsException()
		{
			try
			{
				Builder.Color(null, () => default(Vector3));
				Assert.Fail();
			}
			catch (ArgumentNullException ex)
			{
				Assert.AreEqual("material", ex.ParamName);
				Assert.AreEqual(@"Value cannot be null.
Parameter name: material", ex.Message);
			}
		}

		[TestMethod]
		public void ColorNullThrowsException()
		{
			try
			{
				Builder.Color(at => default(Vector3), null);
				Assert.Fail();
			}
			catch (ArgumentNullException ex)
			{
				Assert.AreEqual("color", ex.ParamName);
				Assert.AreEqual(@"Value cannot be null.
Parameter name: color", ex.Message);
			}
		}

		[TestMethod]
		public void Call()
		{
			Expression<MaterialDelegate> material = at => at * 2.0f;
			Expression<Func<Vector3>> color = () => new Vector3(6.0f, 18.0f, 12.0f);
			Assert.AreEqual(new Vector3(48.0f, 180.0f, 144.0f), material.Color(color).Compile()(new Vector3(4.0f, 5.0f, 6.0f)));
		}
	}
}
