﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;
using System.Linq;
using System.Linq.Expressions;

namespace ExpressionEngine.Compilers.Tests
{
    [TestClass]
    public sealed class TypeFinderTests
    {
        [TestMethod]
        public void SourceNull()
        {
            try
            {
                new TypeFinder().FindTypes<Action>(null);
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("source", ex.ParamName);
                Assert.AreEqual(@"Value cannot be null.
Parameter name: source", ex.Message);
            }
        }

        [TestMethod]
        public void Arguments()
        {
            CollectionAssert.AreEquivalent(new[] { typeof(int), typeof(float) },
                new TypeFinder().FindTypes<Func<int, float, int>>((a, b) => 3).ToArray());
        }

        [TestMethod]
        public void ReturnValue()
        {
            CollectionAssert.AreEquivalent(new[] { typeof(int) },
                new TypeFinder().FindTypes<Func<int>>(() => 3).ToArray());
        }

        [TestMethod]
        public void CalculationsInside()
        {
            CollectionAssert.AreEquivalent(new[] { typeof(float), typeof(int) },
                new TypeFinder().FindTypes<Func<float, float>>(a => (float)((int)a)).ToArray());
        }

        [TestMethod]
        public void DuplicatesRemoved()
        {
            CollectionAssert.AreEquivalent(new[] { typeof(float) },
                new TypeFinder().FindTypes<Func<float, float, float>>((a, b) => 3.0f).ToArray());
        }

        public struct Wrapper
        {
            public int A;
        }

        [TestMethod]
        public void RecursesExpressionsInArgumentsToMethod()
        {
            Expression<Func<int, int>> expression = a => new Wrapper { A = a }.A;

            CollectionAssert.AreEquivalent(new[] { typeof(int), typeof(Wrapper) },
                new TypeFinder().FindTypes<Func<int>>(() => expression.Invoke(e => e(3))).Where(t => t.IsValueType).ToArray());
        }
    }
}
