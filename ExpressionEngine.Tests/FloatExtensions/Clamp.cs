﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionEngine.FloatExtensions.Tests
{
	[TestClass]
	public sealed class Clamp
	{
		[TestMethod]
		public void BetweenMinAndMax()
		{
			Assert.AreEqual(4.0f, 4.0f.Clamp(2.0f, 5.0f));
		}

		[TestMethod]
		public void LessThanMin()
		{
			Assert.AreEqual(2.0f, 1.0f.Clamp(2.0f, 5.0f));
		}

		[TestMethod]
		public void GreaterThanMax()
		{
			Assert.AreEqual(5.0f, 6.0f.Clamp(2.0f, 5.0f));
		}
	}
}
