﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionEngine.FloatExtensions.Tests
{
	[TestClass]
	public sealed class InverseSqrt
	{
		[TestMethod]
		public void Call()
		{
			Assert.AreEqual(0.2f, 25.0f.InverseSqrt());
		}
	}
}
