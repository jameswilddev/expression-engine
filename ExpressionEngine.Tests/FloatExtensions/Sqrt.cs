﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionEngine.FloatExtensions.Tests
{
	[TestClass]
	public sealed class Sqrt
	{
		[TestMethod]
		public void Call()
		{
			Assert.AreEqual(5.0f, 25.0f.Sqrt());
		}
	}
}
