﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionEngine.FloatExtensions.Tests
{
	[TestClass]
	public sealed class Pow
	{
		[TestMethod]
		public void Call()
		{
			Assert.AreEqual(8.0f, 2.0f.Pow(3.0f));
		}
	}
}
