﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionEngine.FloatExtensions.Tests
{
	[TestClass]
	public sealed class Mod
	{
		[TestMethod]
		public void LeftPositiveRightPositive()
		{
			Assert.AreEqual(3.0f, 7.0f.Mod(4.0f));
		}

		[TestMethod]
		public void LeftNegativeRightPositive()
		{
			Assert.AreEqual(1.0f, (-7.0f).Mod(4.0f));
		}

		[TestMethod]
		public void LeftPositiveRightNegative()
		{
			Assert.AreEqual(-1.0f, 7.0f.Mod(-4.0f));
		}

		[TestMethod]
		public void LeftNegativeRightNegative()
		{
			Assert.AreEqual(-3.0f, (-7.0f).Mod(-4.0f));
		}
	}
}
