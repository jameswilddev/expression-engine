﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionEngine.FloatExtensions.Tests
{
	[TestClass]
	public sealed class Sin
	{
		[TestMethod]
		public void Call()
		{
			Assert.AreEqual(0.0f,
				0.0f.Sin());
			Assert.AreEqual(0.70710678118654752440084436210485f,
				0.78539816339744830961566084581988f.Sin());
			Assert.AreEqual(1.0f,
				1.5707963267948966192313216916398f.Sin());
		}
	}
}
