﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionEngine.FloatExtensions.Tests
{
	[TestClass]
	public sealed class Mix
	{
		[TestMethod]
		public void Call()
		{
			Assert.AreEqual(4.5f, 3.0f.Mix(5.0f, 0.75f));
		}
	}
}
