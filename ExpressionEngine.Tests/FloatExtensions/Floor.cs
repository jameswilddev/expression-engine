﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionEngine.FloatExtensions.Tests
{
	[TestClass]
	public sealed class Floor
	{
		[TestMethod]
		public void Zero()
		{
			Assert.AreEqual(0.0f, 0.0f.Floor());
		}

		[TestMethod]
		public void LessThanOneGreaterThanZero()
		{
			Assert.AreEqual(0.0f, 0.6f.Floor());
		}

		[TestMethod]
		public void One()
		{
			Assert.AreEqual(1.0f, 1.0f.Floor());
		}

		[TestMethod]
		public void LessThanTwoGreaterThanOne()
		{
			Assert.AreEqual(1.0f, 1.6f.Floor());
		}

		[TestMethod]
		public void LessThanZeroGreaterThanNegativeOne()
		{
			Assert.AreEqual(-1.0f, (-0.4f).Floor());
		}

		[TestMethod]
		public void NegativeOne()
		{
			Assert.AreEqual(-1.0f, (-1.0f).Floor());
		}

		[TestMethod]
		public void LessThanNegativeOneGreaterThanNegativeTwo()
		{
			Assert.AreEqual(-2.0f, (-1.4f).Floor());
		}
	}
}
