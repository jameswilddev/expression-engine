﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionEngine.FloatExtensions.Tests
{
	[TestClass]
	public sealed class Fract
	{
		[TestMethod]
		public void Zero()
		{
			Assert.AreEqual(0.0f, 0.0f.Fract());
		}

		[TestMethod]
		public void LessThanOneGreaterThanZero()
		{
			Assert.AreEqual(0.6f, 0.6f.Fract());
		}

		[TestMethod]
		public void One()
		{
			Assert.AreEqual(0.0f, 1.0f.Fract());
		}

		[TestMethod]
		public void LessThanTwoGreaterThanOne()
		{
			Assert.AreEqual(0.6f, 1.6f.Fract());
		}

		[TestMethod]
		public void LessThanZeroGreaterThanNegativeOne()
		{
			Assert.AreEqual(0.6f, (-0.4f).Fract());
		}

		[TestMethod]
		public void NegativeOne()
		{
			Assert.AreEqual(0.0f, (-1.0f).Fract());
		}

		[TestMethod]
		public void LessThanNegativeOneGreaterThanNegativeTwo()
		{
			Assert.AreEqual(0.6f, (-1.4f).Fract());
		}
	}
}
