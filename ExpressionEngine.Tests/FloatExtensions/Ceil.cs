﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionEngine.FloatExtensions.Tests
{
	[TestClass]
	public sealed class Ceil
	{
		[TestMethod]
		public void Zero()
		{
			Assert.AreEqual(0.0f, 0.0f.Ceil());
		}

		[TestMethod]
		public void LessThanOneGreaterThanZero()
		{
			Assert.AreEqual(1.0f, 0.6f.Ceil());
		}

		[TestMethod]
		public void One()
		{
			Assert.AreEqual(1.0f, 1.0f.Ceil());
		}

		[TestMethod]
		public void LessThanTwoGreaterThanOne()
		{
			Assert.AreEqual(2.0f, 1.6f.Ceil());
		}

		[TestMethod]
		public void LessThanZeroGreaterThanNegativeOne()
		{
			Assert.AreEqual(0.0f, (-0.4f).Ceil());
		}

		[TestMethod]
		public void NegativeOne()
		{
			Assert.AreEqual(-1.0f, (-1.0f).Ceil());
		}

		[TestMethod]
		public void LessThanNegativeOneGreaterThanNegativeTwo()
		{
			Assert.AreEqual(-1.0f, (-1.4f).Ceil());
		}
	}
}
