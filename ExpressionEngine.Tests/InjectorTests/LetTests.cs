﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionEngine.Tests.InjectorTests
{
	[TestClass]
	public sealed class LetTests
	{
		[TestMethod]
		public void UseNullThrowsException()
		{
			try
			{
				3.Let<int, int>(null);
				Assert.Fail();
			}
			catch (ArgumentNullException ex)
			{
				Assert.AreEqual("use", ex.ParamName);
				Assert.AreEqual(@"Value cannot be null.
Parameter name: use", ex.Message);
			}
		}

		[TestMethod]
		public void Basic()
		{
			Assert.AreEqual(7, (3 * 4).Let(x => x - 5));
		}
	}
}
