﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq.Expressions;

namespace ExpressionEngine.Tests.InjectorTests
{
	[TestClass]
	public sealed class InvokeTests
	{
		[TestMethod]
		public void ExpressionNullThrowsException()
		{
			try
			{
				Injector.Invoke<Func<int>, int>(null, x => x());
				Assert.Fail();
			}
			catch (ArgumentNullException ex)
			{
				Assert.AreEqual("expression", ex.ParamName);
				Assert.AreEqual(@"Value cannot be null.
Parameter name: expression", ex.Message);
			}
		}

		[TestMethod]
		public void InvocationNullThrowsException()
		{
			try
			{
				Injector.Invoke<Func<int>, int>(() => 3, null);
				Assert.Fail();
			}
			catch (ArgumentNullException ex)
			{
				Assert.AreEqual("invocation", ex.ParamName);
				Assert.AreEqual(@"Value cannot be null.
Parameter name: invocation", ex.Message);
			}
		}

		[TestMethod]
		public void InvocationNotCallingExpressionThrowsException()
		{
			try
			{
				Injector.Invoke<Func<int>, int>(() => 3, x => 4);
				Assert.Fail();
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Assert.AreEqual("invocation", ex.ParamName);
				Assert.AreEqual(@"Must be an invocation of the expression
Parameter name: invocation", ex.Message);
			}
		}

		[TestMethod]
		public void Basic()
		{
			Expression<Func<int, int>> expression = e => e * e;
			Assert.AreEqual(64, expression.Invoke(e => e(8)));
		}

		[TestMethod]
		public void TwoLevelInvocation()
		{
			Expression<Func<int, int>> expressionA = e => e * e;
			Expression<Func<int, int>> expressionB = e => e + e;
			Assert.AreEqual(256, expressionA.Invoke(e => e(expressionB.Invoke(f => f(8)))));
		}

        [TestMethod]
        public void MultipleParameters()
        {
            Expression<Func<float, float, float>> expression = (e, f) => e * f;
            Expression<Func<float>> expression2 = () => expression.Invoke(e => e(8.0f, 5.0f));
            Assert.AreEqual(40.0f, expression2.Compile()());
        }
	}
}
