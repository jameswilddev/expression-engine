﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq.Expressions;

namespace ExpressionEngine.Tests.InjectorTests
{
    [TestClass]
    public sealed class InjectTests
    {
        private static readonly Expression<Func<int>> InjectA = () => 3;
        private static readonly Expression<Func<int>> InjectB = () => 7;
        private static readonly Expression<Func<int, int, int>> InjectC = (a, b) => a * b + 4;

        [TestMethod]
        public void IntoNullThrowsException()
        {
            try
            {
                Injector.Inject<Func<int>, Func<int>>(InjectA, null);
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("into", ex.ParamName);
                Assert.AreEqual(@"Value cannot be null.
Parameter name: into", ex.Message);
            }
        }

        [TestMethod]
        public void InjectNullThrowsException()
        {
            try
            {
                Injector.Inject<Func<int>, Func<int>>(null, inj => () => inj() * 4);
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("inject", ex.ParamName);
                Assert.AreEqual(@"Value cannot be null.
Parameter name: inject", ex.Message);
            }
        }

        [TestMethod]
        public void IntoReturnsNullThrowsException()
        {
            try
            {
                Injector.Inject<Func<int>, Func<int>>(InjectA, inj => null);
                Assert.Fail();
            }
            catch (InvalidOperationException ex)
            {
                Assert.AreEqual(@"into should not return null", ex.Message);
            }
        }

        [TestMethod]
        public void InvokingMockThrowsException()
        {
            try
            {
                var result = Injector.Inject<Func<int>, Func<int>>(InjectA, inj =>
                {
                    inj();
                    return null;
                });
                Assert.Fail();
            }
            catch (InvalidOperationException ex)
            {
                Assert.AreEqual(@"This delegate should be used as a mock, and should never be invoked outside of the expression tree being injected into", ex.Message);
            }
        }

        [TestMethod]
        public void Inject()
        {
            var result = Injector.Inject<Func<int>, Func<int>>(InjectA, inj => () => inj() * 4);
            Assert.IsNotNull(result);
            var del = result.Compile();
            Assert.AreEqual(12, del());
        }

        [TestMethod]
        public void InjectReuse()
        {
            var result = Injector.Inject<Func<int>, Func<int>>(InjectA, inj => () => inj() + inj());
            Assert.IsNotNull(result);
            var del = result.Compile();
            Assert.AreEqual(6, del());
        }

        [TestMethod]
        public void InjectTwoSeparateExpressionTrees()
        {
            var result = Injector.Inject<Func<int>, Func<int>>(InjectA, injA =>
                Injector.Inject<Func<int>, Func<int>>(InjectB, injB =>
                () => injA() + injB()));
            Assert.IsNotNull(result);
            var del = result.Compile();
            Assert.AreEqual(10, del());
        }

        [TestMethod]
        public void MultipleParameters()
        {
            var result = Injector.Inject<Func<int>, Func<int, int, int>>(InjectC, inj =>
                () => inj(4, 3) - 5);
            Assert.IsNotNull(result);
            var del = result.Compile();
            Assert.AreEqual(11, del());
        }

        [TestMethod]
        public void MultipleParametersReused()
        {
            var result = Injector.Inject<Func<int>, Func<int, int, int>>(InjectC, inj =>
                () => inj(4, 3) - inj(8, 9));
            Assert.IsNotNull(result);
            var del = result.Compile();
            Assert.AreEqual(-60, del());
        }

        [TestMethod]
        public void NeverUsed()
        {
            var result = Injector.Inject<Func<int>, Func<int>>(InjectA, inj =>
                () => 2);
            Assert.IsNotNull(result);
            var del = result.Compile();
            Assert.AreEqual(2, del());
        }

        private sealed class InstanceContainer
        {
            internal Expression<Func<int>> Inject(Func<int> inj)
            {
                return () => inj() + 15;
            }
        }

        [TestMethod]
        public void InstanceMethodDelegate()
        {
            var result = Injector.Inject<Func<int>, Func<int>>(InjectA, new InstanceContainer().Inject);
            Assert.IsNotNull(result);
            var del = result.Compile();
            Assert.AreEqual(18, del());
        }

        private static Expression<Func<int>> Inject(Func<int> inj)
        {
            return () => inj() + 15;
        }

        [TestMethod]
        public void StaticMethodDelegate()
        {
            var result = Injector.Inject<Func<int>, Func<int>>(InjectA, Inject);
            Assert.IsNotNull(result);
            var del = result.Compile();
            Assert.AreEqual(18, del());
        }
    }
}
