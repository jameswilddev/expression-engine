﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionEngine.IntExtensions.Tests
{
	[TestClass]
	public sealed class Clamp
	{
		[TestMethod]
		public void BetweenMinAndMax()
		{
			Assert.AreEqual(4, 4.Clamp(2, 5));
		}

		[TestMethod]
		public void LessThanMin()
		{
			Assert.AreEqual(2, 1.Clamp(2, 5));
		}

		[TestMethod]
		public void GreaterThanMax()
		{
			Assert.AreEqual(5, 6.Clamp(2, 5));
		}
	}
}
