﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.Vector3Extensions.Tests
{
	[TestClass]
	public sealed class Mix
	{
		[TestMethod]
		public void Call()
		{
			Assert.AreEqual(
				new Vector3(160.0f, 34.0f, 40.0f),
				new Vector3(100.0f, 32.0f, 35.0f)
				.Mix(
					new Vector3(180.0f, 40.0f, 45.0f),
					new Vector3(0.75f, 0.25f, 0.5f)
				));
		}
	}
}
