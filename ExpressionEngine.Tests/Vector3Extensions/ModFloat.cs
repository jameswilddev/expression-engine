﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.Vector3Extensions.Tests
{
	[TestClass]
	public sealed class ModFloat
	{
		[TestMethod]
		public void LeftPositiveRightPositive()
		{
			Assert.AreEqual(new Vector3(3.0f, 2.9f, 2.1f),
				new Vector3(7.0f, 6.9f, 6.1f).Mod(4.0f));
		}

		[TestMethod]
		public void LeftNegativeRightPositive()
		{
			var result = new Vector3(-7.0f, -6.9f, -6.1f).Mod(4.0f);
			Assert.AreEqual(result.X, 1.0f, 0.001f);
			Assert.AreEqual(result.Y, 1.1f, 0.001f);
			Assert.AreEqual(result.Z, 1.9f, 0.001f);
		}

		[TestMethod]
		public void LeftPositiveRightNegative()
		{
			var result = new Vector3(7.0f, 6.9f, 6.1f).Mod(-4.0f);
			Assert.AreEqual(result.X, -1.0f, 0.001f);
			Assert.AreEqual(result.Y, -1.1f, 0.001f);
			Assert.AreEqual(result.Z, -1.9f, 0.001f);
		}

		[TestMethod]
		public void LeftNegativeRightNegative()
		{
			Assert.AreEqual(new Vector3(-3.0f, -2.9f, -2.1f),
				new Vector3(-7.0f, -6.9f, -6.1f).Mod(-4.0f));
		}
	}
}
