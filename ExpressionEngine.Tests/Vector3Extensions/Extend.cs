﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.Vector3Extensions.Tests
{
    [TestClass]
    public sealed class Extend
    {
        [TestMethod]
        public void ToVector4()
        {
            Assert.AreEqual(new Vector4(3.0f, 7.0f, 4.0f, 2.0f), new Vector3(3.0f, 7.0f, 4.0f).Extend(2.0f));
        }
    }
}
