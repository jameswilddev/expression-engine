﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;
using System.Linq.Expressions;

namespace ExpressionEngine.Vector3Extensions.Tests
{
    [TestClass]
    public sealed class SwizzleVec3
    {
        [TestMethod]
        public void Reorder()
        {
            Assert.AreEqual(new Vector3 { X = 0.7f, Y = 0.4f, Z = 0.2f }, new Vector3 { X = 0.4f, Y = 0.7f, Z = 0.2f }.Swizzle(v => v.Y, v => v.X, v => v.Z));
        }

        [TestMethod]
        public void Repeat()
        {
            Assert.AreEqual(new Vector3 { X = 0.7f, Y = 0.7f, Z = 0.2f }, new Vector3 { X = 0.4f, Y = 0.7f, Z = 0.2f }.Swizzle(v => v.Y, v => v.Y, v => v.Z));
        }

        [TestMethod]
        public void XNullThrowsException()
        {
            try
            {
                new Vector3 { X = 0.4f, Y = 0.7f }.Swizzle(null, v => v.Y, v => v.Y);
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("x", ex.ParamName);
                Assert.AreEqual(@"Value cannot be null.
Parameter name: x", ex.Message);
            }
        }

        [TestMethod]
        public void YNullThrowsException()
        {
            try
            {
                new Vector3 { X = 0.4f, Y = 0.7f }.Swizzle(v => v.Y, null, v => v.Y);
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("y", ex.ParamName);
                Assert.AreEqual(@"Value cannot be null.
Parameter name: y", ex.Message);
            }
        }

        [TestMethod]
        public void ZNullThrowsException()
        {
            try
            {
                new Vector3 { X = 0.4f, Y = 0.7f }.Swizzle(v => v.Y, v => v.Y, null);
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("z", ex.ParamName);
                Assert.AreEqual(@"Value cannot be null.
Parameter name: z", ex.Message);
            }
        }

        [TestMethod]
        public void XNonMemberSelect()
        {
            try
            {
                new Vector3 { X = 0.4f, Y = 0.7f, Z = 0.2f }.Swizzle(v => 3.0f, v => v.Y, v => v.Z);
                Assert.Fail();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Assert.AreEqual("x", ex.ParamName);
                Assert.AreEqual(@"Swizzles should only select a member from the input vector
Parameter name: x", ex.Message);
            }
        }

        [TestMethod]
        public void YNonMemberSelect()
        {
            try
            {
                new Vector3 { X = 0.4f, Y = 0.7f, Z = 0.2f }.Swizzle(v => v.X, v => 3.0f, v => v.Z);
                Assert.Fail();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Assert.AreEqual("y", ex.ParamName);
                Assert.AreEqual(@"Swizzles should only select a member from the input vector
Parameter name: y", ex.Message);
            }
        }

        [TestMethod]
        public void ZNonMemberSelect()
        {
            try
            {
                new Vector3 { X = 0.4f, Y = 0.7f, Z = 0.2f }.Swizzle(v => v.X, v => v.Y, v => 3.0f);
                Assert.Fail();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Assert.AreEqual("z", ex.ParamName);
                Assert.AreEqual(@"Swizzles should only select a member from the input vector
Parameter name: z", ex.Message);
            }
        }
    }
}
