﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.Vector3Extensions.Tests
{
	[TestClass]
	public sealed class ClampVector3
	{
		#region X
		[TestMethod]
		public void XBetweenMinAndMax()
		{
			Assert.AreEqual(4.0f, new Vector3 { X = 4.0f }.Clamp(new Vector3(2.0f), new Vector3(5.0f)).X);
		}

		[TestMethod]
		public void XLessThanMin()
		{
			Assert.AreEqual(2.0f, new Vector3 { X = 1.0f }.Clamp(new Vector3(2.0f), new Vector3(5.0f)).X);
		}

		[TestMethod]
		public void XGreaterThanMax()
		{
			Assert.AreEqual(5.0f, new Vector3 { X = 6.0f }.Clamp(new Vector3(2.0f), new Vector3(5.0f)).X);
		}
		#endregion

		#region Y
		[TestMethod]
		public void YBetweenMinAndMax()
		{
			Assert.AreEqual(4.0f, new Vector3 { Y = 4.0f }.Clamp(new Vector3(2.0f), new Vector3(5.0f)).Y);
		}

		[TestMethod]
		public void YLessThanMin()
		{
			Assert.AreEqual(2.0f, new Vector3 { Y = 1.0f }.Clamp(new Vector3(2.0f), new Vector3(5.0f)).Y);
		}

		[TestMethod]
		public void YGreaterThanMax()
		{
			Assert.AreEqual(5.0f, new Vector3 { Y = 6.0f }.Clamp(new Vector3(2.0f), new Vector3(5.0f)).Y);
		}
		#endregion

		#region Z
		[TestMethod]
		public void ZBetweenMinAndMax()
		{
			Assert.AreEqual(4.0f, new Vector3 { Z = 4.0f }.Clamp(new Vector3(2.0f), new Vector3(5.0f)).Z);
		}

		[TestMethod]
		public void ZLessThanMin()
		{
			Assert.AreEqual(2.0f, new Vector3 { Z = 1.0f }.Clamp(new Vector3(2.0f), new Vector3(5.0f)).Z);
		}

		[TestMethod]
		public void ZGreaterThanMax()
		{
			Assert.AreEqual(5.0f, new Vector3 { Z = 6.0f }.Clamp(new Vector3(2.0f), new Vector3(5.0f)).Z);
		}
		#endregion
	}
}
