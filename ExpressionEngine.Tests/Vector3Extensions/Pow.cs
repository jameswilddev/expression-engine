﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.Vector3Extensions.Tests
{
	[TestClass]
	public sealed class Pow
	{
		[TestMethod]
		public void Call()
		{
			Assert.AreEqual(
				new Vector3(27.0f, 64.0f, 625.0f),
				new Vector3(3.0f, 8.0f, 5.0f)
				.Pow(new Vector3(3.0f, 2.0f, 4.0f)));
		}
	}
}
