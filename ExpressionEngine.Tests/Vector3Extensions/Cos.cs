﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.Vector3Extensions.Tests
{
	[TestClass]
	public sealed class Cos
	{
		[TestMethod]
		public void X()
		{
			Assert.AreEqual(1.0f,
				new Vector3 { X = 0.0f }.Cos().X);
			Assert.AreEqual(0.70710678118654752440084436210485f,
				new Vector3 { X = 0.78539816339744830961566084581988f }.Cos().X);
			Assert.AreEqual(0.0f,
				new Vector3 { X = 1.5707963267948966192313216916398f }.Cos().X, 0.001f);
		}

		[TestMethod]
		public void Y()
		{
			Assert.AreEqual(1.0f,
				new Vector3 { Y = 0.0f }.Cos().Y);
			Assert.AreEqual(0.70710678118654752440084436210485f,
				new Vector3 { Y = 0.78539816339744830961566084581988f }.Cos().Y);
			Assert.AreEqual(0.0f,
				new Vector3 { Y = 1.5707963267948966192313216916398f }.Cos().Y, 0.001f);
		}

		[TestMethod]
		public void Z()
		{
			Assert.AreEqual(1.0f,
				new Vector3 { Z = 0.0f }.Cos().Z);
			Assert.AreEqual(0.70710678118654752440084436210485f,
				new Vector3 { Z = 0.78539816339744830961566084581988f }.Cos().Z);
			Assert.AreEqual(0.0f,
				new Vector3 { Z = 1.5707963267948966192313216916398f }.Cos().Z, 0.001f);
		}
	}
}
