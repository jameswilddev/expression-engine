﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.Vector3Extensions.Tests
{
	[TestClass]
	public sealed class InverseSqrt
	{
		[TestMethod]
		public void Call()
		{
			Assert.AreEqual(new Vector3(0.2f, 0.125f, 0.5f), 
				new Vector3(25.0f, 64.0f, 4.0f).InverseSqrt());
		}
	}
}
