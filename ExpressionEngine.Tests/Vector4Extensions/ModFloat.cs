﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.Vector4Extensions.Tests
{
	[TestClass]
	public sealed class ModFloat
	{
		[TestMethod]
		public void LeftPositiveRightPositive()
		{
			Assert.AreEqual(new Vector4(3.0f, 2.9f, 2.1f, 2.4f),
				new Vector4(7.0f, 6.9f, 6.1f, 6.4f).Mod(4.0f));
		}

		[TestMethod]
		public void LeftNegativeRightPositive()
		{
			var result = new Vector4(-7.0f, -6.9f, -6.1f, -6.4f).Mod(4.0f);
			Assert.AreEqual(result.X, 1.0f, 0.001f);
			Assert.AreEqual(result.Y, 1.1f, 0.001f);
			Assert.AreEqual(result.Z, 1.9f, 0.001f);
			Assert.AreEqual(result.W, 1.6f, 0.001f);
		}

		[TestMethod]
		public void LeftPositiveRightNegative()
		{
			var result = new Vector4(7.0f, 6.9f, 6.1f, 6.4f).Mod(-4.0f);
			Assert.AreEqual(result.X, -1.0f, 0.001f);
			Assert.AreEqual(result.Y, -1.1f, 0.001f);
			Assert.AreEqual(result.Z, -1.9f, 0.001f);
			Assert.AreEqual(result.W, -1.6f, 0.001f);
		}

		[TestMethod]
		public void LeftNegativeRightNegative()
		{
			Assert.AreEqual(new Vector4(-3.0f, -2.9f, -2.1f, -2.4f),
				new Vector4(-7.0f, -6.9f, -6.1f, -6.4f).Mod(-4.0f));
		}
	}
}
