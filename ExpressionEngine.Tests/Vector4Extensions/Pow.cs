﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.Vector4Extensions.Tests
{
	[TestClass]
	public sealed class Pow
	{
		[TestMethod]
		public void Call()
		{
			Assert.AreEqual(
				new Vector4(27.0f, 64.0f, 625.0f, 1.0f),
				new Vector4(3.0f, 8.0f, 5.0f, 66.0f)
				.Pow(new Vector4(3.0f, 2.0f, 4.0f, 0.0f)));
		}
	}
}
