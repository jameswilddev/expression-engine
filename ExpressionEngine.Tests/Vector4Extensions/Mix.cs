﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.Vector4Extensions.Tests
{
	[TestClass]
	public sealed class Mix
	{
		[TestMethod]
		public void Call()
		{
			Assert.AreEqual(
				new Vector4(160.0f, 34.0f, 40.0f, 110.0f),
				new Vector4(100.0f, 32.0f, 35.0f, 100.0f)
				.Mix(
					new Vector4(180.0f, 40.0f, 45.0f, 200.0f),
					new Vector4(0.75f, 0.25f, 0.5f, 0.1f)
				));
		}
	}
}
