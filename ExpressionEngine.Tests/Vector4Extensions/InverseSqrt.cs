﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.Vector4Extensions.Tests
{
	[TestClass]
	public sealed class InverseSqrt
	{
		[TestMethod]
		public void Call()
		{
			Assert.AreEqual(new Vector4(0.2f, 0.125f, 0.5f, 0.25f), new Vector4(25.0f, 64.0f, 4.0f, 16.0f).InverseSqrt());
		}
	}
}
