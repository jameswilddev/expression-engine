﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.Vector4Extensions.Tests
{
	[TestClass]
	public sealed class ModVector4
	{
		[TestMethod]
		public void LeftPositiveRightPositive()
		{
			var result = new Vector4(7.0f, 13.9f, 4.1f, 3.4f)
				.Mod(new Vector4(4.0f, 6.0f, 3.0f, 2.0f));
			Assert.AreEqual(result.X, 3.0f, 0.001f);
			Assert.AreEqual(result.Y, 1.9f, 0.001f);
			Assert.AreEqual(result.Z, 1.1f, 0.001f);
			Assert.AreEqual(result.W, 1.4f, 0.001f);
		}

		[TestMethod]
		public void LeftNegativeRightPositive()
		{
			var result = new Vector4(-7.0f, -13.9f, -4.1f, -3.4f)
				.Mod(new Vector4(4.0f, 6.0f, 3.0f, 2.0f));
			Assert.AreEqual(result.X, 1.0f, 0.001f);
			Assert.AreEqual(result.Y, 4.1f, 0.001f);
			Assert.AreEqual(result.Z, 1.9f, 0.001f);
			Assert.AreEqual(result.W, 0.6f, 0.001f);
		}

		[TestMethod]
		public void LeftPositiveRightNegative()
		{
			var result = new Vector4(7.0f, 13.9f, 4.1f, 3.4f)
				.Mod(new Vector4(-4.0f, -6.0f, -3.0f, -2.0f));
			Assert.AreEqual(result.X, -1.0f, 0.001f);
			Assert.AreEqual(result.Y, -4.1f, 0.001f);
			Assert.AreEqual(result.Z, -1.9f, 0.001f);
			Assert.AreEqual(result.W, -0.6f, 0.001f);
		}

		[TestMethod]
		public void LeftNegativeRightNegative()
		{
			var result = new Vector4(-7.0f, -13.9f, -4.1f, -3.4f)
				.Mod(new Vector4(-4.0f, -6.0f, -3.0f, -2.0f));
			Assert.AreEqual(result.X, -3.0f, 0.001f);
			Assert.AreEqual(result.Y, -1.9f, 0.001f);
			Assert.AreEqual(result.Z, -1.1f, 0.001f);
			Assert.AreEqual(result.W, -1.4f, 0.001f);
		}
	}
}
