﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.Vector4Extensions.Tests
{
	[TestClass]
	public sealed class Reflect
	{
		[TestMethod]
		public void Call()
		{
			var result = new Vector4(-3.28221f, 2.81872f, 0.341955f, -0.393624f)
			.Reflect(new Vector4(-0.59644f, 0.0555539f, -0.236268f, 0.765082f));

			Assert.AreEqual(-1.215802f, result.X, 0.0001f);
			Assert.AreEqual(2.62625f, result.Y, 0.0001f);
			Assert.AreEqual(1.160522f, result.Z, 0.0001f);
			Assert.AreEqual(-3.044304f, result.W, 0.0001f);
		}
	}
}
