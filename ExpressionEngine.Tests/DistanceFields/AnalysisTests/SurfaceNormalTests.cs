﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.DistanceFields;
using System.Numerics;

namespace ExpressionEngine.Tests.DistanceFields.AnalysisTests
{
	[TestClass]
	public sealed class SurfaceNormalTests
	{
		[TestMethod]
		public void DistanceFieldNullThrowsException()
		{
			try
			{
				Analysis.SurfaceNormal(null);
				Assert.Fail();
			}
			catch (ArgumentNullException ex)
			{
				Assert.AreEqual("distanceField", ex.ParamName);
				Assert.AreEqual(@"Value cannot be null.
Parameter name: distanceField", ex.Message);
			}
		}

		[TestMethod]
		public void Call()
		{
			var normal = Primitives.Sphere(() => 4.5f, () => new Vector3(3.0f, 7.0f, 9.0f)).SurfaceNormal().Compile()(new Vector3(7.184914994777494105165689827862f, 7.5231143743471867631457112284827f, 10.569343123041560289437133685448f));

			Assert.AreEqual(0.92998110995055424559237551730267f, normal.X, 0.001f);
			Assert.AreEqual(0.11624763874381928069904693966283f, normal.Y, 0.001f);
			Assert.AreEqual(0.3487429162314578420971408189885f, normal.Z, 0.001f);
		}
	}
}
