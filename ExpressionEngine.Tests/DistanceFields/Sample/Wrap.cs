﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.DistanceFields.Tests.SampleTests
{
    [TestClass]
    public sealed class Wrap
    {
        [TestMethod]
        public void DistanceNullThrowsException()
        {
            try
            {
                Sample.Wrap(null, () => 3.0f);
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("distance", ex.ParamName);
                Assert.AreEqual(@"Value cannot be null.
Parameter name: distance", ex.Message);
            }
        }

        [TestMethod]
        public void PayloadNullThrowsException()
        {
            try
            {
                Sample.Wrap<float>(at => 3.0f, null);
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("payload", ex.ParamName);
                Assert.AreEqual(@"Value cannot be null.
Parameter name: payload", ex.Message);
            }
        }

        [TestMethod]
        public void Successful()
        {
            Assert.AreEqual(new Sample<float> { Distance = 585.0f, Payload = 5.0f },
                Sample.Wrap(at => (at * 3.0f).LengthSquared(), () => 5.0f).Compile()(new Vector3(5.0f, 6.0f, 2.0f)));
        }
    }
}
