﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionEngine.DistanceFields.Tests.SampleTests
{
    [TestClass]
    public sealed class ToString
    {
        [TestMethod]
        public void Call()
        {
            Assert.AreEqual("Sample<Single>{ Distance: 0.3, Payload: 0.5 }", new Sample<float> { Distance = 0.3f, Payload = 0.5f }.ToString());
        }
    }
}
