﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ExpressionEngine.Tests.DistanceFields.CSG
{
    [TestClass]
    public sealed class Difference
    {
        private static readonly Vector3 At = new Vector3(6.0f, 4.0f, 7.0f);

        [TestMethod]
        public void OutsideBothFurtherFromLeft()
        {
            Assert.AreEqual(3.0f, ExpressionEngine.DistanceFields.CSG.Difference
                (
                    at => at == At ? 3.0f : 0.0f,
                    at => at == At ? -2.0f : 0.0f
                ).Compile()(At));
        }

        [TestMethod]
        public void OutsideBothFurtherFromRight()
        {
            Assert.AreEqual(3.0f, ExpressionEngine.DistanceFields.CSG.Difference
                (
                    at => at == At ? 2.0f : 0.0f,
                    at => at == At ? -3.0f : 0.0f
                ).Compile()(At));
        }

        [TestMethod]
        public void InsideLeftOutsideRight()
        {
            Assert.AreEqual(2.0f, ExpressionEngine.DistanceFields.CSG.Difference
                (
                    at => at == At ? -0.5f : 0.0f,
                    at => at == At ? -2.0f : 0.0f
                ).Compile()(At));
        }

        [TestMethod]
        public void InsideRightOutsideLeft()
        {
            Assert.AreEqual(2.0f, ExpressionEngine.DistanceFields.CSG.Difference
                (
                    at => at == At ? 2.0f : 0.0f,
                    at => at == At ? 0.5f : 0.0f
                ).Compile()(At));
        }

        [TestMethod]
        public void InsideBothCloserToLeft()
        {
            Assert.AreEqual(1.0f, ExpressionEngine.DistanceFields.CSG.Difference
                (
                    at => at == At ? -2.0f : 0.0f,
                    at => at == At ? -1.0f : 0.0f
                ).Compile()(At));
        }

        [TestMethod]
        public void InsideBothCloserToRight()
        {
            Assert.AreEqual(2.0f, ExpressionEngine.DistanceFields.CSG.Difference
                (
                    at => at == At ? -1.0f : 0.0f,
                    at => at == At ? -2.0f : 0.0f
                ).Compile()(At));
        }
    }
}
