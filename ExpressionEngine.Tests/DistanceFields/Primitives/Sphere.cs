﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Tests.DistanceFields
{
	[TestClass]
	public sealed class Sphere
	{
		[TestMethod]
		public void AboveSurface()
		{
            
			Assert.AreEqual(
				3.43398f,
                ExpressionEngine.DistanceFields.Primitives.Sphere(() => 6.0f, () => new Vector3(4.0f, 8.0f, -3.0f)).Compile()(new Vector3(8.0f, 11.0f, 5.0f)),
				0.0001f
			);
		}

		[TestMethod]
		public void BelowSurface()
		{
            Assert.AreEqual(
                -0.55051f,
                ExpressionEngine.DistanceFields.Primitives.Sphere(() => 3.0f, () => new Vector3(4.0f, 8.0f, -3.0f)).Compile()(new Vector3(2.0f, 7.0f, -2.0f)),
                0.0001f
            );
		}
	}
}
