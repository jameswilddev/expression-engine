﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using ExpressionEngine.Simulations;
using System.ComponentModel.Composition;

namespace ExpressionEngine.Tests.Simulations
{
    [TestClass]
    public sealed class IntervalTests
    {
        public interface IRenderable
        {
            void Render(float progress);
        }

        public interface ITickable
        {
            void Tick();
        }

        #region No subscribers

        [TestMethod]
        public void NoUpdatesNoSubscribers()
        {
            var mocks = new MockRepository();
            var interval = new Interval
            {
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Runtime = mocks.StrictMock<IRuntime>()
            };

            using (mocks.Record())
            using (mocks.Ordered())
            {
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 05)).Repeat.Once();
            }

            using (mocks.Playback())
            {
                interval.OnImportsSatisfied();
            }
        }

        [TestMethod]
        public void UpdateLessThanOneTickNoSubscribers()
        {
            var mocks = new MockRepository();
            var interval = new Interval
            {
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Runtime = mocks.StrictMock<IRuntime>()
            };

            interval.Configuration.Stub(c => c.TickInterval).Return(TimeSpan.FromSeconds(5));

            using (mocks.Record())
            using (mocks.Ordered())
            {
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 05)).Repeat.Once();
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 06)).Repeat.Once();
            }

            using (mocks.Playback())
            {
                interval.OnImportsSatisfied();
                interval.Update();
            }
        }

        [TestMethod]
        public void UpdateTwiceLessThanOneTickNoSubscribers()
        {
            var mocks = new MockRepository();
            var interval = new Interval
            {
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Runtime = mocks.StrictMock<IRuntime>()
            };

            interval.Configuration.Stub(c => c.TickInterval).Return(TimeSpan.FromSeconds(5));

            using (mocks.Record())
            using (mocks.Ordered())
            {
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 05)).Repeat.Once();
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 06)).Repeat.Once();
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 07)).Repeat.Once();
            }

            using (mocks.Playback())
            {
                interval.OnImportsSatisfied();
                interval.Update();
                interval.Update();
            }
        }

        [TestMethod]
        public void UpdateTwiceAddingUpToOneTickNoSubscribers()
        {
            var mocks = new MockRepository();
            var interval = new Interval
            {
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Runtime = mocks.StrictMock<IRuntime>()
            };

            interval.Configuration.Stub(c => c.TickInterval).Return(TimeSpan.FromSeconds(5));

            using (mocks.Record())
            using (mocks.Ordered())
            {
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 05)).Repeat.Once();
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 08)).Repeat.Once();
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 11)).Repeat.Once();
            }

            using (mocks.Playback())
            {
                interval.OnImportsSatisfied();
                interval.Update();
                interval.Update();
            }
        }

        [TestMethod]
        public void UpdateOnceOneTickNoSubscribers()
        {
            var mocks = new MockRepository();
            var interval = new Interval
            {
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Runtime = mocks.StrictMock<IRuntime>()
            };

            interval.Configuration.Stub(c => c.TickInterval).Return(TimeSpan.FromSeconds(5));

            using (mocks.Record())
            using (mocks.Ordered())
            {
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 05)).Repeat.Once();
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 11)).Repeat.Once();
            }

            using (mocks.Playback())
            {
                interval.OnImportsSatisfied();
                interval.Update();
            }
        }

        [TestMethod]
        public void UpdateOnceTwoTicksNoSubscribers()
        {
            var mocks = new MockRepository();
            var interval = new Interval
            {
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Runtime = mocks.StrictMock<IRuntime>()
            };

            interval.Configuration.Stub(c => c.TickInterval).Return(TimeSpan.FromSeconds(5));

            using (mocks.Record())
            using (mocks.Ordered())
            {
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 05)).Repeat.Once();
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 16)).Repeat.Once();
            }

            using (mocks.Playback())
            {
                interval.OnImportsSatisfied();
                interval.Update();
            }
        }

        #endregion

        #region Render subscribers only

        [TestMethod]
        public void NoUpdatesRenderOnly()
        {
            var mocks = new MockRepository();
            var interval = new Interval
            {
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Runtime = mocks.StrictMock<IRuntime>()
            };
            var renderA = (RenderDelegate)mocks.StrictMock<IRenderable>().Render;
            var renderB = (RenderDelegate)mocks.StrictMock<IRenderable>().Render;
            interval.Render += renderA;
            interval.Render += renderB;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 05)).Repeat.Once();
            }

            using (mocks.Playback())
            {
                interval.OnImportsSatisfied();
            }
        }

        [TestMethod]
        public void UpdateLessThanOneTickRenderOnly()
        {
            var mocks = new MockRepository();
            var interval = new Interval
            {
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Runtime = mocks.StrictMock<IRuntime>()
            };
            var renderA = (RenderDelegate)mocks.StrictMock<IRenderable>().Render;
            var renderB = (RenderDelegate)mocks.StrictMock<IRenderable>().Render;
            interval.Render += renderA;
            interval.Render += renderB;

            interval.Configuration.Stub(c => c.TickInterval).Return(TimeSpan.FromSeconds(5));

            using (mocks.Record())
            using (mocks.Ordered())
            {
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 05)).Repeat.Once();
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 06)).Repeat.Once();
                using (mocks.Unordered())
                {
                    renderA(0.2f);
                    renderB(0.2f);
                }
            }

            using (mocks.Playback())
            {
                interval.OnImportsSatisfied();
                interval.Update();
            }
        }

        [TestMethod]
        public void UpdateTwiceLessThanOneTickRenderOnly()
        {
            var mocks = new MockRepository();
            var interval = new Interval
            {
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Runtime = mocks.StrictMock<IRuntime>()
            };
            var renderA = (RenderDelegate)mocks.StrictMock<IRenderable>().Render;
            var renderB = (RenderDelegate)mocks.StrictMock<IRenderable>().Render;
            interval.Render += renderA;
            interval.Render += renderB;

            interval.Configuration.Stub(c => c.TickInterval).Return(TimeSpan.FromSeconds(5));

            using (mocks.Record())
            using (mocks.Ordered())
            {
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 05)).Repeat.Once();
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 06)).Repeat.Once();
                using (mocks.Unordered())
                {
                    renderA(0.2f);
                    renderB(0.2f);
                }
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 07)).Repeat.Once();
                using (mocks.Unordered())
                {
                    renderA(0.4f);
                    renderB(0.4f);
                }
            }

            using (mocks.Playback())
            {
                interval.OnImportsSatisfied();
                interval.Update();
                interval.Update();
            }
        }

        [TestMethod]
        public void UpdateTwiceAddingUpToOneTickRenderOnly()
        {
            var mocks = new MockRepository();
            var interval = new Interval
            {
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Runtime = mocks.StrictMock<IRuntime>()
            };
            var renderA = (RenderDelegate)mocks.StrictMock<IRenderable>().Render;
            var renderB = (RenderDelegate)mocks.StrictMock<IRenderable>().Render;
            interval.Render += renderA;
            interval.Render += renderB;

            interval.Configuration.Stub(c => c.TickInterval).Return(TimeSpan.FromSeconds(5));

            using (mocks.Record())
            using (mocks.Ordered())
            {
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 05)).Repeat.Once();
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 08)).Repeat.Once();
                using (mocks.Unordered())
                {
                    renderA(0.6f);
                    renderB(0.6f);
                }
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 11)).Repeat.Once();
                using (mocks.Unordered())
                {
                    renderA(0.2f);
                    renderB(0.2f);
                }
            }

            using (mocks.Playback())
            {
                interval.OnImportsSatisfied();
                interval.Update();
                interval.Update();
            }
        }

        [TestMethod]
        public void UpdateOnceOneTickRenderOnly()
        {
            var mocks = new MockRepository();
            var interval = new Interval
            {
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Runtime = mocks.StrictMock<IRuntime>()
            };
            var renderA = (RenderDelegate)mocks.StrictMock<IRenderable>().Render;
            var renderB = (RenderDelegate)mocks.StrictMock<IRenderable>().Render;
            interval.Render += renderA;
            interval.Render += renderB;

            interval.Configuration.Stub(c => c.TickInterval).Return(TimeSpan.FromSeconds(5));

            using (mocks.Record())
            using (mocks.Ordered())
            {
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 05)).Repeat.Once();
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 11)).Repeat.Once();
                using (mocks.Unordered())
                {
                    renderA(0.2f);
                    renderB(0.2f);
                }
            }

            using (mocks.Playback())
            {
                interval.OnImportsSatisfied();
                interval.Update();
            }
        }

        [TestMethod]
        public void UpdateOnceTwoTicksRenderOnly()
        {
            var mocks = new MockRepository();
            var interval = new Interval
            {
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Runtime = mocks.StrictMock<IRuntime>()
            };
            var renderA = (RenderDelegate)mocks.StrictMock<IRenderable>().Render;
            var renderB = (RenderDelegate)mocks.StrictMock<IRenderable>().Render;
            interval.Render += renderA;
            interval.Render += renderB;

            interval.Configuration.Stub(c => c.TickInterval).Return(TimeSpan.FromSeconds(5));

            using (mocks.Record())
            using (mocks.Ordered())
            {
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 05)).Repeat.Once();
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 16)).Repeat.Once();
                using (mocks.Unordered())
                {
                    renderA(0.2f);
                    renderB(0.2f);
                }
            }

            using (mocks.Playback())
            {
                interval.OnImportsSatisfied();
                interval.Update();
            }
        }

        #endregion

        #region Render and tick subscribers

        [TestMethod]
        public void NoUpdatesRenderAndTick()
        {
            var mocks = new MockRepository();
            var interval = new Interval
            {
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Runtime = mocks.StrictMock<IRuntime>()
            };
            var tickA = (Action)mocks.StrictMock<ITickable>().Tick;
            var tickB = (Action)mocks.StrictMock<ITickable>().Tick;
            interval.Tick += tickA;
            interval.Tick += tickB;

            var renderA = (RenderDelegate)mocks.StrictMock<IRenderable>().Render;
            var renderB = (RenderDelegate)mocks.StrictMock<IRenderable>().Render;
            interval.Render += renderA;
            interval.Render += renderB;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 05)).Repeat.Once();
            }

            using (mocks.Playback())
            {
                interval.OnImportsSatisfied();
            }
        }

        [TestMethod]
        public void UpdateLessThanOneTickRenderAndTick()
        {
            var mocks = new MockRepository();
            var interval = new Interval
            {
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Runtime = mocks.StrictMock<IRuntime>()
            };
            var tickA = (Action)mocks.StrictMock<ITickable>().Tick;
            var tickB = (Action)mocks.StrictMock<ITickable>().Tick;
            interval.Tick += tickA;
            interval.Tick += tickB;

            var renderA = (RenderDelegate)mocks.StrictMock<IRenderable>().Render;
            var renderB = (RenderDelegate)mocks.StrictMock<IRenderable>().Render;
            interval.Render += renderA;
            interval.Render += renderB;

            interval.Configuration.Stub(c => c.TickInterval).Return(TimeSpan.FromSeconds(5));

            using (mocks.Record())
            using (mocks.Ordered())
            {
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 05)).Repeat.Once();
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 06)).Repeat.Once();
                using (mocks.Unordered())
                {
                    renderA(0.2f);
                    renderB(0.2f);
                }
            }

            using (mocks.Playback())
            {
                interval.OnImportsSatisfied();
                interval.Update();
            }
        }

        [TestMethod]
        public void UpdateTwiceLessThanOneTickRenderAndTick()
        {
            var mocks = new MockRepository();
            var interval = new Interval
            {
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Runtime = mocks.StrictMock<IRuntime>()
            };
            var tickA = (Action)mocks.StrictMock<ITickable>().Tick;
            var tickB = (Action)mocks.StrictMock<ITickable>().Tick;
            interval.Tick += tickA;
            interval.Tick += tickB;

            var renderA = (RenderDelegate)mocks.StrictMock<IRenderable>().Render;
            var renderB = (RenderDelegate)mocks.StrictMock<IRenderable>().Render;
            interval.Render += renderA;
            interval.Render += renderB;

            interval.Configuration.Stub(c => c.TickInterval).Return(TimeSpan.FromSeconds(5));

            using (mocks.Record())
            using (mocks.Ordered())
            {
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 05)).Repeat.Once();
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 06)).Repeat.Once();
                using (mocks.Unordered())
                {
                    renderA(0.2f);
                    renderB(0.2f);
                }
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 07)).Repeat.Once();
                using (mocks.Unordered())
                {
                    renderA(0.4f);
                    renderB(0.4f);
                }
            }

            using (mocks.Playback())
            {
                interval.OnImportsSatisfied();
                interval.Update();
                interval.Update();
            }
        }

        [TestMethod]
        public void UpdateTwiceAddingUpToOneTickRenderAndTick()
        {
            var mocks = new MockRepository();
            var interval = new Interval
            {
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Runtime = mocks.StrictMock<IRuntime>()
            };
            var tickA = (Action)mocks.StrictMock<ITickable>().Tick;
            var tickB = (Action)mocks.StrictMock<ITickable>().Tick;
            interval.Tick += tickA;
            interval.Tick += tickB;

            var renderA = (RenderDelegate)mocks.StrictMock<IRenderable>().Render;
            var renderB = (RenderDelegate)mocks.StrictMock<IRenderable>().Render;
            interval.Render += renderA;
            interval.Render += renderB;

            interval.Configuration.Stub(c => c.TickInterval).Return(TimeSpan.FromSeconds(5));

            using (mocks.Record())
            using (mocks.Ordered())
            {
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 05)).Repeat.Once();
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 08)).Repeat.Once();
                using (mocks.Unordered())
                {
                    renderA(0.6f);
                    renderB(0.6f);
                }
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 11)).Repeat.Once();
                using (mocks.Unordered())
                {
                    tickA();
                    tickB();
                }
                using (mocks.Unordered())
                {
                    renderA(0.2f);
                    renderB(0.2f);
                }
            }

            using (mocks.Playback())
            {
                interval.OnImportsSatisfied();
                interval.Update();
                interval.Update();
            }
        }

        [TestMethod]
        public void UpdateOnceOneTickRenderAndTick()
        {
            var mocks = new MockRepository();
            var interval = new Interval
            {
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Runtime = mocks.StrictMock<IRuntime>()
            };
            var tickA = (Action)mocks.StrictMock<ITickable>().Tick;
            var tickB = (Action)mocks.StrictMock<ITickable>().Tick;
            interval.Tick += tickA;
            interval.Tick += tickB;

            var renderA = (RenderDelegate)mocks.StrictMock<IRenderable>().Render;
            var renderB = (RenderDelegate)mocks.StrictMock<IRenderable>().Render;
            interval.Render += renderA;
            interval.Render += renderB;

            interval.Configuration.Stub(c => c.TickInterval).Return(TimeSpan.FromSeconds(5));

            using (mocks.Record())
            using (mocks.Ordered())
            {
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 05)).Repeat.Once();
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 11)).Repeat.Once();
                using (mocks.Unordered())
                {
                    tickA();
                    tickB();
                }
                using (mocks.Unordered())
                {
                    renderA(0.2f);
                    renderB(0.2f);
                }
            }

            using (mocks.Playback())
            {
                interval.OnImportsSatisfied();
                interval.Update();
            }
        }

        [TestMethod]
        public void UpdateOnceTwoTicksRenderAndTick()
        {
            var mocks = new MockRepository();
            var interval = new Interval
            {
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Runtime = mocks.StrictMock<IRuntime>()
            };
            var tickA = (Action)mocks.StrictMock<ITickable>().Tick;
            var tickB = (Action)mocks.StrictMock<ITickable>().Tick;
            interval.Tick += tickA;
            interval.Tick += tickB;

            var renderA = (RenderDelegate)mocks.StrictMock<IRenderable>().Render;
            var renderB = (RenderDelegate)mocks.StrictMock<IRenderable>().Render;
            interval.Render += renderA;
            interval.Render += renderB;

            interval.Configuration.Stub(c => c.TickInterval).Return(TimeSpan.FromSeconds(5));

            using (mocks.Record())
            using (mocks.Ordered())
            {
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 05)).Repeat.Once();
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 16)).Repeat.Once();
                using (mocks.Unordered())
                {
                    tickA();
                    tickB();
                }
                using (mocks.Unordered())
                {
                    tickA();
                    tickB();
                }
                using (mocks.Unordered())
                {
                    renderA(0.2f);
                    renderB(0.2f);
                }
            }

            using (mocks.Playback())
            {
                interval.OnImportsSatisfied();
                interval.Update();
            }
        }

        #endregion    

        #region Render and update subscribers

        [TestMethod]
        public void NoUpdatesTickOnly()
        {
            var mocks = new MockRepository();
            var interval = new Interval
            {
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Runtime = mocks.StrictMock<IRuntime>()
            };
            var tickA = (Action)mocks.StrictMock<ITickable>().Tick;
            var tickB = (Action)mocks.StrictMock<ITickable>().Tick;
            interval.Tick += tickA;
            interval.Tick += tickB;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 05)).Repeat.Once();
            }

            using (mocks.Playback())
            {
                interval.OnImportsSatisfied();
            }
        }

        [TestMethod]
        public void UpdateLessThanOneTickTickOnly()
        {
            var mocks = new MockRepository();
            var interval = new Interval
            {
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Runtime = mocks.StrictMock<IRuntime>()
            };
            var tickA = (Action)mocks.StrictMock<ITickable>().Tick;
            var tickB = (Action)mocks.StrictMock<ITickable>().Tick;
            interval.Tick += tickA;
            interval.Tick += tickB;

            interval.Configuration.Stub(c => c.TickInterval).Return(TimeSpan.FromSeconds(5));

            using (mocks.Record())
            using (mocks.Ordered())
            {
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 05)).Repeat.Once();
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 06)).Repeat.Once();
            }

            using (mocks.Playback())
            {
                interval.OnImportsSatisfied();
                interval.Update();
            }
        }

        [TestMethod]
        public void UpdateTwiceLessThanOneTickTickOnly()
        {
            var mocks = new MockRepository();
            var interval = new Interval
            {
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Runtime = mocks.StrictMock<IRuntime>()
            };
            var tickA = (Action)mocks.StrictMock<ITickable>().Tick;
            var tickB = (Action)mocks.StrictMock<ITickable>().Tick;
            interval.Tick += tickA;
            interval.Tick += tickB;

            interval.Configuration.Stub(c => c.TickInterval).Return(TimeSpan.FromSeconds(5));

            using (mocks.Record())
            using (mocks.Ordered())
            {
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 05)).Repeat.Once();
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 06)).Repeat.Once();
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 07)).Repeat.Once();
            }

            using (mocks.Playback())
            {
                interval.OnImportsSatisfied();
                interval.Update();
                interval.Update();
            }
        }

        [TestMethod]
        public void UpdateTwiceAddingUpToOneTickTickOnly()
        {
            var mocks = new MockRepository();
            var interval = new Interval
            {
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Runtime = mocks.StrictMock<IRuntime>()
            };
            var tickA = (Action)mocks.StrictMock<ITickable>().Tick;
            var tickB = (Action)mocks.StrictMock<ITickable>().Tick;
            interval.Tick += tickA;
            interval.Tick += tickB;

            interval.Configuration.Stub(c => c.TickInterval).Return(TimeSpan.FromSeconds(5));

            using (mocks.Record())
            using (mocks.Ordered())
            {
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 05)).Repeat.Once();
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 08)).Repeat.Once();
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 11)).Repeat.Once();
                using (mocks.Unordered())
                {
                    tickA();
                    tickB();
                }
            }

            using (mocks.Playback())
            {
                interval.OnImportsSatisfied();
                interval.Update();
                interval.Update();
            }
        }

        [TestMethod]
        public void UpdateOnceOneTickTickOnly()
        {
            var mocks = new MockRepository();
            var interval = new Interval
            {
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Runtime = mocks.StrictMock<IRuntime>()
            };
            var tickA = (Action)mocks.StrictMock<ITickable>().Tick;
            var tickB = (Action)mocks.StrictMock<ITickable>().Tick;
            interval.Tick += tickA;
            interval.Tick += tickB;

            interval.Configuration.Stub(c => c.TickInterval).Return(TimeSpan.FromSeconds(5));

            using (mocks.Record())
            using (mocks.Ordered())
            {
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 05)).Repeat.Once();
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 11)).Repeat.Once();
                using (mocks.Unordered())
                {
                    tickA();
                    tickB();
                }
            }

            using (mocks.Playback())
            {
                interval.OnImportsSatisfied();
                interval.Update();
            }
        }

        [TestMethod]
        public void UpdateOnceTwoTicksTickOnly()
        {
            var mocks = new MockRepository();
            var interval = new Interval
            {
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Runtime = mocks.StrictMock<IRuntime>()
            };
            var tickA = (Action)mocks.StrictMock<ITickable>().Tick;
            var tickB = (Action)mocks.StrictMock<ITickable>().Tick;
            interval.Tick += tickA;
            interval.Tick += tickB;

            interval.Configuration.Stub(c => c.TickInterval).Return(TimeSpan.FromSeconds(5));

            using (mocks.Record())
            using (mocks.Ordered())
            {
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 05)).Repeat.Once();
                interval.Runtime.Expect(r => r.Now).Return(new DateTime(2008, 10, 14, 20, 30, 16)).Repeat.Once();
                using (mocks.Unordered())
                {
                    tickA();
                    tickB();
                }
                using (mocks.Unordered())
                {
                    tickA();
                    tickB();
                }
            }

            using (mocks.Playback())
            {
                interval.OnImportsSatisfied();
                interval.Update();
            }
        }

        #endregion   
    }
}
