﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using System.Runtime.InteropServices;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace ExpressionEngine.Platforms.OpenGL.Tests.RenderPassTests
{
    [TestClass]
    public sealed class Create
    {
        [TestMethod]
        public void SourceNullThrowsException()
        {
            try
            {
                new RenderPassFactory().CreateInstance(null);
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("source", ex.ParamName);
                Assert.AreEqual(@"Value cannot be null.
Parameter name: source", ex.Message);
            }
        }

        [TestMethod]
        public void AllocatingVertexShaderFails()
        {
            var mocks = new MockRepository();
            var factory = new RenderPassFactory
            {
                GLExtensions = mocks.StrictMock<IGLExtensions>()
            };

            using (mocks.Record())
            using (mocks.Ordered())
            {
                factory.GLExtensions.Expect(gle => gle.CreateShader(GLConstants.GL_VERTEX_SHADER)).Return(0).Repeat.Once();
            }

            using (mocks.Playback())
            {
                try
                {
                    factory.CreateInstance("Test Fragment Shader");
                    Assert.Fail();
                }
                catch (ExternalException ex)
                {
                    Assert.AreEqual("Failed to allocate a vertex shader", ex.Message);
                }
            }
        }

        [TestMethod]
        public void VertexShaderFailsToCompile()
        {
            var mocks = new MockRepository();
            var factory = new RenderPassFactory
            {
                GLExtensions = mocks.StrictMock<IGLExtensions>()
            };

            UInt32 vertexShader = 32398;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                factory.GLExtensions.Expect(gle => gle.CreateShader(GLConstants.GL_VERTEX_SHADER)).Return(vertexShader).Repeat.Once();

                factory.GLExtensions.ShaderSource(
                    Arg.Is(vertexShader),
                    Arg.Is(1),
                    Arg<string[]>.List.ContainsAll(new[] { "void main() { gl_Position = ftransform(); }" }),
                    Arg<int[]>.List.ContainsAll(new[] { "void main() { gl_Position = ftransform(); }".Length })
                );

                factory.GLExtensions.CompileShader(vertexShader);

                factory.GLExtensions.Expect(gle => gle.GetShaderiv(Arg.Is(vertexShader), Arg.Is(GLConstants.GL_COMPILE_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 0)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.GetShaderiv(Arg.Is(vertexShader), Arg.Is(GLConstants.GL_INFO_LOG_LENGTH), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], "Test Compilation Log".Length)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.GetShaderInfoLog(Arg.Is(vertexShader), Arg.Is("Test Compilation Log".Length), Arg<IntPtr>.Is.Anything, Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi =>
                    {
                        Marshal.WriteInt32((IntPtr)mi.Arguments[2], "Test Compilation Log".Length);
                        Marshal.Copy(Encoding.ASCII.GetBytes("Test Compilation Log"), 0, (IntPtr)mi.Arguments[3], "Test Compilation Log".Length);
                    }).Repeat.Once();

                factory.GLExtensions.DeleteShader(vertexShader);
            }

            using (mocks.Playback())
            {
                try
                {
                    factory.CreateInstance("Test Fragment Shader");
                    Assert.Fail();
                }
                catch (ExternalException ex)
                {
                    Assert.AreEqual("An error occurred while compiling the vertex shader: \"Test Compilation Log\"", ex.Message);
                }
            }
        }

        [TestMethod]
        public void AllocatingFragmentShaderFails()
        {
            var mocks = new MockRepository();
            var factory = new RenderPassFactory
            {
                GLExtensions = mocks.StrictMock<IGLExtensions>()
            };

            UInt32 vertexShader = 32398;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                factory.GLExtensions.Expect(gle => gle.CreateShader(GLConstants.GL_VERTEX_SHADER)).Return(vertexShader).Repeat.Once();

                factory.GLExtensions.ShaderSource(
                    Arg.Is(vertexShader),
                    Arg.Is(1),
                    Arg<string[]>.List.ContainsAll(new[] { "void main() { gl_Position = ftransform(); }" }),
                    Arg<int[]>.List.ContainsAll(new[] { "void main() { gl_Position = ftransform(); }".Length })
                );

                factory.GLExtensions.CompileShader(vertexShader);

                factory.GLExtensions.Expect(gle => gle.GetShaderiv(Arg.Is(vertexShader), Arg.Is(GLConstants.GL_COMPILE_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.CreateShader(GLConstants.GL_FRAGMENT_SHADER)).Return(0).Repeat.Once();

                factory.GLExtensions.DeleteShader(vertexShader);
            }

            using (mocks.Playback())
            {
                try
                {
                    factory.CreateInstance("Test Fragment Shader");
                    Assert.Fail();
                }
                catch (ExternalException ex)
                {
                    Assert.AreEqual("Failed to allocate a fragment shader", ex.Message);
                }
            }
        }

        [TestMethod]
        public void FragmentShaderFailsToCompile()
        {
            var mocks = new MockRepository();
            var factory = new RenderPassFactory
            {
                GLExtensions = mocks.StrictMock<IGLExtensions>()
            };

            UInt32 vertexShader = 32398;
            UInt32 fragmentShader = 9801920;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                factory.GLExtensions.Expect(gle => gle.CreateShader(GLConstants.GL_VERTEX_SHADER)).Return(vertexShader).Repeat.Once();

                factory.GLExtensions.ShaderSource(
                    Arg.Is(vertexShader),
                    Arg.Is(1),
                    Arg<string[]>.List.ContainsAll(new[] { "void main() { gl_Position = ftransform(); }" }),
                    Arg<int[]>.List.ContainsAll(new[] { "void main() { gl_Position = ftransform(); }".Length })
                );

                factory.GLExtensions.CompileShader(vertexShader);

                factory.GLExtensions.Expect(gle => gle.GetShaderiv(Arg.Is(vertexShader), Arg.Is(GLConstants.GL_COMPILE_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.CreateShader(GLConstants.GL_FRAGMENT_SHADER)).Return(fragmentShader).Repeat.Once();

                factory.GLExtensions.ShaderSource(
                    Arg.Is(fragmentShader),
                    Arg.Is(1),
                    Arg<string[]>.List.ContainsAll(new[] { "Test Fragment Shader" }),
                    Arg<int[]>.List.ContainsAll(new[] { "Test Fragment Shader".Length })
                );

                factory.GLExtensions.CompileShader(fragmentShader);

                factory.GLExtensions.Expect(gle => gle.GetShaderiv(Arg.Is(fragmentShader), Arg.Is(GLConstants.GL_COMPILE_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 0)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.GetShaderiv(Arg.Is(fragmentShader), Arg.Is(GLConstants.GL_INFO_LOG_LENGTH), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], "Test Compilation Log".Length)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.GetShaderInfoLog(Arg.Is(fragmentShader), Arg.Is("Test Compilation Log".Length), Arg<IntPtr>.Is.Anything, Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi =>
                    {
                        Marshal.WriteInt32((IntPtr)mi.Arguments[2], "Test Compilation Log".Length);
                        Marshal.Copy(Encoding.ASCII.GetBytes("Test Compilation Log"), 0, (IntPtr)mi.Arguments[3], "Test Compilation Log".Length);
                    }).Repeat.Once();

                factory.GLExtensions.DeleteShader(fragmentShader);
                factory.GLExtensions.DeleteShader(vertexShader);
            }

            using (mocks.Playback())
            {
                try
                {
                    factory.CreateInstance("Test Fragment Shader");
                    Assert.Fail();
                }
                catch (ExternalException ex)
                {
                    Assert.AreEqual("An error occurred while compiling the fragment shader: \"Test Compilation Log\"", ex.Message);
                }
            }
        }

        [TestMethod]
        public void AllocatingProgramFails()
        {
            var mocks = new MockRepository();
            var factory = new RenderPassFactory
            {
                GLExtensions = mocks.StrictMock<IGLExtensions>()
            };

            UInt32 vertexShader = 32398;
            UInt32 fragmentShader = 9801920;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                factory.GLExtensions.Expect(gle => gle.CreateShader(GLConstants.GL_VERTEX_SHADER)).Return(vertexShader).Repeat.Once();

                factory.GLExtensions.ShaderSource(
                    Arg.Is(vertexShader),
                    Arg.Is(1),
                    Arg<string[]>.List.ContainsAll(new[] { "void main() { gl_Position = ftransform(); }" }),
                    Arg<int[]>.List.ContainsAll(new[] { "void main() { gl_Position = ftransform(); }".Length })
                );

                factory.GLExtensions.CompileShader(vertexShader);

                factory.GLExtensions.Expect(gle => gle.GetShaderiv(Arg.Is(vertexShader), Arg.Is(GLConstants.GL_COMPILE_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.CreateShader(GLConstants.GL_FRAGMENT_SHADER)).Return(fragmentShader).Repeat.Once();

                factory.GLExtensions.ShaderSource(
                    Arg.Is(fragmentShader),
                    Arg.Is(1),
                    Arg<string[]>.List.ContainsAll(new[] { "Test Fragment Shader" }),
                    Arg<int[]>.List.ContainsAll(new[] { "Test Fragment Shader".Length })
                );

                factory.GLExtensions.CompileShader(fragmentShader);

                factory.GLExtensions.Expect(gle => gle.GetShaderiv(Arg.Is(fragmentShader), Arg.Is(GLConstants.GL_COMPILE_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.CreateProgram()).Return(0).Repeat.Once();

                factory.GLExtensions.DeleteShader(fragmentShader);
                factory.GLExtensions.DeleteShader(vertexShader);
            }

            using (mocks.Playback())
            {
                try
                {
                    factory.CreateInstance("Test Fragment Shader");
                    Assert.Fail();
                }
                catch (ExternalException ex)
                {
                    Assert.AreEqual("Failed to allocate a program", ex.Message);
                }
            }
        }

        [TestMethod]
        public void ProgramFailsToLink()
        {
            var mocks = new MockRepository();
            var factory = new RenderPassFactory
            {
                GLExtensions = mocks.StrictMock<IGLExtensions>()
            };

            UInt32 vertexShader = 32398;
            UInt32 fragmentShader = 9801920;
            UInt32 program = 238987;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                factory.GLExtensions.Expect(gle => gle.CreateShader(GLConstants.GL_VERTEX_SHADER)).Return(vertexShader).Repeat.Once();

                factory.GLExtensions.ShaderSource(
                    Arg.Is(vertexShader),
                    Arg.Is(1),
                    Arg<string[]>.List.ContainsAll(new[] { "void main() { gl_Position = ftransform(); }" }),
                    Arg<int[]>.List.ContainsAll(new[] { "void main() { gl_Position = ftransform(); }".Length })
                );

                factory.GLExtensions.CompileShader(vertexShader);

                factory.GLExtensions.Expect(gle => gle.GetShaderiv(Arg.Is(vertexShader), Arg.Is(GLConstants.GL_COMPILE_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.CreateShader(GLConstants.GL_FRAGMENT_SHADER)).Return(fragmentShader).Repeat.Once();

                factory.GLExtensions.ShaderSource(
                    Arg.Is(fragmentShader),
                    Arg.Is(1),
                    Arg<string[]>.List.ContainsAll(new[] { "Test Fragment Shader" }),
                    Arg<int[]>.List.ContainsAll(new[] { "Test Fragment Shader".Length })
                );

                factory.GLExtensions.CompileShader(fragmentShader);

                factory.GLExtensions.Expect(gle => gle.GetShaderiv(Arg.Is(fragmentShader), Arg.Is(GLConstants.GL_COMPILE_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.CreateProgram()).Return(program).Repeat.Once();

                factory.GLExtensions.AttachShader(program, vertexShader);
                factory.GLExtensions.AttachShader(program, fragmentShader);
                factory.GLExtensions.LinkProgram(program);

                factory.GLExtensions.Expect(gle => gle.GetProgramiv(Arg.Is(program), Arg.Is(GLConstants.GL_LINK_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 0)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.GetProgramiv(Arg.Is(program), Arg.Is(GLConstants.GL_INFO_LOG_LENGTH), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], "Test Link Log".Length)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.GetProgramInfoLog(Arg.Is(program), Arg.Is("Test Link Log".Length), Arg<IntPtr>.Is.Anything, Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi =>
                    {
                        Marshal.WriteInt32((IntPtr)mi.Arguments[2], "Test Link Log".Length);
                        Marshal.Copy(Encoding.ASCII.GetBytes("Test Link Log"), 0, (IntPtr)mi.Arguments[3], "Test Link Log".Length);
                    }).Repeat.Once();

                factory.GLExtensions.DetachShader(program, fragmentShader);
                factory.GLExtensions.DetachShader(program, vertexShader);
                factory.GLExtensions.DeleteProgram(program);
                factory.GLExtensions.DeleteShader(fragmentShader);
                factory.GLExtensions.DeleteShader(vertexShader);
            }

            using (mocks.Playback())
            {
                try
                {
                    factory.CreateInstance("Test Fragment Shader");
                    Assert.Fail();
                }
                catch (ExternalException ex)
                {
                    Assert.AreEqual("An error occurred while linking the program: \"Test Link Log\"", ex.Message);
                }
            }
        }

        [TestMethod]
        public void Successful()
        {
            var mocks = new MockRepository();
            var factory = new RenderPassFactory
            {
                GLExtensions = mocks.StrictMock<IGLExtensions>()
            };

            UInt32 vertexShader = 32398;
            UInt32 fragmentShader = 9801920;
            UInt32 program = 238987;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                factory.GLExtensions.Expect(gle => gle.CreateShader(GLConstants.GL_VERTEX_SHADER)).Return(vertexShader).Repeat.Once();

                factory.GLExtensions.ShaderSource(
                    Arg.Is(vertexShader),
                    Arg.Is(1),
                    Arg<string[]>.List.ContainsAll(new[] { "void main() { gl_Position = ftransform(); }" }),
                    Arg<int[]>.List.ContainsAll(new[] { "void main() { gl_Position = ftransform(); }".Length })
                );

                factory.GLExtensions.CompileShader(vertexShader);

                factory.GLExtensions.Expect(gle => gle.GetShaderiv(Arg.Is(vertexShader), Arg.Is(GLConstants.GL_COMPILE_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.CreateShader(GLConstants.GL_FRAGMENT_SHADER)).Return(fragmentShader).Repeat.Once();

                factory.GLExtensions.ShaderSource(
                    Arg.Is(fragmentShader),
                    Arg.Is(1),
                    Arg<string[]>.List.ContainsAll(new[] { "Test Fragment Shader" }),
                    Arg<int[]>.List.ContainsAll(new[] { "Test Fragment Shader".Length })
                );

                factory.GLExtensions.CompileShader(fragmentShader);

                factory.GLExtensions.Expect(gle => gle.GetShaderiv(Arg.Is(fragmentShader), Arg.Is(GLConstants.GL_COMPILE_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.CreateProgram()).Return(program).Repeat.Once();

                factory.GLExtensions.AttachShader(program, vertexShader);
                factory.GLExtensions.AttachShader(program, fragmentShader);
                factory.GLExtensions.LinkProgram(program);

                factory.GLExtensions.Expect(gle => gle.GetProgramiv(Arg.Is(program), Arg.Is(GLConstants.GL_LINK_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.DetachShader(program, fragmentShader);
                factory.GLExtensions.DetachShader(program, vertexShader);
                factory.GLExtensions.DeleteShader(fragmentShader);
                factory.GLExtensions.DeleteShader(vertexShader);
            }

            using (mocks.Playback())
            {
                Assert.IsNotNull(factory.CreateInstance("Test Fragment Shader"));
            }
        }
    }
}
