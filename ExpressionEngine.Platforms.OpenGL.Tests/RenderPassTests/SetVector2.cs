﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using System.Runtime.InteropServices;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Numerics;

namespace ExpressionEngine.Platforms.OpenGL.Tests.RenderPassTests
{
    [TestClass]
    public sealed class SetVector2
    {
        [TestMethod]
        public void Successful()
        {
            var mocks = new MockRepository();
            var factory = new RenderPassFactory
            {
                GLExtensions = mocks.StrictMock<IGLExtensions>()
            };

            UInt32 vertexShader = 32398;
            UInt32 fragmentShader = 9801920;
            UInt32 program = 238987;
            Int32 location = 487;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                factory.GLExtensions.Expect(gle => gle.CreateShader(GLConstants.GL_VERTEX_SHADER)).Return(vertexShader).Repeat.Once();

                factory.GLExtensions.ShaderSource(
                    Arg.Is(vertexShader),
                    Arg.Is(1),
                    Arg<string[]>.List.ContainsAll(new[] { "void main() { gl_Position = ftransform(); }" }),
                    Arg<int[]>.List.ContainsAll(new[] { "void main() { gl_Position = ftransform(); }".Length })
                );

                factory.GLExtensions.CompileShader(vertexShader);

                factory.GLExtensions.Expect(gle => gle.GetShaderiv(Arg.Is(vertexShader), Arg.Is(GLConstants.GL_COMPILE_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.CreateShader(GLConstants.GL_FRAGMENT_SHADER)).Return(fragmentShader).Repeat.Once();

                factory.GLExtensions.ShaderSource(
                    Arg.Is(fragmentShader),
                    Arg.Is(1),
                    Arg<string[]>.List.ContainsAll(new[] { "Test Fragment Shader" }),
                    Arg<int[]>.List.ContainsAll(new[] { "Test Fragment Shader".Length })
                );

                factory.GLExtensions.CompileShader(fragmentShader);

                factory.GLExtensions.Expect(gle => gle.GetShaderiv(Arg.Is(fragmentShader), Arg.Is(GLConstants.GL_COMPILE_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.CreateProgram()).Return(program).Repeat.Once();

                factory.GLExtensions.AttachShader(program, vertexShader);
                factory.GLExtensions.AttachShader(program, fragmentShader);
                factory.GLExtensions.LinkProgram(program);

                factory.GLExtensions.Expect(gle => gle.GetProgramiv(Arg.Is(program), Arg.Is(GLConstants.GL_LINK_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.DetachShader(program, fragmentShader);
                factory.GLExtensions.DetachShader(program, vertexShader);
                factory.GLExtensions.DeleteShader(fragmentShader);
                factory.GLExtensions.DeleteShader(vertexShader);

                factory.GLExtensions.Expect(gle => gle.GetUniformLocation(program, "parameter_Test Parameter")).Return(location).Repeat.Once();
                factory.GLExtensions.UseProgram(program);
                factory.GLExtensions.Uniform2f(location, 3.4f, 7.8f);
                factory.GLExtensions.UseProgram(0);
            }

            using (mocks.Playback())
            {
                var instance = factory.CreateInstance("Test Fragment Shader");
                instance.Set("Test Parameter", new Vector2(3.4f, 7.8f));
            }
        }

        [TestMethod]
        public void DisposedThrowsException()
        {
            var mocks = new MockRepository();
            var factory = new RenderPassFactory
            {
                GLExtensions = mocks.StrictMock<IGLExtensions>()
            };

            UInt32 vertexShader = 32398;
            UInt32 fragmentShader = 9801920;
            UInt32 program = 238987;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                factory.GLExtensions.Expect(gle => gle.CreateShader(GLConstants.GL_VERTEX_SHADER)).Return(vertexShader).Repeat.Once();

                factory.GLExtensions.ShaderSource(
                    Arg.Is(vertexShader),
                    Arg.Is(1),
                    Arg<string[]>.List.ContainsAll(new[] { "void main() { gl_Position = ftransform(); }" }),
                    Arg<int[]>.List.ContainsAll(new[] { "void main() { gl_Position = ftransform(); }".Length })
                );

                factory.GLExtensions.CompileShader(vertexShader);

                factory.GLExtensions.Expect(gle => gle.GetShaderiv(Arg.Is(vertexShader), Arg.Is(GLConstants.GL_COMPILE_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.CreateShader(GLConstants.GL_FRAGMENT_SHADER)).Return(fragmentShader).Repeat.Once();

                factory.GLExtensions.ShaderSource(
                    Arg.Is(fragmentShader),
                    Arg.Is(1),
                    Arg<string[]>.List.ContainsAll(new[] { "Test Fragment Shader" }),
                    Arg<int[]>.List.ContainsAll(new[] { "Test Fragment Shader".Length })
                );

                factory.GLExtensions.CompileShader(fragmentShader);

                factory.GLExtensions.Expect(gle => gle.GetShaderiv(Arg.Is(fragmentShader), Arg.Is(GLConstants.GL_COMPILE_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.CreateProgram()).Return(program).Repeat.Once();

                factory.GLExtensions.AttachShader(program, vertexShader);
                factory.GLExtensions.AttachShader(program, fragmentShader);
                factory.GLExtensions.LinkProgram(program);

                factory.GLExtensions.Expect(gle => gle.GetProgramiv(Arg.Is(program), Arg.Is(GLConstants.GL_LINK_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.DetachShader(program, fragmentShader);
                factory.GLExtensions.DetachShader(program, vertexShader);
                factory.GLExtensions.DeleteShader(fragmentShader);
                factory.GLExtensions.DeleteShader(vertexShader);

                factory.GLExtensions.DeleteProgram(program);
            }

            using (mocks.Playback())
            {
                var instance = factory.CreateInstance("Test Fragment Shader");
                instance.Dispose();
                try
                {
                    instance.Set("Test Parameter", new Vector2(3.4f, 7.8f));
                    Assert.Fail();
                }
                catch(InvalidOperationException ex)
                {
                    Assert.AreEqual("Cannot set parameters once disposed", ex.Message);
                }
            }
        }

        [TestMethod]
        public void NonexistentUniformThrowsException()
        {
            var mocks = new MockRepository();
            var factory = new RenderPassFactory
            {
                GLExtensions = mocks.StrictMock<IGLExtensions>()
            };

            UInt32 vertexShader = 32398;
            UInt32 fragmentShader = 9801920;
            UInt32 program = 238987;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                factory.GLExtensions.Expect(gle => gle.CreateShader(GLConstants.GL_VERTEX_SHADER)).Return(vertexShader).Repeat.Once();

                factory.GLExtensions.ShaderSource(
                    Arg.Is(vertexShader),
                    Arg.Is(1),
                    Arg<string[]>.List.ContainsAll(new[] { "void main() { gl_Position = ftransform(); }" }),
                    Arg<int[]>.List.ContainsAll(new[] { "void main() { gl_Position = ftransform(); }".Length })
                );

                factory.GLExtensions.CompileShader(vertexShader);

                factory.GLExtensions.Expect(gle => gle.GetShaderiv(Arg.Is(vertexShader), Arg.Is(GLConstants.GL_COMPILE_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.CreateShader(GLConstants.GL_FRAGMENT_SHADER)).Return(fragmentShader).Repeat.Once();

                factory.GLExtensions.ShaderSource(
                    Arg.Is(fragmentShader),
                    Arg.Is(1),
                    Arg<string[]>.List.ContainsAll(new[] { "Test Fragment Shader" }),
                    Arg<int[]>.List.ContainsAll(new[] { "Test Fragment Shader".Length })
                );

                factory.GLExtensions.CompileShader(fragmentShader);

                factory.GLExtensions.Expect(gle => gle.GetShaderiv(Arg.Is(fragmentShader), Arg.Is(GLConstants.GL_COMPILE_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.CreateProgram()).Return(program).Repeat.Once();

                factory.GLExtensions.AttachShader(program, vertexShader);
                factory.GLExtensions.AttachShader(program, fragmentShader);
                factory.GLExtensions.LinkProgram(program);

                factory.GLExtensions.Expect(gle => gle.GetProgramiv(Arg.Is(program), Arg.Is(GLConstants.GL_LINK_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.DetachShader(program, fragmentShader);
                factory.GLExtensions.DetachShader(program, vertexShader);
                factory.GLExtensions.DeleteShader(fragmentShader);
                factory.GLExtensions.DeleteShader(vertexShader);

                factory.GLExtensions.Expect(gle => gle.GetUniformLocation(program, "parameter_Test Parameter")).Return(-1).Repeat.Once();
            }

            using (mocks.Playback())
            {
                var instance = factory.CreateInstance("Test Fragment Shader");
                try
                {
                    instance.Set("Test Parameter", new Vector2(3.4f, 7.8f));
                    Assert.Fail();
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    Assert.AreEqual("parameter", ex.ParamName);
                    Assert.AreEqual(@"No matching uniform exists in the shader
Parameter name: parameter", ex.Message);
                }
            }
        }

        [TestMethod]
        public void ParameterNameNullThrowsException()
        {
            var mocks = new MockRepository();
            var factory = new RenderPassFactory
            {
                GLExtensions = mocks.StrictMock<IGLExtensions>()
            };

            UInt32 vertexShader = 32398;
            UInt32 fragmentShader = 9801920;
            UInt32 program = 238987;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                factory.GLExtensions.Expect(gle => gle.CreateShader(GLConstants.GL_VERTEX_SHADER)).Return(vertexShader).Repeat.Once();

                factory.GLExtensions.ShaderSource(
                    Arg.Is(vertexShader),
                    Arg.Is(1),
                    Arg<string[]>.List.ContainsAll(new[] { "void main() { gl_Position = ftransform(); }" }),
                    Arg<int[]>.List.ContainsAll(new[] { "void main() { gl_Position = ftransform(); }".Length })
                );

                factory.GLExtensions.CompileShader(vertexShader);

                factory.GLExtensions.Expect(gle => gle.GetShaderiv(Arg.Is(vertexShader), Arg.Is(GLConstants.GL_COMPILE_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.CreateShader(GLConstants.GL_FRAGMENT_SHADER)).Return(fragmentShader).Repeat.Once();

                factory.GLExtensions.ShaderSource(
                    Arg.Is(fragmentShader),
                    Arg.Is(1),
                    Arg<string[]>.List.ContainsAll(new[] { "Test Fragment Shader" }),
                    Arg<int[]>.List.ContainsAll(new[] { "Test Fragment Shader".Length })
                );

                factory.GLExtensions.CompileShader(fragmentShader);

                factory.GLExtensions.Expect(gle => gle.GetShaderiv(Arg.Is(fragmentShader), Arg.Is(GLConstants.GL_COMPILE_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.CreateProgram()).Return(program).Repeat.Once();

                factory.GLExtensions.AttachShader(program, vertexShader);
                factory.GLExtensions.AttachShader(program, fragmentShader);
                factory.GLExtensions.LinkProgram(program);

                factory.GLExtensions.Expect(gle => gle.GetProgramiv(Arg.Is(program), Arg.Is(GLConstants.GL_LINK_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.DetachShader(program, fragmentShader);
                factory.GLExtensions.DetachShader(program, vertexShader);
                factory.GLExtensions.DeleteShader(fragmentShader);
                factory.GLExtensions.DeleteShader(vertexShader);

            }

            using (mocks.Playback())
            {
                var instance = factory.CreateInstance("Test Fragment Shader");
                try
                {
                    instance.Set(null, new Vector2(3.4f, 7.8f));
                    Assert.Fail();
                }
                catch (ArgumentNullException ex)
                {
                    Assert.AreEqual("parameter", ex.ParamName);
                    Assert.AreEqual(@"Value cannot be null.
Parameter name: parameter", ex.Message);
                }
            }
        }
    }
}
