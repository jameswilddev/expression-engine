﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using System.Runtime.InteropServices;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace ExpressionEngine.Platforms.OpenGL.Tests.RenderPassTests
{
    [TestClass]
    public sealed class Dispose
    {
        [TestMethod]
        public void Successful()
        {
            var mocks = new MockRepository();
            var factory = new RenderPassFactory
            {
                GLExtensions = mocks.StrictMock<IGLExtensions>()
            };

            UInt32 vertexShader = 32398;
            UInt32 fragmentShader = 9801920;
            UInt32 program = 238987;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                factory.GLExtensions.Expect(gle => gle.CreateShader(GLConstants.GL_VERTEX_SHADER)).Return(vertexShader).Repeat.Once();

                factory.GLExtensions.ShaderSource(
                    Arg.Is(vertexShader),
                    Arg.Is(1),
                    Arg<string[]>.List.ContainsAll(new[] { "void main() { gl_Position = ftransform(); }" }),
                    Arg<int[]>.List.ContainsAll(new[] { "void main() { gl_Position = ftransform(); }".Length })
                );

                factory.GLExtensions.CompileShader(vertexShader);

                factory.GLExtensions.Expect(gle => gle.GetShaderiv(Arg.Is(vertexShader), Arg.Is(GLConstants.GL_COMPILE_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.CreateShader(GLConstants.GL_FRAGMENT_SHADER)).Return(fragmentShader).Repeat.Once();

                factory.GLExtensions.ShaderSource(
                    Arg.Is(fragmentShader),
                    Arg.Is(1),
                    Arg<string[]>.List.ContainsAll(new[] { "Test Fragment Shader" }),
                    Arg<int[]>.List.ContainsAll(new[] { "Test Fragment Shader".Length })
                );

                factory.GLExtensions.CompileShader(fragmentShader);

                factory.GLExtensions.Expect(gle => gle.GetShaderiv(Arg.Is(fragmentShader), Arg.Is(GLConstants.GL_COMPILE_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.CreateProgram()).Return(program).Repeat.Once();

                factory.GLExtensions.AttachShader(program, vertexShader);
                factory.GLExtensions.AttachShader(program, fragmentShader);
                factory.GLExtensions.LinkProgram(program);

                factory.GLExtensions.Expect(gle => gle.GetProgramiv(Arg.Is(program), Arg.Is(GLConstants.GL_LINK_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.DetachShader(program, fragmentShader);
                factory.GLExtensions.DetachShader(program, vertexShader);
                factory.GLExtensions.DeleteShader(fragmentShader);
                factory.GLExtensions.DeleteShader(vertexShader);

                factory.GLExtensions.DeleteProgram(program);
            }

            using (mocks.Playback())
            {
                factory.CreateInstance("Test Fragment Shader").Dispose();
            }
        }

        [TestMethod]
        public void SuccessfulDisposedTwice()
        {
            var mocks = new MockRepository();
            var factory = new RenderPassFactory
            {
                GLExtensions = mocks.StrictMock<IGLExtensions>()
            };

            UInt32 vertexShader = 32398;
            UInt32 fragmentShader = 9801920;
            UInt32 program = 238987;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                factory.GLExtensions.Expect(gle => gle.CreateShader(GLConstants.GL_VERTEX_SHADER)).Return(vertexShader).Repeat.Once();

                factory.GLExtensions.ShaderSource(
                    Arg.Is(vertexShader),
                    Arg.Is(1),
                    Arg<string[]>.List.ContainsAll(new[] { "void main() { gl_Position = ftransform(); }" }),
                    Arg<int[]>.List.ContainsAll(new[] { "void main() { gl_Position = ftransform(); }".Length })
                );

                factory.GLExtensions.CompileShader(vertexShader);

                factory.GLExtensions.Expect(gle => gle.GetShaderiv(Arg.Is(vertexShader), Arg.Is(GLConstants.GL_COMPILE_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.CreateShader(GLConstants.GL_FRAGMENT_SHADER)).Return(fragmentShader).Repeat.Once();

                factory.GLExtensions.ShaderSource(
                    Arg.Is(fragmentShader),
                    Arg.Is(1),
                    Arg<string[]>.List.ContainsAll(new[] { "Test Fragment Shader" }),
                    Arg<int[]>.List.ContainsAll(new[] { "Test Fragment Shader".Length })
                );

                factory.GLExtensions.CompileShader(fragmentShader);

                factory.GLExtensions.Expect(gle => gle.GetShaderiv(Arg.Is(fragmentShader), Arg.Is(GLConstants.GL_COMPILE_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.Expect(gle => gle.CreateProgram()).Return(program).Repeat.Once();

                factory.GLExtensions.AttachShader(program, vertexShader);
                factory.GLExtensions.AttachShader(program, fragmentShader);
                factory.GLExtensions.LinkProgram(program);

                factory.GLExtensions.Expect(gle => gle.GetProgramiv(Arg.Is(program), Arg.Is(GLConstants.GL_LINK_STATUS), Arg<IntPtr>.Is.Anything))
                    .WhenCalled(mi => Marshal.WriteInt32((IntPtr)mi.Arguments[2], 1)).Repeat.Once();

                factory.GLExtensions.DetachShader(program, fragmentShader);
                factory.GLExtensions.DetachShader(program, vertexShader);
                factory.GLExtensions.DeleteShader(fragmentShader);
                factory.GLExtensions.DeleteShader(vertexShader);

                factory.GLExtensions.DeleteProgram(program);
            }

            using (mocks.Playback())
            {
                var shader = factory.CreateInstance("Test Fragment Shader");
                shader.Dispose();
                shader.Dispose();
            }
        }
    }
}
