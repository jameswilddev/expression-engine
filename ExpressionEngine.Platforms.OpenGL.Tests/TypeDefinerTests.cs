﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;

namespace ExpressionEngine.Platforms.OpenGL.Tests
{
    [TestClass]
    public sealed class TypeDefinerTests
    {
        [TestMethod]
        public void TypesNullThrowsException()
        {
            try
            {
                new TypeDefiner().DefineTypes(null);
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("types", ex.ParamName);
                Assert.AreEqual(@"Value cannot be null.
Parameter name: types", ex.Message);
            }
        }

        [TestMethod]
        public void TypesEmpty()
        {
            Assert.AreEqual("", new TypeDefiner().DefineTypes(Enumerable.Empty<Type>()));
        }

        [TestMethod]
        public void GLSLNativeTypesNotDefined()
        {
            Assert.AreEqual("", new TypeDefiner().DefineTypes(new[] { typeof(int), typeof(float), typeof(bool), typeof(Vector2), typeof(Vector3), typeof(Vector4), typeof(Matrix4x4) }));
        }

        public struct BasicStruct
        {
            public int FieldA;
            public float FieldB;
            public bool FieldC;
            public Vector2 FieldD;
            public Vector3 FieldE;
            public Vector4 FieldF;
            public Matrix4x4 FieldG;
        }

        [TestMethod]
        public void BasicStructDefinition()
        {
            Assert.AreEqual(@"struct customStruct_ExpressionEngine_Platforms_OpenGL_Tests_TypeDefinerTests_BasicStruct {
	int FieldA;
	float FieldB;
	bool FieldC;
	vec2 FieldD;
	vec3 FieldE;
	vec4 FieldF;
	mat4 FieldG;
};", new TypeDefiner().DefineTypes(new[] { typeof(BasicStruct) }));
        }

        [TestMethod]
        public void NullIgnored()
        {
            Assert.AreEqual(@"struct customStruct_ExpressionEngine_Platforms_OpenGL_Tests_TypeDefinerTests_BasicStruct {
	int FieldA;
	float FieldB;
	bool FieldC;
	vec2 FieldD;
	vec3 FieldE;
	vec4 FieldF;
	mat4 FieldG;
};", new TypeDefiner().DefineTypes(new[] { typeof(BasicStruct), null }));
        }


        [TestMethod]
        public void ReferenceTypesIgnored()
        {
            Assert.AreEqual(@"struct customStruct_ExpressionEngine_Platforms_OpenGL_Tests_TypeDefinerTests_BasicStruct {
	int FieldA;
	float FieldB;
	bool FieldC;
	vec2 FieldD;
	vec3 FieldE;
	vec4 FieldF;
	mat4 FieldG;
};", new TypeDefiner().DefineTypes(new[] { typeof(BasicStruct), typeof(Object) }));
        }

        public struct BasicStructWrapper
        {
            public int FieldA;
            public BasicStruct FieldB;
            public int FieldC;
        }

        [TestMethod]
        public void MultiLevelStructA()
        {
            Assert.AreEqual(@"struct customStruct_ExpressionEngine_Platforms_OpenGL_Tests_TypeDefinerTests_BasicStruct {
	int FieldA;
	float FieldB;
	bool FieldC;
	vec2 FieldD;
	vec3 FieldE;
	vec4 FieldF;
	mat4 FieldG;
};

struct customStruct_ExpressionEngine_Platforms_OpenGL_Tests_TypeDefinerTests_BasicStructWrapper {
	int FieldA;
	customStruct_ExpressionEngine_Platforms_OpenGL_Tests_TypeDefinerTests_BasicStruct FieldB;
	int FieldC;
};", new TypeDefiner().DefineTypes(new[] { typeof(BasicStruct), typeof(BasicStructWrapper) }));
        }

        [TestMethod]
        public void MultiLevelStructB()
        {
            Assert.AreEqual(@"struct customStruct_ExpressionEngine_Platforms_OpenGL_Tests_TypeDefinerTests_BasicStruct {
	int FieldA;
	float FieldB;
	bool FieldC;
	vec2 FieldD;
	vec3 FieldE;
	vec4 FieldF;
	mat4 FieldG;
};

struct customStruct_ExpressionEngine_Platforms_OpenGL_Tests_TypeDefinerTests_BasicStructWrapper {
	int FieldA;
	customStruct_ExpressionEngine_Platforms_OpenGL_Tests_TypeDefinerTests_BasicStruct FieldB;
	int FieldC;
};", new TypeDefiner().DefineTypes(new[] { typeof(BasicStruct), typeof(BasicStructWrapper) }));
        }

		public struct ContainsIgnoredMembers
		{
			public int Field;
			public static int StaticField;
			public int InstanceProperty { get; set; }
			public static int StaticProperty { get; set; }
			public void InstanceMethod() { throw new NotImplementedException(); }
			public static void StaticMethod() { throw new NotImplementedException(); }
			public event Action InstanceEvent;
			public static event Action StaticEvent;
		}

		[TestMethod]
		public void UnsupportedMembersAreIgnored()
		{
			Assert.AreEqual(@"struct customStruct_ExpressionEngine_Platforms_OpenGL_Tests_TypeDefinerTests_ContainsIgnoredMembers {
	int Field;
};", new TypeDefiner().DefineTypes(new[] { typeof(ContainsIgnoredMembers) }));
		}

		public struct GenericStruct<T>
		{
			public int FieldA;
			public T FieldB;
			public float FieldC;
		}

        [TestMethod]
        public void GenericType()
        {
			Assert.AreEqual(@"struct customStruct_ExpressionEngine_Platforms_OpenGL_Tests_TypeDefinerTests_GenericStruct__vec3 {
	int FieldA;
	vec3 FieldB;
	float FieldC;
};", new TypeDefiner().DefineTypes(new[] { typeof(GenericStruct<Vector3>) }));
        }

		public struct EmptyStruct
		{
			public static int StaticField;
			public int InstanceProperty { get; set; }
			public static int StaticProperty { get; set; }
			public void InstanceMethod() { throw new NotImplementedException(); }
			public static void StaticMethod() { throw new NotImplementedException(); }
			public event Action InstanceEvent;
			public static event Action StaticEvent;
		}

        [TestMethod]
        public void EmptyStructThrowsException()
        {
			try
			{
				new TypeDefiner().DefineTypes(new[] { typeof(EmptyStruct) });
				Assert.Fail();
			}
			catch (NotSupportedException ex)
			{
				Assert.AreEqual("Empty custom structs are not supported", ex.Message);
			}
        }

		public struct ContainsReferenceType
		{
			public int FieldA;
			public string FieldB;
			public float FieldC;
		}
        
        [TestMethod]
        public void ReferenceTypeThrowsException()
        {
			try
			{
				new TypeDefiner().DefineTypes(new[] { typeof(ContainsReferenceType) });
				Assert.Fail();
			}
			catch (NotSupportedException ex)
			{
				Assert.AreEqual("Reference types are not supported", ex.Message);
			}
        }

		[StructLayout(LayoutKind.Explicit)]
		public struct ExplicitLayout
		{
			[FieldOffset(0)]
			public int Field;
		}

		[TestMethod]
		public void ExplicitLayoutThrowsException()
		{
			try
			{
				new TypeDefiner().DefineTypes(new[] { typeof(ExplicitLayout) });
				Assert.Fail();
			}
			catch (NotSupportedException ex)
			{
				Assert.AreEqual("Custom structs with layout types other than sequential are not supported", ex.Message);
			}
		}

		[StructLayout(LayoutKind.Auto)]
		public struct AutoLayout
		{
			public int Field;
		}

		[TestMethod]
		public void AutoLayoutThrowsException()
		{
			try
			{
				new TypeDefiner().DefineTypes(new[] { typeof(AutoLayout) });
				Assert.Fail();
			}
			catch (NotSupportedException ex)
			{
				Assert.AreEqual("Custom structs with layout types other than sequential are not supported", ex.Message);
			}
		}

		[TestMethod]
		public void UnsupportedPrimitiveTypeThrowsException()
		{
			try
			{
				new TypeDefiner().DefineTypes(new[] { typeof(Byte) });
				Assert.Fail();
			}
			catch (NotImplementedException ex)
			{
				Assert.AreEqual("The primitive type \"System.Byte\" is currently unimplemented", ex.Message);
			}
		}
    }
}
