﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Compilers;
using System.Numerics;
using System.Linq.Expressions;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Vec4
{
    [TestClass]
    public sealed class Arithmetic
    {
        [TestMethod]
        public void Negate()
        {
            Assert.AreEqual(@"vec4 temp_0 = -parameter_input;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Vector4, Vector4>>)(
                input => -input)));
        }

        [TestMethod]
        public void AddVec4()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.0);
vec4 temp_1 = vec4(8.0);
vec4 temp_2 = temp_0 + temp_1;
return temp_2;",
            new FunctionCompiler().Compile((Expression<Func<Vector4>>)(
                () => new Vector4(4.0f) + new Vector4(8.0f))));
        }

        [TestMethod]
        public void SubtractVec4()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.0);
vec4 temp_1 = vec4(8.0);
vec4 temp_2 = temp_0 - temp_1;
return temp_2;",
            new FunctionCompiler().Compile((Expression<Func<Vector4>>)(
                () => new Vector4(4.0f) - new Vector4(8.0f))));
        }

        [TestMethod]
        public void MultiplyVec4()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.0);
vec4 temp_1 = vec4(8.0);
vec4 temp_2 = temp_0 * temp_1;
return temp_2;",
            new FunctionCompiler().Compile((Expression<Func<Vector4>>)(
                () => new Vector4(4.0f) * new Vector4(8.0f))));
        }

        [TestMethod]
        public void MultiplyFloat()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.0);
vec4 temp_1 = temp_0 * 8.0;
return temp_1;",
            new FunctionCompiler().Compile((Expression<Func<Vector4>>)(
                () => new Vector4(4.0f) * 8.0f)));
        }

        [TestMethod]
        public void DivideVec4()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.0);
vec4 temp_1 = vec4(8.0);
vec4 temp_2 = temp_0 / temp_1;
return temp_2;",
            new FunctionCompiler().Compile((Expression<Func<Vector4>>)(
                () => new Vector4(4.0f) / new Vector4(8.0f))));
        }

        [TestMethod]
        public void DivideFloat()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.0);
vec4 temp_1 = temp_0 / 8.0;
return temp_1;",
            new FunctionCompiler().Compile((Expression<Func<Vector4>>)(
                () => new Vector4(4.0f) / 8.0f)));
        }
    }
}
