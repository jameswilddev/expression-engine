﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;
using ExpressionEngine.Compilers;
using System.Linq.Expressions;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Vec4
{
    [TestClass]
    public sealed class Comparisons
    {
        [TestMethod]
        public void Equals()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left == parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Vector4, Vector4, Boolean>>)(
                (left, right) => left == right)));
        }

        [TestMethod]
        public void NotEqual()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left != parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Vector4, Vector4, Boolean>>)(
                (left, right) => left != right)));
        }

        [TestMethod]
        public void EqualsCall()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left == parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Vector4, Vector4, Boolean>>)(
                (left, right) => left.Equals(right))));
        }
    }
}
