﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq.Expressions;
using ExpressionEngine.Compilers;
using System.Numerics;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Vec4
{
    [TestClass]
    public sealed class Read
    {
        [TestMethod]
        public void Vec4ReadX()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.0);
return temp_0.x;",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => new Vector4(4.0f).X)));
        }

        [TestMethod]
        public void Vec4ReadY()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.0);
return temp_0.y;",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => new Vector4(4.0f).Y)));
        }

        [TestMethod]
        public void Vec4ReadZ()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.0);
return temp_0.z;",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => new Vector4(4.0f).Z)));
        }

        [TestMethod]
        public void Vec4ReadW()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.0);
return temp_0.w;",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => new Vector4(4.0f).W)));
        }
    }
}
