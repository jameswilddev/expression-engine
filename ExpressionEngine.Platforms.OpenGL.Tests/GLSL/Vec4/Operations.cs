﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Compilers;
using System.Linq.Expressions;
using System.Numerics;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Vec4
{
    [TestClass]
    public sealed class Operations
    {
        [TestMethod]
        public void Length()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(7.4);
float temp_1 = length(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<float>>)(
                    () => new Vector4(7.4f).Length())));
        }

        [TestMethod]
        public void Distance()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(7.4);
vec4 temp_1 = vec4(9.6);
float temp_2 = distance(temp_0, temp_1);
return temp_2;",
            new FunctionCompiler().Compile((Expression<Func<float>>)(
                () => Vector4.Distance(new Vector4(7.4f), new Vector4(9.6f)))));
        }

        [TestMethod]
        public void Sin()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.5);
vec4 temp_1 = sin(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector4(4.5f).Sin())));
        }

        [TestMethod]
        public void Cos()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.5);
vec4 temp_1 = cos(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector4(4.5f).Cos())));
        }

        [TestMethod]
        public void Sqrt()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.5);
vec4 temp_1 = sqrt(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => Vector4.SquareRoot(new Vector4(4.5f)))));
        }

        [TestMethod]
        public void InvSqrt()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.5);
vec4 temp_1 = inversesqrt(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector4(4.5f).InverseSqrt())));
        }

        [TestMethod]
        public void Pow()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.5);
vec4 temp_1 = vec4(5.0);
vec4 temp_2 = pow(temp_0, temp_1);
return temp_2;",
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector4(4.5f).Pow(new Vector4(5.0f)))));
        }

        [TestMethod]
        public void Abs()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.5);
vec4 temp_1 = abs(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => Vector4.Abs(new Vector4(4.5f)))));
        }

        [TestMethod]
        public void Min()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.5);
vec4 temp_1 = vec4(7.0);
vec4 temp_2 = min(temp_0, temp_1);
return temp_2;",
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => Vector4.Min(new Vector4(4.5f), new Vector4(7.0f)))));
        }

        [TestMethod]
        public void Max()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.5);
vec4 temp_1 = vec4(7.0);
vec4 temp_2 = max(temp_0, temp_1);
return temp_2;",
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => Vector4.Max(new Vector4(4.5f), new Vector4(7.0f)))));
        }

        [TestMethod]
        public void ModFloat()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.5);
vec4 temp_1 = mod(temp_0, 7.0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector4(4.5f).Mod(7.0f))));
        }

        [TestMethod]
        public void ModVec4()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.5);
vec4 temp_1 = vec4(7.0);
vec4 temp_2 = mod(temp_0, temp_1);
return temp_2;",
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector4(4.5f).Mod(new Vector4(7.0f)))));
        }

        [TestMethod]
        public void Fract()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.5);
vec4 temp_1 = fract(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector4(4.5f).Fract())));
        }

        [TestMethod]
        public void Floor()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.5);
vec4 temp_1 = floor(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector4(4.5f).Floor())));
        }

        [TestMethod]
        public void Ceil()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.5);
vec4 temp_1 = ceil(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector4(4.5f).Ceil())));
        }

        [TestMethod]
        public void ClampVec4()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.5);
vec4 temp_1 = vec4(7.0);
vec4 temp_2 = vec4(9.0);
vec4 temp_3 = clamp(temp_0, temp_1, temp_2);
return temp_3;",
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector4(4.5f).Clamp(new Vector4(7.0f), new Vector4(9.0f)))));
        }

        [TestMethod]
        public void ClampFloat()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.5);
vec4 temp_1 = clamp(temp_0, 7.0, 9.0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector4(4.5f).Clamp(7.0f, 9.0f))));
        }

        [TestMethod]
        public void MixVec4()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.5);
vec4 temp_1 = vec4(7.0);
vec4 temp_2 = vec4(9.0);
vec4 temp_3 = mix(temp_0, temp_1, temp_2);
return temp_3;",
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector4(4.5f).Mix(new Vector4(7.0f), new Vector4(9.0f)))));
        }

        [TestMethod]
        public void MixFloat()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.5);
vec4 temp_1 = vec4(7.0);
vec4 temp_2 = mix(temp_0, temp_1, 9.0);
return temp_2;",
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => Vector4.Lerp(new Vector4(4.5f), new Vector4(7.0f), 9.0f))));
        }

        [TestMethod]
        public void Dot()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.5);
vec4 temp_1 = vec4(7.0);
float temp_2 = dot(temp_0, temp_1);
return temp_2;",
                new FunctionCompiler().Compile((Expression<Func<float>>)(() => Vector4.Dot(new Vector4(4.5f), new Vector4(7.0f)))));
        }

        [TestMethod]
        public void Normalize()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.5);
vec4 temp_1 = normalize(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => Vector4.Normalize(new Vector4(4.5f)))));
        }

        [TestMethod]
        public void Reflect()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(4.5);
vec4 temp_1 = vec4(7.0);
vec4 temp_2 = reflect(temp_0, temp_1);
return temp_2;",
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector4(4.5f).Reflect(new Vector4(7.0f)))));
        }
    }
}
