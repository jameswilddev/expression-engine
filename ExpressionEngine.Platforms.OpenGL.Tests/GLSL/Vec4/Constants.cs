﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Compilers;
using System.Linq.Expressions;
using System.Numerics;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Vec4
{
    [TestClass]
    public sealed class Constants
    {
        [TestMethod]
        public void MemberInitFull()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(0.4, 0.7, 0.9, 0.2);
return temp_0;",
               new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector4 { Y = 0.7f, Z = 0.9f, W = 0.2f, X = 0.4f })));
        }

        [TestMethod]
        public void MemberInitPartial()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(0.4, 0.0, 0.0, 0.2);
return temp_0;",
               new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector4 { W = 0.2f, X = 0.4f })));
        }

        [TestMethod]
        public void MemberInitEmpty()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(0.0, 0.0, 0.0, 0.0);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(
                    () => new Vector4 { })));
        }

        [TestMethod]
        public void Default()
        {
            Assert.AreEqual(@"return vec4(0.0);",
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(
                    () => default(Vector4))));
        }

        [TestMethod]
        public void ConstructorMultipleParameters()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(0.4, 0.7, 0.9, 0.2);
return temp_0;",
               new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector4(0.4f, 0.7f, 0.9f, 0.2f))));
        }

        [TestMethod]
        public void ConstructorSingleParameter()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(7.4);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(
                    () => new Vector4(7.4f))));
        }

        [TestMethod]
        public void ConstructorParameterless()
        {
            Assert.AreEqual(@"return vec4(0.0);",
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(
                    () => new Vector4())));
        }
    }
}
