﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;
using Rhino.Mocks;
using System.Linq.Expressions;
using ExpressionEngine.Compilers;
using System.Linq;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL
{
	[TestClass]
	public sealed class CompilerTests
	{
		[TestMethod]
		public void SourceNullThrowsException()
		{
			try
			{
				new Compiler().Compile<Func<int>>(null);
				Assert.Fail();
			}
			catch (ArgumentNullException ex)
			{
				Assert.AreEqual("source", ex.ParamName);
				Assert.AreEqual(@"Value cannot be null.
Parameter name: source", ex.Message);
			}
		}

		[TestMethod]
		public void NoParametersThrowsException()
		{
			try
			{
				new Compiler().Compile<Func<Vector3>>(() => new Vector3());
				Assert.Fail();
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Assert.AreEqual("source", ex.ParamName);
				Assert.AreEqual(@"Must have at least one parameter
Parameter name: source", ex.Message);
			}
		}

		[TestMethod]
		public void FirstParameterNotVector2ThrowsException()
		{
			try
			{
				new Compiler().Compile<Func<float, int, Vector3>>((pixel, a) => new Vector3());
				Assert.Fail();
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Assert.AreEqual("source", ex.ParamName);
				Assert.AreEqual(@"The first parameter must be a Vector2
Parameter name: source", ex.Message);
			}
		}

		[TestMethod]
		public void FirstParameterNotCalledPixelThrowsException()
		{
			try
			{
				new Compiler().Compile<Func<Vector2, int, Vector3>>((pix, a) => new Vector3());
				Assert.Fail();
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Assert.AreEqual("source", ex.ParamName);
				Assert.AreEqual(@"The first parameter's name must be ""pixel""
Parameter name: source", ex.Message);
			}
		}

		[TestMethod]
		public void NoParametersOrCustomTypes()
		{
			var mocks = new MockRepository();

			var compiler = new Compiler
			{
				TypeFinder = mocks.StrictMock<ITypeFinder>(),
				TypeDefiner = mocks.StrictMock<ITypeDefiner>(),
				FunctionCompiler = mocks.StrictMock<IFunctionCompiler>()
			};

			var types = Enumerable.Empty<Type>();
			var expression = (Expression<Func<Vector2, Vector3>>)((pixel) => new Vector3());

			using(mocks.Record())
			using(mocks.Unordered())
			{
				compiler.TypeFinder.Expect(tf => tf.FindTypes(expression)).Return(types).Repeat.Once();
				compiler.TypeDefiner.Expect(td => td.DefineTypes(types)).Return("").Repeat.Once();
				compiler.FunctionCompiler.Expect(fc => fc.Compile(expression)).Return(@"function body
on multiple lines
	with indented section
and unindented section").Repeat.Once();
			}
		
			using(mocks.Playback())
			{
				Assert.AreEqual(@"vec3 run(vec2 parameter_pixel) {
	function body
	on multiple lines
		with indented section
	and unindented section
}

void main() {
	gl_FragColor = vec4(run(gl_FragCoord.xy), 1.0);
}", compiler.Compile(expression));
			}
		}

		[TestMethod]
		public void ParametersAndCustomTypes()
		{
			var mocks = new MockRepository();

			var compiler = new Compiler
			{
				TypeFinder = mocks.StrictMock<ITypeFinder>(),
				TypeDefiner = mocks.StrictMock<ITypeDefiner>(),
				FunctionCompiler = mocks.StrictMock<IFunctionCompiler>()
			};

			var types = Enumerable.Empty<Type>();
			var expression = (Expression<Func<Vector2, bool, int, float, Vector2, Vector3, Vector4, Matrix4x4, Vector3>>)((pixel, a, b, c, d, e, f, g) => new Vector3());

			using (mocks.Record())
			using (mocks.Unordered())
			{
				compiler.TypeFinder.Expect(tf => tf.FindTypes(expression)).Return(types).Repeat.Once();
				compiler.TypeDefiner.Expect(td => td.DefineTypes(types)).Return(@"multiple type
definitions
on their own lines").Repeat.Once();
				compiler.FunctionCompiler.Expect(fc => fc.Compile(expression)).Return(@"function body
on multiple lines
	with indented section
and unindented section").Repeat.Once();
			}

			using (mocks.Playback())
			{
				Assert.AreEqual(@"multiple type
definitions
on their own lines

uniform bool parameter_a;
uniform int parameter_b;
uniform float parameter_c;
uniform vec2 parameter_d;
uniform vec3 parameter_e;
uniform vec4 parameter_f;
uniform mat4 parameter_g;

vec3 run(vec2 parameter_pixel) {
	function body
	on multiple lines
		with indented section
	and unindented section
}

void main() {
	gl_FragColor = vec4(run(gl_FragCoord.xy), 1.0);
}", compiler.Compile(expression));
			}
		}

		[TestMethod]
		public void ParametersButNoCustomTypes()
		{
			var mocks = new MockRepository();

			var compiler = new Compiler
			{
				TypeFinder = mocks.StrictMock<ITypeFinder>(),
				TypeDefiner = mocks.StrictMock<ITypeDefiner>(),
				FunctionCompiler = mocks.StrictMock<IFunctionCompiler>()
			};

			var types = Enumerable.Empty<Type>();
			var expression = (Expression<Func<Vector2, bool, int, float, Vector2, Vector3, Vector4, Matrix4x4, Vector3>>)((pixel, a, b, c, d, e, f, g) => new Vector3());

			using (mocks.Record())
			using (mocks.Unordered())
			{
				compiler.TypeFinder.Expect(tf => tf.FindTypes(expression)).Return(types).Repeat.Once();
				compiler.TypeDefiner.Expect(td => td.DefineTypes(types)).Return(@"").Repeat.Once();
				compiler.FunctionCompiler.Expect(fc => fc.Compile(expression)).Return(@"function body
on multiple lines
	with indented section
and unindented section").Repeat.Once();
			}

			using (mocks.Playback())
			{
				Assert.AreEqual(@"uniform bool parameter_a;
uniform int parameter_b;
uniform float parameter_c;
uniform vec2 parameter_d;
uniform vec3 parameter_e;
uniform vec4 parameter_f;
uniform mat4 parameter_g;

vec3 run(vec2 parameter_pixel) {
	function body
	on multiple lines
		with indented section
	and unindented section
}

void main() {
	gl_FragColor = vec4(run(gl_FragCoord.xy), 1.0);
}", compiler.Compile(expression));
			}
		}

		[TestMethod]
		public void CustomTypesButNoParameters()
		{
			var mocks = new MockRepository();

			var compiler = new Compiler
			{
				TypeFinder = mocks.StrictMock<ITypeFinder>(),
				TypeDefiner = mocks.StrictMock<ITypeDefiner>(),
				FunctionCompiler = mocks.StrictMock<IFunctionCompiler>()
			};

			var types = Enumerable.Empty<Type>();
			var expression = (Expression<Func<Vector2, Vector3>>)(pixel => new Vector3());

			using (mocks.Record())
			using (mocks.Unordered())
			{
				compiler.TypeFinder.Expect(tf => tf.FindTypes(expression)).Return(types).Repeat.Once();
				compiler.TypeDefiner.Expect(td => td.DefineTypes(types)).Return(@"multiple type
definitions
on their own lines").Repeat.Once();
				compiler.FunctionCompiler.Expect(fc => fc.Compile(expression)).Return(@"function body
on multiple lines
	with indented section
and unindented section").Repeat.Once();
			}

			using (mocks.Playback())
			{
				Assert.AreEqual(@"multiple type
definitions
on their own lines

vec3 run(vec2 parameter_pixel) {
	function body
	on multiple lines
		with indented section
	and unindented section
}

void main() {
	gl_FragColor = vec4(run(gl_FragCoord.xy), 1.0);
}", compiler.Compile(expression));
			}
		}

		public struct CustomStruct
		{
			public int Field;
		}

		public struct CustomStructGeneric<T>
		{
			public T Field;
		}

		[TestMethod]
		public void CustomStructParameters()
		{
			var mocks = new MockRepository();

			var compiler = new Compiler
			{
				TypeFinder = mocks.StrictMock<ITypeFinder>(),
				TypeDefiner = mocks.StrictMock<ITypeDefiner>(),
				FunctionCompiler = mocks.StrictMock<IFunctionCompiler>()
			};

			var types = Enumerable.Empty<Type>();
			var expression = (Expression<Func<Vector2, CustomStruct, CustomStructGeneric<int>, Vector3>>)((pixel, a, b) => new Vector3());

			using (mocks.Record())
			using (mocks.Unordered())
			{
				compiler.TypeFinder.Expect(tf => tf.FindTypes(expression)).Return(types).Repeat.Once();
				compiler.TypeDefiner.Expect(td => td.DefineTypes(types)).Return(@"").Repeat.Once();
				compiler.FunctionCompiler.Expect(fc => fc.Compile(expression)).Return(@"function body
on multiple lines
	with indented section
and unindented section").Repeat.Once();
			}

			using (mocks.Playback())
			{
				Assert.AreEqual(@"uniform customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CompilerTests_CustomStruct parameter_a;
uniform customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CompilerTests_CustomStructGeneric__int parameter_b;

vec3 run(vec2 parameter_pixel) {
	function body
	on multiple lines
		with indented section
	and unindented section
}

void main() {
	gl_FragColor = vec4(run(gl_FragCoord.xy), 1.0);
}", compiler.Compile(expression));
			}
		}
	}
}
