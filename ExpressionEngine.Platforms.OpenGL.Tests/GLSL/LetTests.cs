﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL
{
	[TestClass]
	public sealed class LetTests
	{
		[TestMethod]
		public void UseNullThrowsException()
		{
			try
			{
				new FunctionCompiler().Compile<Func<int>>(() => 3.Let<int, int>(null));
				Assert.Fail();
			}
			catch (ArgumentNullException ex)
			{
				Assert.AreEqual("source", ex.ParamName);
				Assert.AreEqual(@"Let usages cannot be null
Parameter name: source", ex.Message);
			}
		}

		[TestMethod]
		public void Basic()
		{
			Assert.AreEqual(@"int temp_0 = parameter_y * 4;
int temp_1 = temp_0 - 5;
int temp_2 = temp_1 * temp_0;
return temp_2;", new FunctionCompiler().Compile<Func<int, int>>(y => (y * 4).Let(x => (x - 5) * x)));
		}
	}
}
