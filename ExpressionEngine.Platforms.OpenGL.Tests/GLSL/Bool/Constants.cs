﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Compilers;
using System.Linq.Expressions;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Bool
{
    [TestClass]
    public sealed class Constants
    {
        [TestMethod]
        public void True()
        {
            Assert.AreEqual(@"return true;",
            new FunctionCompiler().Compile((Expression<Func<Boolean>>)(
                () => true)));
        }

        [TestMethod]
        public void False()
        {
            Assert.AreEqual(@"return false;",
            new FunctionCompiler().Compile((Expression<Func<Boolean>>)(
                () => false)));
        }
    }
}
