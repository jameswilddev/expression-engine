﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Compilers;
using System.Linq.Expressions;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Bool
{
    [TestClass]
    public sealed class Operators
    {
        [TestMethod]
        public void Not()
        {
            Assert.AreEqual(@"bool temp_0 = !parameter_input;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Boolean, Boolean>>)(
                input => !input)));
        }

        [TestMethod]
        public void And()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left && parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Boolean, Boolean, Boolean>>)(
                (left, right) => left && right)));
        }

        [TestMethod]
        public void Or()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left || parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Boolean, Boolean, Boolean>>)(
                (left, right) => left || right)));
        }

        [TestMethod]
        public void Equals()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left == parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Boolean, Boolean, Boolean>>)(
                (left, right) => left == right)));
        }

        [TestMethod]
        public void EqualsCall()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left == parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Boolean, Boolean, Boolean>>)(
                (left, right) => left.Equals(right))));
        }

        [TestMethod]
        public void NotEqual()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left != parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Boolean, Boolean, Boolean>>)(
                (left, right) => left != right)));
        }

        [TestMethod]
        public void Conditional()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_a > 3;
int temp_1 = temp_0 ? parameter_b : 4;
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<int, int, int>>)(
                    (a, b) => a > 3 ? b : 4)));
        }
    }
}
