﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Compilers;
using System.Numerics;
using System.Linq.Expressions;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Vec3
{
    [TestClass]
    public sealed class Constants
    {
        [TestMethod]
        public void MemberInitFull()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(0.4, 0.7, 0.9);
return temp_0;",
               new FunctionCompiler().Compile((Expression<Func<Vector3>>)(() => new Vector3 { Y = 0.7f, Z = 0.9f, X = 0.4f })));
        }

        [TestMethod]
        public void MemberInitPartial()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(0.4, 0.0, 0.9);
return temp_0;",
               new FunctionCompiler().Compile((Expression<Func<Vector3>>)(() => new Vector3 { Z = 0.9f, X = 0.4f })));
        }

        [TestMethod]
        public void MemberInitEmpty()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(0.0, 0.0, 0.0);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<Vector3>>)(
                    () => new Vector3 { })));
        }

        [TestMethod]
        public void Default()
        {
            Assert.AreEqual(@"return vec3(0.0);",
                new FunctionCompiler().Compile((Expression<Func<Vector3>>)(
                    () => default(Vector3))));
        }

        [TestMethod]
        public void ConstructorMultipleParameters()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(0.4, 0.7, 0.9);
return temp_0;",
               new FunctionCompiler().Compile((Expression<Func<Vector3>>)(() => new Vector3(0.4f, 0.7f, 0.9f))));
        }

        [TestMethod]
        public void ConstructorSingleParameter()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(7.4);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<Vector3>>)(
                    () => new Vector3(7.4f))));
        }

        [TestMethod]
        public void ConstructorParameterless()
        {
            Assert.AreEqual(@"return vec3(0.0);",
                new FunctionCompiler().Compile((Expression<Func<Vector3>>)(
                    () => new Vector3())));
        }
    }
}
