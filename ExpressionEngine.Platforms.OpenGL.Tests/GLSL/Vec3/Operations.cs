﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;
using System.Linq.Expressions;
using ExpressionEngine.Compilers;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Vec3
{
    [TestClass]
    public sealed class Operations
    {
        [TestMethod]
        public void Sin()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.5);
vec3 temp_1 = sin(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector3>>)(() => new Vector3(4.5f).Sin())));
        }

        [TestMethod]
        public void Cos()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.5);
vec3 temp_1 = cos(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector3>>)(() => new Vector3(4.5f).Cos())));
        }

        [TestMethod]
        public void Sqrt()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.5);
vec3 temp_1 = sqrt(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector3>>)(() => Vector3.SquareRoot(new Vector3(4.5f)))));
        }

        [TestMethod]
        public void InvSqrt()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.5);
vec3 temp_1 = inversesqrt(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector3>>)(() => new Vector3(4.5f).InverseSqrt())));
        }

        [TestMethod]
        public void Pow()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.5);
vec3 temp_1 = vec3(5.0);
vec3 temp_2 = pow(temp_0, temp_1);
return temp_2;",
                new FunctionCompiler().Compile((Expression<Func<Vector3>>)(() => new Vector3(4.5f).Pow(new Vector3(5.0f)))));
        }

        [TestMethod]
        public void Abs()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.5);
vec3 temp_1 = abs(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector3>>)(() => Vector3.Abs(new Vector3(4.5f)))));
        }

        [TestMethod]
        public void Min()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.5);
vec3 temp_1 = vec3(7.0);
vec3 temp_2 = min(temp_0, temp_1);
return temp_2;",
                new FunctionCompiler().Compile((Expression<Func<Vector3>>)(() => Vector3.Min(new Vector3(4.5f), new Vector3(7.0f)))));
        }

        [TestMethod]
        public void Max()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.5);
vec3 temp_1 = vec3(7.0);
vec3 temp_2 = max(temp_0, temp_1);
return temp_2;",
                new FunctionCompiler().Compile((Expression<Func<Vector3>>)(() => Vector3.Max(new Vector3(4.5f), new Vector3(7.0f)))));
        }

        [TestMethod]
        public void ModFloat()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.5);
vec3 temp_1 = mod(temp_0, 7.0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector3>>)(() => new Vector3(4.5f).Mod(7.0f))));
        }

        [TestMethod]
        public void ModVec3()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.5);
vec3 temp_1 = vec3(7.0);
vec3 temp_2 = mod(temp_0, temp_1);
return temp_2;",
                new FunctionCompiler().Compile((Expression<Func<Vector3>>)(() => new Vector3(4.5f).Mod(new Vector3(7.0f)))));
        }

        [TestMethod]
        public void Fract()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.5);
vec3 temp_1 = fract(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector3>>)(() => new Vector3(4.5f).Fract())));
        }

        [TestMethod]
        public void Floor()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.5);
vec3 temp_1 = floor(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector3>>)(() => new Vector3(4.5f).Floor())));
        }

        [TestMethod]
        public void Ceil()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.5);
vec3 temp_1 = ceil(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector3>>)(() => new Vector3(4.5f).Ceil())));
        }

        [TestMethod]
        public void ClampVec3()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.5);
vec3 temp_1 = vec3(7.0);
vec3 temp_2 = vec3(9.0);
vec3 temp_3 = clamp(temp_0, temp_1, temp_2);
return temp_3;",
                new FunctionCompiler().Compile((Expression<Func<Vector3>>)(() => new Vector3(4.5f).Clamp(new Vector3(7.0f), new Vector3(9.0f)))));
        }

        [TestMethod]
        public void ClampFloat()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.5);
vec3 temp_1 = clamp(temp_0, 7.0, 9.0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector3>>)(() => new Vector3(4.5f).Clamp(7.0f, 9.0f))));
        }

        [TestMethod]
        public void MixVec3()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.5);
vec3 temp_1 = vec3(7.0);
vec3 temp_2 = vec3(9.0);
vec3 temp_3 = mix(temp_0, temp_1, temp_2);
return temp_3;",
                new FunctionCompiler().Compile((Expression<Func<Vector3>>)(() => new Vector3(4.5f).Mix(new Vector3(7.0f), new Vector3(9.0f)))));
        }

        [TestMethod]
        public void MixFloat()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.5);
vec3 temp_1 = vec3(7.0);
vec3 temp_2 = mix(temp_0, temp_1, 9.0);
return temp_2;",
                new FunctionCompiler().Compile((Expression<Func<Vector3>>)(() => Vector3.Lerp(new Vector3(4.5f), new Vector3(7.0f), 9.0f))));
        }

        [TestMethod]
        public void Dot()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.5);
vec3 temp_1 = vec3(7.0);
float temp_2 = dot(temp_0, temp_1);
return temp_2;",
                new FunctionCompiler().Compile((Expression<Func<float>>)(() => Vector3.Dot(new Vector3(4.5f), new Vector3(7.0f)))));
        }

        [TestMethod]
        public void Normalize()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.5);
vec3 temp_1 = normalize(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector3>>)(() => Vector3.Normalize(new Vector3(4.5f)))));
        }

        [TestMethod]
        public void Reflect()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.5);
vec3 temp_1 = vec3(7.0);
vec3 temp_2 = reflect(temp_0, temp_1);
return temp_2;",
                new FunctionCompiler().Compile((Expression<Func<Vector3>>)(() => Vector3.Reflect(new Vector3(4.5f), new Vector3(7.0f)))));
        }

        [TestMethod]
        public void Cross()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.5);
vec3 temp_1 = vec3(7.0);
vec3 temp_2 = cross(temp_0, temp_1);
return temp_2;",
                new FunctionCompiler().Compile((Expression<Func<Vector3>>)(() => Vector3.Cross(new Vector3(4.5f), new Vector3(7.0f)))));
        }

        [TestMethod]
        public void Length()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(7.4);
float temp_1 = length(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<float>>)(
                    () => new Vector3(7.4f).Length())));
        }

        [TestMethod]
        public void Distance()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(7.4);
vec3 temp_1 = vec3(9.6);
float temp_2 = distance(temp_0, temp_1);
return temp_2;",
            new FunctionCompiler().Compile((Expression<Func<float>>)(
                () => Vector3.Distance(new Vector3(7.4f), new Vector3(9.6f)))));
        }
    }
}
