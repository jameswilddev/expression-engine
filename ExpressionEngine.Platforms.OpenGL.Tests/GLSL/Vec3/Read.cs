﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Compilers;
using System.Numerics;
using System.Linq.Expressions;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Vec3
{
    [TestClass]
    public sealed class Read
    {
        [TestMethod]
        public void  X()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.0);
return temp_0.x;",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => new Vector3(4.0f).X)));
        }

        [TestMethod]
        public void  Y()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.0);
return temp_0.y;",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => new Vector3(4.0f).Y)));
        }

        [TestMethod]
        public void  Z()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.0);
return temp_0.z;",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => new Vector3(4.0f).Z)));
        }
    }
}
