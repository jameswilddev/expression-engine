﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Compilers;
using System.Numerics;
using System.Linq.Expressions;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Vec3
{
    [TestClass]
    public sealed class Arithmetic
    {
        [TestMethod]
        public void Vec3Negate()
        {
            Assert.AreEqual(@"vec3 temp_0 = -parameter_input;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Vector3, Vector3>>)(
                input => -input)));
        }

        [TestMethod]
        public void Vec3AddVec3()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.0);
vec3 temp_1 = vec3(8.0);
vec3 temp_2 = temp_0 + temp_1;
return temp_2;",
            new FunctionCompiler().Compile((Expression<Func<Vector3>>)(
                () => new Vector3(4.0f) + new Vector3(8.0f))));
        }

        [TestMethod]
        public void Vec3SubtractVec3()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.0);
vec3 temp_1 = vec3(8.0);
vec3 temp_2 = temp_0 - temp_1;
return temp_2;",
            new FunctionCompiler().Compile((Expression<Func<Vector3>>)(
                () => new Vector3(4.0f) - new Vector3(8.0f))));
        }

        [TestMethod]
        public void Vec3MultiplyVec3()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.0);
vec3 temp_1 = vec3(8.0);
vec3 temp_2 = temp_0 * temp_1;
return temp_2;",
            new FunctionCompiler().Compile((Expression<Func<Vector3>>)(
                () => new Vector3(4.0f) * new Vector3(8.0f))));
        }

        [TestMethod]
        public void Vec3MultiplyFloat()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.0);
vec3 temp_1 = temp_0 * 8.0;
return temp_1;",
            new FunctionCompiler().Compile((Expression<Func<Vector3>>)(
                () => new Vector3(4.0f) * 8.0f)));
        }

        [TestMethod]
        public void Vec3DivideVec3()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.0);
vec3 temp_1 = vec3(8.0);
vec3 temp_2 = temp_0 / temp_1;
return temp_2;",
            new FunctionCompiler().Compile((Expression<Func<Vector3>>)(
                () => new Vector3(4.0f) / new Vector3(8.0f))));
        }

        [TestMethod]
        public void Vec3DivideFloat()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(4.0);
vec3 temp_1 = temp_0 / 8.0;
return temp_1;",
            new FunctionCompiler().Compile((Expression<Func<Vector3>>)(
                () => new Vector3(4.0f) / 8.0f)));
        }
    }
}
