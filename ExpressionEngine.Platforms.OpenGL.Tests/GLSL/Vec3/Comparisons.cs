﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;
using System.Linq.Expressions;
using ExpressionEngine.Compilers;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Vec3
{
    [TestClass]
    public sealed class Comparisons
    {
        [TestMethod]
        public void Vec3Equals()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left == parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Vector3, Vector3, Boolean>>)(
                (left, right) => left == right)));
        }

        [TestMethod]
        public void Vec3NotEqual()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left != parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Vector3, Vector3, Boolean>>)(
                (left, right) => left != right)));
        }

        [TestMethod]
        public void Vec3EqualsCall()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left == parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Vector3, Vector3, Boolean>>)(
                (left, right) => left.Equals(right))));
        }
    }
}
