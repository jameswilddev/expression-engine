﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;
using System.Linq.Expressions;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Vec3
{
    [TestClass]
    public sealed class SwizzleVec4
    {
        [TestMethod]
        public void Repeat()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(0.4, 0.7, 0.9);
return temp_0.yzxy;",
               new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector3 { X = 0.4f, Y = 0.7f, Z = 0.9f }.Swizzle(v => v.Y, v => v.Z, v => v.X, v => v.Y))));
        }

        [TestMethod]
        public void XNullThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector3 { X = 0.4f, Y = 0.7f }.Swizzle(null, v => v.Y, v => v.Y, v => v.Y)));
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("source", ex.ParamName);
                Assert.AreEqual(@"Swizzle selectors cannot be null
Parameter name: source", ex.Message);
            }
        }

        [TestMethod]
        public void YNullThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector3 { X = 0.4f, Y = 0.7f }.Swizzle(v => v.Y, null, v => v.Y, v => v.Y)));
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("source", ex.ParamName);
                Assert.AreEqual(@"Swizzle selectors cannot be null
Parameter name: source", ex.Message);
            }
        }

        [TestMethod]
        public void ZNullThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector3 { X = 0.4f, Y = 0.7f }.Swizzle(v => v.Y, v => v.Y, null, v => v.Y)));
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("source", ex.ParamName);
                Assert.AreEqual(@"Swizzle selectors cannot be null
Parameter name: source", ex.Message);
            }
        }

        [TestMethod]
        public void WNullThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector3 { X = 0.4f, Y = 0.7f }.Swizzle(v => v.Y, v => v.Y, v => v.Y, null)));
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("source", ex.ParamName);
                Assert.AreEqual(@"Swizzle selectors cannot be null
Parameter name: source", ex.Message);
            }
        }

        [TestMethod]
        public void XNonMemberSelect()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector3 { X = 0.4f, Y = 0.7f, Z = 0.9f }.Swizzle(v => 3.0f, v => v.Y, v => v.Y, v => v.Y)));
                Assert.Fail();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Assert.AreEqual("source", ex.ParamName);
                Assert.AreEqual(@"Swizzles should only select a member from the input vector
Parameter name: source", ex.Message);
            }
        }

        [TestMethod]
        public void YNonMemberSelect()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector3 { X = 0.4f, Y = 0.7f, Z = 0.9f }.Swizzle(v => v.X, v => 3.0f, v => v.Y, v => v.Y)));
                Assert.Fail();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Assert.AreEqual("source", ex.ParamName);
                Assert.AreEqual(@"Swizzles should only select a member from the input vector
Parameter name: source", ex.Message);
            }
        }

        [TestMethod]
        public void ZNonMemberSelect()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector3 { X = 0.4f, Y = 0.7f, Z = 0.9f }.Swizzle(v => v.X, v => v.Y, v => 3.0f, v => v.Y)));
                Assert.Fail();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Assert.AreEqual("source", ex.ParamName);
                Assert.AreEqual(@"Swizzles should only select a member from the input vector
Parameter name: source", ex.Message);
            }
        }

        [TestMethod]
        public void WNonMemberSelect()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector3 { X = 0.4f, Y = 0.7f, Z = 0.9f }.Swizzle(v => v.X, v => v.Y, v => v.Y, v => 3.0f)));
                Assert.Fail();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Assert.AreEqual("source", ex.ParamName);
                Assert.AreEqual(@"Swizzles should only select a member from the input vector
Parameter name: source", ex.Message);
            }
        }
    }
}
