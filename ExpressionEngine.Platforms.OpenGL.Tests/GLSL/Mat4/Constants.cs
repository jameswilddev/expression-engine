﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq.Expressions;
using System.Numerics;
using ExpressionEngine.Compilers;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Mat4
{
    [TestClass]
    public sealed class Constants
    {
        [TestMethod]
        public void Default()
        {
            Assert.AreEqual(@"return mat4(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);",
                new FunctionCompiler().Compile((Expression<Func<Matrix4x4>>)(
                    () => default(Matrix4x4))));
        }

        [TestMethod]
        public void ParameterlessConstructor()
        {
            Assert.AreEqual(@"mat4 temp_0 = mat4(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<Matrix4x4>>)(
                    () => new Matrix4x4())));
        }

        [TestMethod]
        public void Identity()
        {
            Assert.AreEqual(@"return mat4(1.0);",
            new FunctionCompiler().Compile((Expression<Func<Matrix4x4>>)(
                () => Matrix4x4.Identity)));
        }

        [TestMethod]
        public void MemberInitEmpty()
        {
            Assert.AreEqual(@"mat4 temp_0 = mat4(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<Matrix4x4>>)(
                    () => new Matrix4x4 { })));
        }

        [TestMethod]
        public void MemberInitPartial()
        {
            Assert.AreEqual(@"mat4 temp_0 = mat4(0.0, 0.0, 0.0, 6.0, 0.0, 0.0, 0.0, 7.0, 4.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<Matrix4x4>>)(
                    () => new Matrix4x4 { M13 = 4.0f, M42 = 7.0f, M41 = 6.0f })));
        }

        [TestMethod]
        public void ConstructorMultipleParameters()
        {
            Assert.AreEqual(@"mat4 temp_0 = mat4(1.0, 5.0, 9.0, 13.0, 2.0, 6.0, 10.0, 14.0, 3.0, 7.0, 11.0, 15.0, 4.0, 8.0, 12.0, 16.0);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<Matrix4x4>>)(
                    () => new Matrix4x4(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f, 16.0f))));
        }

        [TestMethod]
        public void ConstructorParameterless()
        {
            Assert.AreEqual(@"mat4 temp_0 = mat4(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<Matrix4x4>>)(
                    () => new Matrix4x4())));
        }
    }
}
