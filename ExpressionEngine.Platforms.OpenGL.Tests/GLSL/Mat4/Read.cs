﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Compilers;
using System.Linq.Expressions;
using System.Numerics;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Mat4
{
    [TestClass]
    public sealed class Read
    {
        [TestMethod]
        public void XX()
        {
            Assert.AreEqual(@"return mat4(1.0)[0][0];",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => Matrix4x4.Identity.M11)));
        }

        [TestMethod]
        public void XY()
        {
            Assert.AreEqual(@"return mat4(1.0)[0][1];",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => Matrix4x4.Identity.M21)));
        }

        [TestMethod]
        public void XZ()
        {
            Assert.AreEqual(@"return mat4(1.0)[0][2];",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => Matrix4x4.Identity.M31)));
        }

        [TestMethod]
        public void XW()
        {
            Assert.AreEqual(@"return mat4(1.0)[0][3];",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => Matrix4x4.Identity.M41)));
        }

        [TestMethod]
        public void YX()
        {
            Assert.AreEqual(@"return mat4(1.0)[1][0];",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => Matrix4x4.Identity.M12)));
        }

        [TestMethod]
        public void YY()
        {
            Assert.AreEqual(@"return mat4(1.0)[1][1];",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => Matrix4x4.Identity.M22)));
        }

        [TestMethod]
        public void YZ()
        {
            Assert.AreEqual(@"return mat4(1.0)[1][2];",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => Matrix4x4.Identity.M32)));
        }

        [TestMethod]
        public void YW()
        {
            Assert.AreEqual(@"return mat4(1.0)[1][3];",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => Matrix4x4.Identity.M42)));
        }

        [TestMethod]
        public void ZX()
        {
            Assert.AreEqual(@"return mat4(1.0)[2][0];",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => Matrix4x4.Identity.M13)));
        }

        [TestMethod]
        public void ZY()
        {
            Assert.AreEqual(@"return mat4(1.0)[2][1];",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => Matrix4x4.Identity.M23)));
        }

        [TestMethod]
        public void ZZ()
        {
            Assert.AreEqual(@"return mat4(1.0)[2][2];",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => Matrix4x4.Identity.M33)));
        }

        [TestMethod]
        public void ZW()
        {
            Assert.AreEqual(@"return mat4(1.0)[2][3];",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => Matrix4x4.Identity.M43)));
        }

        [TestMethod]
        public void WX()
        {
            Assert.AreEqual(@"return mat4(1.0)[3][0];",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => Matrix4x4.Identity.M14)));
        }

        [TestMethod]
        public void WY()
        {
            Assert.AreEqual(@"return mat4(1.0)[3][1];",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => Matrix4x4.Identity.M24)));
        }

        [TestMethod]
        public void WZ()
        {
            Assert.AreEqual(@"return mat4(1.0)[3][2];",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => Matrix4x4.Identity.M34)));
        }

        [TestMethod]
        public void WW()
        {
            Assert.AreEqual(@"return mat4(1.0)[3][3];",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => Matrix4x4.Identity.M44)));
        }
    }
}
