﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;
using System.Linq.Expressions;
using ExpressionEngine.Compilers;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Mat4
{
    [TestClass]
    public sealed class Comparisons
    {
        [TestMethod]
        public void Equals()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left == parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Matrix4x4, Matrix4x4, bool>>)(
                (left, right) => left == right)));
        }

        [TestMethod]
        public void NotEqual()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left != parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Matrix4x4, Matrix4x4, bool>>)(
                (left, right) => left != right)));
        }

        [TestMethod]
        public void EqualsCall()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left == parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Matrix4x4, Matrix4x4, bool>>)(
                (left, right) => left.Equals(right))));
        }
    }
}
