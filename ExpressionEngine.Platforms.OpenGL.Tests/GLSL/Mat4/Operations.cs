﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Compilers;
using System.Linq.Expressions;
using System.Numerics;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Mat4
{
    [TestClass]
    public sealed class Operations
    {
        [TestMethod]
        public void Mat4()
        {
            Assert.AreEqual(@"mat4 temp_0 = parameter_a * parameter_b;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Matrix4x4, Matrix4x4, Matrix4x4>>)(
                (a, b) => a * b)));
        }

        [TestMethod]
        public void Vec2()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec4(parameter_a, 0.0, 1.0) * parameter_b;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Vector2, Matrix4x4, Vector2>>)(
                (a, b) => Vector2.Transform(a, b))));
        }

        [TestMethod]
        public void Vec3()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec4(parameter_a, 1.0) * parameter_b;
return temp_0;",
           new FunctionCompiler().Compile((Expression<Func<Vector3, Matrix4x4, Vector3>>)(
               (a, b) => Vector3.Transform(a, b))));
        }

        [TestMethod]
        public void Vec4()
        {
            Assert.AreEqual(@"vec4 temp_0 = parameter_a * parameter_b;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Vector4, Matrix4x4, Vector4>>)(
                (a, b) => Vector4.Transform(a, b))));
        }

        [TestMethod]
        public void Transpose()
        {
            Assert.AreEqual(@"mat4 temp_0 = transpose(parameter_input);
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Matrix4x4, Matrix4x4>>)(
                input => Matrix4x4.Transpose(input))));
        }
    }
}
