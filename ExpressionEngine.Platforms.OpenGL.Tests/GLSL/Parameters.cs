﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq.Expressions;
using ExpressionEngine.Compilers;
using ExpressionEngine.Platforms.OpenGL.Tests.GLSL.CustomStructs;
using System.Collections.Generic;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL
{
    [TestClass]
    public sealed class Parameters
    {
        [TestMethod]
        public void ReadPropertyOfParameterThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile(
                    (Expression<Func<CustomStruct, int>>)(input => input.InstancePropertyIgnored));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Access of members other than fields is not supported", ex.Message);
            }
        }

        [TestMethod]
        public void ReferenceTypeParameterThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<List<Byte>, int>>)(input => 1));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Reference types are not supported", ex.Message);
            }
        }

        [TestMethod]
        public void ReadParameter()
        {
            Assert.AreEqual(@"return parameter_b;",
                new FunctionCompiler().Compile((Expression<Func<int, int, int, int>>)(
                    (a, b, c) => b)));
        }
    }
}
