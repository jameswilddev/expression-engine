﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Compilers;
using System.Linq.Expressions;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.CustomStructs
{
    [TestClass]
    public sealed class Comparisons
    {
        [TestMethod]
        public void CustomStructEquals()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_a == parameter_b;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<CustomStruct, CustomStruct, Boolean>>)(
                (a, b) => a == b)));
        }

        [TestMethod]
        public void CustomStructNotEqual()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_a != parameter_b;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<CustomStruct, CustomStruct, Boolean>>)(
                (a, b) => a != b)));
        }

        [TestMethod]
        public void CustomStructEqualsCall()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_a == parameter_b;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<CustomStruct, CustomStruct, Boolean>>)(
                (a, b) => a.Equals(b))));
        }
    }
}
