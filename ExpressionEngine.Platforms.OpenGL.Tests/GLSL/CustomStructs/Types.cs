﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.CustomStructs
{
    public struct CustomStruct
    {
        public int ValueA;
        public float ValueB;
        public int InstancePropertyIgnored { get; set; }
        public event Action InstanceEventIgnored;
        public int InstanceMethodIgnored() { throw new NotImplementedException(); }
        public int this[int instanceIndexerIgnored] { get { throw new NotImplementedException(); } set { throw new NotImplementedException(); } }
        public static int StaticFieldIgnored;
        public static int StaticPropertyIgnored { get; set; }
        public static event Action StaticEventIgnored;
        public static void StaticMethodIgnored() { throw new NotImplementedException(); }
        public CustomStruct(int ignoredConstructor) { throw new NotImplementedException(); }

        public static bool operator ==(CustomStruct a, CustomStruct b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(CustomStruct a, CustomStruct b)
        {
            return !a.Equals(b);
        }
    }

    public struct MultiLevelStruct
    {
        public int ValueC;
        public CustomStruct ValueD;
    }

    public struct CustomStructWithGeneric<T>
    {
        public T Value;
    }
}
