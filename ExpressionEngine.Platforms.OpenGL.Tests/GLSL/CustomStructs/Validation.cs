﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Compilers;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.Collections.Generic;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.CustomStructs
{
    [TestClass]
    public sealed class Validation
    {
        public struct EmptyCustomStruct
        {
            public int InstancePropertyIgnored { get; set; }
            public event Action InstanceEventIgnored;
            public void InstanceMethodIgnored() { throw new NotImplementedException(); }
            public int this[int instanceIndexerIgnored] { get { throw new NotImplementedException(); } set { throw new NotImplementedException(); } }
            public static int StaticFieldIgnored;
            public static int StaticPropertyIgnored { get; set; }
            public static event Action StaticEventIgnored;
            public static void StaticMethodIgnored() { throw new NotImplementedException(); }
        }

        [TestMethod]
        public void DefaultCustomStructEmptyThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<EmptyCustomStruct>>)(
                () => default(EmptyCustomStruct)));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Empty custom structs are not supported", ex.Message);
            }
        }

        [TestMethod]
        public void ParameterlessCustomStructEmptyThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<EmptyCustomStruct>>)(
                () => new EmptyCustomStruct()));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Empty custom structs are not supported", ex.Message);
            }
        }

        public struct CustomStructContainingEmptyStruct
        {
            public EmptyCustomStruct Value;
        }

        [TestMethod]
        public void CustomStructEmptyFieldParameterThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructContainingEmptyStruct, int>>)(
                input => 4));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Empty custom structs are not supported", ex.Message);
            }
        }

        [TestMethod]
        public void CustomStructEmptyFieldDefaultThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructContainingEmptyStruct>>)(
                () => default(CustomStructContainingEmptyStruct)));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Empty custom structs are not supported", ex.Message);
            }
        }

        [TestMethod]
        public void CustomStructEmptyFieldParameterlessThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructContainingEmptyStruct>>)(
                () => new CustomStructContainingEmptyStruct()));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Empty custom structs are not supported", ex.Message);
            }
        }

        [TestMethod]
        public void CustomStructEmptyFieldMemberwiseThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructContainingEmptyStruct>>)(
                () => new CustomStructContainingEmptyStruct { }));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Empty custom structs are not supported", ex.Message);
            }
        }

        [StructLayout(LayoutKind.Auto)]
        public struct CustomStructWithAutoLayout
        {
            public int Value;
        }

        [TestMethod]
        public void AutoLayoutStructThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<int>>)(
                () => new CustomStructWithAutoLayout().Value));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Custom structs with layout types other than sequential are not supported", ex.Message);
            }
        }

        [StructLayout(LayoutKind.Explicit)]
        public struct CustomStructWithExplicitLayout
        {
            [FieldOffset(0)]
            public int Value;
        }

        [TestMethod]
        public void ExplicitLayoutStructThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<int>>)(
                () => new CustomStructWithExplicitLayout().Value));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Custom structs with layout types other than sequential are not supported", ex.Message);
            }
        }

        public struct CustomStructWithReferenceTypeField
        {
            public int ValidFieldA;
            public ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Misc.ReferenceTypeContainingField InvalidField;
            public float ValidFieldB;
        }

        [TestMethod]
        public void SettingReferenceTypeFieldInCustomStructThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile(
                    (Expression<Func<int>>)(() => new CustomStructWithReferenceTypeField { InvalidField = { Field = 3 } }.ValidFieldA));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Reference types are not supported", ex.Message);
            }
        }

        [TestMethod]
        public void NotSettingReferenceTypeFieldInCustomStructThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile(
                    (Expression<Func<int>>)(() => new CustomStructWithReferenceTypeField { }.ValidFieldA));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Reference types are not supported", ex.Message);
            }
        }

        public struct CustomStructWithArrayField
        {
            public List<int> Value;
        }

        [TestMethod]
        public void ArrayConstructorInMemberInitThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructWithArrayField>>)(
                    () => new CustomStructWithArrayField { Value = { 1, 2, 3 } }));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Reference types are not supported", ex.Message);
            }
        }

        [TestMethod]
        public void SettingPropertyInMemberInitializerThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStruct>>)(() => new CustomStruct { InstancePropertyIgnored = 3 }));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Initializing members other than fields in a member initializer is not supported", ex.Message);
            }
        }
    }
}
