﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Compilers;
using System.Linq.Expressions;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.CustomStructs
{
    [TestClass]
    public sealed class Read
    {
        [TestMethod]
        public void ReadMemberOfCustomStruct()
        {
            Assert.AreEqual(@"customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_CustomStruct temp_0 = customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_CustomStruct(0, 0.0);
return temp_0.ValueB;",
                new FunctionCompiler().Compile((Expression<Func<float>>)(
                    () => new CustomStruct { }.ValueB)));
        }

        [TestMethod]
        public void ReadMemberOfMemberOfCustomStruct()
        {
            Assert.AreEqual(@"customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_CustomStruct temp_0 = customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_CustomStruct(6, 3.1);
customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_MultiLevelStruct temp_1 = customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_MultiLevelStruct(-9, temp_0);
return temp_1.ValueD.ValueA;",
                new FunctionCompiler().Compile((Expression<Func<int>>)(
                    () => new MultiLevelStruct { ValueD = new CustomStruct { ValueB = 3.1f, ValueA = 6 }, ValueC = -9 }.ValueD.ValueA)));
        }
    }
}
