﻿
using ExpressionEngine.Compilers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.CustomStructs
{
    [TestClass]
    public sealed class Constants
    {
        [TestMethod]
        public void Default()
        {
            Assert.AreEqual(@"return customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_CustomStruct(0, 0.0);",
                new FunctionCompiler().Compile((Expression<Func<CustomStruct>>)(
                    () => default(CustomStruct))));
        }

        [TestMethod]
        public void MemberInitFull()
        {
            Assert.AreEqual(@"customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_CustomStruct temp_0 = customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_CustomStruct(8, 3.0);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<CustomStruct>>)(
                    () => new CustomStruct { ValueB = 3.0f, ValueA = 8 })));
        }

        [TestMethod]
        public void MemberInitPartial()
        {
            Assert.AreEqual(@"customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_CustomStruct temp_0 = customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_CustomStruct(0, 3.0);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<CustomStruct>>)(
                    () => new CustomStruct { ValueB = 3.0f })));
        }

        [Ignore]
        public void MemberInitEmpty()
        {
            Assert.AreEqual(@"customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_CustomStruct temp_0 = customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_CustomStruct(0, 0.0);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<CustomStruct>>)(
                    () => new CustomStruct { })));
        }

        [TestMethod]
        public void ConstructorParameterless()
        {
            Assert.AreEqual(@"customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_CustomStruct temp_0 = customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_CustomStruct(0, 0.0);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<CustomStruct>>)(
                    () => new CustomStruct())));
        }

        [TestMethod]
        public void ConstructorWithParametersThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStruct>>)(
                () => new CustomStruct(3)));
                Assert.Fail();
            }
            catch (NotImplementedException ex)
            {
                Assert.AreEqual("Constructor expressions with parameters for type \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.CustomStructs.CustomStruct\" are currently unimplemented", ex.Message);
            }
        }

        [TestMethod]
        public void MultiLevelNotDefined()
        {
            Assert.AreEqual(@"customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_MultiLevelStruct temp_0 = customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_MultiLevelStruct(-9, customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_CustomStruct(0, 0.0));
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<MultiLevelStruct>>)(
                    () => new MultiLevelStruct { ValueC = -9 })));
        }

        [TestMethod]
        public void MultiLevelInline()
        {
            Assert.AreEqual(@"customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_CustomStruct temp_0 = customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_CustomStruct(6, 3.1);
customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_MultiLevelStruct temp_1 = customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_MultiLevelStruct(-9, temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<MultiLevelStruct>>)(
                    () => new MultiLevelStruct { ValueD = { ValueB = 3.1f, ValueA = 6 }, ValueC = -9 })));
        }

        [TestMethod]
        public void MultiLevelInstance()
        {
            Assert.AreEqual(@"customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_CustomStruct temp_0 = customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_CustomStruct(6, 3.1);
customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_MultiLevelStruct temp_1 = customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_MultiLevelStruct(-9, temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<MultiLevelStruct>>)(
                    () => new MultiLevelStruct { ValueD = new CustomStruct { ValueB = 3.1f, ValueA = 6 }, ValueC = -9 })));
        }

        [TestMethod]
        public void GenericCustomStruct()
        {
            Assert.AreEqual(@"customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_CustomStructWithGeneric__int temp_0 = customStruct_ExpressionEngine_Platforms_OpenGL_Tests_GLSL_CustomStructs_CustomStructWithGeneric__int(4);
return temp_0.Value;",
            new FunctionCompiler().Compile((Expression<Func<int>>)(
                () => new CustomStructWithGeneric<int> { Value = 4 }.Value)));
        }
    }
}
