﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq.Expressions;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL
{
	[TestClass]
	public sealed class InvokeTests
	{
		[TestMethod]
		public void ExpressionNullThrowsException()
		{
			try
			{
				new FunctionCompiler().Compile<Func<int>>(() => Injector.Invoke<Func<int>, int>(null, x => x()));
				Assert.Fail();
			}
			catch (ArgumentNullException ex)
			{
				Assert.AreEqual("source", ex.ParamName);
				Assert.AreEqual(@"Cannot invoke null expressions
Parameter name: source", ex.Message);
			}
		}

		[TestMethod]
		public void InvocationNullThrowsException()
		{
			try
			{
				new FunctionCompiler().Compile<Func<int>>(() => Injector.Invoke<Func<int>, int>(() => 3, null));
				Assert.Fail();
			}
			catch (ArgumentNullException ex)
			{
				Assert.AreEqual("source", ex.ParamName);
				Assert.AreEqual(@"Cannot invoke using null invocations
Parameter name: source", ex.Message);
			}
		}

		[TestMethod]
		public void InvocationNotCallingExpressionThrowsException()
		{
			try
			{
				new FunctionCompiler().Compile<Func<int>>(() => Injector.Invoke<Func<int>, int>(() => 3, x => 4));
				Assert.Fail();
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Assert.AreEqual("source", ex.ParamName);
				Assert.AreEqual(@"Invocations must invoke the expression parameter
Parameter name: source", ex.Message);
			}
		}

		[TestMethod]
		public void Basic()
		{
			Expression<Func<int, int>> expression = e => e * e;
			Assert.AreEqual(@"int temp_0 = 8 * 8;
return temp_0;", new FunctionCompiler().Compile<Func<int>>(() => expression.Invoke(e => e(8))));
		}

        [TestMethod]
        public void MultipleParameters()
        {
            Expression<Func<float, float, float>> expression = (e, f) => e * f;
            Assert.AreEqual(@"float temp_0 = 8.0 * 5.0;
return temp_0;", new FunctionCompiler().Compile<Func<float>>(() => expression.Invoke(e => e(8.0f, 5.0f))));
        }

		[TestMethod]
		public void TwoLevelInvocation()
		{
			Expression<Func<int, int>> expressionA = e => e * e;
			Expression<Func<int, int>> expressionB = e => e + e;
			Assert.AreEqual(@"int temp_0 = 8 + 8;
int temp_1 = temp_0 * temp_0;
return temp_1;", new FunctionCompiler().Compile<Func<int>>(() => expressionA.Invoke(e => e(expressionB.Invoke(f => f(8))))));
		}

		[TestMethod]
		public void TwoLevelInvocationWithShortcut()
		{
			Expression<Func<int, int>> expressionA = e => e * e;
			Expression<Func<int, int>> expressionB = e => e + e;
			Assert.AreEqual(@"int temp_0 = parameter_p + parameter_p;
int temp_1 = temp_0 * temp_0;
return temp_1;", new FunctionCompiler().Compile<Func<int, int>>(p => expressionA.Invoke(e => e(expressionB.Invoke(f => f(p))))));
		}
	}
}
