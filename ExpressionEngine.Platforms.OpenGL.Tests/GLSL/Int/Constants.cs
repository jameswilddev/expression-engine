﻿using ExpressionEngine.Compilers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Int
{
    [TestClass]
    public sealed class Constants
    {
        [TestMethod]
        public void Zero()
        {
            Assert.AreEqual("return 0;", new FunctionCompiler().Compile((Expression<Func<int>>)(() => 0)));
        }

        [TestMethod]
        public void Positive()
        {
            Assert.AreEqual("return 4;", new FunctionCompiler().Compile((Expression<Func<int>>)(() => 4)));
        }

        [TestMethod]
        public void Negative()
        {
            Assert.AreEqual("return -7;", new FunctionCompiler().Compile((Expression<Func<int>>)(() => -7)));
        }
    }
}
