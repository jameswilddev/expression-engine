﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Compilers;
using System.Linq.Expressions;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Int
{
    [TestClass]
    public sealed class Comparisons
    {
        [TestMethod]
        public void Equals()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left == parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<int, int, Boolean>>)(
                (left, right) => left == right)));
        }

        [TestMethod]
        public void NotEqual()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left != parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<int, int, Boolean>>)(
                (left, right) => left != right)));
        }

        [TestMethod]
        public void EqualsCall()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left == parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<int, int, Boolean>>)(
                (left, right) => left.Equals(right))));
        }

        [TestMethod]
        public void GreaterThan()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left > parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<int, int, Boolean>>)(
                (left, right) => left > right)));
        }

        [TestMethod]
        public void LessThan()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left < parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<int, int, Boolean>>)(
                (left, right) => left < right)));
        }

        [TestMethod]
        public void GreaterThanOrEqualTo()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left >= parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<int, int, Boolean>>)(
                (left, right) => left >= right)));
        }

        [TestMethod]
        public void LessThanOrEqualTo()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left <= parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<int, int, Boolean>>)(
                (left, right) => left <= right)));
        }
    }
}
