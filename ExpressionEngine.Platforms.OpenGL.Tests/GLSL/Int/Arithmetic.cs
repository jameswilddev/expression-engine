﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Compilers;
using System.Linq.Expressions;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Int
{
    [TestClass]
    public sealed class Arithmetic
    {
        [TestMethod]
        public void AddInt()
        {
            Assert.AreEqual(@"int temp_0 = parameter_a + parameter_b;
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<int, int, int>>)(
                    (a, b) => a + b)));
        }

        [TestMethod]
        public void SubtractInt()
        {
            Assert.AreEqual(@"int temp_0 = parameter_a - parameter_b;
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<int, int, int>>)(
                    (a, b) => a - b)));
        }

        [TestMethod]
        public void MultiplyInt()
        {
            Assert.AreEqual(@"int temp_0 = parameter_a * parameter_b;
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<int, int, int>>)(
                    (a, b) => a * b)));
        }

        [TestMethod]
        public void DivideInt()
        {
            Assert.AreEqual(@"int temp_0 = parameter_a / parameter_b;
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<int, int, int>>)(
                    (a, b) => a / b)));
        }

        [TestMethod]
        public void Negate()
        {
            Assert.AreEqual(@"int temp_0 = -parameter_input;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<int, int>>)(
                input => -input)));
        }
    }
}
