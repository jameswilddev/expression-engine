﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Compilers;
using System.Linq.Expressions;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Int
{
    [TestClass]
    public sealed class Operations
    {
        [TestMethod]
        public void GreaterThan()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left > parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<int, int, Boolean>>)(
                (left, right) => left > right)));
        }

        [TestMethod]
        public void LessThan()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left < parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<int, int, Boolean>>)(
                (left, right) => left < right)));
        }

        [TestMethod]
        public void GreaterThanOrEqualTo()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left >= parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<int, int, Boolean>>)(
                (left, right) => left >= right)));
        }

        [TestMethod]
        public void LessThanOrEqualTo()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left <= parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<int, int, Boolean>>)(
                (left, right) => left <= right)));
        }

        [TestMethod]
        public void Abs()
        {
            Assert.AreEqual(@"int temp_0 = abs(-4);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<int>>)(
                    () => Math.Abs(-4))));
        }

        [TestMethod]
        public void Min()
        {
            Assert.AreEqual(@"int temp_0 = -4 < 7 ? -4 : 7;
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<int>>)(
                    () => Math.Min(-4, 7))));
        }

        [TestMethod]
        public void Max()
        {
            Assert.AreEqual(@"int temp_0 = -4 > 7 ? -4 : 7;
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<int>>)(
                    () => Math.Max(-4, 7))));
        }

        [TestMethod]
        public void Clamp()
        {
            Assert.AreEqual(@"int temp_0 = clamp(4, 5, 7);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<int>>)(
                    () => 4.Clamp(5, 7))));
        }
    }
}
