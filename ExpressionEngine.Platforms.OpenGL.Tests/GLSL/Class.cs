﻿using ExpressionEngine.Compilers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL
{
    [TestClass]
    public sealed class ClassTests
    {
        public sealed class Class
        {
            public int Value;

            public Class() { throw new NotImplementedException(); }
            public Class(int value) { throw new NotImplementedException(); }
        }

        [TestMethod]
        public void ConstructorThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<int>>)(
                () => new Class(3).Value));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Reference types are not supported", ex.Message);
            }
        }

        [TestMethod]
        public void DefaultThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<int>>)(
                () => default(Class).Value));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Reference types are not supported", ex.Message);
            }
        }

        [TestMethod]
        public void MemberInitThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<int>>)(
                () => new Class { }.Value));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Reference types are not supported", ex.Message);
            }
        }

        [TestMethod]
        public void ConstructorParameterlessThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<int>>)(
                () => new Class().Value));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Reference types are not supported", ex.Message);
            }
        }
    }
}
