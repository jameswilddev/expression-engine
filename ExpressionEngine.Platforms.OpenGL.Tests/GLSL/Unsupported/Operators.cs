﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Compilers;
using System.Linq.Expressions;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported
{
    [TestClass]
    public sealed class Operators
    {
        public struct CustomStructWithUnsupportedBooleanOperators
        {
            public int Field;

            public static CustomStructWithUnsupportedBooleanOperators operator |(CustomStructWithUnsupportedBooleanOperators a, CustomStructWithUnsupportedBooleanOperators b)
            {
                throw new NotImplementedException();
            }

            public static CustomStructWithUnsupportedBooleanOperators operator &(CustomStructWithUnsupportedBooleanOperators a, CustomStructWithUnsupportedBooleanOperators b)
            {
                throw new NotImplementedException();
            }

            public static CustomStructWithUnsupportedBooleanOperators operator !(CustomStructWithUnsupportedBooleanOperators b)
            {
                throw new NotImplementedException();
            }
        }

        [TestMethod]
        public void LogicalNot()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructWithUnsupportedBooleanOperators, CustomStructWithUnsupportedBooleanOperators>>)(
                input => !input));
                Assert.Fail();
            }
            catch (NotImplementedException ex)
            {
                Assert.AreEqual("Not expressions for type \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported.Operators+CustomStructWithUnsupportedBooleanOperators\" are currently unimplemented", ex.Message);
            }
        }

        public struct CustomStructWithUnsupportedBitwiseOperators
        {
            public int Field;

            public static CustomStructWithUnsupportedBitwiseOperators operator <<(CustomStructWithUnsupportedBitwiseOperators a, int b)
            {
                throw new NotImplementedException();
            }

            public static CustomStructWithUnsupportedBitwiseOperators operator >>(CustomStructWithUnsupportedBitwiseOperators a, int b)
            {
                throw new NotImplementedException();
            }

            public static CustomStructWithUnsupportedBitwiseOperators operator ~(CustomStructWithUnsupportedBitwiseOperators b)
            {
                throw new NotImplementedException();
            }
        }

        [TestMethod]
        public void BitwiseNot()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructWithUnsupportedBitwiseOperators, CustomStructWithUnsupportedBitwiseOperators>>)(
                input => ~input));
                Assert.Fail();
            }
            catch (NotImplementedException ex)
            {
                Assert.AreEqual("Not expressions for type \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported.Operators+CustomStructWithUnsupportedBitwiseOperators\" are currently unimplemented", ex.Message);
            }
        }

        [TestMethod]
        public void Or()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructWithUnsupportedBooleanOperators, CustomStructWithUnsupportedBooleanOperators>>)(
                input => input | input));
                Assert.Fail();
            }
            catch (NotImplementedException ex)
            {
                Assert.AreEqual("Expressions of type \"Or\" are currently unimplemented", ex.Message);
            }
        }

        [TestMethod]
        public void And()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructWithUnsupportedBooleanOperators, CustomStructWithUnsupportedBooleanOperators>>)(
                input => input & input));
                Assert.Fail();
            }
            catch (NotImplementedException ex)
            {
                Assert.AreEqual("Expressions of type \"And\" are currently unimplemented", ex.Message);
            }
        }

        [TestMethod]
        public void LeftShift()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructWithUnsupportedBitwiseOperators, CustomStructWithUnsupportedBitwiseOperators>>)(
                input => input << 4));
                Assert.Fail();
            }
            catch (NotImplementedException ex)
            {
                Assert.AreEqual("Expressions of type \"LeftShift\" are currently unimplemented", ex.Message);
            }
        }

        [TestMethod]
        public void RightShift()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructWithUnsupportedBitwiseOperators, CustomStructWithUnsupportedBitwiseOperators>>)(
                input => input >> 4));
                Assert.Fail();
            }
            catch (NotImplementedException ex)
            {
                Assert.AreEqual("Expressions of type \"RightShift\" are currently unimplemented", ex.Message);
            }
        }

        [TestMethod]
        public void Modulo()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<int, int, int>>)(
                (a, b) => a % b));
                Assert.Fail();
            }
            catch (NotImplementedException ex)
            {
                Assert.AreEqual("Expressions of type \"Modulo\" are currently unimplemented", ex.Message);
            }
        }
    }
}
