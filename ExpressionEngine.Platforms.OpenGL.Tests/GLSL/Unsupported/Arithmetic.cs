﻿using ExpressionEngine.Compilers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported
{
    [TestClass]
    public sealed class Arithmetic
    {
        public struct CustomStructWithUnsupportedArithmeticOperators
        {
            public int Field;

            public static CustomStructWithUnsupportedArithmeticOperators operator +(CustomStructWithUnsupportedArithmeticOperators a, CustomStructWithUnsupportedArithmeticOperators b)
            {
                throw new NotImplementedException();
            }

            public static CustomStructWithUnsupportedArithmeticOperators operator -(CustomStructWithUnsupportedArithmeticOperators a, CustomStructWithUnsupportedArithmeticOperators b)
            {
                throw new NotImplementedException();
            }

            public static CustomStructWithUnsupportedArithmeticOperators operator *(CustomStructWithUnsupportedArithmeticOperators a, CustomStructWithUnsupportedArithmeticOperators b)
            {
                throw new NotImplementedException();
            }

            public static CustomStructWithUnsupportedArithmeticOperators operator /(CustomStructWithUnsupportedArithmeticOperators a, CustomStructWithUnsupportedArithmeticOperators b)
            {
                throw new NotImplementedException();
            }

            public static CustomStructWithUnsupportedArithmeticOperators operator -(CustomStructWithUnsupportedArithmeticOperators b)
            {
                throw new NotImplementedException();
            }
        }

        [TestMethod]
        public void Addition()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructWithUnsupportedArithmeticOperators, CustomStructWithUnsupportedArithmeticOperators, CustomStructWithUnsupportedArithmeticOperators>>)(
                (a, b) => a + b));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Arithmetic operations between types \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported.Arithmetic+CustomStructWithUnsupportedArithmeticOperators\" and \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported.Arithmetic+CustomStructWithUnsupportedArithmeticOperators\" are not supported", ex.Message);
            }
        }

        [TestMethod]
        public void Subtraction()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructWithUnsupportedArithmeticOperators, CustomStructWithUnsupportedArithmeticOperators, CustomStructWithUnsupportedArithmeticOperators>>)(
                (a, b) => a - b));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Arithmetic operations between types \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported.Arithmetic+CustomStructWithUnsupportedArithmeticOperators\" and \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported.Arithmetic+CustomStructWithUnsupportedArithmeticOperators\" are not supported", ex.Message);
            }
        }

        [TestMethod]
        public void Multiplication()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructWithUnsupportedArithmeticOperators, CustomStructWithUnsupportedArithmeticOperators, CustomStructWithUnsupportedArithmeticOperators>>)(
                (a, b) => a * b));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Arithmetic operations between types \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported.Arithmetic+CustomStructWithUnsupportedArithmeticOperators\" and \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported.Arithmetic+CustomStructWithUnsupportedArithmeticOperators\" are not supported", ex.Message);
            }
        }

        [TestMethod]
        public void Division()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructWithUnsupportedArithmeticOperators, CustomStructWithUnsupportedArithmeticOperators, CustomStructWithUnsupportedArithmeticOperators>>)(
                (a, b) => a / b));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Arithmetic operations between types \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported.Arithmetic+CustomStructWithUnsupportedArithmeticOperators\" and \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported.Arithmetic+CustomStructWithUnsupportedArithmeticOperators\" are not supported", ex.Message);
            }
        }

        [TestMethod]
        public void Negation()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructWithUnsupportedArithmeticOperators, CustomStructWithUnsupportedArithmeticOperators>>)(
                    input => -input));
                Assert.Fail();
            }
            catch (NotImplementedException ex)
            {
                Assert.AreEqual("Negate expressions for type \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported.Arithmetic+CustomStructWithUnsupportedArithmeticOperators\" are currently unimplemented", ex.Message);
            }
        }
    }
}
