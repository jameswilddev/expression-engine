﻿using ExpressionEngine.Compilers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported
{
    [TestClass]
    public sealed class Comparisons
    {
        public struct CustomStructWithUnsupportedComparisons
        {
            public int Field;

            public static bool operator ==(CustomStructWithUnsupportedComparisons a, int b)
            {
                throw new NotImplementedException();
            }

            public static bool operator !=(CustomStructWithUnsupportedComparisons a, int b)
            {
                throw new NotImplementedException();
            }

            public static bool operator >(CustomStructWithUnsupportedComparisons a, CustomStructWithUnsupportedComparisons b)
            {
                throw new NotImplementedException();
            }

            public static bool operator <(CustomStructWithUnsupportedComparisons a, CustomStructWithUnsupportedComparisons b)
            {
                throw new NotImplementedException();
            }

            public static bool operator >=(CustomStructWithUnsupportedComparisons a, CustomStructWithUnsupportedComparisons b)
            {
                throw new NotImplementedException();
            }

            public static bool operator <=(CustomStructWithUnsupportedComparisons a, CustomStructWithUnsupportedComparisons b)
            {
                throw new NotImplementedException();
            }
        }

        [TestMethod]
        public void Equals()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructWithUnsupportedComparisons, int, bool>>)(
                (a, b) => a == b));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Equality comparisons between types \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported.Comparisons+CustomStructWithUnsupportedComparisons\" and \"System.Int32\" are not supported", ex.Message);
            }
        }

        [TestMethod]
        public void NotEqual()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructWithUnsupportedComparisons, int, bool>>)(
                (a, b) => a != b));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Equality comparisons between types \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported.Comparisons+CustomStructWithUnsupportedComparisons\" and \"System.Int32\" are not supported", ex.Message);
            }
        }

        [TestMethod]
        public void EqualsCall()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructWithUnsupportedComparisons, int, bool>>)(
                (a, b) => a.Equals(b)));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Equality comparisons between types \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported.Comparisons+CustomStructWithUnsupportedComparisons\" and \"System.Int32\" are not supported", ex.Message);
            }
        }

        [TestMethod]
        public void GreaterThan()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructWithUnsupportedComparisons, CustomStructWithUnsupportedComparisons, bool>>)(
                (a, b) => a > b));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Comparisons between types \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported.Comparisons+CustomStructWithUnsupportedComparisons\" and \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported.Comparisons+CustomStructWithUnsupportedComparisons\" are not supported", ex.Message);
            }
        }

        [TestMethod]
        public void GreaterThanOrEqualTo()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructWithUnsupportedComparisons, CustomStructWithUnsupportedComparisons, bool>>)(
                (a, b) => a >= b));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Comparisons between types \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported.Comparisons+CustomStructWithUnsupportedComparisons\" and \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported.Comparisons+CustomStructWithUnsupportedComparisons\" are not supported", ex.Message);
            }
        }

        [TestMethod]
        public void LessThan()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructWithUnsupportedComparisons, CustomStructWithUnsupportedComparisons, bool>>)(
                (a, b) => a < b));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Comparisons between types \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported.Comparisons+CustomStructWithUnsupportedComparisons\" and \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported.Comparisons+CustomStructWithUnsupportedComparisons\" are not supported", ex.Message);
            }
        }

        [TestMethod]
        public void LessThanOrEqualTo()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructWithUnsupportedComparisons, CustomStructWithUnsupportedComparisons, bool>>)(
                (a, b) => a <= b));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Comparisons between types \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported.Comparisons+CustomStructWithUnsupportedComparisons\" and \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Unsupported.Comparisons+CustomStructWithUnsupportedComparisons\" are not supported", ex.Message);
            }
        }
    }
}
