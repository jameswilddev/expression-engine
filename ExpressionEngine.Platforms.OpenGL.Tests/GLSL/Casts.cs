﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq.Expressions;
using ExpressionEngine.Compilers;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL
{
    [TestClass]
    public sealed class Casts
    {
		public struct CustomStructWithExplicitCast
		{
			public int Contained;

			public static explicit operator int(CustomStructWithExplicitCast d)
			{
				throw new NotImplementedException();
			}
		}

        [TestMethod]
        public void UnsupportedExplicitCast()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructWithExplicitCast, int>>)(input => (int)input));
                Assert.Fail();
            }
            catch (NotImplementedException ex)
            {
                Assert.AreEqual("Conversions from type \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Casts+CustomStructWithExplicitCast\" to type \"System.Int32\" are currently unimplemented", ex.Message);
            }
        }

		public struct CustomStructWithImplicitCast
		{
			public int Contained;

			public static implicit operator int(CustomStructWithImplicitCast d)
			{
				throw new NotImplementedException();
			}
		}

        [TestMethod]
        public void UnsupportedImplicitCast()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStructWithImplicitCast, int>>)(input => input));
                Assert.Fail();
            }
            catch (NotImplementedException ex)
            {
				Assert.AreEqual("Conversions from type \"ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Casts+CustomStructWithImplicitCast\" to type \"System.Int32\" are currently unimplemented", ex.Message);
            }
        }

        [TestMethod]
        public void CastIntToFloatExplicitly()
        {
            Assert.AreEqual(@"float temp_0 = float(parameter_input);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<int, float>>)(
                    input => (float)input)));
        }

        [TestMethod]
        public void CastIntToFloatImplicitly()
        {
            Assert.AreEqual(@"float temp_0 = float(parameter_input);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<int, float>>)(
                    input => input)));
        }

        [TestMethod]
        public void CastFloatToIntExplicitly()
        {
            Assert.AreEqual(@"int temp_0 = int(parameter_input);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<float, int>>)(
                    input => (int)input)));
        }
    }
}
