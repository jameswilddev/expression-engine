﻿using ExpressionEngine.Compilers;
using ExpressionEngine.Platforms.OpenGL.Tests.GLSL.CustomStructs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL
{
    [TestClass]
    public sealed class Misc
    {
        [TestMethod]
        public void SourceNullThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Action>)null);
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("source", ex.ParamName);
                Assert.AreEqual(@"Value cannot be null.
Parameter name: source", ex.Message);
            }
        }

        [TestMethod]
        public void UnimplementedPrimitiveTypeThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<Byte>>)(() => 4));
                Assert.Fail();
            }
            catch (NotImplementedException ex)
            {
                Assert.AreEqual("The primitive type \"System.Byte\" is currently unimplemented", ex.Message);
            }
        }

        [TestMethod]
        public void NullConstantExpressionThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Action>)null);
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("source", ex.ParamName);
                Assert.AreEqual(@"Value cannot be null.
Parameter name: source", ex.Message);
            }
        }

        public sealed class ReferenceTypeContainingField
        {
            public int Field;
        }

        [TestMethod]
        public void ReferenceTypeReturnTypeThrowsException()
        {
            try
            {
                var output = new List<Byte>();
                new FunctionCompiler().Compile((Expression<Func<List<Byte>>>)(() => output));
                Assert.Fail();
            }
            catch (NotSupportedException ex)
            {
                Assert.AreEqual("Reference types are not supported", ex.Message);
            }
        }

        [TestMethod]
        public void ExecuteUnmappedMethodThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<CustomStruct, int>>)(input => input.InstanceMethodIgnored()));
                Assert.Fail();
            }
            catch (NotImplementedException ex)
            {
                Assert.AreEqual("Calls to \"Int32 InstanceMethodIgnored()\" are currently unimplemented", ex.Message);
            }
        }

        [TestMethod]
        public void SameExpressionTwiceGetsReused()
        {
            Assert.AreEqual(@"float temp_0 = parameter_a * 4.0;
float temp_1 = temp_0 + temp_0;
return temp_1;",
               new FunctionCompiler().Compile((Expression<Func<float, float>>)(a => (a * 4) + (a * 4))));
        }
    }
}
