﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Compilers;
using System.Linq.Expressions;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Float
{
    [TestClass]
    public sealed class Arithmetic
    {
        [TestMethod]
        public void Add()
        {
            Assert.AreEqual(@"float temp_0 = parameter_a + parameter_b;
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<float, float, float>>)(
                    (a, b) => a + b)));
        }

        [TestMethod]
        public void Subtract()
        {

            Assert.AreEqual(@"float temp_0 = parameter_a - parameter_b;
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<float, float, float>>)(
                    (a, b) => a - b)));
        }

        [TestMethod]
        public void Multiply()
        {
            Assert.AreEqual(@"float temp_0 = parameter_a * parameter_b;
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<float, float, float>>)(
                    (a, b) => a * b)));
        }

        [TestMethod]
        public void Divide()
        {
            Assert.AreEqual(@"float temp_0 = parameter_a / parameter_b;
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<float, float, float>>)(
                    (a, b) => a / b)));
        }

        [TestMethod]
        public void Negate()
        {
            Assert.AreEqual(@"float temp_0 = -parameter_input;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<float, float>>)(
                input => -input)));
        }
    }
}
