﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Compilers;
using System.Linq.Expressions;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Float
{
    [TestClass]
    public sealed class Operations
    {
        [TestMethod]
        public void Abs()
        {
            Assert.AreEqual(@"float temp_0 = abs(-4.3);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<float>>)(
                    () => Math.Abs(-4.3f))));
        }

        [TestMethod]
        public void Min()
        {
            Assert.AreEqual(@"float temp_0 = min(-4.3, 7.4);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<float>>)(
                    () => Math.Min(-4.3f, 7.4f))));
        }

        [TestMethod]
        public void Max()
        {
            Assert.AreEqual(@"float temp_0 = max(-4.3, 7.4);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<float>>)(
                    () => Math.Max(-4.3f, 7.4f))));
        }

        [TestMethod]
        public void Mod()
        {
            Assert.AreEqual(@"float temp_0 = mod(-4.3, 7.4);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<float>>)(
                    () => (-4.3f).Mod(7.4f))));
        }

        [TestMethod]
        public void Fract()
        {
            Assert.AreEqual(@"float temp_0 = fract(-4.3);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<float>>)(
                    () => (-4.3f).Fract())));
        }

        [TestMethod]
        public void Floor()
        {
            Assert.AreEqual(@"float temp_0 = floor(-4.3);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<float>>)(
                    () => (-4.3f).Floor())));
        }

        [TestMethod]
        public void Ceil()
        {
            Assert.AreEqual(@"float temp_0 = ceil(-4.3);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<float>>)(
                    () => (-4.3f).Ceil())));
        }

        [TestMethod]
        public void Clamp()
        {
            Assert.AreEqual(@"float temp_0 = clamp(-4.3, -2.1, 1.6);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<float>>)(
                    () => (-4.3f).Clamp(-2.1f, 1.6f))));
        }

        [TestMethod]
        public void Mix()
        {
            Assert.AreEqual(@"float temp_0 = mix(-4.3, -2.1, 1.6);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<float>>)(
                    () => (-4.3f).Mix(-2.1f, 1.6f))));
        }

        [TestMethod]
        public void Sin()
        {
            Assert.AreEqual(@"float temp_0 = sin(4.5);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<float>>)(() => 4.5f.Sin())));
        }

        [TestMethod]
        public void Cos()
        {
            Assert.AreEqual(@"float temp_0 = cos(4.5);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<float>>)(() => 4.5f.Cos())));
        }

        [TestMethod]
        public void Sqrt()
        {
            Assert.AreEqual(@"float temp_0 = sqrt(4.5);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<float>>)(() => 4.5f.Sqrt())));
        }

        [TestMethod]
        public void InverseSqrt()
        {
            Assert.AreEqual(@"float temp_0 = inversesqrt(4.5);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<float>>)(() => 4.5f.InverseSqrt())));
        }

        [TestMethod]
        public void Pow()
        {
            Assert.AreEqual(@"float temp_0 = pow(4.5, -5.6);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<float>>)(() => 4.5f.Pow(-5.6f))));
        }
    }
}
