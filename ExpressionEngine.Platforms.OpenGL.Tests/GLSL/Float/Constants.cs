﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Compilers;
using System.Linq.Expressions;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Float
{
    [TestClass]
    public sealed class Constants
    {
        [TestMethod]
        public void Zero()
        {
            Assert.AreEqual(@"return 0.0;", new FunctionCompiler().Compile((Expression<Func<float>>)(() => 0.0f)));
        }

        [TestMethod]
        public void PositiveLessThanOne()
        {
            Assert.AreEqual(@"return 0.3;", new FunctionCompiler().Compile((Expression<Func<float>>)(() => 0.3f)));
        }

        [TestMethod]
        public void Positive()
        {
            Assert.AreEqual(@"return 4.7;", new FunctionCompiler().Compile((Expression<Func<float>>)(() => 4.7f)));
        }

        [TestMethod]
        public void NegativeGreaterThanNegativeOne()
        {
            Assert.AreEqual(@"return -0.3;", new FunctionCompiler().Compile((Expression<Func<float>>)(() => -0.3f)));
        }

        [TestMethod]
        public void Negative()
        {
            Assert.AreEqual(@"return -4.7;", new FunctionCompiler().Compile((Expression<Func<float>>)(() => -4.7f)));
        }
    }
}
