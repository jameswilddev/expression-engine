﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;
using System.Linq.Expressions;
using ExpressionEngine.Compilers;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Vec2
{
    [TestClass]
    public sealed class Constants
    {
        [TestMethod]
        public void MemberInitFull()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(0.4, 0.7);
return temp_0;",
               new FunctionCompiler().Compile((Expression<Func<Vector2>>)(() => new Vector2 { Y = 0.7f, X = 0.4f })));
        }

        [TestMethod]
        public void MemberInitPartial()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(0.0, 0.7);
return temp_0;",
               new FunctionCompiler().Compile((Expression<Func<Vector2>>)(() => new Vector2 { Y = 0.7f })));
        }

        [TestMethod]
        public void MemberInitEmpty()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(0.0, 0.0);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<Vector2>>)(
                    () => new Vector2 { })));
        }

        [TestMethod]
        public void Default()
        {
            Assert.AreEqual(@"return vec2(0.0);",
                new FunctionCompiler().Compile((Expression<Func<Vector2>>)(
                    () => default(Vector2))));
        }

        [TestMethod]
        public void ConstructorMultipleParameters()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(0.4, 0.7);
return temp_0;",
               new FunctionCompiler().Compile((Expression<Func<Vector2>>)(() => new Vector2(0.4f, 0.7f))));
        }

        [TestMethod]
        public void ConstructorSingleParameter()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(7.4);
return temp_0;",
                new FunctionCompiler().Compile((Expression<Func<Vector2>>)(
                    () => new Vector2(7.4f))));
        }

        [TestMethod]
        public void ConstructorParameterless()
        {
            Assert.AreEqual(@"return vec2(0.0);",
                new FunctionCompiler().Compile((Expression<Func<Vector2>>)(
                    () => new Vector2())));
        }
    }
}
