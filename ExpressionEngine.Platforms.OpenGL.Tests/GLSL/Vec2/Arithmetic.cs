﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Compilers;
using System.Numerics;
using System.Linq.Expressions;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Vec2
{
    [TestClass]
    public sealed class Arithmetic
    {
        [TestMethod]
        public void Negate()
        {
            Assert.AreEqual(@"vec2 temp_0 = -parameter_input;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Vector2, Vector2>>)(
                input => -input)));
        }

        [TestMethod]
        public void AddVec2()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.0);
vec2 temp_1 = vec2(8.0);
vec2 temp_2 = temp_0 + temp_1;
return temp_2;",
            new FunctionCompiler().Compile((Expression<Func<Vector2>>)(
                () => new Vector2(4.0f) + new Vector2(8.0f))));
        }

        [TestMethod]
        public void SubtractVec2()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.0);
vec2 temp_1 = vec2(8.0);
vec2 temp_2 = temp_0 - temp_1;
return temp_2;",
            new FunctionCompiler().Compile((Expression<Func<Vector2>>)(
                () => new Vector2(4.0f) - new Vector2(8.0f))));
        }

        [TestMethod]
        public void MultiplyVec2()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.0);
vec2 temp_1 = vec2(8.0);
vec2 temp_2 = temp_0 * temp_1;
return temp_2;",
            new FunctionCompiler().Compile((Expression<Func<Vector2>>)(
                () => new Vector2(4.0f) * new Vector2(8.0f))));
        }

        [TestMethod]
        public void MultiplyFloat()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.0);
vec2 temp_1 = temp_0 * 8.0;
return temp_1;",
            new FunctionCompiler().Compile((Expression<Func<Vector2>>)(
                () => new Vector2(4.0f) * 8.0f)));
        }

        [TestMethod]
        public void DivideVec2()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.0);
vec2 temp_1 = vec2(8.0);
vec2 temp_2 = temp_0 / temp_1;
return temp_2;",
            new FunctionCompiler().Compile((Expression<Func<Vector2>>)(
                () => new Vector2(4.0f) / new Vector2(8.0f))));
        }

        [TestMethod]
        public void DivideFloat()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.0);
vec2 temp_1 = temp_0 / 8.0;
return temp_1;",
            new FunctionCompiler().Compile((Expression<Func<Vector2>>)(
                () => new Vector2(4.0f) / 8.0f)));
        }
    }
}
