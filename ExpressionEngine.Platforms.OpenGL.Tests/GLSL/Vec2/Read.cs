﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Compilers;
using System.Linq.Expressions;
using System.Numerics;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Vec2
{
    [TestClass]
    public sealed class Read
    {
        [TestMethod]
        public void X()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.0);
return temp_0.x;",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => new Vector2(4.0f).X)));
        }

        [TestMethod]
        public void Y()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.0);
return temp_0.y;",
               new FunctionCompiler().Compile((Expression<Func<float>>)(
               () => new Vector2(4.0f).Y)));
        }
    }
}
