﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq.Expressions;
using ExpressionEngine.Compilers;
using System.Numerics;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Vec2
{
    [TestClass]
    public sealed class Comparisons
    {
        [TestMethod]
        public void Equals()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left == parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Vector2, Vector2, Boolean>>)(
                (left, right) => left == right)));
        }

        [TestMethod]
        public void NotEqual()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left != parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Vector2, Vector2, Boolean>>)(
                (left, right) => left != right)));
        }

        [TestMethod]
        public void EqualsCall()
        {
            Assert.AreEqual(@"bool temp_0 = parameter_left == parameter_right;
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Vector2, Vector2, Boolean>>)(
                (left, right) => left.Equals(right))));
        }
    }
}
