﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;
using System.Linq.Expressions;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Vec2
{
    [TestClass]
    public sealed class SwizzleVec4
    {
        [TestMethod]
        public void Repeat()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(0.4, 0.7);
return temp_0.yyxy;",
               new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector2 { X = 0.4f, Y = 0.7f }.Swizzle(v => v.Y, v => v.Y, v => v.X, v => v.Y))));
        }

        [TestMethod]
        public void XNullThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector2 { X = 0.4f, Y = 0.7f }.Swizzle(null, v => v.Y, v => v.Y, v => v.Y)));
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("source", ex.ParamName);
                Assert.AreEqual(@"Swizzle selectors cannot be null
Parameter name: source", ex.Message);
            }
        }

        [TestMethod]
        public void YNullThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector2 { X = 0.4f, Y = 0.7f }.Swizzle(v => v.Y, null, v => v.Y, v => v.Y)));
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("source", ex.ParamName);
                Assert.AreEqual(@"Swizzle selectors cannot be null
Parameter name: source", ex.Message);
            }
        }

        [TestMethod]
        public void ZNullThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector2 { X = 0.4f, Y = 0.7f }.Swizzle(v => v.Y, v => v.Y, null, v => v.Y)));
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("source", ex.ParamName);
                Assert.AreEqual(@"Swizzle selectors cannot be null
Parameter name: source", ex.Message);
            }
        }

        [TestMethod]
        public void WNullThrowsException()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector2 { X = 0.4f, Y = 0.7f }.Swizzle(v => v.Y, v => v.Y, v => v.Y, null)));
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("source", ex.ParamName);
                Assert.AreEqual(@"Swizzle selectors cannot be null
Parameter name: source", ex.Message);
            }
        }

        [TestMethod]
        public void XNonMemberSelect()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector2 { X = 0.4f, Y = 0.7f }.Swizzle(v => 3.0f, v => v.Y, v => v.Y, v => v.Y)));
                Assert.Fail();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Assert.AreEqual("source", ex.ParamName);
                Assert.AreEqual(@"Swizzles should only select a member from the input vector
Parameter name: source", ex.Message);
            }
        }

        [TestMethod]
        public void YNonMemberSelect()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector2 { X = 0.4f, Y = 0.7f }.Swizzle(v => v.X, v => 3.0f, v => v.Y, v => v.Y)));
                Assert.Fail();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Assert.AreEqual("source", ex.ParamName);
                Assert.AreEqual(@"Swizzles should only select a member from the input vector
Parameter name: source", ex.Message);
            }
        }

        [TestMethod]
        public void ZNonMemberSelect()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector2 { X = 0.4f, Y = 0.7f }.Swizzle(v => v.X, v => v.Y, v => 3.0f, v => v.Y)));
                Assert.Fail();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Assert.AreEqual("source", ex.ParamName);
                Assert.AreEqual(@"Swizzles should only select a member from the input vector
Parameter name: source", ex.Message);
            }
        }

        [TestMethod]
        public void WNonMemberSelect()
        {
            try
            {
                new FunctionCompiler().Compile((Expression<Func<Vector4>>)(() => new Vector2 { X = 0.4f, Y = 0.7f }.Swizzle(v => v.X, v => v.Y, v => v.Y, v => 3.0f)));
                Assert.Fail();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Assert.AreEqual("source", ex.ParamName);
                Assert.AreEqual(@"Swizzles should only select a member from the input vector
Parameter name: source", ex.Message);
            }
        }
    }
}
