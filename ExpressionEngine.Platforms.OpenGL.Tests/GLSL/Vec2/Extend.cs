﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Compilers;
using System.Numerics;
using System.Linq.Expressions;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Vec2
{
    [TestClass]
    public sealed class Extend
    {
        [TestMethod]
        public void ToVector3()
        {
            Assert.AreEqual(@"vec3 temp_0 = vec3(parameter_input, 4.0);
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Vector2, Vector3>>)(
                input => input.Extend(4.0f))));
        }

        [TestMethod]
        public void ToVector4()
        {
            Assert.AreEqual(@"vec4 temp_0 = vec4(parameter_input, 4.0, 5.0);
return temp_0;",
            new FunctionCompiler().Compile((Expression<Func<Vector2, Vector4>>)(
                input => input.Extend(4.0f, 5.0f))));
        }
    }
}
