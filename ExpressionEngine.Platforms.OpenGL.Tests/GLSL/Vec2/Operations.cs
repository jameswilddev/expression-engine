﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExpressionEngine.Compilers;
using System.Numerics;
using System.Linq.Expressions;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL.Vec2
{
    [TestClass]
    public sealed class Operations
    {
        [TestMethod]
        public void Length()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(7.4);
float temp_1 = length(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<float>>)(
                    () => new Vector2(7.4f).Length())));
        }

        [TestMethod]
        public void Distance()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(7.4);
vec2 temp_1 = vec2(9.6);
float temp_2 = distance(temp_0, temp_1);
return temp_2;",
            new FunctionCompiler().Compile((Expression<Func<float>>)(
                () => Vector2.Distance(new Vector2(7.4f), new Vector2(9.6f)))));
        }

        [TestMethod]
        public void Sin()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.5);
vec2 temp_1 = sin(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector2>>)(() => new Vector2(4.5f).Sin())));
        }

        [TestMethod]
        public void Cos()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.5);
vec2 temp_1 = cos(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector2>>)(() => new Vector2(4.5f).Cos())));
        }

        [TestMethod]
        public void Sqrt()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.5);
vec2 temp_1 = sqrt(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector2>>)(() => Vector2.SquareRoot(new Vector2(4.5f)))));
        }

        [TestMethod]
        public void InvSqrt()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.5);
vec2 temp_1 = inversesqrt(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector2>>)(() => new Vector2(4.5f).InverseSqrt())));
        }

        [TestMethod]
        public void Pow()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.5);
vec2 temp_1 = vec2(5.0);
vec2 temp_2 = pow(temp_0, temp_1);
return temp_2;",
                new FunctionCompiler().Compile((Expression<Func<Vector2>>)(() => new Vector2(4.5f).Pow(new Vector2(5.0f)))));
        }

        [TestMethod]
        public void Abs()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.5);
vec2 temp_1 = abs(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector2>>)(() => Vector2.Abs(new Vector2(4.5f)))));
        }

        [TestMethod]
        public void Min()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.5);
vec2 temp_1 = vec2(7.0);
vec2 temp_2 = min(temp_0, temp_1);
return temp_2;",
                new FunctionCompiler().Compile((Expression<Func<Vector2>>)(() => Vector2.Min(new Vector2(4.5f), new Vector2(7.0f)))));
        }

        [TestMethod]
        public void Max()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.5);
vec2 temp_1 = vec2(7.0);
vec2 temp_2 = max(temp_0, temp_1);
return temp_2;",
                new FunctionCompiler().Compile((Expression<Func<Vector2>>)(() => Vector2.Max(new Vector2(4.5f), new Vector2(7.0f)))));
        }

        [TestMethod]
        public void ModFloat()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.5);
vec2 temp_1 = mod(temp_0, 7.0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector2>>)(() => new Vector2(4.5f).Mod(7.0f))));
        }

        [TestMethod]
        public void ModVec2()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.5);
vec2 temp_1 = vec2(7.0);
vec2 temp_2 = mod(temp_0, temp_1);
return temp_2;",
                new FunctionCompiler().Compile((Expression<Func<Vector2>>)(() => new Vector2(4.5f).Mod(new Vector2(7.0f)))));
        }

        [TestMethod]
        public void Fract()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.5);
vec2 temp_1 = fract(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector2>>)(() => new Vector2(4.5f).Fract())));
        }

        [TestMethod]
        public void Floor()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.5);
vec2 temp_1 = floor(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector2>>)(() => new Vector2(4.5f).Floor())));
        }

        [TestMethod]
        public void Ceil()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.5);
vec2 temp_1 = ceil(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector2>>)(() => new Vector2(4.5f).Ceil())));
        }

        [TestMethod]
        public void ClampVec2()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.5);
vec2 temp_1 = vec2(7.0);
vec2 temp_2 = vec2(9.0);
vec2 temp_3 = clamp(temp_0, temp_1, temp_2);
return temp_3;",
                new FunctionCompiler().Compile((Expression<Func<Vector2>>)(() => new Vector2(4.5f).Clamp(new Vector2(7.0f), new Vector2(9.0f)))));
        }

        [TestMethod]
        public void ClampFloat()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.5);
vec2 temp_1 = clamp(temp_0, 7.0, 9.0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector2>>)(() => new Vector2(4.5f).Clamp(7.0f, 9.0f))));
        }

        [TestMethod]
        public void MixVec2()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.5);
vec2 temp_1 = vec2(7.0);
vec2 temp_2 = vec2(9.0);
vec2 temp_3 = mix(temp_0, temp_1, temp_2);
return temp_3;",
                new FunctionCompiler().Compile((Expression<Func<Vector2>>)(() => new Vector2(4.5f).Mix(new Vector2(7.0f), new Vector2(9.0f)))));
        }

        [TestMethod]
        public void MixFloat()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.5);
vec2 temp_1 = vec2(7.0);
vec2 temp_2 = mix(temp_0, temp_1, 9.0);
return temp_2;",
                new FunctionCompiler().Compile((Expression<Func<Vector2>>)(() => Vector2.Lerp(new Vector2(4.5f), new Vector2(7.0f), 9.0f))));
        }

        [TestMethod]
        public void Dot()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.5);
vec2 temp_1 = vec2(7.0);
float temp_2 = dot(temp_0, temp_1);
return temp_2;",
                new FunctionCompiler().Compile((Expression<Func<float>>)(() => Vector2.Dot(new Vector2(4.5f), new Vector2(7.0f)))));
        }

        [TestMethod]
        public void Normalize()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.5);
vec2 temp_1 = normalize(temp_0);
return temp_1;",
                new FunctionCompiler().Compile((Expression<Func<Vector2>>)(() => Vector2.Normalize(new Vector2(4.5f)))));
        }

        [TestMethod]
        public void Reflect()
        {
            Assert.AreEqual(@"vec2 temp_0 = vec2(4.5);
vec2 temp_1 = vec2(7.0);
vec2 temp_2 = reflect(temp_0, temp_1);
return temp_2;",
                new FunctionCompiler().Compile((Expression<Func<Vector2>>)(() => Vector2.Reflect(new Vector2(4.5f), new Vector2(7.0f)))));
        }
    }
}
