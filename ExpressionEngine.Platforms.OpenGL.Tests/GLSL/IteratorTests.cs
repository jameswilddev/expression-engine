﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionEngine.Platforms.OpenGL.Tests.GLSL
{
	[TestClass]
	public sealed class IteratorTests
	{
		[TestMethod]
		public void TransformNullThrowsException()
		{
			try
			{
				new FunctionCompiler().Compile<Func<float>>(() => Iterator.Iterate<float>(4.0f, null, 5));
				Assert.Fail();
			}
			catch (ArgumentNullException ex)
			{
				Assert.AreEqual("source", ex.ParamName);
				Assert.AreEqual(@"Iterator transforms cannot be null
Parameter name: source", ex.Message);
			}
		}

		[TestMethod]
		public void IterationsLessThanOneThrowsException()
		{
			try
			{
				new FunctionCompiler().Compile<Func<float>>(() => Iterator.Iterate(4.0f, i => i, 0));
				Assert.Fail();
			}
			catch (ArgumentOutOfRangeException ex)
			{
				Assert.AreEqual("source", ex.ParamName);
				Assert.AreEqual(@"Iterators' iteration counts cannot be less than one
Parameter name: source", ex.Message);
			}
		}

		[TestMethod]
		public void BasicIteration()
		{
			Assert.AreEqual(@"int temp_0 = -5;
for(int temp_1 = 0; temp_1 < 5; temp_1++) {
int temp_2 = temp_0 + 4;
temp_0 = temp_2;
}
return temp_0;", new FunctionCompiler().Compile<Func<int>>(() => Iterator.Iterate(-5, x => x + 4, 5)));
		}

		[TestMethod]
		public void TwoLevelIteration()
		{
			Assert.AreEqual(@"int temp_0 = -5;
for(int temp_1 = 0; temp_1 < 5; temp_1++) {
int temp_2 = temp_0;
for(int temp_3 = 0; temp_3 < 4; temp_3++) {
int temp_4 = temp_2 + 2;
temp_2 = temp_4;
}
temp_0 = temp_2;
}
return temp_0;", new FunctionCompiler().Compile<Func<int>>(() => Iterator.Iterate(-5, x => Iterator.Iterate(x, y => y + 2, 4), 5)));
		}

		[TestMethod]
		public void TemporaryVariableCounterContinuesFromTransform()
		{
			Assert.AreEqual(@"int temp_0 = -5;
for(int temp_1 = 0; temp_1 < 5; temp_1++) {
int temp_2 = temp_0 + 4;
temp_0 = temp_2;
}
int temp_3 = temp_0 + 4;
return temp_3;", new FunctionCompiler().Compile<Func<int, int>>(y => Iterator.Iterate(-5, x => x + 4, 5) + 4));
		}

		[TestMethod]
		public void ExpressionsInsideTransformAreNotReusedOutsideLoop()
		{
			Assert.AreEqual(@"int temp_0 = -5;
for(int temp_1 = 0; temp_1 < 5; temp_1++) {
int temp_2 = 4 * parameter_y;
int temp_3 = temp_0 + temp_2;
temp_0 = temp_3;
}
int temp_4 = 4 * parameter_y;
int temp_5 = temp_0 + temp_4;
return temp_5;", new FunctionCompiler().Compile<Func<int, int>>(y => Iterator.Iterate(-5, x => x + (4 * y), 5) + (4 * y)));
		}
	}
}
