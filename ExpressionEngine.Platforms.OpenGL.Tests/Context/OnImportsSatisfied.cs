﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using System.Runtime.InteropServices;
using ExpressionEngine.Simulations;

namespace ExpressionEngine.Platforms.OpenGL.Tests
{
    [TestClass]
    public sealed class ContextTests
    {
        [TestMethod]
        public void InitFails()
        {
            var mocks = new MockRepository();
            var context = new GLContext
            {
                GLFW = mocks.StrictMock<IGLFW>()
            };

            using(mocks.Record())
            using(mocks.Ordered())
            {
                context.GLFW.Expect(g => g.Init()).Return(false).Repeat.Once();
            }

            using(mocks.Playback())
            {
                try
                {
                    context.OnImportsSatisfied();
                    Assert.Fail();
                }
                catch(ExternalException ex)
                {
                    Assert.AreEqual("GLFW failed to initialize", ex.Message);
                }
            }
        }

        [TestMethod]
        public void InitFailsThenDisposed()
        {
            var mocks = new MockRepository();
            var context = new GLContext
            {
                GLFW = mocks.StrictMock<IGLFW>()
            };

            using (mocks.Record())
            using (mocks.Ordered())
            {
                context.GLFW.Expect(g => g.Init()).Return(false).Repeat.Once();
            }

            using (mocks.Playback())
            {
                try
                {
                    context.OnImportsSatisfied();
                    Assert.Fail();
                }
                catch (ExternalException ex)
                {
                    Assert.AreEqual("GLFW failed to initialize", ex.Message);
                }
                context.Dispose();
            }
        }

        [TestMethod]
        public void CreateWindowFails()
        {
            var mocks = new MockRepository();
            var context = new GLContext
            {
                GLFW = mocks.StrictMock<IGLFW>(),
                Configuration = MockRepository.GenerateStub<IConfiguration>()
            };
            context.Configuration.Stub(c => c.WindowWidth).Return(512);
            context.Configuration.Stub(c => c.WindowHeight).Return(384);
            context.Configuration.Stub(c => c.ApplicationName).Return("Test Window Title");

            using (mocks.Record())
            using (mocks.Ordered())
            {
                context.GLFW.Expect(g => g.Init()).Return(true).Repeat.Once();
                context.GLFW.Expect(g => g.CreateWindow(512, 384, "Test Window Title", (IntPtr)null, (IntPtr)null)).Return((IntPtr)null).Repeat.Once();
            }

            using (mocks.Playback())
            {
                try
                {
                    context.OnImportsSatisfied();
                    Assert.Fail();
                }
                catch (ExternalException ex)
                {
                    Assert.AreEqual("GLFW failed to create a window", ex.Message);
                }
            }
        }

        [TestMethod]
        public void CreateWindowFailsThenDisposed()
        {
            var mocks = new MockRepository();
            var context = new GLContext
            {
                GLFW = mocks.StrictMock<IGLFW>(),
                Configuration = MockRepository.GenerateStub<IConfiguration>()
            };
            context.Configuration.Stub(c => c.WindowWidth).Return(512);
            context.Configuration.Stub(c => c.WindowHeight).Return(384);
            context.Configuration.Stub(c => c.ApplicationName).Return("Test Window Title");

            using (mocks.Record())
            using (mocks.Ordered())
            {
                context.GLFW.Expect(g => g.Init()).Return(true).Repeat.Once();
                context.GLFW.Expect(g => g.CreateWindow(512, 384, "Test Window Title", (IntPtr)null, (IntPtr)null)).Return((IntPtr)null).Repeat.Once();
                context.GLFW.Terminate();
            }

            using (mocks.Playback())
            {
                try
                {
                    context.OnImportsSatisfied();
                    Assert.Fail();
                }
                catch (ExternalException ex)
                {
                    Assert.AreEqual("GLFW failed to create a window", ex.Message);
                }
                context.Dispose();
            }
        }

        [TestMethod]
        public void Successful()
        {
            var mocks = new MockRepository();
            var context = new GLContext
            {
                GLFW = mocks.StrictMock<IGLFW>(),
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Interval = mocks.StrictMock<IInterval>()
            };
            context.Configuration.Stub(c => c.WindowWidth).Return(512);
            context.Configuration.Stub(c => c.WindowHeight).Return(384);
            context.Configuration.Stub(c => c.ApplicationName).Return("Test Window Title");

            IntPtr window = (IntPtr)3429387;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                context.GLFW.Expect(g => g.Init()).Return(true).Repeat.Once();
                context.GLFW.Expect(g => g.CreateWindow(512, 384, "Test Window Title", (IntPtr)null, (IntPtr)null)).Return(window).Repeat.Once();
                context.GLFW.SetWindowSizeCallback(Arg.Is(window), Arg<WindowSizeDelegate>.Is.Anything);
                context.GLFW.SwapInterval(1);
                context.GLFW.MakeContextCurrent(window);
            }

            using (mocks.Playback())
            {
                context.OnImportsSatisfied();
            }
        }

        [TestMethod]
        public void SuccessfulThenDisposed()
        {
            var mocks = new MockRepository();
            var context = new GLContext
            {
                GLFW = mocks.StrictMock<IGLFW>(),
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Interval = mocks.StrictMock<IInterval>()
            };
            context.Configuration.Stub(c => c.WindowWidth).Return(512);
            context.Configuration.Stub(c => c.WindowHeight).Return(384);
            context.Configuration.Stub(c => c.ApplicationName).Return("Test Window Title");

            IntPtr window = (IntPtr)3429387;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                context.GLFW.Expect(g => g.Init()).Return(true).Repeat.Once();
                context.GLFW.Expect(g => g.CreateWindow(512, 384, "Test Window Title", (IntPtr)null, (IntPtr)null)).Return(window).Repeat.Once();
                context.GLFW.SetWindowSizeCallback(Arg.Is(window), Arg<WindowSizeDelegate>.Is.Anything);
                context.GLFW.SwapInterval(1);
                context.GLFW.MakeContextCurrent(window);

                context.GLFW.DestroyWindow(window);
                context.GLFW.Terminate();
            }

            using (mocks.Playback())
            {
                context.OnImportsSatisfied();
                context.Dispose();
            }
        }
    }
}
