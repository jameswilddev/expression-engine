﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using ExpressionEngine.Simulations;
using System.Runtime.InteropServices;

namespace ExpressionEngine.Platforms.OpenGL.Tests.Context
{
    [TestClass]
    public sealed class Resize
    {
        [TestMethod]
        public void InitialWidthAndHeight()
        {
            var mocks = new MockRepository();
            var context = new GLContext
            {
                GLFW = mocks.StrictMock<IGLFW>(),
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Interval = mocks.StrictMock<IInterval>()
            };
            context.Configuration.Stub(c => c.WindowWidth).Return(512);
            context.Configuration.Stub(c => c.WindowHeight).Return(384);

            IntPtr window = (IntPtr)3429387;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                context.GLFW.Expect(g => g.Init()).Return(true).Repeat.Once();
                context.GLFW.Expect(g => g.CreateWindow(Arg.Is(512), Arg.Is(384), Arg<String>.Is.Anything, Arg<IntPtr>.Is.Anything, Arg<IntPtr>.Is.Anything)).Return(window).Repeat.Once();
                context.GLFW.SetWindowSizeCallback(Arg.Is(window), Arg<WindowSizeDelegate>.Is.Anything);
                context.GLFW.SwapInterval(1);
                context.GLFW.MakeContextCurrent(window);
            }

            using (mocks.Playback())
            {
                context.OnImportsSatisfied();
                Assert.AreEqual(512, context.Width);
                Assert.AreEqual(384, context.Height);
            }
        }

        [TestMethod]
        public void ResizeValid()
        {
            var mocks = new MockRepository();
            var context = new GLContext
            {
                GLFW = mocks.StrictMock<IGLFW>(),
                GL = mocks.StrictMock<IGL>(),
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Interval = mocks.StrictMock<IInterval>()
            };
            context.Configuration.Stub(c => c.WindowWidth).Return(512);
            context.Configuration.Stub(c => c.WindowHeight).Return(384);

            IntPtr window = (IntPtr)3429387;
            WindowSizeDelegate windowSize = null;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                context.GLFW.Expect(g => g.Init()).Return(true).Repeat.Once();
                context.GLFW.Expect(g => g.CreateWindow(Arg.Is(512), Arg.Is(384), Arg<String>.Is.Anything, Arg<IntPtr>.Is.Anything, Arg<IntPtr>.Is.Anything)).Return(window).Repeat.Once();
                context.GLFW.Expect(g => g.SetWindowSizeCallback(Arg.Is(window), Arg<WindowSizeDelegate>.Is.NotNull))
                    .WhenCalled(mi => windowSize = mi.Arguments[1] as WindowSizeDelegate).Repeat.Once();
                context.GLFW.SwapInterval(1);
                context.GLFW.MakeContextCurrent(window);

                context.GL.Viewport(0, 0, 1024, 768);
                context.Interval.Update();
                context.GLFW.SwapBuffers(window);
            }

            using (mocks.Playback())
            {
                context.OnImportsSatisfied();
                windowSize(window, 1024, 768);
                Assert.AreEqual(1024, context.Width);
                Assert.AreEqual(768, context.Height);
            }
        }

        [TestMethod]
        public void ResizeWidthZeroIgnoresUpdate()
        {
            var mocks = new MockRepository();
            var context = new GLContext
            {
                GLFW = mocks.StrictMock<IGLFW>(),
                GL = mocks.StrictMock<IGL>(),
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Interval = mocks.StrictMock<IInterval>()
            };
            context.Configuration.Stub(c => c.WindowWidth).Return(512);
            context.Configuration.Stub(c => c.WindowHeight).Return(384);

            IntPtr window = (IntPtr)3429387;
            WindowSizeDelegate windowSize = null;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                context.GLFW.Expect(g => g.Init()).Return(true).Repeat.Once();
                context.GLFW.Expect(g => g.CreateWindow(Arg.Is(512), Arg.Is(384), Arg<String>.Is.Anything, Arg<IntPtr>.Is.Anything, Arg<IntPtr>.Is.Anything)).Return(window).Repeat.Once();
                context.GLFW.Expect(g => g.SetWindowSizeCallback(Arg.Is(window), Arg<WindowSizeDelegate>.Is.NotNull))
                    .WhenCalled(mi => windowSize = mi.Arguments[1] as WindowSizeDelegate).Repeat.Once();
                context.GLFW.SwapInterval(1);
                context.GLFW.MakeContextCurrent(window);
            }

            using (mocks.Playback())
            {
                context.OnImportsSatisfied();
                windowSize(window, 0, 768);
                Assert.AreEqual(512, context.Width);
                Assert.AreEqual(384, context.Height);
            }
        }

        [TestMethod]
        public void ResizeHeightZeroIgnoresUpdate()
        {
            var mocks = new MockRepository();
            var context = new GLContext
            {
                GLFW = mocks.StrictMock<IGLFW>(),
                GL = mocks.StrictMock<IGL>(),
                Configuration = MockRepository.GenerateStub<IConfiguration>(),
                Interval = mocks.StrictMock<IInterval>()
            };
            context.Configuration.Stub(c => c.WindowWidth).Return(512);
            context.Configuration.Stub(c => c.WindowHeight).Return(384);

            IntPtr window = (IntPtr)3429387;
            WindowSizeDelegate windowSize = null;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                context.GLFW.Expect(g => g.Init()).Return(true).Repeat.Once();
                context.GLFW.Expect(g => g.CreateWindow(Arg.Is(512), Arg.Is(384), Arg<String>.Is.Anything, Arg<IntPtr>.Is.Anything, Arg<IntPtr>.Is.Anything)).Return(window).Repeat.Once();
                context.GLFW.Expect(g => g.SetWindowSizeCallback(Arg.Is(window), Arg<WindowSizeDelegate>.Is.NotNull))
                    .WhenCalled(mi => windowSize = mi.Arguments[1] as WindowSizeDelegate).Repeat.Once();
                context.GLFW.SwapInterval(1);
                context.GLFW.MakeContextCurrent(window);
            }

            using (mocks.Playback())
            {
                context.OnImportsSatisfied();
                windowSize(window, 1024, 0);
                Assert.AreEqual(512, context.Width);
                Assert.AreEqual(384, context.Height);
            }
        }
    }
}
