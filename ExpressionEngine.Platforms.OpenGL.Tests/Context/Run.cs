﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using ExpressionEngine.Simulations;
using System.Runtime.InteropServices;

namespace ExpressionEngine.Platforms.OpenGL.Tests.Context
{
    [TestClass]
    public sealed class Run
    {
        public interface IContinueRun
        {
            bool ContinueRun(bool closeClicked);
        }

        [TestMethod]
        public void ContinueRunNullThrowsException()
        {
            var mocks = new MockRepository();
            var context = new GLContext
            {
                GLFW = mocks.StrictMock<IGLFW>(),
                Interval = mocks.StrictMock<IInterval>(),
                Configuration = MockRepository.GenerateStub<IConfiguration>()
            };
            var continueRun = mocks.StrictMock<IContinueRun>();

            using (mocks.Record())
            using (mocks.Ordered())
            {
            }

            using (mocks.Playback())
            {
                try
                {
                    context.Run(null);
                    Assert.Fail();
                }
                catch (ArgumentNullException ex)
                {
                    Assert.AreEqual("continueRun", ex.ParamName);
                    Assert.AreEqual(@"Value cannot be null.
Parameter name: continueRun", ex.Message);
                }
            }
        }

        [TestMethod]
        public void RunThenExitWithWindowClose()
        {
            var mocks = new MockRepository();
            var context = new GLContext
            {
                GLFW = mocks.StrictMock<IGLFW>(),
                Interval = mocks.StrictMock<IInterval>(),
                Configuration = MockRepository.GenerateStub<IConfiguration>()
            };
            var continueRun = mocks.StrictMock<IContinueRun>();
            IntPtr window = (IntPtr)3429387;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                context.GLFW.Expect(g => g.Init()).Return(true).Repeat.Once();
                context.GLFW.Expect(g => g.CreateWindow(Arg<int>.Is.Anything, Arg<int>.Is.Anything, Arg<string>.Is.Anything, Arg<IntPtr>.Is.Anything, Arg<IntPtr>.Is.Anything)).Return(window).Repeat.Once();
                context.GLFW.SetWindowSizeCallback(Arg.Is(window), Arg<WindowSizeDelegate>.Is.Anything);
                context.GLFW.SwapInterval(1);
                context.GLFW.MakeContextCurrent(window);

                context.GLFW.SetWindowShouldClose(window, false);

                context.GLFW.Expect(g => g.WindowShouldClose(window)).Return(false).Repeat.Once();
                continueRun.Expect(cr => cr.ContinueRun(false)).Return(true).Repeat.Once();
                context.GLFW.SetWindowShouldClose(window, false);
                context.Interval.Update();
                context.GLFW.SwapBuffers(window);
                context.GLFW.PollEvents();

                context.GLFW.Expect(g => g.WindowShouldClose(window)).Return(false).Repeat.Once();
                continueRun.Expect(cr => cr.ContinueRun(false)).Return(true).Repeat.Once();
                context.GLFW.SetWindowShouldClose(window, false);
                context.Interval.Update();
                context.GLFW.SwapBuffers(window);
                context.GLFW.PollEvents();

                context.GLFW.Expect(g => g.WindowShouldClose(window)).Return(true).Repeat.Once();
                continueRun.Expect(cr => cr.ContinueRun(true)).Return(false).Repeat.Once();
            }

            using (mocks.Playback())
            {
                context.OnImportsSatisfied();
                context.Run(continueRun.ContinueRun);
            }
        }

        [TestMethod]
        public void RunThenExitBeforeWindowClose()
        {
            var mocks = new MockRepository();
            var context = new GLContext
            {
                GLFW = mocks.StrictMock<IGLFW>(),
                Interval = mocks.StrictMock<IInterval>(),
                Configuration = MockRepository.GenerateStub<IConfiguration>()
            };
            var continueRun = mocks.StrictMock<IContinueRun>();
            IntPtr window = (IntPtr)3429387;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                context.GLFW.Expect(g => g.Init()).Return(true).Repeat.Once();
                context.GLFW.Expect(g => g.CreateWindow(Arg<int>.Is.Anything, Arg<int>.Is.Anything, Arg<string>.Is.Anything, Arg<IntPtr>.Is.Anything, Arg<IntPtr>.Is.Anything)).Return(window).Repeat.Once();
                context.GLFW.SetWindowSizeCallback(Arg.Is(window), Arg<WindowSizeDelegate>.Is.Anything);
                context.GLFW.SwapInterval(1);
                context.GLFW.MakeContextCurrent(window);

                context.GLFW.SetWindowShouldClose(window, false);

                context.GLFW.Expect(g => g.WindowShouldClose(window)).Return(false).Repeat.Once();
                continueRun.Expect(cr => cr.ContinueRun(false)).Return(true).Repeat.Once();
                context.GLFW.SetWindowShouldClose(window, false);
                context.Interval.Update();
                context.GLFW.SwapBuffers(window);
                context.GLFW.PollEvents();

                context.GLFW.Expect(g => g.WindowShouldClose(window)).Return(false).Repeat.Once();
                continueRun.Expect(cr => cr.ContinueRun(false)).Return(true).Repeat.Once();
                context.GLFW.SetWindowShouldClose(window, false);
                context.Interval.Update();
                context.GLFW.SwapBuffers(window);
                context.GLFW.PollEvents();

                context.GLFW.Expect(g => g.WindowShouldClose(window)).Return(false).Repeat.Once();
                continueRun.Expect(cr => cr.ContinueRun(false)).Return(false).Repeat.Once();
            }

            using (mocks.Playback())
            {
                context.OnImportsSatisfied();
                context.Run(continueRun.ContinueRun);
            }
        }

        [TestMethod]
        public void RunThenExitAfterWindowClose()
        {
            var mocks = new MockRepository();
            var context = new GLContext
            {
                GLFW = mocks.StrictMock<IGLFW>(),
                Interval = mocks.StrictMock<IInterval>(),
                Configuration = MockRepository.GenerateStub<IConfiguration>()
            };
            var continueRun = mocks.StrictMock<IContinueRun>();
            IntPtr window = (IntPtr)3429387;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                context.GLFW.Expect(g => g.Init()).Return(true).Repeat.Once();
                context.GLFW.Expect(g => g.CreateWindow(Arg<int>.Is.Anything, Arg<int>.Is.Anything, Arg<string>.Is.Anything, Arg<IntPtr>.Is.Anything, Arg<IntPtr>.Is.Anything)).Return(window).Repeat.Once();
                context.GLFW.SetWindowSizeCallback(Arg.Is(window), Arg<WindowSizeDelegate>.Is.Anything);
                context.GLFW.SwapInterval(1);
                context.GLFW.MakeContextCurrent(window);

                context.GLFW.SetWindowShouldClose(window, false);

                context.GLFW.Expect(g => g.WindowShouldClose(window)).Return(false).Repeat.Once();
                continueRun.Expect(cr => cr.ContinueRun(false)).Return(true).Repeat.Once();
                context.GLFW.SetWindowShouldClose(window, false);
                context.Interval.Update();
                context.GLFW.SwapBuffers(window);
                context.GLFW.PollEvents();

                context.GLFW.Expect(g => g.WindowShouldClose(window)).Return(true).Repeat.Once();
                continueRun.Expect(cr => cr.ContinueRun(true)).Return(true).Repeat.Once();
                context.GLFW.SetWindowShouldClose(window, false);
                context.Interval.Update();
                context.GLFW.SwapBuffers(window);
                context.GLFW.PollEvents();

                context.GLFW.Expect(g => g.WindowShouldClose(window)).Return(false).Repeat.Once();
                continueRun.Expect(cr => cr.ContinueRun(false)).Return(false).Repeat.Once();
            }

            using (mocks.Playback())
            {
                context.OnImportsSatisfied();
                context.Run(continueRun.ContinueRun);
            }
        }

        [TestMethod]
        public void RunTwice()
        {
            var mocks = new MockRepository();
            var context = new GLContext
            {
                GLFW = mocks.StrictMock<IGLFW>(),
                Interval = mocks.StrictMock<IInterval>(),
                Configuration = MockRepository.GenerateStub<IConfiguration>()
            };
            var continueRun = mocks.StrictMock<IContinueRun>();
            var continueRun2 = mocks.StrictMock<IContinueRun>();
            IntPtr window = (IntPtr)3429387;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                context.GLFW.Expect(g => g.Init()).Return(true).Repeat.Once();
                context.GLFW.Expect(g => g.CreateWindow(Arg<int>.Is.Anything, Arg<int>.Is.Anything, Arg<string>.Is.Anything, Arg<IntPtr>.Is.Anything, Arg<IntPtr>.Is.Anything)).Return(window).Repeat.Once();
                context.GLFW.SetWindowSizeCallback(Arg.Is(window), Arg<WindowSizeDelegate>.Is.Anything);
                context.GLFW.SwapInterval(1);
                context.GLFW.MakeContextCurrent(window);

                context.GLFW.SetWindowShouldClose(window, false);

                context.GLFW.Expect(g => g.WindowShouldClose(window)).Return(false).Repeat.Once();
                continueRun.Expect(cr => cr.ContinueRun(false)).Return(true).Repeat.Once();
                context.GLFW.SetWindowShouldClose(window, false);
                context.Interval.Update();
                context.GLFW.SwapBuffers(window);
                context.GLFW.PollEvents();

                context.GLFW.Expect(g => g.WindowShouldClose(window)).Return(false).Repeat.Once();
                continueRun.Expect(cr => cr.ContinueRun(false)).Return(false).Repeat.Once();

                context.GLFW.SetWindowShouldClose(window, false);

                context.GLFW.Expect(g => g.WindowShouldClose(window)).Return(false).Repeat.Once();
                continueRun2.Expect(cr => cr.ContinueRun(false)).Return(true).Repeat.Once();
                context.GLFW.SetWindowShouldClose(window, false);
                context.Interval.Update();
                context.GLFW.SwapBuffers(window);
                context.GLFW.PollEvents();

                context.GLFW.Expect(g => g.WindowShouldClose(window)).Return(false).Repeat.Once();
                continueRun2.Expect(cr => cr.ContinueRun(false)).Return(false).Repeat.Once();
            }

            using (mocks.Playback())
            {
                context.OnImportsSatisfied();
                context.Run(continueRun.ContinueRun);
                context.Run(continueRun2.ContinueRun);
            }
        }

        [TestMethod]
        public void RunTwiceSimultaneouslyThrowsException()
        {
            var mocks = new MockRepository();
            var context = new GLContext
            {
                GLFW = mocks.StrictMock<IGLFW>(),
                Interval = mocks.StrictMock<IInterval>(),
                Configuration = MockRepository.GenerateStub<IConfiguration>()
            };
            var continueRun = mocks.StrictMock<IContinueRun>();
            var continueRun2 = mocks.StrictMock<IContinueRun>();
            IntPtr window = (IntPtr)3429387;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                context.GLFW.Expect(g => g.Init()).Return(true).Repeat.Once();
                context.GLFW.Expect(g => g.CreateWindow(Arg<int>.Is.Anything, Arg<int>.Is.Anything, Arg<string>.Is.Anything, Arg<IntPtr>.Is.Anything, Arg<IntPtr>.Is.Anything)).Return(window).Repeat.Once();
                context.GLFW.SetWindowSizeCallback(Arg.Is(window), Arg<WindowSizeDelegate>.Is.Anything);
                context.GLFW.SwapInterval(1);
                context.GLFW.MakeContextCurrent(window);

                context.GLFW.SetWindowShouldClose(window, false);

                context.GLFW.Expect(g => g.WindowShouldClose(window)).Return(false).Repeat.Once();
                continueRun.Expect(cr => cr.ContinueRun(false)).Return(true).Repeat.Once();
                context.GLFW.SetWindowShouldClose(window, false);
                context.Interval.Update();
                context.GLFW.SwapBuffers(window);
                context.GLFW.PollEvents();

                context.GLFW.Expect(g => g.WindowShouldClose(window)).Return(false).Repeat.Once();
                continueRun.Expect(cr => cr.ContinueRun(false)).Return(false)
                    .WhenCalled(mi => context.Run(continueRun2.ContinueRun)).Repeat.Once();
            }

            using (mocks.Playback())
            {
                context.OnImportsSatisfied();

                try
                {
                    context.Run(continueRun.ContinueRun);
                    Assert.Fail();
                }
                catch (InvalidOperationException ex)
                {
                    Assert.AreEqual("Run should not be called recursively", ex.Message);
                }
            }
        }

        [TestMethod]
        public void ExceptionThrownExecutingCallback()
        {
            var mocks = new MockRepository();
            var context = new GLContext
            {
                GLFW = mocks.StrictMock<IGLFW>(),
                Interval = mocks.StrictMock<IInterval>(),
                Configuration = MockRepository.GenerateStub<IConfiguration>()
            };
            var continueRun = mocks.StrictMock<IContinueRun>();
            IntPtr window = (IntPtr)3429387;

            var exception = new Exception();

            using (mocks.Record())
            using (mocks.Ordered())
            {
                context.GLFW.Expect(g => g.Init()).Return(true).Repeat.Once();
                context.GLFW.Expect(g => g.CreateWindow(Arg<int>.Is.Anything, Arg<int>.Is.Anything, Arg<string>.Is.Anything, Arg<IntPtr>.Is.Anything, Arg<IntPtr>.Is.Anything)).Return(window).Repeat.Once();
                context.GLFW.SetWindowSizeCallback(Arg.Is(window), Arg<WindowSizeDelegate>.Is.Anything);
                context.GLFW.SwapInterval(1);
                context.GLFW.MakeContextCurrent(window);

                context.GLFW.SetWindowShouldClose(window, false);

                context.GLFW.Expect(g => g.WindowShouldClose(window)).Return(false).Repeat.Once();
                continueRun.Expect(cr => cr.ContinueRun(false)).Return(true).Repeat.Once();
                context.GLFW.SetWindowShouldClose(window, false);
                context.Interval.Update();
                context.GLFW.SwapBuffers(window);
                context.GLFW.PollEvents();

                context.GLFW.Expect(g => g.WindowShouldClose(window)).Return(false).Repeat.Once();
                continueRun.Expect(cr => cr.ContinueRun(false)).Throw(exception).Repeat.Once();
            }

            using (mocks.Playback())
            {
                context.OnImportsSatisfied();
                try
                {
                    context.Run(continueRun.ContinueRun);
                    Assert.Fail();
                }
                catch (Exception ex)
                {
                    Assert.AreEqual(exception, ex);
                }
            }
        }

        [TestMethod]
        public void ExceptionThrownCallingInterval()
        {
            var mocks = new MockRepository();
            var context = new GLContext
            {
                GLFW = mocks.StrictMock<IGLFW>(),
                Interval = mocks.StrictMock<IInterval>(),
                Configuration = MockRepository.GenerateStub<IConfiguration>()
            };
            var continueRun = mocks.StrictMock<IContinueRun>();
            IntPtr window = (IntPtr)3429387;

            var exception = new Exception();

            using (mocks.Record())
            using (mocks.Ordered())
            {
                context.GLFW.Expect(g => g.Init()).Return(true).Repeat.Once();
                context.GLFW.Expect(g => g.CreateWindow(Arg<int>.Is.Anything, Arg<int>.Is.Anything, Arg<string>.Is.Anything, Arg<IntPtr>.Is.Anything, Arg<IntPtr>.Is.Anything)).Return(window).Repeat.Once();
                context.GLFW.SetWindowSizeCallback(Arg.Is(window), Arg<WindowSizeDelegate>.Is.Anything);
                context.GLFW.SwapInterval(1);
                context.GLFW.MakeContextCurrent(window);

                context.GLFW.SetWindowShouldClose(window, false);

                context.GLFW.Expect(g => g.WindowShouldClose(window)).Return(false).Repeat.Once();
                continueRun.Expect(cr => cr.ContinueRun(false)).Return(true).Repeat.Once();
                context.GLFW.SetWindowShouldClose(window, false);
                context.Interval.Update();
                context.GLFW.SwapBuffers(window);
                context.GLFW.PollEvents();

                context.GLFW.Expect(g => g.WindowShouldClose(window)).Return(false).Repeat.Once();
                continueRun.Expect(cr => cr.ContinueRun(false)).Return(true).Repeat.Once();
                context.GLFW.SetWindowShouldClose(window, false);
                context.Interval.Expect(i => i.Update()).Throw(exception).Repeat.Once();
            }

            using (mocks.Playback())
            {
                context.OnImportsSatisfied();
                try
                {
                    context.Run(continueRun.ContinueRun);
                    Assert.Fail();
                }
                catch (Exception ex)
                {
                    Assert.AreEqual(exception, ex);
                }
            }
        }
    }
}
