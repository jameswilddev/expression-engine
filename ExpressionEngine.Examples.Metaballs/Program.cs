﻿using ExpressionEngine.Compilers;
using ExpressionEngine.Simulations;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Linq.Expressions;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.Examples.Metaballs
{
    internal static class MainClass
    {
        [Export(typeof(IConfiguration))]
        public sealed class Configuration : IConfiguration
        {
            public TimeSpan TickInterval
            {
                get { return TimeSpan.FromSeconds(1.0 / 20.0); }
            }

            public string ApplicationName
            {
                get { return "Expression Engine Metaballs Example"; }
            }

            public int WindowWidth
            {
                get { return 640; }
            }

            public int WindowHeight
            {
                get { return 480; }
            }
        }

        [Export]
        public sealed class Program : IPartImportsSatisfiedNotification
        {
            [Import]
            public IContext Context;

            [Import]
            public IRenderPassFactory RenderPassFactory;

            [Import]
            public ICompiler Compiler;

            [Import]
            public IInterval Interval;

            /// <summary>Determines where to draw a metaball.</summary>
            Expression<Func<float, Vector2>> GetLocation
            {
                get
                {
                    return (time) => (new Vector2(3.0f, 5.0f) + new Vector2(time)).Sin() * 0.25f;
                }
            }

            /// <summary>Determines the weight of a specific metaball at a specific pixel.</summary>
            Expression<Func<Vector2, Vector2, float, float>> Metaball
            {
                get
                {
                    return (pixel, location, weight) => weight / Vector2.Distance(pixel, location);
                }
            }

            /// <summary>Generates rainbow metaballs.</summary>
            Expression<Func<Vector2, Vector2, float, Vector3>> Simulation
            {
                get
                {
                    return
                        Injector.Inject(GetLocation, getLocation =>
                        Injector.Inject<Func<Vector2, Vector2, float, Vector3>, Func<Vector2, Vector2, float, float>>(Metaball, metaball =>

                    (pixel, resolution, time) =>
                        (new Vector3(0.1f, 0.3f, 0.42f) * (
                            metaball(pixel / resolution - new Vector2(0.5f), getLocation(time), 1.0f)
                            + metaball(pixel / resolution - new Vector2(0.5f), getLocation(time * 1.2f + 3.0f) * 0.87f, 1.0f)
                            + metaball(pixel / resolution - new Vector2(0.5f), getLocation(time * 3.0f) * 1.2f, 1.0f)
                        )).Sin()));
                }
            }

            public void OnImportsSatisfied()
            {
                var ticks = 0;
                using (var pass = RenderPassFactory.CreateInstance(Compiler.Compile(Simulation)))
                {
                    Interval.Tick += () =>
                    {
                        ticks++;
                    };

                    Interval.Render += delta =>
                    {
                        pass.Set("time", (ticks + delta) / 10.0f);
                        pass.Draw();
                        pass.Set("resolution", new Vector2(Context.Width, Context.Height));
                    };
                    Context.Run(wc => !wc);
                }
            }
        }

        static void Main(string[] args)
        {
            using (var container = new CompositionContainer(new AggregateCatalog(new DirectoryCatalog("."), new DirectoryCatalog(".", "*.exe"))))
            {
                container.GetExportedValue<Program>();
            }
        }
    }
}
